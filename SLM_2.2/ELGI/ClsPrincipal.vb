﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Threading
Imports CrystalDecisions.Shared
Imports ELGI.ELGI.Reporte

Public Class ClsPrincipal
    Public Sub crearResultadoCarpeta(ByVal id As String)
        Dim FileDelete As String
        Dim FileDelete2 As String

        FileDelete = "C:\Resultados\resultado" + id + ".pdf"
        FileDelete2 = "C:\Resultados\resultado0.pdf"
        If FileDelete <> FileDelete2 Then

            Dim cryRpt As New E_ReporteResultadoIndividual
            cryRpt.SetParameterValue("@id_orden", id)

            cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
            Try
                Dim CrExportOptions As ExportOptions
                Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
                'Dim CrFormatTypeOptions As New ExcelFormatOptions
                Dim CrFormatTypeOptions As New PdfFormatOptions

                If System.IO.File.Exists(FileDelete) = True Then
                    CrDiskFileDestinationOptions.DiskFileName = id
                Else
                    CrDiskFileDestinationOptions.DiskFileName = id

                End If
                CrExportOptions = cryRpt.ExportOptions
                With CrExportOptions
                    .ExportDestinationType = ExportDestinationType.DiskFile
                    '.ExportFormatType = ExportFormatType.Excel
                    .ExportFormatType = ExportFormatType.PortableDocFormat
                    .DestinationOptions = CrDiskFileDestinationOptions
                    .FormatOptions = CrFormatTypeOptions
                End With

                cryRpt.Export()
                enviarMailResultadoLocal("erickgallardo89@yahoo.com", id)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub
    Public Sub crearResultado(ByVal id As String)
        Dim FileDelete As String
        Dim FileDelete2 As String

        FileDelete = "C:\Resultados\resultado" + id + ".pdf"
        FileDelete2 = "C:\Resultados\resultado0.pdf"
        If FileDelete <> FileDelete2 Then

            Dim cryRpt As New E_ReporteResultadoIndividual
        cryRpt.SetParameterValue("@id_orden", id)

        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
                'Dim CrFormatTypeOptions As New ExcelFormatOptions
                Dim CrFormatTypeOptions As New PdfFormatOptions

                If System.IO.File.Exists(FileDelete) = True Then
                    CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + id + Date.Now.Minute.ToString + ".pdf"
                Else
                    CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + id + ".pdf"

                End If
                CrExportOptions = cryRpt.ExportOptions
                    With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()
                ' enviarMailResultadoLocal("erickgallardo89@yahoo.com", id)
            Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        End If
    End Sub
    Sub enviarMailResultadoLocal(correoNoti As String, id_orden As String)

        'In the shadows of the moon
        'enviarMailResultado("sinergia@laboratoriosmedicos.hn", "Lmsinergia2020", "587", True, "mail.laboratoriosmedicos.hn", "erickgallardo89@yahoo.com", "Resultados")
        Dim correoSalida As String = "resultados@laboratoriosmedicos.hn"
        Dim pass As String = "Lbresultados76"
        Dim puerto As String = "587"
        Dim sslOK As Boolean = True
        Dim host As String = "mail.laboratoriosmedicos.hn"
        Dim texto As String = "Resultados "


        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(correoSalida, pass)
            Smtp_Server.Port = puerto
            Smtp_Server.EnableSsl = sslOK
            Smtp_Server.Host = host

            e_mail = New MailMessage()
            'txtfrom.text
            e_mail.From = New MailAddress(correoSalida)
            'txtto.text
            e_mail.To.Add(correoNoti)
            e_mail.Subject = "Resultados:"

            Dim archivos As String = id_orden

            Dim archivoAdjunto As New System.Net.Mail.Attachment(archivos)

            e_mail.Attachments.Add(archivoAdjunto)
            e_mail.IsBodyHtml = True
            'txtMessage.text
            Dim body As String
            body = "<p>Estimado cliente</p>

<p>Adjunto encontrará el resultado de sus análisis.</p>
<p>Laboratorios Médicos, 45 años agradeciendo su confianza.</p>
<p>Si tiene consultas, favor comunicarse con nuestro Contact Center al 2239-1950 o al correo info@laboratoriosmedicos.hn</p>"

            e_mail.Body = body
            Smtp_Server.Send(e_mail)
            e_mail.Attachments.Clear()


            'omitir mensaje
            ' MsgBox("Mail Enviado")

        Catch ex As Exception
            MsgBox("No se envío el correo. " + ex.Message)
        End Try


        Exit Sub
    End Sub

    Public Function RecuperarFacturasPendientes() As SqlDataReader
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "exec E_slmELGI"
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteReader
    End Function
End Class
