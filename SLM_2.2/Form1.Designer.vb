﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnContabilidad = New System.Windows.Forms.Button()
        Me.btnAlmacen = New System.Windows.Forms.Button()
        Me.btnTalentoHumano = New System.Windows.Forms.Button()
        Me.btnSistema = New System.Windows.Forms.Button()
        Me.btnFacturacion = New System.Windows.Forms.Button()
        Me.btnlaboratorio = New System.Windows.Forms.Button()
        Me.panelMenu = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tcMenu = New System.Windows.Forms.TabControl()
        Me.TpMantenimiento = New System.Windows.Forms.TabPage()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnPeriodoContable = New System.Windows.Forms.PictureBox()
        Me.btnAsientos = New System.Windows.Forms.PictureBox()
        Me.btnCategoriaProveedor = New System.Windows.Forms.PictureBox()
        Me.btnProveedor = New System.Windows.Forms.PictureBox()
        Me.btnFormaPago = New System.Windows.Forms.PictureBox()
        Me.btnCAI = New System.Windows.Forms.PictureBox()
        Me.btnBancos = New System.Windows.Forms.PictureBox()
        Me.btnCuentas = New System.Windows.Forms.PictureBox()
        Me.tpPrecios = New System.Windows.Forms.TabPage()
        Me.pbxItemExamen = New System.Windows.Forms.PictureBox()
        Me.btnPrecio = New System.Windows.Forms.PictureBox()
        Me.btnListaPrecio = New System.Windows.Forms.PictureBox()
        Me.btnDescuento = New System.Windows.Forms.PictureBox()
        Me.btnPromociones = New System.Windows.Forms.PictureBox()
        Me.TpProcesos = New System.Windows.Forms.TabPage()
        Me.btnPlanilla = New System.Windows.Forms.PictureBox()
        Me.btnDepreciacion = New System.Windows.Forms.PictureBox()
        Me.btnConsolidar = New System.Windows.Forms.PictureBox()
        Me.tpCheque = New System.Windows.Forms.TabPage()
        Me.btnCheques = New System.Windows.Forms.PictureBox()
        Me.btnChequera = New System.Windows.Forms.PictureBox()
        Me.btnDepoBanc = New System.Windows.Forms.PictureBox()
        Me.tpPagos = New System.Windows.Forms.TabPage()
        Me.btnFacturaCompra = New System.Windows.Forms.PictureBox()
        Me.btnPagos = New System.Windows.Forms.PictureBox()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox42 = New System.Windows.Forms.PictureBox()
        Me.PictureBox41 = New System.Windows.Forms.PictureBox()
        Me.PictureBox40 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.btnIngresos = New System.Windows.Forms.PictureBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.pbxTipoClasificacion = New System.Windows.Forms.PictureBox()
        Me.pbxCategoriaCliente = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox34 = New System.Windows.Forms.PictureBox()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.TabPage18 = New System.Windows.Forms.TabPage()
        Me.btnClienteEmpresarial = New System.Windows.Forms.PictureBox()
        Me.btnCotizacionEmpresarial = New System.Windows.Forms.PictureBox()
        Me.btnFacturaEmpresarial = New System.Windows.Forms.PictureBox()
        Me.TabPage21 = New System.Windows.Forms.TabPage()
        Me.PictureBox44 = New System.Windows.Forms.PictureBox()
        Me.PanelLab = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnAreasLaboratorio = New System.Windows.Forms.PictureBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.btnSubAreas = New System.Windows.Forms.Button()
        Me.PictureBox76 = New System.Windows.Forms.PictureBox()
        Me.btnGrupoExamenes = New System.Windows.Forms.Button()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.btnInformes = New System.Windows.Forms.Button()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.btnValoresRef = New System.Windows.Forms.Button()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.btnEntrega = New System.Windows.Forms.Button()
        Me.btnHojaTrabajo = New System.Windows.Forms.Button()
        Me.btnTrabajo = New System.Windows.Forms.Button()
        Me.btnExamen = New System.Windows.Forms.Button()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnSede2 = New System.Windows.Forms.PictureBox()
        Me.btnSucursal = New System.Windows.Forms.PictureBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.btnMedico2 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnPaciente2 = New System.Windows.Forms.PictureBox()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btnReenvio = New System.Windows.Forms.PictureBox()
        Me.TabPage20 = New System.Windows.Forms.TabPage()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.PictureBox37 = New System.Windows.Forms.PictureBox()
        Me.PictureBox36 = New System.Windows.Forms.PictureBox()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.PanelFactura = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PanelTalentoHumano = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabControl4 = New System.Windows.Forms.TabControl()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.btnConstanciaPlantillas = New System.Windows.Forms.PictureBox()
        Me.btnReporteria = New System.Windows.Forms.PictureBox()
        Me.btnTipoPermiso = New System.Windows.Forms.PictureBox()
        Me.btnCapacitaciones = New System.Windows.Forms.PictureBox()
        Me.btnCandidatos = New System.Windows.Forms.PictureBox()
        Me.btnDepto = New System.Windows.Forms.PictureBox()
        Me.btnPuestoTrabajo = New System.Windows.Forms.PictureBox()
        Me.lblUserCod = New System.Windows.Forms.Label()
        Me.btnHorarios = New System.Windows.Forms.PictureBox()
        Me.btnContratos = New System.Windows.Forms.PictureBox()
        Me.btnProfesion = New System.Windows.Forms.PictureBox()
        Me.btnTipoDeducciones = New System.Windows.Forms.PictureBox()
        Me.btnArea = New System.Windows.Forms.PictureBox()
        Me.btnVacaciones = New System.Windows.Forms.PictureBox()
        Me.btnPermisos = New System.Windows.Forms.PictureBox()
        Me.btnSucursales = New System.Windows.Forms.PictureBox()
        Me.btnEmpleados = New System.Windows.Forms.PictureBox()
        Me.TabControl3 = New System.Windows.Forms.TabControl()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.btnCorreoResultado = New System.Windows.Forms.PictureBox()
        Me.btnFirmaDigital = New System.Windows.Forms.PictureBox()
        Me.btnFeriados = New System.Windows.Forms.PictureBox()
        Me.btnUsuarios = New System.Windows.Forms.PictureBox()
        Me.btnPerfiles = New System.Windows.Forms.PictureBox()
        Me.btnServidorCorreo = New System.Windows.Forms.PictureBox()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.btnLogsCambios = New System.Windows.Forms.PictureBox()
        Me.btnLogsExcepciones = New System.Windows.Forms.PictureBox()
        Me.btnLogsLogin = New System.Windows.Forms.PictureBox()
        Me.TabPage19 = New System.Windows.Forms.TabPage()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PanelSistema = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PanelAlmacen = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabControl5 = New System.Windows.Forms.TabControl()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.btnCuentasAlmacen = New System.Windows.Forms.PictureBox()
        Me.btnTipoMovimiento = New System.Windows.Forms.PictureBox()
        Me.btnEntradas = New System.Windows.Forms.PictureBox()
        Me.btnFactCompra = New System.Windows.Forms.PictureBox()
        Me.btnOrdeCompra = New System.Windows.Forms.PictureBox()
        Me.btnSalidas = New System.Windows.Forms.PictureBox()
        Me.btnAlmacenes = New System.Windows.Forms.PictureBox()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.btnUnidadMedida = New System.Windows.Forms.PictureBox()
        Me.btnCategoria = New System.Windows.Forms.PictureBox()
        Me.btnProducto = New System.Windows.Forms.PictureBox()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.btnInventario = New System.Windows.Forms.PictureBox()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.btnBI = New System.Windows.Forms.PictureBox()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.PictureBox38 = New System.Windows.Forms.PictureBox()
        Me.PictureBox84 = New System.Windows.Forms.PictureBox()
        Me.btnOrdenInterna = New System.Windows.Forms.PictureBox()
        Me.btnAutorizacion = New System.Windows.Forms.PictureBox()
        Me.PictureBox66 = New System.Windows.Forms.PictureBox()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.btnEvaluacionReactivo = New System.Windows.Forms.PictureBox()
        Me.btnEvaluacionP = New System.Windows.Forms.PictureBox()
        Me.btnEvaluacionServicio = New System.Windows.Forms.PictureBox()
        Me.btnProveedores = New System.Windows.Forms.PictureBox()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.reporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblMiUser = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.btnCerrarSesion = New System.Windows.Forms.LinkLabel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.bntCerrar = New System.Windows.Forms.Button()
        Me.lblCajero = New System.Windows.Forms.Label()
        Me.btnConserjes = New System.Windows.Forms.Button()
        Me.btnTomaDeMuestra = New System.Windows.Forms.Button()
        Me.PanelTM = New System.Windows.Forms.Panel()
        Me.TabControl6 = New System.Windows.Forms.TabControl()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.PictureBox39 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.btnImpresorasTM = New System.Windows.Forms.PictureBox()
        Me.btnUsuariosTM = New System.Windows.Forms.PictureBox()
        Me.btnTomaMuestra = New System.Windows.Forms.PictureBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pbxNoti = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.panelMenu.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcMenu.SuspendLayout()
        Me.TpMantenimiento.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPeriodoContable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnAsientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCategoriaProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCAI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBancos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPrecios.SuspendLayout()
        CType(Me.pbxItemExamen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnListaPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TpProcesos.SuspendLayout()
        CType(Me.btnPlanilla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDepreciacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnConsolidar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpCheque.SuspendLayout()
        CType(Me.btnCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnChequera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDepoBanc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpPagos.SuspendLayout()
        CType(Me.btnFacturaCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage16.SuspendLayout()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnIngresos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.pbxTipoClasificacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxCategoriaCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage18.SuspendLayout()
        CType(Me.btnClienteEmpresarial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCotizacionEmpresarial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFacturaEmpresarial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage21.SuspendLayout()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelLab.SuspendLayout()
        Me.Panel8.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.btnAreasLaboratorio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.btnSede2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.btnMedico2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.btnPaciente2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnReenvio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage20.SuspendLayout()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelFactura.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelTalentoHumano.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl4.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        CType(Me.btnConstanciaPlantillas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnReporteria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnTipoPermiso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCapacitaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCandidatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPuestoTrabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnHorarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnContratos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnProfesion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnTipoDeducciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnVacaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPermisos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEmpleados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl3.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCorreoResultado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFirmaDigital, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFeriados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnUsuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnPerfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnServidorCorreo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage14.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLogsCambios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLogsExcepciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLogsLogin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage19.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelSistema.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelAlmacen.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl5.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCuentasAlmacen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnTipoMovimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFactCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnOrdeCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnSalidas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnAlmacenes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage11.SuspendLayout()
        CType(Me.btnUnidadMedida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnInventario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBI, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage8.SuspendLayout()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnOrdenInterna, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnAutorizacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage12.SuspendLayout()
        CType(Me.btnEvaluacionReactivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEvaluacionP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEvaluacionServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage15.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelTM.SuspendLayout()
        Me.TabControl6.SuspendLayout()
        Me.TabPage13.SuspendLayout()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnImpresorasTM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnUsuariosTM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnTomaMuestra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxNoti, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnContabilidad
        '
        Me.btnContabilidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnContabilidad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnContabilidad.Enabled = False
        Me.btnContabilidad.FlatAppearance.BorderSize = 0
        Me.btnContabilidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContabilidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContabilidad.ForeColor = System.Drawing.Color.White
        Me.btnContabilidad.Location = New System.Drawing.Point(0, 162)
        Me.btnContabilidad.Name = "btnContabilidad"
        Me.btnContabilidad.Size = New System.Drawing.Size(122, 44)
        Me.btnContabilidad.TabIndex = 4
        Me.btnContabilidad.Text = "Contabilidad"
        Me.btnContabilidad.UseVisualStyleBackColor = False
        '
        'btnAlmacen
        '
        Me.btnAlmacen.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnAlmacen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAlmacen.Enabled = False
        Me.btnAlmacen.FlatAppearance.BorderSize = 0
        Me.btnAlmacen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAlmacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAlmacen.ForeColor = System.Drawing.Color.White
        Me.btnAlmacen.Location = New System.Drawing.Point(0, 112)
        Me.btnAlmacen.Name = "btnAlmacen"
        Me.btnAlmacen.Size = New System.Drawing.Size(122, 44)
        Me.btnAlmacen.TabIndex = 10
        Me.btnAlmacen.Text = "Almacén"
        Me.btnAlmacen.UseVisualStyleBackColor = False
        '
        'btnTalentoHumano
        '
        Me.btnTalentoHumano.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnTalentoHumano.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTalentoHumano.Enabled = False
        Me.btnTalentoHumano.FlatAppearance.BorderSize = 0
        Me.btnTalentoHumano.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTalentoHumano.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTalentoHumano.ForeColor = System.Drawing.Color.White
        Me.btnTalentoHumano.Location = New System.Drawing.Point(0, 312)
        Me.btnTalentoHumano.Name = "btnTalentoHumano"
        Me.btnTalentoHumano.Size = New System.Drawing.Size(122, 44)
        Me.btnTalentoHumano.TabIndex = 9
        Me.btnTalentoHumano.Text = "Talento Humano"
        Me.btnTalentoHumano.UseVisualStyleBackColor = False
        '
        'btnSistema
        '
        Me.btnSistema.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnSistema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSistema.Enabled = False
        Me.btnSistema.FlatAppearance.BorderSize = 0
        Me.btnSistema.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSistema.ForeColor = System.Drawing.Color.White
        Me.btnSistema.Location = New System.Drawing.Point(0, 212)
        Me.btnSistema.Name = "btnSistema"
        Me.btnSistema.Size = New System.Drawing.Size(122, 44)
        Me.btnSistema.TabIndex = 8
        Me.btnSistema.Text = "Sistema"
        Me.btnSistema.UseVisualStyleBackColor = False
        '
        'btnFacturacion
        '
        Me.btnFacturacion.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnFacturacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnFacturacion.Enabled = False
        Me.btnFacturacion.FlatAppearance.BorderSize = 0
        Me.btnFacturacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFacturacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturacion.ForeColor = System.Drawing.Color.White
        Me.btnFacturacion.Location = New System.Drawing.Point(0, 62)
        Me.btnFacturacion.Name = "btnFacturacion"
        Me.btnFacturacion.Size = New System.Drawing.Size(122, 44)
        Me.btnFacturacion.TabIndex = 2
        Me.btnFacturacion.Text = "Facturación"
        Me.btnFacturacion.UseVisualStyleBackColor = False
        '
        'btnlaboratorio
        '
        Me.btnlaboratorio.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnlaboratorio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnlaboratorio.Enabled = False
        Me.btnlaboratorio.FlatAppearance.BorderSize = 0
        Me.btnlaboratorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnlaboratorio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnlaboratorio.ForeColor = System.Drawing.Color.White
        Me.btnlaboratorio.Location = New System.Drawing.Point(0, 262)
        Me.btnlaboratorio.Name = "btnlaboratorio"
        Me.btnlaboratorio.Size = New System.Drawing.Size(122, 44)
        Me.btnlaboratorio.TabIndex = 1
        Me.btnlaboratorio.Text = "Laboratorio"
        Me.btnlaboratorio.UseVisualStyleBackColor = False
        '
        'panelMenu
        '
        Me.panelMenu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelMenu.BackColor = System.Drawing.Color.White
        Me.panelMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.panelMenu.Controls.Add(Me.Panel6)
        Me.panelMenu.Controls.Add(Me.tcMenu)
        Me.panelMenu.Location = New System.Drawing.Point(123, 59)
        Me.panelMenu.Name = "panelMenu"
        Me.panelMenu.Size = New System.Drawing.Size(584, 549)
        Me.panelMenu.TabIndex = 4
        Me.panelMenu.Visible = False
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel6.Controls.Add(Me.PictureBox4)
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel6.Location = New System.Drawing.Point(1, 1)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(584, 37)
        Me.Panel6.TabIndex = 11
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox4.Location = New System.Drawing.Point(550, 7)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 8
        Me.PictureBox4.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(2, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(264, 25)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Módulo De Contabilidad"
        '
        'tcMenu
        '
        Me.tcMenu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcMenu.Controls.Add(Me.TpMantenimiento)
        Me.tcMenu.Controls.Add(Me.tpPrecios)
        Me.tcMenu.Controls.Add(Me.TpProcesos)
        Me.tcMenu.Controls.Add(Me.tpCheque)
        Me.tcMenu.Controls.Add(Me.tpPagos)
        Me.tcMenu.Controls.Add(Me.TabPage16)
        Me.tcMenu.Location = New System.Drawing.Point(1, 38)
        Me.tcMenu.Name = "tcMenu"
        Me.tcMenu.SelectedIndex = 0
        Me.tcMenu.Size = New System.Drawing.Size(586, 495)
        Me.tcMenu.TabIndex = 6
        '
        'TpMantenimiento
        '
        Me.TpMantenimiento.BackColor = System.Drawing.Color.AliceBlue
        Me.TpMantenimiento.Controls.Add(Me.PictureBox1)
        Me.TpMantenimiento.Controls.Add(Me.btnPeriodoContable)
        Me.TpMantenimiento.Controls.Add(Me.btnAsientos)
        Me.TpMantenimiento.Controls.Add(Me.btnCategoriaProveedor)
        Me.TpMantenimiento.Controls.Add(Me.btnProveedor)
        Me.TpMantenimiento.Controls.Add(Me.btnFormaPago)
        Me.TpMantenimiento.Controls.Add(Me.btnCAI)
        Me.TpMantenimiento.Controls.Add(Me.btnBancos)
        Me.TpMantenimiento.Controls.Add(Me.btnCuentas)
        Me.TpMantenimiento.Location = New System.Drawing.Point(4, 22)
        Me.TpMantenimiento.Name = "TpMantenimiento"
        Me.TpMantenimiento.Padding = New System.Windows.Forms.Padding(3)
        Me.TpMantenimiento.Size = New System.Drawing.Size(578, 469)
        Me.TpMantenimiento.TabIndex = 2
        Me.TpMantenimiento.Text = "Mantenimiento"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_04
        Me.PictureBox1.Location = New System.Drawing.Point(24, 302)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 51
        Me.PictureBox1.TabStop = False
        '
        'btnPeriodoContable
        '
        Me.btnPeriodoContable.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_26
        Me.btnPeriodoContable.Location = New System.Drawing.Point(144, 160)
        Me.btnPeriodoContable.Name = "btnPeriodoContable"
        Me.btnPeriodoContable.Size = New System.Drawing.Size(92, 110)
        Me.btnPeriodoContable.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPeriodoContable.TabIndex = 50
        Me.btnPeriodoContable.TabStop = False
        '
        'btnAsientos
        '
        Me.btnAsientos.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_24
        Me.btnAsientos.Location = New System.Drawing.Point(144, 29)
        Me.btnAsientos.Name = "btnAsientos"
        Me.btnAsientos.Size = New System.Drawing.Size(92, 110)
        Me.btnAsientos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnAsientos.TabIndex = 49
        Me.btnAsientos.TabStop = False
        '
        'btnCategoriaProveedor
        '
        Me.btnCategoriaProveedor.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_25
        Me.btnCategoriaProveedor.Location = New System.Drawing.Point(277, 29)
        Me.btnCategoriaProveedor.Name = "btnCategoriaProveedor"
        Me.btnCategoriaProveedor.Size = New System.Drawing.Size(92, 110)
        Me.btnCategoriaProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCategoriaProveedor.TabIndex = 48
        Me.btnCategoriaProveedor.TabStop = False
        '
        'btnProveedor
        '
        Me.btnProveedor.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_28
        Me.btnProveedor.Location = New System.Drawing.Point(395, 29)
        Me.btnProveedor.Name = "btnProveedor"
        Me.btnProveedor.Size = New System.Drawing.Size(91, 110)
        Me.btnProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnProveedor.TabIndex = 47
        Me.btnProveedor.TabStop = False
        '
        'btnFormaPago
        '
        Me.btnFormaPago.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_29
        Me.btnFormaPago.Location = New System.Drawing.Point(277, 160)
        Me.btnFormaPago.Name = "btnFormaPago"
        Me.btnFormaPago.Size = New System.Drawing.Size(92, 110)
        Me.btnFormaPago.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFormaPago.TabIndex = 46
        Me.btnFormaPago.TabStop = False
        '
        'btnCAI
        '
        Me.btnCAI.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_30
        Me.btnCAI.Location = New System.Drawing.Point(24, 163)
        Me.btnCAI.Name = "btnCAI"
        Me.btnCAI.Size = New System.Drawing.Size(92, 110)
        Me.btnCAI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCAI.TabIndex = 45
        Me.btnCAI.TabStop = False
        '
        'btnBancos
        '
        Me.btnBancos.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_27
        Me.btnBancos.Location = New System.Drawing.Point(395, 160)
        Me.btnBancos.Name = "btnBancos"
        Me.btnBancos.Size = New System.Drawing.Size(92, 110)
        Me.btnBancos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnBancos.TabIndex = 44
        Me.btnBancos.TabStop = False
        '
        'btnCuentas
        '
        Me.btnCuentas.Image = Global.SLM_2._2.My.Resources.Resources.mantenimiento_23
        Me.btnCuentas.Location = New System.Drawing.Point(24, 29)
        Me.btnCuentas.Name = "btnCuentas"
        Me.btnCuentas.Size = New System.Drawing.Size(92, 110)
        Me.btnCuentas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCuentas.TabIndex = 43
        Me.btnCuentas.TabStop = False
        '
        'tpPrecios
        '
        Me.tpPrecios.BackColor = System.Drawing.Color.AliceBlue
        Me.tpPrecios.Controls.Add(Me.pbxItemExamen)
        Me.tpPrecios.Controls.Add(Me.btnPrecio)
        Me.tpPrecios.Controls.Add(Me.btnListaPrecio)
        Me.tpPrecios.Controls.Add(Me.btnDescuento)
        Me.tpPrecios.Controls.Add(Me.btnPromociones)
        Me.tpPrecios.Location = New System.Drawing.Point(4, 22)
        Me.tpPrecios.Name = "tpPrecios"
        Me.tpPrecios.Size = New System.Drawing.Size(578, 469)
        Me.tpPrecios.TabIndex = 3
        Me.tpPrecios.Text = "Precios y Descuentos"
        '
        'pbxItemExamen
        '
        Me.pbxItemExamen.Image = Global.SLM_2._2.My.Resources.Resources.examenes_61
        Me.pbxItemExamen.Location = New System.Drawing.Point(9, 150)
        Me.pbxItemExamen.Name = "pbxItemExamen"
        Me.pbxItemExamen.Size = New System.Drawing.Size(93, 115)
        Me.pbxItemExamen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbxItemExamen.TabIndex = 47
        Me.pbxItemExamen.TabStop = False
        '
        'btnPrecio
        '
        Me.btnPrecio.Image = Global.SLM_2._2.My.Resources.Resources.precios_y_descuentos_31
        Me.btnPrecio.Location = New System.Drawing.Point(7, 24)
        Me.btnPrecio.Name = "btnPrecio"
        Me.btnPrecio.Size = New System.Drawing.Size(93, 115)
        Me.btnPrecio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPrecio.TabIndex = 46
        Me.btnPrecio.TabStop = False
        '
        'btnListaPrecio
        '
        Me.btnListaPrecio.Image = Global.SLM_2._2.My.Resources.Resources.precios_y_descuentos_32
        Me.btnListaPrecio.Location = New System.Drawing.Point(126, 24)
        Me.btnListaPrecio.Name = "btnListaPrecio"
        Me.btnListaPrecio.Size = New System.Drawing.Size(95, 115)
        Me.btnListaPrecio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnListaPrecio.TabIndex = 45
        Me.btnListaPrecio.TabStop = False
        '
        'btnDescuento
        '
        Me.btnDescuento.Image = Global.SLM_2._2.My.Resources.Resources.precios_y_descuentos_33
        Me.btnDescuento.Location = New System.Drawing.Point(247, 24)
        Me.btnDescuento.Name = "btnDescuento"
        Me.btnDescuento.Size = New System.Drawing.Size(95, 115)
        Me.btnDescuento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnDescuento.TabIndex = 44
        Me.btnDescuento.TabStop = False
        '
        'btnPromociones
        '
        Me.btnPromociones.Image = Global.SLM_2._2.My.Resources.Resources.precios_y_descuentos_34
        Me.btnPromociones.Location = New System.Drawing.Point(365, 25)
        Me.btnPromociones.Name = "btnPromociones"
        Me.btnPromociones.Size = New System.Drawing.Size(95, 115)
        Me.btnPromociones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPromociones.TabIndex = 43
        Me.btnPromociones.TabStop = False
        '
        'TpProcesos
        '
        Me.TpProcesos.BackColor = System.Drawing.Color.AliceBlue
        Me.TpProcesos.Controls.Add(Me.btnPlanilla)
        Me.TpProcesos.Controls.Add(Me.btnDepreciacion)
        Me.TpProcesos.Controls.Add(Me.btnConsolidar)
        Me.TpProcesos.Location = New System.Drawing.Point(4, 22)
        Me.TpProcesos.Name = "TpProcesos"
        Me.TpProcesos.Size = New System.Drawing.Size(578, 469)
        Me.TpProcesos.TabIndex = 4
        Me.TpProcesos.Text = "Procesos"
        '
        'btnPlanilla
        '
        Me.btnPlanilla.BackColor = System.Drawing.Color.Transparent
        Me.btnPlanilla.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_03
        Me.btnPlanilla.Location = New System.Drawing.Point(288, 13)
        Me.btnPlanilla.Name = "btnPlanilla"
        Me.btnPlanilla.Size = New System.Drawing.Size(103, 111)
        Me.btnPlanilla.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPlanilla.TabIndex = 45
        Me.btnPlanilla.TabStop = False
        '
        'btnDepreciacion
        '
        Me.btnDepreciacion.BackColor = System.Drawing.Color.Transparent
        Me.btnDepreciacion.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_02
        Me.btnDepreciacion.Location = New System.Drawing.Point(142, 13)
        Me.btnDepreciacion.Name = "btnDepreciacion"
        Me.btnDepreciacion.Size = New System.Drawing.Size(104, 111)
        Me.btnDepreciacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnDepreciacion.TabIndex = 44
        Me.btnDepreciacion.TabStop = False
        '
        'btnConsolidar
        '
        Me.btnConsolidar.BackColor = System.Drawing.Color.Transparent
        Me.btnConsolidar.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_01
        Me.btnConsolidar.Location = New System.Drawing.Point(8, 13)
        Me.btnConsolidar.Name = "btnConsolidar"
        Me.btnConsolidar.Size = New System.Drawing.Size(104, 111)
        Me.btnConsolidar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnConsolidar.TabIndex = 43
        Me.btnConsolidar.TabStop = False
        '
        'tpCheque
        '
        Me.tpCheque.BackColor = System.Drawing.Color.AliceBlue
        Me.tpCheque.Controls.Add(Me.btnCheques)
        Me.tpCheque.Controls.Add(Me.btnChequera)
        Me.tpCheque.Controls.Add(Me.btnDepoBanc)
        Me.tpCheque.Location = New System.Drawing.Point(4, 22)
        Me.tpCheque.Name = "tpCheque"
        Me.tpCheque.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCheque.Size = New System.Drawing.Size(578, 469)
        Me.tpCheque.TabIndex = 0
        Me.tpCheque.Text = "Cheque"
        '
        'btnCheques
        '
        Me.btnCheques.Image = Global.SLM_2._2.My.Resources.Resources.cheque_39
        Me.btnCheques.Location = New System.Drawing.Point(24, 34)
        Me.btnCheques.Name = "btnCheques"
        Me.btnCheques.Size = New System.Drawing.Size(92, 106)
        Me.btnCheques.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCheques.TabIndex = 46
        Me.btnCheques.TabStop = False
        '
        'btnChequera
        '
        Me.btnChequera.Image = Global.SLM_2._2.My.Resources.Resources.cheque_38
        Me.btnChequera.Location = New System.Drawing.Point(148, 34)
        Me.btnChequera.Name = "btnChequera"
        Me.btnChequera.Size = New System.Drawing.Size(93, 106)
        Me.btnChequera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnChequera.TabIndex = 45
        Me.btnChequera.TabStop = False
        '
        'btnDepoBanc
        '
        Me.btnDepoBanc.Image = Global.SLM_2._2.My.Resources.Resources.cheque_40
        Me.btnDepoBanc.Location = New System.Drawing.Point(270, 33)
        Me.btnDepoBanc.Name = "btnDepoBanc"
        Me.btnDepoBanc.Size = New System.Drawing.Size(93, 106)
        Me.btnDepoBanc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnDepoBanc.TabIndex = 44
        Me.btnDepoBanc.TabStop = False
        '
        'tpPagos
        '
        Me.tpPagos.BackColor = System.Drawing.Color.AliceBlue
        Me.tpPagos.Controls.Add(Me.btnFacturaCompra)
        Me.tpPagos.Controls.Add(Me.btnPagos)
        Me.tpPagos.Location = New System.Drawing.Point(4, 22)
        Me.tpPagos.Name = "tpPagos"
        Me.tpPagos.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPagos.Size = New System.Drawing.Size(578, 469)
        Me.tpPagos.TabIndex = 1
        Me.tpPagos.Text = "Pagos"
        '
        'btnFacturaCompra
        '
        Me.btnFacturaCompra.Image = Global.SLM_2._2.My.Resources.Resources.pagos_41
        Me.btnFacturaCompra.Location = New System.Drawing.Point(21, 29)
        Me.btnFacturaCompra.Name = "btnFacturaCompra"
        Me.btnFacturaCompra.Size = New System.Drawing.Size(93, 110)
        Me.btnFacturaCompra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFacturaCompra.TabIndex = 45
        Me.btnFacturaCompra.TabStop = False
        '
        'btnPagos
        '
        Me.btnPagos.Image = Global.SLM_2._2.My.Resources.Resources.pagos_42
        Me.btnPagos.Location = New System.Drawing.Point(160, 29)
        Me.btnPagos.Name = "btnPagos"
        Me.btnPagos.Size = New System.Drawing.Size(91, 110)
        Me.btnPagos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPagos.TabIndex = 44
        Me.btnPagos.TabStop = False
        '
        'TabPage16
        '
        Me.TabPage16.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage16.Controls.Add(Me.PictureBox43)
        Me.TabPage16.Controls.Add(Me.PictureBox42)
        Me.TabPage16.Controls.Add(Me.PictureBox41)
        Me.TabPage16.Controls.Add(Me.PictureBox40)
        Me.TabPage16.Controls.Add(Me.PictureBox6)
        Me.TabPage16.Controls.Add(Me.btnIngresos)
        Me.TabPage16.Location = New System.Drawing.Point(4, 22)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage16.Size = New System.Drawing.Size(578, 469)
        Me.TabPage16.TabIndex = 5
        Me.TabPage16.Text = "Reportes Financieros"
        '
        'PictureBox43
        '
        Me.PictureBox43.Image = CType(resources.GetObject("PictureBox43.Image"), System.Drawing.Image)
        Me.PictureBox43.Location = New System.Drawing.Point(402, 23)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox43.TabIndex = 57
        Me.PictureBox43.TabStop = False
        '
        'PictureBox42
        '
        Me.PictureBox42.Image = CType(resources.GetObject("PictureBox42.Image"), System.Drawing.Image)
        Me.PictureBox42.Location = New System.Drawing.Point(276, 23)
        Me.PictureBox42.Name = "PictureBox42"
        Me.PictureBox42.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox42.TabIndex = 56
        Me.PictureBox42.TabStop = False
        '
        'PictureBox41
        '
        Me.PictureBox41.Image = CType(resources.GetObject("PictureBox41.Image"), System.Drawing.Image)
        Me.PictureBox41.Location = New System.Drawing.Point(147, 25)
        Me.PictureBox41.Name = "PictureBox41"
        Me.PictureBox41.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox41.TabIndex = 55
        Me.PictureBox41.TabStop = False
        '
        'PictureBox40
        '
        Me.PictureBox40.Image = CType(resources.GetObject("PictureBox40.Image"), System.Drawing.Image)
        Me.PictureBox40.Location = New System.Drawing.Point(20, 25)
        Me.PictureBox40.Name = "PictureBox40"
        Me.PictureBox40.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox40.TabIndex = 54
        Me.PictureBox40.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_06
        Me.PictureBox6.Location = New System.Drawing.Point(144, 157)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(92, 110)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 53
        Me.PictureBox6.TabStop = False
        '
        'btnIngresos
        '
        Me.btnIngresos.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_05
        Me.btnIngresos.Location = New System.Drawing.Point(22, 157)
        Me.btnIngresos.Name = "btnIngresos"
        Me.btnIngresos.Size = New System.Drawing.Size(92, 110)
        Me.btnIngresos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnIngresos.TabIndex = 52
        Me.btnIngresos.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 600)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 10, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(710, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage18)
        Me.TabControl1.Controls.Add(Me.TabPage21)
        Me.TabControl1.Location = New System.Drawing.Point(1, 35)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(583, 498)
        Me.TabControl1.TabIndex = 6
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage1.Controls.Add(Me.pbxTipoClasificacion)
        Me.TabPage1.Controls.Add(Me.pbxCategoriaCliente)
        Me.TabPage1.Controls.Add(Me.PictureBox52)
        Me.TabPage1.Controls.Add(Me.PictureBox51)
        Me.TabPage1.Controls.Add(Me.PictureBox50)
        Me.TabPage1.Controls.Add(Me.PictureBox47)
        Me.TabPage1.Controls.Add(Me.PictureBox46)
        Me.TabPage1.Controls.Add(Me.PictureBox34)
        Me.TabPage1.Controls.Add(Me.PictureBox33)
        Me.TabPage1.Controls.Add(Me.PictureBox32)
        Me.TabPage1.Controls.Add(Me.PictureBox31)
        Me.TabPage1.Controls.Add(Me.PictureBox30)
        Me.TabPage1.Controls.Add(Me.PictureBox29)
        Me.TabPage1.Controls.Add(Me.PictureBox28)
        Me.TabPage1.Controls.Add(Me.PictureBox27)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(575, 472)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Facturación"
        '
        'pbxTipoClasificacion
        '
        Me.pbxTipoClasificacion.BackColor = System.Drawing.Color.Transparent
        Me.pbxTipoClasificacion.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_57
        Me.pbxTipoClasificacion.Location = New System.Drawing.Point(413, 279)
        Me.pbxTipoClasificacion.Name = "pbxTipoClasificacion"
        Me.pbxTipoClasificacion.Size = New System.Drawing.Size(86, 120)
        Me.pbxTipoClasificacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbxTipoClasificacion.TabIndex = 60
        Me.pbxTipoClasificacion.TabStop = False
        '
        'pbxCategoriaCliente
        '
        Me.pbxCategoriaCliente.BackColor = System.Drawing.Color.Transparent
        Me.pbxCategoriaCliente.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_48
        Me.pbxCategoriaCliente.Location = New System.Drawing.Point(413, 148)
        Me.pbxCategoriaCliente.Name = "pbxCategoriaCliente"
        Me.pbxCategoriaCliente.Size = New System.Drawing.Size(85, 120)
        Me.pbxCategoriaCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbxCategoriaCliente.TabIndex = 59
        Me.pbxCategoriaCliente.TabStop = False
        '
        'PictureBox52
        '
        Me.PictureBox52.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox52.Image = CType(resources.GetObject("PictureBox52.Image"), System.Drawing.Image)
        Me.PictureBox52.Location = New System.Drawing.Point(25, 279)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(92, 120)
        Me.PictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox52.TabIndex = 56
        Me.PictureBox52.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox51.Image = CType(resources.GetObject("PictureBox51.Image"), System.Drawing.Image)
        Me.PictureBox51.Location = New System.Drawing.Point(123, 279)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(85, 120)
        Me.PictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox51.TabIndex = 55
        Me.PictureBox51.TabStop = False
        '
        'PictureBox50
        '
        Me.PictureBox50.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox50.Image = CType(resources.GetObject("PictureBox50.Image"), System.Drawing.Image)
        Me.PictureBox50.Location = New System.Drawing.Point(214, 279)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(86, 120)
        Me.PictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox50.TabIndex = 54
        Me.PictureBox50.TabStop = False
        '
        'PictureBox47
        '
        Me.PictureBox47.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox47.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_56
        Me.PictureBox47.Location = New System.Drawing.Point(306, 279)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(85, 120)
        Me.PictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox47.TabIndex = 53
        Me.PictureBox47.TabStop = False
        '
        'PictureBox46
        '
        Me.PictureBox46.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox46.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_48
        Me.PictureBox46.Location = New System.Drawing.Point(25, 148)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(92, 120)
        Me.PictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox46.TabIndex = 52
        Me.PictureBox46.TabStop = False
        '
        'PictureBox34
        '
        Me.PictureBox34.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox34.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_51
        Me.PictureBox34.Location = New System.Drawing.Point(123, 149)
        Me.PictureBox34.Name = "PictureBox34"
        Me.PictureBox34.Size = New System.Drawing.Size(85, 119)
        Me.PictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox34.TabIndex = 51
        Me.PictureBox34.TabStop = False
        '
        'PictureBox33
        '
        Me.PictureBox33.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox33.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_50
        Me.PictureBox33.Location = New System.Drawing.Point(213, 149)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(87, 119)
        Me.PictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox33.TabIndex = 50
        Me.PictureBox33.TabStop = False
        '
        'PictureBox32
        '
        Me.PictureBox32.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox32.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_49
        Me.PictureBox32.Location = New System.Drawing.Point(306, 149)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(85, 119)
        Me.PictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox32.TabIndex = 49
        Me.PictureBox32.TabStop = False
        '
        'PictureBox31
        '
        Me.PictureBox31.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox31.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_43
        Me.PictureBox31.Location = New System.Drawing.Point(25, 22)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(93, 120)
        Me.PictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox31.TabIndex = 48
        Me.PictureBox31.TabStop = False
        '
        'PictureBox30
        '
        Me.PictureBox30.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox30.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_44
        Me.PictureBox30.Location = New System.Drawing.Point(123, 22)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(85, 120)
        Me.PictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox30.TabIndex = 47
        Me.PictureBox30.TabStop = False
        '
        'PictureBox29
        '
        Me.PictureBox29.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox29.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_45
        Me.PictureBox29.Location = New System.Drawing.Point(214, 22)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(86, 120)
        Me.PictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox29.TabIndex = 46
        Me.PictureBox29.TabStop = False
        '
        'PictureBox28
        '
        Me.PictureBox28.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox28.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_46
        Me.PictureBox28.Location = New System.Drawing.Point(306, 22)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(85, 120)
        Me.PictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox28.TabIndex = 45
        Me.PictureBox28.TabStop = False
        '
        'PictureBox27
        '
        Me.PictureBox27.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox27.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_47
        Me.PictureBox27.Location = New System.Drawing.Point(413, 22)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(86, 120)
        Me.PictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox27.TabIndex = 44
        Me.PictureBox27.TabStop = False
        '
        'TabPage18
        '
        Me.TabPage18.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage18.Controls.Add(Me.btnClienteEmpresarial)
        Me.TabPage18.Controls.Add(Me.btnCotizacionEmpresarial)
        Me.TabPage18.Controls.Add(Me.btnFacturaEmpresarial)
        Me.TabPage18.Location = New System.Drawing.Point(4, 22)
        Me.TabPage18.Name = "TabPage18"
        Me.TabPage18.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage18.Size = New System.Drawing.Size(575, 472)
        Me.TabPage18.TabIndex = 1
        Me.TabPage18.Text = "Empresarial"
        '
        'btnClienteEmpresarial
        '
        Me.btnClienteEmpresarial.Image = CType(resources.GetObject("btnClienteEmpresarial.Image"), System.Drawing.Image)
        Me.btnClienteEmpresarial.InitialImage = Nothing
        Me.btnClienteEmpresarial.Location = New System.Drawing.Point(241, 28)
        Me.btnClienteEmpresarial.Margin = New System.Windows.Forms.Padding(2)
        Me.btnClienteEmpresarial.Name = "btnClienteEmpresarial"
        Me.btnClienteEmpresarial.Size = New System.Drawing.Size(85, 120)
        Me.btnClienteEmpresarial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClienteEmpresarial.TabIndex = 51
        Me.btnClienteEmpresarial.TabStop = False
        '
        'btnCotizacionEmpresarial
        '
        Me.btnCotizacionEmpresarial.BackColor = System.Drawing.Color.Transparent
        Me.btnCotizacionEmpresarial.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_44
        Me.btnCotizacionEmpresarial.Location = New System.Drawing.Point(129, 27)
        Me.btnCotizacionEmpresarial.Name = "btnCotizacionEmpresarial"
        Me.btnCotizacionEmpresarial.Size = New System.Drawing.Size(85, 120)
        Me.btnCotizacionEmpresarial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCotizacionEmpresarial.TabIndex = 50
        Me.btnCotizacionEmpresarial.TabStop = False
        '
        'btnFacturaEmpresarial
        '
        Me.btnFacturaEmpresarial.BackColor = System.Drawing.Color.Transparent
        Me.btnFacturaEmpresarial.Image = Global.SLM_2._2.My.Resources.Resources.facturacion_43
        Me.btnFacturaEmpresarial.Location = New System.Drawing.Point(14, 28)
        Me.btnFacturaEmpresarial.Name = "btnFacturaEmpresarial"
        Me.btnFacturaEmpresarial.Size = New System.Drawing.Size(93, 120)
        Me.btnFacturaEmpresarial.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFacturaEmpresarial.TabIndex = 49
        Me.btnFacturaEmpresarial.TabStop = False
        '
        'TabPage21
        '
        Me.TabPage21.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.TabPage21.Controls.Add(Me.PictureBox44)
        Me.TabPage21.Location = New System.Drawing.Point(4, 22)
        Me.TabPage21.Name = "TabPage21"
        Me.TabPage21.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage21.Size = New System.Drawing.Size(575, 472)
        Me.TabPage21.TabIndex = 2
        Me.TabPage21.Text = "Actualizar Domicilio"
        '
        'PictureBox44
        '
        Me.PictureBox44.Image = CType(resources.GetObject("PictureBox44.Image"), System.Drawing.Image)
        Me.PictureBox44.Location = New System.Drawing.Point(16, 16)
        Me.PictureBox44.Name = "PictureBox44"
        Me.PictureBox44.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox44.TabIndex = 57
        Me.PictureBox44.TabStop = False
        '
        'PanelLab
        '
        Me.PanelLab.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelLab.BackColor = System.Drawing.Color.White
        Me.PanelLab.Controls.Add(Me.Panel8)
        Me.PanelLab.Controls.Add(Me.TabControl2)
        Me.PanelLab.Location = New System.Drawing.Point(123, 59)
        Me.PanelLab.Name = "PanelLab"
        Me.PanelLab.Size = New System.Drawing.Size(582, 549)
        Me.PanelLab.TabIndex = 8
        Me.PanelLab.Visible = False
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel8.Controls.Add(Me.PictureBox7)
        Me.Panel8.Controls.Add(Me.Label7)
        Me.Panel8.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel8.Location = New System.Drawing.Point(1, 1)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(584, 37)
        Me.Panel8.TabIndex = 11
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox7.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox7.Location = New System.Drawing.Point(550, 7)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox7.TabIndex = 10
        Me.PictureBox7.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(252, 25)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Módulo De Laboratorio"
        '
        'TabControl2
        '
        Me.TabControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Controls.Add(Me.TabPage2)
        Me.TabControl2.Controls.Add(Me.TabPage17)
        Me.TabControl2.Controls.Add(Me.TabPage20)
        Me.TabControl2.Location = New System.Drawing.Point(1, 38)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(582, 495)
        Me.TabControl2.TabIndex = 6
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage3.Controls.Add(Me.btnAreasLaboratorio)
        Me.TabPage3.Controls.Add(Me.PictureBox19)
        Me.TabPage3.Controls.Add(Me.btnSubAreas)
        Me.TabPage3.Controls.Add(Me.PictureBox76)
        Me.TabPage3.Controls.Add(Me.btnGrupoExamenes)
        Me.TabPage3.Controls.Add(Me.PictureBox49)
        Me.TabPage3.Controls.Add(Me.btnInformes)
        Me.TabPage3.Controls.Add(Me.PictureBox48)
        Me.TabPage3.Controls.Add(Me.btnValoresRef)
        Me.TabPage3.Controls.Add(Me.PictureBox23)
        Me.TabPage3.Controls.Add(Me.PictureBox21)
        Me.TabPage3.Controls.Add(Me.PictureBox20)
        Me.TabPage3.Controls.Add(Me.btnEntrega)
        Me.TabPage3.Controls.Add(Me.btnHojaTrabajo)
        Me.TabPage3.Controls.Add(Me.btnTrabajo)
        Me.TabPage3.Controls.Add(Me.btnExamen)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(574, 469)
        Me.TabPage3.TabIndex = 1
        Me.TabPage3.Text = "Exámenes"
        '
        'btnAreasLaboratorio
        '
        Me.btnAreasLaboratorio.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_83
        Me.btnAreasLaboratorio.Location = New System.Drawing.Point(150, 165)
        Me.btnAreasLaboratorio.Name = "btnAreasLaboratorio"
        Me.btnAreasLaboratorio.Size = New System.Drawing.Size(88, 108)
        Me.btnAreasLaboratorio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnAreasLaboratorio.TabIndex = 47
        Me.btnAreasLaboratorio.TabStop = False
        '
        'PictureBox19
        '
        Me.PictureBox19.Image = Global.SLM_2._2.My.Resources.Resources.examenes_65
        Me.PictureBox19.InitialImage = Nothing
        Me.PictureBox19.Location = New System.Drawing.Point(399, 162)
        Me.PictureBox19.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(87, 111)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox19.TabIndex = 22
        Me.PictureBox19.TabStop = False
        '
        'btnSubAreas
        '
        Me.btnSubAreas.Enabled = False
        Me.btnSubAreas.Location = New System.Drawing.Point(412, 316)
        Me.btnSubAreas.Name = "btnSubAreas"
        Me.btnSubAreas.Size = New System.Drawing.Size(99, 23)
        Me.btnSubAreas.TabIndex = 21
        Me.btnSubAreas.Text = "SubAreas"
        Me.btnSubAreas.UseVisualStyleBackColor = True
        Me.btnSubAreas.Visible = False
        '
        'PictureBox76
        '
        Me.PictureBox76.Image = Global.SLM_2._2.My.Resources.Resources.examenes_66
        Me.PictureBox76.InitialImage = Nothing
        Me.PictureBox76.Location = New System.Drawing.Point(281, 162)
        Me.PictureBox76.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox76.Name = "PictureBox76"
        Me.PictureBox76.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox76.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox76.TabIndex = 20
        Me.PictureBox76.TabStop = False
        '
        'btnGrupoExamenes
        '
        Me.btnGrupoExamenes.Enabled = False
        Me.btnGrupoExamenes.Location = New System.Drawing.Point(287, 316)
        Me.btnGrupoExamenes.Name = "btnGrupoExamenes"
        Me.btnGrupoExamenes.Size = New System.Drawing.Size(99, 23)
        Me.btnGrupoExamenes.TabIndex = 19
        Me.btnGrupoExamenes.Text = "Grupo Exámenes"
        Me.btnGrupoExamenes.UseVisualStyleBackColor = True
        Me.btnGrupoExamenes.Visible = False
        '
        'PictureBox49
        '
        Me.PictureBox49.Image = Global.SLM_2._2.My.Resources.Resources.examenes_64
        Me.PictureBox49.InitialImage = Nothing
        Me.PictureBox49.Location = New System.Drawing.Point(396, 13)
        Me.PictureBox49.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(90, 111)
        Me.PictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox49.TabIndex = 18
        Me.PictureBox49.TabStop = False
        '
        'btnInformes
        '
        Me.btnInformes.Enabled = False
        Me.btnInformes.Location = New System.Drawing.Point(395, 127)
        Me.btnInformes.Name = "btnInformes"
        Me.btnInformes.Size = New System.Drawing.Size(99, 23)
        Me.btnInformes.TabIndex = 17
        Me.btnInformes.Text = "Informes"
        Me.btnInformes.UseVisualStyleBackColor = True
        Me.btnInformes.Visible = False
        '
        'PictureBox48
        '
        Me.PictureBox48.Image = Global.SLM_2._2.My.Resources.Resources.examenes_63
        Me.PictureBox48.InitialImage = Nothing
        Me.PictureBox48.Location = New System.Drawing.Point(279, 12)
        Me.PictureBox48.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(90, 112)
        Me.PictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox48.TabIndex = 16
        Me.PictureBox48.TabStop = False
        '
        'btnValoresRef
        '
        Me.btnValoresRef.Enabled = False
        Me.btnValoresRef.Location = New System.Drawing.Point(270, 127)
        Me.btnValoresRef.Name = "btnValoresRef"
        Me.btnValoresRef.Size = New System.Drawing.Size(99, 23)
        Me.btnValoresRef.TabIndex = 15
        Me.btnValoresRef.Text = "Valores Ref."
        Me.btnValoresRef.UseVisualStyleBackColor = True
        Me.btnValoresRef.Visible = False
        '
        'PictureBox23
        '
        Me.PictureBox23.Image = Global.SLM_2._2.My.Resources.Resources.examenes_62
        Me.PictureBox23.InitialImage = Nothing
        Me.PictureBox23.Location = New System.Drawing.Point(150, 13)
        Me.PictureBox23.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(89, 111)
        Me.PictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox23.TabIndex = 14
        Me.PictureBox23.TabStop = False
        '
        'PictureBox21
        '
        Me.PictureBox21.Image = Global.SLM_2._2.My.Resources.Resources.examenes_68
        Me.PictureBox21.InitialImage = Nothing
        Me.PictureBox21.Location = New System.Drawing.Point(23, 162)
        Me.PictureBox21.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(91, 111)
        Me.PictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox21.TabIndex = 12
        Me.PictureBox21.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.Image = Global.SLM_2._2.My.Resources.Resources.examenes_61
        Me.PictureBox20.InitialImage = Nothing
        Me.PictureBox20.Location = New System.Drawing.Point(23, 13)
        Me.PictureBox20.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(89, 111)
        Me.PictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox20.TabIndex = 11
        Me.PictureBox20.TabStop = False
        '
        'btnEntrega
        '
        Me.btnEntrega.Location = New System.Drawing.Point(159, 316)
        Me.btnEntrega.Name = "btnEntrega"
        Me.btnEntrega.Size = New System.Drawing.Size(99, 23)
        Me.btnEntrega.TabIndex = 10
        Me.btnEntrega.Text = "Entrega Result."
        Me.btnEntrega.UseVisualStyleBackColor = True
        Me.btnEntrega.Visible = False
        '
        'btnHojaTrabajo
        '
        Me.btnHojaTrabajo.Location = New System.Drawing.Point(34, 316)
        Me.btnHojaTrabajo.Name = "btnHojaTrabajo"
        Me.btnHojaTrabajo.Size = New System.Drawing.Size(99, 23)
        Me.btnHojaTrabajo.TabIndex = 9
        Me.btnHojaTrabajo.Text = "Hoja Trabajo"
        Me.btnHojaTrabajo.UseVisualStyleBackColor = True
        Me.btnHojaTrabajo.Visible = False
        '
        'btnTrabajo
        '
        Me.btnTrabajo.Location = New System.Drawing.Point(142, 128)
        Me.btnTrabajo.Name = "btnTrabajo"
        Me.btnTrabajo.Size = New System.Drawing.Size(99, 23)
        Me.btnTrabajo.TabIndex = 8
        Me.btnTrabajo.Text = "Or. Trabajo"
        Me.btnTrabajo.UseVisualStyleBackColor = True
        Me.btnTrabajo.Visible = False
        '
        'btnExamen
        '
        Me.btnExamen.Location = New System.Drawing.Point(17, 128)
        Me.btnExamen.Name = "btnExamen"
        Me.btnExamen.Size = New System.Drawing.Size(99, 23)
        Me.btnExamen.TabIndex = 7
        Me.btnExamen.Text = "Exámenes"
        Me.btnExamen.UseVisualStyleBackColor = True
        Me.btnExamen.Visible = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage4.Controls.Add(Me.btnSede2)
        Me.TabPage4.Controls.Add(Me.btnSucursal)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(574, 469)
        Me.TabPage4.TabIndex = 2
        Me.TabPage4.Text = "Sedes"
        '
        'btnSede2
        '
        Me.btnSede2.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_725
        Me.btnSede2.InitialImage = Nothing
        Me.btnSede2.Location = New System.Drawing.Point(9, 9)
        Me.btnSede2.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSede2.Name = "btnSede2"
        Me.btnSede2.Size = New System.Drawing.Size(87, 111)
        Me.btnSede2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnSede2.TabIndex = 14
        Me.btnSede2.TabStop = False
        '
        'btnSucursal
        '
        Me.btnSucursal.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_73
        Me.btnSucursal.InitialImage = Nothing
        Me.btnSucursal.Location = New System.Drawing.Point(119, 9)
        Me.btnSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSucursal.Name = "btnSucursal"
        Me.btnSucursal.Size = New System.Drawing.Size(87, 111)
        Me.btnSucursal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnSucursal.TabIndex = 13
        Me.btnSucursal.TabStop = False
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage5.Controls.Add(Me.btnMedico2)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(574, 469)
        Me.TabPage5.TabIndex = 3
        Me.TabPage5.Text = "Médicos"
        '
        'btnMedico2
        '
        Me.btnMedico2.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_71
        Me.btnMedico2.InitialImage = Nothing
        Me.btnMedico2.Location = New System.Drawing.Point(22, 9)
        Me.btnMedico2.Margin = New System.Windows.Forms.Padding(2)
        Me.btnMedico2.Name = "btnMedico2"
        Me.btnMedico2.Size = New System.Drawing.Size(83, 111)
        Me.btnMedico2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnMedico2.TabIndex = 12
        Me.btnMedico2.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage2.Controls.Add(Me.btnPaciente2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(574, 469)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "Clientes"
        '
        'btnPaciente2
        '
        Me.btnPaciente2.Image = CType(resources.GetObject("btnPaciente2.Image"), System.Drawing.Image)
        Me.btnPaciente2.InitialImage = Nothing
        Me.btnPaciente2.Location = New System.Drawing.Point(30, 19)
        Me.btnPaciente2.Margin = New System.Windows.Forms.Padding(2)
        Me.btnPaciente2.Name = "btnPaciente2"
        Me.btnPaciente2.Size = New System.Drawing.Size(84, 111)
        Me.btnPaciente2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnPaciente2.TabIndex = 8
        Me.btnPaciente2.TabStop = False
        '
        'TabPage17
        '
        Me.TabPage17.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage17.Controls.Add(Me.PictureBox26)
        Me.TabPage17.Controls.Add(Me.PictureBox2)
        Me.TabPage17.Controls.Add(Me.btnReenvio)
        Me.TabPage17.Location = New System.Drawing.Point(4, 22)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage17.Size = New System.Drawing.Size(574, 469)
        Me.TabPage17.TabIndex = 4
        Me.TabPage17.Text = "Resultados"
        '
        'PictureBox26
        '
        Me.PictureBox26.Image = CType(resources.GetObject("PictureBox26.Image"), System.Drawing.Image)
        Me.PictureBox26.InitialImage = Nothing
        Me.PictureBox26.Location = New System.Drawing.Point(241, 19)
        Me.PictureBox26.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox26.TabIndex = 27
        Me.PictureBox26.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.SLM_2._2.My.Resources.Resources.examenes_67
        Me.PictureBox2.InitialImage = Nothing
        Me.PictureBox2.Location = New System.Drawing.Point(126, 19)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 24
        Me.PictureBox2.TabStop = False
        '
        'btnReenvio
        '
        Me.btnReenvio.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_19
        Me.btnReenvio.InitialImage = Nothing
        Me.btnReenvio.Location = New System.Drawing.Point(11, 19)
        Me.btnReenvio.Margin = New System.Windows.Forms.Padding(2)
        Me.btnReenvio.Name = "btnReenvio"
        Me.btnReenvio.Size = New System.Drawing.Size(89, 111)
        Me.btnReenvio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnReenvio.TabIndex = 12
        Me.btnReenvio.TabStop = False
        '
        'TabPage20
        '
        Me.TabPage20.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage20.Controls.Add(Me.PictureBox25)
        Me.TabPage20.Controls.Add(Me.PictureBox37)
        Me.TabPage20.Controls.Add(Me.PictureBox36)
        Me.TabPage20.Controls.Add(Me.PictureBox35)
        Me.TabPage20.Location = New System.Drawing.Point(4, 22)
        Me.TabPage20.Name = "TabPage20"
        Me.TabPage20.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage20.Size = New System.Drawing.Size(574, 469)
        Me.TabPage20.TabIndex = 5
        Me.TabPage20.Text = "Curvas"
        '
        'PictureBox25
        '
        Me.PictureBox25.Image = CType(resources.GetObject("PictureBox25.Image"), System.Drawing.Image)
        Me.PictureBox25.InitialImage = Nothing
        Me.PictureBox25.Location = New System.Drawing.Point(330, 14)
        Me.PictureBox25.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox25.TabIndex = 30
        Me.PictureBox25.TabStop = False
        '
        'PictureBox37
        '
        Me.PictureBox37.Image = CType(resources.GetObject("PictureBox37.Image"), System.Drawing.Image)
        Me.PictureBox37.InitialImage = Nothing
        Me.PictureBox37.Location = New System.Drawing.Point(231, 14)
        Me.PictureBox37.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox37.Name = "PictureBox37"
        Me.PictureBox37.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox37.TabIndex = 29
        Me.PictureBox37.TabStop = False
        '
        'PictureBox36
        '
        Me.PictureBox36.Image = CType(resources.GetObject("PictureBox36.Image"), System.Drawing.Image)
        Me.PictureBox36.InitialImage = Nothing
        Me.PictureBox36.Location = New System.Drawing.Point(126, 14)
        Me.PictureBox36.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox36.Name = "PictureBox36"
        Me.PictureBox36.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox36.TabIndex = 28
        Me.PictureBox36.TabStop = False
        '
        'PictureBox35
        '
        Me.PictureBox35.Image = CType(resources.GetObject("PictureBox35.Image"), System.Drawing.Image)
        Me.PictureBox35.InitialImage = Nothing
        Me.PictureBox35.Location = New System.Drawing.Point(13, 14)
        Me.PictureBox35.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(88, 111)
        Me.PictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox35.TabIndex = 27
        Me.PictureBox35.TabStop = False
        '
        'PanelFactura
        '
        Me.PanelFactura.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelFactura.BackColor = System.Drawing.Color.White
        Me.PanelFactura.Controls.Add(Me.Panel3)
        Me.PanelFactura.Controls.Add(Me.TabControl1)
        Me.PanelFactura.Location = New System.Drawing.Point(123, 59)
        Me.PanelFactura.Name = "PanelFactura"
        Me.PanelFactura.Size = New System.Drawing.Size(584, 547)
        Me.PanelFactura.TabIndex = 7
        Me.PanelFactura.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel3.Controls.Add(Me.PictureBox9)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel3.Location = New System.Drawing.Point(1, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(583, 37)
        Me.Panel3.TabIndex = 10
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox9.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox9.Location = New System.Drawing.Point(547, 8)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox9.TabIndex = 12
        Me.PictureBox9.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(256, 25)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Módulo De Facturación"
        '
        'PanelTalentoHumano
        '
        Me.PanelTalentoHumano.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelTalentoHumano.BackColor = System.Drawing.Color.White
        Me.PanelTalentoHumano.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PanelTalentoHumano.Controls.Add(Me.Panel5)
        Me.PanelTalentoHumano.Controls.Add(Me.TabControl4)
        Me.PanelTalentoHumano.Location = New System.Drawing.Point(123, 59)
        Me.PanelTalentoHumano.Name = "PanelTalentoHumano"
        Me.PanelTalentoHumano.Size = New System.Drawing.Size(584, 549)
        Me.PanelTalentoHumano.TabIndex = 8
        Me.PanelTalentoHumano.Visible = False
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel5.Controls.Add(Me.PictureBox11)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(583, 37)
        Me.Panel5.TabIndex = 11
        '
        'PictureBox11
        '
        Me.PictureBox11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox11.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox11.Location = New System.Drawing.Point(548, 8)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox11.TabIndex = 14
        Me.PictureBox11.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(304, 25)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Módulo De Talento Humano"
        '
        'TabControl4
        '
        Me.TabControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl4.Controls.Add(Me.TabPage9)
        Me.TabControl4.Location = New System.Drawing.Point(0, 37)
        Me.TabControl4.Name = "TabControl4"
        Me.TabControl4.SelectedIndex = 0
        Me.TabControl4.Size = New System.Drawing.Size(585, 496)
        Me.TabControl4.TabIndex = 6
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage9.Controls.Add(Me.btnConstanciaPlantillas)
        Me.TabPage9.Controls.Add(Me.btnReporteria)
        Me.TabPage9.Controls.Add(Me.btnTipoPermiso)
        Me.TabPage9.Controls.Add(Me.btnCapacitaciones)
        Me.TabPage9.Controls.Add(Me.btnCandidatos)
        Me.TabPage9.Controls.Add(Me.btnDepto)
        Me.TabPage9.Controls.Add(Me.btnPuestoTrabajo)
        Me.TabPage9.Controls.Add(Me.lblUserCod)
        Me.TabPage9.Controls.Add(Me.btnHorarios)
        Me.TabPage9.Controls.Add(Me.btnContratos)
        Me.TabPage9.Controls.Add(Me.btnProfesion)
        Me.TabPage9.Controls.Add(Me.btnTipoDeducciones)
        Me.TabPage9.Controls.Add(Me.btnArea)
        Me.TabPage9.Controls.Add(Me.btnVacaciones)
        Me.TabPage9.Controls.Add(Me.btnPermisos)
        Me.TabPage9.Controls.Add(Me.btnSucursales)
        Me.TabPage9.Controls.Add(Me.btnEmpleados)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(577, 470)
        Me.TabPage9.TabIndex = 0
        Me.TabPage9.Text = "Talento Humano"
        '
        'btnConstanciaPlantillas
        '
        Me.btnConstanciaPlantillas.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_79
        Me.btnConstanciaPlantillas.Location = New System.Drawing.Point(481, 30)
        Me.btnConstanciaPlantillas.Name = "btnConstanciaPlantillas"
        Me.btnConstanciaPlantillas.Size = New System.Drawing.Size(85, 111)
        Me.btnConstanciaPlantillas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnConstanciaPlantillas.TabIndex = 61
        Me.btnConstanciaPlantillas.TabStop = False
        '
        'btnReporteria
        '
        Me.btnReporteria.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_85
        Me.btnReporteria.Location = New System.Drawing.Point(481, 151)
        Me.btnReporteria.Name = "btnReporteria"
        Me.btnReporteria.Size = New System.Drawing.Size(85, 109)
        Me.btnReporteria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnReporteria.TabIndex = 59
        Me.btnReporteria.TabStop = False
        '
        'btnTipoPermiso
        '
        Me.btnTipoPermiso.Image = CType(resources.GetObject("btnTipoPermiso.Image"), System.Drawing.Image)
        Me.btnTipoPermiso.Location = New System.Drawing.Point(296, 272)
        Me.btnTipoPermiso.Name = "btnTipoPermiso"
        Me.btnTipoPermiso.Size = New System.Drawing.Size(88, 109)
        Me.btnTipoPermiso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnTipoPermiso.TabIndex = 57
        Me.btnTipoPermiso.TabStop = False
        '
        'btnCapacitaciones
        '
        Me.btnCapacitaciones.Image = CType(resources.GetObject("btnCapacitaciones.Image"), System.Drawing.Image)
        Me.btnCapacitaciones.Location = New System.Drawing.Point(201, 272)
        Me.btnCapacitaciones.Name = "btnCapacitaciones"
        Me.btnCapacitaciones.Size = New System.Drawing.Size(86, 109)
        Me.btnCapacitaciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCapacitaciones.TabIndex = 55
        Me.btnCapacitaciones.TabStop = False
        '
        'btnCandidatos
        '
        Me.btnCandidatos.Image = CType(resources.GetObject("btnCandidatos.Image"), System.Drawing.Image)
        Me.btnCandidatos.Location = New System.Drawing.Point(109, 272)
        Me.btnCandidatos.Name = "btnCandidatos"
        Me.btnCandidatos.Size = New System.Drawing.Size(86, 109)
        Me.btnCandidatos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCandidatos.TabIndex = 53
        Me.btnCandidatos.TabStop = False
        '
        'btnDepto
        '
        Me.btnDepto.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_84
        Me.btnDepto.Location = New System.Drawing.Point(388, 151)
        Me.btnDepto.Name = "btnDepto"
        Me.btnDepto.Size = New System.Drawing.Size(86, 109)
        Me.btnDepto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnDepto.TabIndex = 52
        Me.btnDepto.TabStop = False
        '
        'btnPuestoTrabajo
        '
        Me.btnPuestoTrabajo.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_74
        Me.btnPuestoTrabajo.Location = New System.Drawing.Point(16, 30)
        Me.btnPuestoTrabajo.Name = "btnPuestoTrabajo"
        Me.btnPuestoTrabajo.Size = New System.Drawing.Size(87, 110)
        Me.btnPuestoTrabajo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPuestoTrabajo.TabIndex = 51
        Me.btnPuestoTrabajo.TabStop = False
        '
        'lblUserCod
        '
        Me.lblUserCod.AutoSize = True
        Me.lblUserCod.Location = New System.Drawing.Point(22, 431)
        Me.lblUserCod.Name = "lblUserCod"
        Me.lblUserCod.Size = New System.Drawing.Size(54, 13)
        Me.lblUserCod.TabIndex = 27
        Me.lblUserCod.Text = "UserCode"
        Me.lblUserCod.Visible = False
        '
        'btnHorarios
        '
        Me.btnHorarios.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_75
        Me.btnHorarios.Location = New System.Drawing.Point(109, 30)
        Me.btnHorarios.Name = "btnHorarios"
        Me.btnHorarios.Size = New System.Drawing.Size(86, 110)
        Me.btnHorarios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnHorarios.TabIndex = 50
        Me.btnHorarios.TabStop = False
        '
        'btnContratos
        '
        Me.btnContratos.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_76
        Me.btnContratos.Location = New System.Drawing.Point(201, 30)
        Me.btnContratos.Name = "btnContratos"
        Me.btnContratos.Size = New System.Drawing.Size(86, 110)
        Me.btnContratos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnContratos.TabIndex = 49
        Me.btnContratos.TabStop = False
        '
        'btnProfesion
        '
        Me.btnProfesion.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_77
        Me.btnProfesion.Location = New System.Drawing.Point(294, 30)
        Me.btnProfesion.Name = "btnProfesion"
        Me.btnProfesion.Size = New System.Drawing.Size(88, 110)
        Me.btnProfesion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnProfesion.TabIndex = 48
        Me.btnProfesion.TabStop = False
        '
        'btnTipoDeducciones
        '
        Me.btnTipoDeducciones.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_78
        Me.btnTipoDeducciones.Location = New System.Drawing.Point(388, 30)
        Me.btnTipoDeducciones.Name = "btnTipoDeducciones"
        Me.btnTipoDeducciones.Size = New System.Drawing.Size(86, 110)
        Me.btnTipoDeducciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnTipoDeducciones.TabIndex = 47
        Me.btnTipoDeducciones.TabStop = False
        '
        'btnArea
        '
        Me.btnArea.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_83
        Me.btnArea.Location = New System.Drawing.Point(296, 152)
        Me.btnArea.Name = "btnArea"
        Me.btnArea.Size = New System.Drawing.Size(88, 108)
        Me.btnArea.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnArea.TabIndex = 46
        Me.btnArea.TabStop = False
        '
        'btnVacaciones
        '
        Me.btnVacaciones.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_82
        Me.btnVacaciones.Location = New System.Drawing.Point(201, 152)
        Me.btnVacaciones.Name = "btnVacaciones"
        Me.btnVacaciones.Size = New System.Drawing.Size(86, 108)
        Me.btnVacaciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnVacaciones.TabIndex = 45
        Me.btnVacaciones.TabStop = False
        '
        'btnPermisos
        '
        Me.btnPermisos.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_81
        Me.btnPermisos.Location = New System.Drawing.Point(108, 152)
        Me.btnPermisos.Name = "btnPermisos"
        Me.btnPermisos.Size = New System.Drawing.Size(87, 108)
        Me.btnPermisos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPermisos.TabIndex = 44
        Me.btnPermisos.TabStop = False
        '
        'btnSucursales
        '
        Me.btnSucursales.Image = Global.SLM_2._2.My.Resources.Resources.talento_humano_80
        Me.btnSucursales.Location = New System.Drawing.Point(15, 152)
        Me.btnSucursales.Name = "btnSucursales"
        Me.btnSucursales.Size = New System.Drawing.Size(88, 108)
        Me.btnSucursales.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnSucursales.TabIndex = 43
        Me.btnSucursales.TabStop = False
        '
        'btnEmpleados
        '
        Me.btnEmpleados.Image = CType(resources.GetObject("btnEmpleados.Image"), System.Drawing.Image)
        Me.btnEmpleados.Location = New System.Drawing.Point(16, 272)
        Me.btnEmpleados.Name = "btnEmpleados"
        Me.btnEmpleados.Size = New System.Drawing.Size(86, 109)
        Me.btnEmpleados.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnEmpleados.TabIndex = 42
        Me.btnEmpleados.TabStop = False
        '
        'TabControl3
        '
        Me.TabControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl3.Controls.Add(Me.TabPage6)
        Me.TabControl3.Controls.Add(Me.TabPage14)
        Me.TabControl3.Controls.Add(Me.TabPage19)
        Me.TabControl3.Location = New System.Drawing.Point(1, 37)
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.SelectedIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(586, 496)
        Me.TabControl3.TabIndex = 7
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage6.Controls.Add(Me.PictureBox24)
        Me.TabPage6.Controls.Add(Me.btnCorreoResultado)
        Me.TabPage6.Controls.Add(Me.btnFirmaDigital)
        Me.TabPage6.Controls.Add(Me.btnFeriados)
        Me.TabPage6.Controls.Add(Me.btnUsuarios)
        Me.TabPage6.Controls.Add(Me.btnPerfiles)
        Me.TabPage6.Controls.Add(Me.btnServidorCorreo)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(578, 470)
        Me.TabPage6.TabIndex = 3
        Me.TabPage6.Text = "Configuración de Sistema"
        '
        'PictureBox24
        '
        Me.PictureBox24.Image = CType(resources.GetObject("PictureBox24.Image"), System.Drawing.Image)
        Me.PictureBox24.Location = New System.Drawing.Point(277, 175)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox24.TabIndex = 52
        Me.PictureBox24.TabStop = False
        '
        'btnCorreoResultado
        '
        Me.btnCorreoResultado.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_14
        Me.btnCorreoResultado.Location = New System.Drawing.Point(151, 175)
        Me.btnCorreoResultado.Name = "btnCorreoResultado"
        Me.btnCorreoResultado.Size = New System.Drawing.Size(101, 115)
        Me.btnCorreoResultado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCorreoResultado.TabIndex = 51
        Me.btnCorreoResultado.TabStop = False
        '
        'btnFirmaDigital
        '
        Me.btnFirmaDigital.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_13
        Me.btnFirmaDigital.Location = New System.Drawing.Point(13, 175)
        Me.btnFirmaDigital.Name = "btnFirmaDigital"
        Me.btnFirmaDigital.Size = New System.Drawing.Size(101, 115)
        Me.btnFirmaDigital.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFirmaDigital.TabIndex = 50
        Me.btnFirmaDigital.TabStop = False
        '
        'btnFeriados
        '
        Me.btnFeriados.Image = Global.SLM_2._2.My.Resources.Resources.configuracion_de_sistema_921
        Me.btnFeriados.Location = New System.Drawing.Point(419, 25)
        Me.btnFeriados.Name = "btnFeriados"
        Me.btnFeriados.Size = New System.Drawing.Size(101, 115)
        Me.btnFeriados.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFeriados.TabIndex = 48
        Me.btnFeriados.TabStop = False
        '
        'btnUsuarios
        '
        Me.btnUsuarios.Image = Global.SLM_2._2.My.Resources.Resources.configuracion_de_sistema_90
        Me.btnUsuarios.Location = New System.Drawing.Point(13, 25)
        Me.btnUsuarios.Name = "btnUsuarios"
        Me.btnUsuarios.Size = New System.Drawing.Size(101, 115)
        Me.btnUsuarios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnUsuarios.TabIndex = 46
        Me.btnUsuarios.TabStop = False
        '
        'btnPerfiles
        '
        Me.btnPerfiles.Image = Global.SLM_2._2.My.Resources.Resources.configuracion_de_sistema_91
        Me.btnPerfiles.Location = New System.Drawing.Point(150, 25)
        Me.btnPerfiles.Name = "btnPerfiles"
        Me.btnPerfiles.Size = New System.Drawing.Size(101, 115)
        Me.btnPerfiles.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnPerfiles.TabIndex = 45
        Me.btnPerfiles.TabStop = False
        '
        'btnServidorCorreo
        '
        Me.btnServidorCorreo.Image = Global.SLM_2._2.My.Resources.Resources.configuracion_de_sistema_931
        Me.btnServidorCorreo.Location = New System.Drawing.Point(281, 25)
        Me.btnServidorCorreo.Name = "btnServidorCorreo"
        Me.btnServidorCorreo.Size = New System.Drawing.Size(101, 115)
        Me.btnServidorCorreo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnServidorCorreo.TabIndex = 44
        Me.btnServidorCorreo.TabStop = False
        '
        'TabPage14
        '
        Me.TabPage14.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage14.Controls.Add(Me.PictureBox16)
        Me.TabPage14.Controls.Add(Me.PictureBox14)
        Me.TabPage14.Controls.Add(Me.btnLogsCambios)
        Me.TabPage14.Controls.Add(Me.btnLogsExcepciones)
        Me.TabPage14.Controls.Add(Me.btnLogsLogin)
        Me.TabPage14.Location = New System.Drawing.Point(4, 22)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage14.Size = New System.Drawing.Size(578, 470)
        Me.TabPage14.TabIndex = 4
        Me.TabPage14.Text = "Logs"
        '
        'PictureBox16
        '
        Me.PictureBox16.Image = CType(resources.GetObject("PictureBox16.Image"), System.Drawing.Image)
        Me.PictureBox16.Location = New System.Drawing.Point(11, 147)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox16.TabIndex = 57
        Me.PictureBox16.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.Image = CType(resources.GetObject("PictureBox14.Image"), System.Drawing.Image)
        Me.PictureBox14.Location = New System.Drawing.Point(424, 13)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox14.TabIndex = 56
        Me.PictureBox14.TabStop = False
        '
        'btnLogsCambios
        '
        Me.btnLogsCambios.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_18
        Me.btnLogsCambios.Location = New System.Drawing.Point(295, 15)
        Me.btnLogsCambios.Name = "btnLogsCambios"
        Me.btnLogsCambios.Size = New System.Drawing.Size(101, 115)
        Me.btnLogsCambios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnLogsCambios.TabIndex = 55
        Me.btnLogsCambios.TabStop = False
        '
        'btnLogsExcepciones
        '
        Me.btnLogsExcepciones.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_17
        Me.btnLogsExcepciones.Location = New System.Drawing.Point(157, 15)
        Me.btnLogsExcepciones.Name = "btnLogsExcepciones"
        Me.btnLogsExcepciones.Size = New System.Drawing.Size(101, 115)
        Me.btnLogsExcepciones.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnLogsExcepciones.TabIndex = 54
        Me.btnLogsExcepciones.TabStop = False
        '
        'btnLogsLogin
        '
        Me.btnLogsLogin.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_15
        Me.btnLogsLogin.Location = New System.Drawing.Point(11, 15)
        Me.btnLogsLogin.Name = "btnLogsLogin"
        Me.btnLogsLogin.Size = New System.Drawing.Size(101, 115)
        Me.btnLogsLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnLogsLogin.TabIndex = 53
        Me.btnLogsLogin.TabStop = False
        '
        'TabPage19
        '
        Me.TabPage19.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage19.Controls.Add(Me.PictureBox15)
        Me.TabPage19.Controls.Add(Me.PictureBox13)
        Me.TabPage19.Location = New System.Drawing.Point(4, 22)
        Me.TabPage19.Name = "TabPage19"
        Me.TabPage19.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage19.Size = New System.Drawing.Size(578, 470)
        Me.TabPage19.TabIndex = 5
        Me.TabPage19.Text = "Herramientas"
        '
        'PictureBox15
        '
        Me.PictureBox15.Image = CType(resources.GetObject("PictureBox15.Image"), System.Drawing.Image)
        Me.PictureBox15.Location = New System.Drawing.Point(164, 23)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox15.TabIndex = 56
        Me.PictureBox15.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.Image = Global.SLM_2._2.My.Resources.Resources.rangocai
        Me.PictureBox13.Location = New System.Drawing.Point(16, 23)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(101, 115)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox13.TabIndex = 55
        Me.PictureBox13.TabStop = False
        '
        'PanelSistema
        '
        Me.PanelSistema.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelSistema.BackColor = System.Drawing.Color.White
        Me.PanelSistema.Controls.Add(Me.Panel7)
        Me.PanelSistema.Controls.Add(Me.TabControl3)
        Me.PanelSistema.Location = New System.Drawing.Point(123, 59)
        Me.PanelSistema.Margin = New System.Windows.Forms.Padding(2)
        Me.PanelSistema.Name = "PanelSistema"
        Me.PanelSistema.Size = New System.Drawing.Size(584, 547)
        Me.PanelSistema.TabIndex = 23
        Me.PanelSistema.Visible = False
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel7.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel7.Controls.Add(Me.PictureBox5)
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(584, 37)
        Me.Panel7.TabIndex = 11
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox5.Location = New System.Drawing.Point(551, 8)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 9
        Me.PictureBox5.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(216, 25)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Módulo De Sistema"
        '
        'PanelAlmacen
        '
        Me.PanelAlmacen.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelAlmacen.BackColor = System.Drawing.Color.White
        Me.PanelAlmacen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PanelAlmacen.Controls.Add(Me.Panel4)
        Me.PanelAlmacen.Controls.Add(Me.TabControl5)
        Me.PanelAlmacen.Location = New System.Drawing.Point(123, 59)
        Me.PanelAlmacen.Name = "PanelAlmacen"
        Me.PanelAlmacen.Size = New System.Drawing.Size(584, 549)
        Me.PanelAlmacen.TabIndex = 24
        Me.PanelAlmacen.Visible = False
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel4.Controls.Add(Me.PictureBox10)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel4.Location = New System.Drawing.Point(1, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(584, 37)
        Me.Panel4.TabIndex = 11
        '
        'PictureBox10
        '
        Me.PictureBox10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox10.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox10.Location = New System.Drawing.Point(550, 8)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox10.TabIndex = 13
        Me.PictureBox10.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(222, 25)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Módulo De Almacén"
        '
        'TabControl5
        '
        Me.TabControl5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl5.Controls.Add(Me.TabPage10)
        Me.TabControl5.Controls.Add(Me.TabPage11)
        Me.TabControl5.Controls.Add(Me.TabPage7)
        Me.TabControl5.Controls.Add(Me.TabPage8)
        Me.TabControl5.Controls.Add(Me.TabPage12)
        Me.TabControl5.Controls.Add(Me.TabPage15)
        Me.TabControl5.Location = New System.Drawing.Point(1, 37)
        Me.TabControl5.Name = "TabControl5"
        Me.TabControl5.SelectedIndex = 0
        Me.TabControl5.Size = New System.Drawing.Size(585, 496)
        Me.TabControl5.TabIndex = 6
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage10.Controls.Add(Me.PictureBox22)
        Me.TabPage10.Controls.Add(Me.btnCuentasAlmacen)
        Me.TabPage10.Controls.Add(Me.btnTipoMovimiento)
        Me.TabPage10.Controls.Add(Me.btnEntradas)
        Me.TabPage10.Controls.Add(Me.btnFactCompra)
        Me.TabPage10.Controls.Add(Me.btnOrdeCompra)
        Me.TabPage10.Controls.Add(Me.btnSalidas)
        Me.TabPage10.Controls.Add(Me.btnAlmacenes)
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(577, 470)
        Me.TabPage10.TabIndex = 0
        Me.TabPage10.Text = "Almacén"
        '
        'PictureBox22
        '
        Me.PictureBox22.Image = CType(resources.GetObject("PictureBox22.Image"), System.Drawing.Image)
        Me.PictureBox22.Location = New System.Drawing.Point(257, 172)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(93, 102)
        Me.PictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox22.TabIndex = 47
        Me.PictureBox22.TabStop = False
        '
        'btnCuentasAlmacen
        '
        Me.btnCuentasAlmacen.Image = CType(resources.GetObject("btnCuentasAlmacen.Image"), System.Drawing.Image)
        Me.btnCuentasAlmacen.Location = New System.Drawing.Point(384, 170)
        Me.btnCuentasAlmacen.Name = "btnCuentasAlmacen"
        Me.btnCuentasAlmacen.Size = New System.Drawing.Size(93, 104)
        Me.btnCuentasAlmacen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCuentasAlmacen.TabIndex = 46
        Me.btnCuentasAlmacen.TabStop = False
        '
        'btnTipoMovimiento
        '
        Me.btnTipoMovimiento.BackColor = System.Drawing.Color.Transparent
        Me.btnTipoMovimiento.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_07
        Me.btnTipoMovimiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnTipoMovimiento.Location = New System.Drawing.Point(399, 39)
        Me.btnTipoMovimiento.Name = "btnTipoMovimiento"
        Me.btnTipoMovimiento.Size = New System.Drawing.Size(92, 104)
        Me.btnTipoMovimiento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnTipoMovimiento.TabIndex = 43
        Me.btnTipoMovimiento.TabStop = False
        '
        'btnEntradas
        '
        Me.btnEntradas.Image = Global.SLM_2._2.My.Resources.Resources.almacen_05
        Me.btnEntradas.Location = New System.Drawing.Point(8, 170)
        Me.btnEntradas.Name = "btnEntradas"
        Me.btnEntradas.Size = New System.Drawing.Size(92, 104)
        Me.btnEntradas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnEntradas.TabIndex = 35
        Me.btnEntradas.TabStop = False
        '
        'btnFactCompra
        '
        Me.btnFactCompra.Image = Global.SLM_2._2.My.Resources.Resources.almacen_041
        Me.btnFactCompra.Location = New System.Drawing.Point(257, 39)
        Me.btnFactCompra.Name = "btnFactCompra"
        Me.btnFactCompra.Size = New System.Drawing.Size(95, 104)
        Me.btnFactCompra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnFactCompra.TabIndex = 34
        Me.btnFactCompra.TabStop = False
        '
        'btnOrdeCompra
        '
        Me.btnOrdeCompra.Image = CType(resources.GetObject("btnOrdeCompra.Image"), System.Drawing.Image)
        Me.btnOrdeCompra.Location = New System.Drawing.Point(123, 39)
        Me.btnOrdeCompra.Name = "btnOrdeCompra"
        Me.btnOrdeCompra.Size = New System.Drawing.Size(92, 103)
        Me.btnOrdeCompra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnOrdeCompra.TabIndex = 33
        Me.btnOrdeCompra.TabStop = False
        '
        'btnSalidas
        '
        Me.btnSalidas.Image = CType(resources.GetObject("btnSalidas.Image"), System.Drawing.Image)
        Me.btnSalidas.Location = New System.Drawing.Point(120, 172)
        Me.btnSalidas.Name = "btnSalidas"
        Me.btnSalidas.Size = New System.Drawing.Size(93, 102)
        Me.btnSalidas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnSalidas.TabIndex = 32
        Me.btnSalidas.TabStop = False
        '
        'btnAlmacenes
        '
        Me.btnAlmacenes.Image = Global.SLM_2._2.My.Resources.Resources.almacen_01
        Me.btnAlmacenes.Location = New System.Drawing.Point(6, 39)
        Me.btnAlmacenes.Name = "btnAlmacenes"
        Me.btnAlmacenes.Size = New System.Drawing.Size(92, 102)
        Me.btnAlmacenes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnAlmacenes.TabIndex = 31
        Me.btnAlmacenes.TabStop = False
        '
        'TabPage11
        '
        Me.TabPage11.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage11.Controls.Add(Me.btnUnidadMedida)
        Me.TabPage11.Controls.Add(Me.btnCategoria)
        Me.TabPage11.Controls.Add(Me.btnProducto)
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(577, 470)
        Me.TabPage11.TabIndex = 1
        Me.TabPage11.Text = "Producto"
        '
        'btnUnidadMedida
        '
        Me.btnUnidadMedida.Image = Global.SLM_2._2.My.Resources.Resources.productos_15
        Me.btnUnidadMedida.Location = New System.Drawing.Point(298, 23)
        Me.btnUnidadMedida.Name = "btnUnidadMedida"
        Me.btnUnidadMedida.Size = New System.Drawing.Size(92, 102)
        Me.btnUnidadMedida.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnUnidadMedida.TabIndex = 34
        Me.btnUnidadMedida.TabStop = False
        '
        'btnCategoria
        '
        Me.btnCategoria.Image = Global.SLM_2._2.My.Resources.Resources.productos_14
        Me.btnCategoria.Location = New System.Drawing.Point(159, 23)
        Me.btnCategoria.Name = "btnCategoria"
        Me.btnCategoria.Size = New System.Drawing.Size(92, 102)
        Me.btnCategoria.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnCategoria.TabIndex = 33
        Me.btnCategoria.TabStop = False
        '
        'btnProducto
        '
        Me.btnProducto.Image = Global.SLM_2._2.My.Resources.Resources.productos_13
        Me.btnProducto.Location = New System.Drawing.Point(24, 23)
        Me.btnProducto.Name = "btnProducto"
        Me.btnProducto.Size = New System.Drawing.Size(92, 102)
        Me.btnProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnProducto.TabIndex = 32
        Me.btnProducto.TabStop = False
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage7.Controls.Add(Me.PictureBox18)
        Me.TabPage7.Controls.Add(Me.PictureBox17)
        Me.TabPage7.Controls.Add(Me.btnInventario)
        Me.TabPage7.Controls.Add(Me.PictureBox54)
        Me.TabPage7.Controls.Add(Me.btnBI)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(577, 470)
        Me.TabPage7.TabIndex = 2
        Me.TabPage7.Text = "Inventario"
        '
        'PictureBox18
        '
        Me.PictureBox18.Image = CType(resources.GetObject("PictureBox18.Image"), System.Drawing.Image)
        Me.PictureBox18.Location = New System.Drawing.Point(13, 188)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(92, 102)
        Me.PictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox18.TabIndex = 50
        Me.PictureBox18.TabStop = False
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = CType(resources.GetObject("PictureBox17.Image"), System.Drawing.Image)
        Me.PictureBox17.Location = New System.Drawing.Point(220, 56)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(92, 102)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox17.TabIndex = 49
        Me.PictureBox17.TabStop = False
        '
        'btnInventario
        '
        Me.btnInventario.Image = Global.SLM_2._2.My.Resources.Resources.inventario_16
        Me.btnInventario.Location = New System.Drawing.Point(13, 56)
        Me.btnInventario.Name = "btnInventario"
        Me.btnInventario.Size = New System.Drawing.Size(92, 102)
        Me.btnInventario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnInventario.TabIndex = 47
        Me.btnInventario.TabStop = False
        '
        'PictureBox54
        '
        Me.PictureBox54.Image = Global.SLM_2._2.My.Resources.Resources.inventario_18
        Me.PictureBox54.Location = New System.Drawing.Point(117, 56)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(92, 102)
        Me.PictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox54.TabIndex = 46
        Me.PictureBox54.TabStop = False
        '
        'btnBI
        '
        Me.btnBI.Image = Global.SLM_2._2.My.Resources.Resources.inventario_17
        Me.btnBI.Location = New System.Drawing.Point(473, 56)
        Me.btnBI.Name = "btnBI"
        Me.btnBI.Size = New System.Drawing.Size(92, 102)
        Me.btnBI.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnBI.TabIndex = 48
        Me.btnBI.TabStop = False
        Me.btnBI.Visible = False
        '
        'TabPage8
        '
        Me.TabPage8.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage8.Controls.Add(Me.PictureBox38)
        Me.TabPage8.Controls.Add(Me.PictureBox84)
        Me.TabPage8.Controls.Add(Me.btnOrdenInterna)
        Me.TabPage8.Controls.Add(Me.btnAutorizacion)
        Me.TabPage8.Controls.Add(Me.PictureBox66)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(577, 470)
        Me.TabPage8.TabIndex = 3
        Me.TabPage8.Text = "Solicitudes"
        '
        'PictureBox38
        '
        Me.PictureBox38.Image = Global.SLM_2._2.My.Resources.Resources.almacen_07
        Me.PictureBox38.Location = New System.Drawing.Point(150, 20)
        Me.PictureBox38.Name = "PictureBox38"
        Me.PictureBox38.Size = New System.Drawing.Size(93, 104)
        Me.PictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox38.TabIndex = 48
        Me.PictureBox38.TabStop = False
        '
        'PictureBox84
        '
        Me.PictureBox84.Image = Global.SLM_2._2.My.Resources.Resources.almacen_08
        Me.PictureBox84.Location = New System.Drawing.Point(17, 142)
        Me.PictureBox84.Name = "PictureBox84"
        Me.PictureBox84.Size = New System.Drawing.Size(94, 105)
        Me.PictureBox84.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox84.TabIndex = 47
        Me.PictureBox84.TabStop = False
        '
        'btnOrdenInterna
        '
        Me.btnOrdenInterna.Image = CType(resources.GetObject("btnOrdenInterna.Image"), System.Drawing.Image)
        Me.btnOrdenInterna.Location = New System.Drawing.Point(17, 20)
        Me.btnOrdenInterna.Name = "btnOrdenInterna"
        Me.btnOrdenInterna.Size = New System.Drawing.Size(93, 104)
        Me.btnOrdenInterna.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnOrdenInterna.TabIndex = 46
        Me.btnOrdenInterna.TabStop = False
        '
        'btnAutorizacion
        '
        Me.btnAutorizacion.Image = CType(resources.GetObject("btnAutorizacion.Image"), System.Drawing.Image)
        Me.btnAutorizacion.Location = New System.Drawing.Point(254, 146)
        Me.btnAutorizacion.Name = "btnAutorizacion"
        Me.btnAutorizacion.Size = New System.Drawing.Size(90, 101)
        Me.btnAutorizacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnAutorizacion.TabIndex = 0
        Me.btnAutorizacion.TabStop = False
        '
        'PictureBox66
        '
        Me.PictureBox66.Image = CType(resources.GetObject("PictureBox66.Image"), System.Drawing.Image)
        Me.PictureBox66.Location = New System.Drawing.Point(133, 144)
        Me.PictureBox66.Name = "PictureBox66"
        Me.PictureBox66.Size = New System.Drawing.Size(94, 101)
        Me.PictureBox66.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox66.TabIndex = 43
        Me.PictureBox66.TabStop = False
        '
        'TabPage12
        '
        Me.TabPage12.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage12.Controls.Add(Me.btnEvaluacionReactivo)
        Me.TabPage12.Controls.Add(Me.btnEvaluacionP)
        Me.TabPage12.Controls.Add(Me.btnEvaluacionServicio)
        Me.TabPage12.Controls.Add(Me.btnProveedores)
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(577, 470)
        Me.TabPage12.TabIndex = 4
        Me.TabPage12.Text = "Proveedores"
        '
        'btnEvaluacionReactivo
        '
        Me.btnEvaluacionReactivo.BackColor = System.Drawing.Color.Transparent
        Me.btnEvaluacionReactivo.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_16
        Me.btnEvaluacionReactivo.Location = New System.Drawing.Point(345, 14)
        Me.btnEvaluacionReactivo.Name = "btnEvaluacionReactivo"
        Me.btnEvaluacionReactivo.Size = New System.Drawing.Size(91, 102)
        Me.btnEvaluacionReactivo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnEvaluacionReactivo.TabIndex = 46
        Me.btnEvaluacionReactivo.TabStop = False
        '
        'btnEvaluacionP
        '
        Me.btnEvaluacionP.BackColor = System.Drawing.Color.Transparent
        Me.btnEvaluacionP.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_11
        Me.btnEvaluacionP.Location = New System.Drawing.Point(227, 14)
        Me.btnEvaluacionP.Name = "btnEvaluacionP"
        Me.btnEvaluacionP.Size = New System.Drawing.Size(91, 102)
        Me.btnEvaluacionP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnEvaluacionP.TabIndex = 42
        Me.btnEvaluacionP.TabStop = False
        '
        'btnEvaluacionServicio
        '
        Me.btnEvaluacionServicio.BackColor = System.Drawing.Color.Transparent
        Me.btnEvaluacionServicio.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_10
        Me.btnEvaluacionServicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnEvaluacionServicio.Location = New System.Drawing.Point(123, 13)
        Me.btnEvaluacionServicio.Name = "btnEvaluacionServicio"
        Me.btnEvaluacionServicio.Size = New System.Drawing.Size(92, 103)
        Me.btnEvaluacionServicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnEvaluacionServicio.TabIndex = 40
        Me.btnEvaluacionServicio.TabStop = False
        '
        'btnProveedores
        '
        Me.btnProveedores.Image = Global.SLM_2._2.My.Resources.Resources.proveedores_21
        Me.btnProveedores.Location = New System.Drawing.Point(13, 14)
        Me.btnProveedores.Name = "btnProveedores"
        Me.btnProveedores.Size = New System.Drawing.Size(92, 102)
        Me.btnProveedores.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnProveedores.TabIndex = 38
        Me.btnProveedores.TabStop = False
        '
        'TabPage15
        '
        Me.TabPage15.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage15.Controls.Add(Me.Label9)
        Me.TabPage15.Controls.Add(Me.Button9)
        Me.TabPage15.Controls.Add(Me.DataGridView1)
        Me.TabPage15.Location = New System.Drawing.Point(4, 22)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(577, 470)
        Me.TabPage15.TabIndex = 5
        Me.TabPage15.Text = "Reportes"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(192, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(177, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Doble clic para cargar reporte"
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Yellow
        Me.Button9.Location = New System.Drawing.Point(6, 11)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(109, 23)
        Me.Button9.TabIndex = 1
        Me.Button9.Text = "Cargar Reportes"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'DataGridView1
        '
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.reporte})
        Me.DataGridView1.Location = New System.Drawing.Point(7, 78)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(566, 389)
        Me.DataGridView1.TabIndex = 0
        '
        'reporte
        '
        Me.reporte.DataPropertyName = "reporte"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.reporte.DefaultCellStyle = DataGridViewCellStyle3
        Me.reporte.HeaderText = "REPORTE"
        Me.reporte.Name = "reporte"
        '
        'lblMiUser
        '
        Me.lblMiUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMiUser.AutoSize = True
        Me.lblMiUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.lblMiUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMiUser.ForeColor = System.Drawing.Color.White
        Me.lblMiUser.Location = New System.Drawing.Point(582, 605)
        Me.lblMiUser.Name = "lblMiUser"
        Me.lblMiUser.Size = New System.Drawing.Size(45, 13)
        Me.lblMiUser.TabIndex = 26
        Me.lblMiUser.Text = "Label3"
        '
        'Panel9
        '
        Me.Panel9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel9.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel9.Controls.Add(Me.btnCerrarSesion)
        Me.Panel9.Controls.Add(Me.PictureBox3)
        Me.Panel9.Controls.Add(Me.Label2)
        Me.Panel9.Location = New System.Drawing.Point(0, -1)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(710, 62)
        Me.Panel9.TabIndex = 1
        '
        'btnCerrarSesion
        '
        Me.btnCerrarSesion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrarSesion.AutoSize = True
        Me.btnCerrarSesion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCerrarSesion.LinkColor = System.Drawing.Color.White
        Me.btnCerrarSesion.Location = New System.Drawing.Point(601, 13)
        Me.btnCerrarSesion.Name = "btnCerrarSesion"
        Me.btnCerrarSesion.Size = New System.Drawing.Size(103, 16)
        Me.btnCerrarSesion.TabIndex = 9
        Me.btnCerrarSesion.TabStop = True
        Me.btnCerrarSesion.Text = "Cerrar Sesión"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.logo_sistema_100
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(12, 3)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(69, 54)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 9
        Me.PictureBox3.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(261, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(169, 25)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Menu Principal"
        '
        'lblFecha
        '
        Me.lblFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblFecha.AutoSize = True
        Me.lblFecha.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblFecha.Location = New System.Drawing.Point(12, 605)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(45, 13)
        Me.lblFecha.TabIndex = 57
        Me.lblFecha.Text = "Label8"
        '
        'lblHora
        '
        Me.lblHora.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHora.AutoSize = True
        Me.lblHora.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.lblHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHora.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblHora.Location = New System.Drawing.Point(431, 605)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(155, 13)
        Me.lblHora.TabIndex = 58
        Me.lblHora.Text = "Usted esta logeado como:"
        '
        'bntCerrar
        '
        Me.bntCerrar.BackColor = System.Drawing.Color.Red
        Me.bntCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.bntCerrar.FlatAppearance.BorderSize = 0
        Me.bntCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bntCerrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bntCerrar.ForeColor = System.Drawing.Color.White
        Me.bntCerrar.Location = New System.Drawing.Point(0, 462)
        Me.bntCerrar.Name = "bntCerrar"
        Me.bntCerrar.Size = New System.Drawing.Size(122, 44)
        Me.bntCerrar.TabIndex = 61
        Me.bntCerrar.Text = "Salir"
        Me.bntCerrar.UseVisualStyleBackColor = False
        '
        'lblCajero
        '
        Me.lblCajero.AutoSize = True
        Me.lblCajero.Location = New System.Drawing.Point(57, 577)
        Me.lblCajero.Name = "lblCajero"
        Me.lblCajero.Size = New System.Drawing.Size(59, 13)
        Me.lblCajero.TabIndex = 62
        Me.lblCajero.Text = "labelCajero"
        Me.lblCajero.Visible = False
        '
        'btnConserjes
        '
        Me.btnConserjes.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnConserjes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnConserjes.FlatAppearance.BorderSize = 0
        Me.btnConserjes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConserjes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConserjes.ForeColor = System.Drawing.Color.White
        Me.btnConserjes.Location = New System.Drawing.Point(0, 362)
        Me.btnConserjes.Name = "btnConserjes"
        Me.btnConserjes.Size = New System.Drawing.Size(122, 44)
        Me.btnConserjes.TabIndex = 63
        Me.btnConserjes.Text = "Conserjería"
        Me.btnConserjes.UseVisualStyleBackColor = False
        '
        'btnTomaDeMuestra
        '
        Me.btnTomaDeMuestra.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.btnTomaDeMuestra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTomaDeMuestra.FlatAppearance.BorderSize = 0
        Me.btnTomaDeMuestra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTomaDeMuestra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTomaDeMuestra.ForeColor = System.Drawing.Color.White
        Me.btnTomaDeMuestra.Location = New System.Drawing.Point(0, 412)
        Me.btnTomaDeMuestra.Name = "btnTomaDeMuestra"
        Me.btnTomaDeMuestra.Size = New System.Drawing.Size(122, 44)
        Me.btnTomaDeMuestra.TabIndex = 64
        Me.btnTomaDeMuestra.Text = "Toma de Muestras"
        Me.btnTomaDeMuestra.UseVisualStyleBackColor = False
        '
        'PanelTM
        '
        Me.PanelTM.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelTM.BackColor = System.Drawing.Color.White
        Me.PanelTM.Controls.Add(Me.TabControl6)
        Me.PanelTM.Controls.Add(Me.Panel10)
        Me.PanelTM.Location = New System.Drawing.Point(125, 61)
        Me.PanelTM.Name = "PanelTM"
        Me.PanelTM.Size = New System.Drawing.Size(585, 541)
        Me.PanelTM.TabIndex = 61
        Me.PanelTM.Visible = False
        '
        'TabControl6
        '
        Me.TabControl6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl6.Controls.Add(Me.TabPage13)
        Me.TabControl6.Location = New System.Drawing.Point(-3, 39)
        Me.TabControl6.Name = "TabControl6"
        Me.TabControl6.SelectedIndex = 0
        Me.TabControl6.Size = New System.Drawing.Size(585, 488)
        Me.TabControl6.TabIndex = 12
        '
        'TabPage13
        '
        Me.TabPage13.BackColor = System.Drawing.Color.AliceBlue
        Me.TabPage13.Controls.Add(Me.PictureBox39)
        Me.TabPage13.Controls.Add(Me.PictureBox12)
        Me.TabPage13.Controls.Add(Me.btnImpresorasTM)
        Me.TabPage13.Controls.Add(Me.btnUsuariosTM)
        Me.TabPage13.Controls.Add(Me.btnTomaMuestra)
        Me.TabPage13.Location = New System.Drawing.Point(4, 22)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage13.Size = New System.Drawing.Size(577, 462)
        Me.TabPage13.TabIndex = 0
        Me.TabPage13.Text = "Toma de muestra"
        '
        'PictureBox39
        '
        Me.PictureBox39.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox39.Image = CType(resources.GetObject("PictureBox39.Image"), System.Drawing.Image)
        Me.PictureBox39.Location = New System.Drawing.Point(167, 145)
        Me.PictureBox39.Name = "PictureBox39"
        Me.PictureBox39.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox39.TabIndex = 4
        Me.PictureBox39.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.Image = CType(resources.GetObject("PictureBox12.Image"), System.Drawing.Image)
        Me.PictureBox12.Location = New System.Drawing.Point(19, 143)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox12.TabIndex = 3
        Me.PictureBox12.TabStop = False
        '
        'btnImpresorasTM
        '
        Me.btnImpresorasTM.BackColor = System.Drawing.Color.Transparent
        Me.btnImpresorasTM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnImpresorasTM.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_25
        Me.btnImpresorasTM.Location = New System.Drawing.Point(320, 22)
        Me.btnImpresorasTM.Name = "btnImpresorasTM"
        Me.btnImpresorasTM.Size = New System.Drawing.Size(100, 100)
        Me.btnImpresorasTM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnImpresorasTM.TabIndex = 2
        Me.btnImpresorasTM.TabStop = False
        '
        'btnUsuariosTM
        '
        Me.btnUsuariosTM.BackColor = System.Drawing.Color.Transparent
        Me.btnUsuariosTM.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_24
        Me.btnUsuariosTM.Location = New System.Drawing.Point(167, 22)
        Me.btnUsuariosTM.Name = "btnUsuariosTM"
        Me.btnUsuariosTM.Size = New System.Drawing.Size(100, 100)
        Me.btnUsuariosTM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnUsuariosTM.TabIndex = 1
        Me.btnUsuariosTM.TabStop = False
        '
        'btnTomaMuestra
        '
        Me.btnTomaMuestra.BackColor = System.Drawing.Color.Transparent
        Me.btnTomaMuestra.Image = Global.SLM_2._2.My.Resources.Resources.iconos_sistema_2_23
        Me.btnTomaMuestra.Location = New System.Drawing.Point(19, 22)
        Me.btnTomaMuestra.Name = "btnTomaMuestra"
        Me.btnTomaMuestra.Size = New System.Drawing.Size(100, 100)
        Me.btnTomaMuestra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnTomaMuestra.TabIndex = 0
        Me.btnTomaMuestra.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel10.Controls.Add(Me.PictureBox8)
        Me.Panel10.Controls.Add(Me.Label8)
        Me.Panel10.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel10.Location = New System.Drawing.Point(1, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(583, 37)
        Me.Panel10.TabIndex = 11
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.Image = Global.SLM_2._2.My.Resources.Resources.close
        Me.PictureBox8.Location = New System.Drawing.Point(548, 6)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(31, 23)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox8.TabIndex = 11
        Me.PictureBox8.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(318, 25)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Módulo De Toma De Muestra"
        '
        'pbxNoti
        '
        Me.pbxNoti.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pbxNoti.Location = New System.Drawing.Point(6, 570)
        Me.pbxNoti.Name = "pbxNoti"
        Me.pbxNoti.Size = New System.Drawing.Size(29, 27)
        Me.pbxNoti.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbxNoti.TabIndex = 7
        Me.pbxNoti.TabStop = False
        Me.pbxNoti.Visible = False
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(267, 605)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Label10"
        '
        'BackgroundWorker1
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(710, 622)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.btnTomaDeMuestra)
        Me.Controls.Add(Me.lblMiUser)
        Me.Controls.Add(Me.btnConserjes)
        Me.Controls.Add(Me.lblCajero)
        Me.Controls.Add(Me.bntCerrar)
        Me.Controls.Add(Me.lblHora)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.pbxNoti)
        Me.Controls.Add(Me.btnAlmacen)
        Me.Controls.Add(Me.btnTalentoHumano)
        Me.Controls.Add(Me.btnlaboratorio)
        Me.Controls.Add(Me.btnFacturacion)
        Me.Controls.Add(Me.btnSistema)
        Me.Controls.Add(Me.btnContabilidad)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PanelAlmacen)
        Me.Controls.Add(Me.PanelTalentoHumano)
        Me.Controls.Add(Me.panelMenu)
        Me.Controls.Add(Me.PanelSistema)
        Me.Controls.Add(Me.PanelLab)
        Me.Controls.Add(Me.PanelTM)
        Me.Controls.Add(Me.PanelFactura)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bienvenido al Sistema de Laboratorios Médicos - SLM"
        Me.panelMenu.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcMenu.ResumeLayout(False)
        Me.TpMantenimiento.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPeriodoContable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnAsientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCategoriaProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCAI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBancos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPrecios.ResumeLayout(False)
        CType(Me.pbxItemExamen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnListaPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TpProcesos.ResumeLayout(False)
        CType(Me.btnPlanilla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDepreciacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnConsolidar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpCheque.ResumeLayout(False)
        CType(Me.btnCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnChequera, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDepoBanc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpPagos.ResumeLayout(False)
        CType(Me.btnFacturaCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPagos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage16.ResumeLayout(False)
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnIngresos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.pbxTipoClasificacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxCategoriaCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage18.ResumeLayout(False)
        CType(Me.btnClienteEmpresarial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCotizacionEmpresarial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFacturaEmpresarial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage21.ResumeLayout(False)
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelLab.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        CType(Me.btnAreasLaboratorio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        CType(Me.btnSede2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        CType(Me.btnMedico2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.btnPaciente2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnReenvio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage20.ResumeLayout(False)
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelFactura.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelTalentoHumano.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl4.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        CType(Me.btnConstanciaPlantillas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnReporteria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnTipoPermiso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCapacitaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCandidatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPuestoTrabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnHorarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnContratos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnProfesion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnTipoDeducciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnVacaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPermisos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEmpleados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl3.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCorreoResultado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFirmaDigital, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFeriados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnUsuarios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnPerfiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnServidorCorreo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage14.ResumeLayout(False)
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLogsCambios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLogsExcepciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLogsLogin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage19.ResumeLayout(False)
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelSistema.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelAlmacen.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl5.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCuentasAlmacen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnTipoMovimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFactCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnOrdeCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnSalidas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnAlmacenes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage11.ResumeLayout(False)
        CType(Me.btnUnidadMedida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCategoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnInventario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBI, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage8.ResumeLayout(False)
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnOrdenInterna, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnAutorizacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage12.ResumeLayout(False)
        CType(Me.btnEvaluacionReactivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEvaluacionP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEvaluacionServicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage15.ResumeLayout(False)
        Me.TabPage15.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelTM.ResumeLayout(False)
        Me.TabControl6.ResumeLayout(False)
        Me.TabPage13.ResumeLayout(False)
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnImpresorasTM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnUsuariosTM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnTomaMuestra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxNoti, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnContabilidad As Button
    Friend WithEvents btnlaboratorio As Button
    Friend WithEvents btnFacturacion As Button
    Friend WithEvents panelMenu As Panel
    Friend WithEvents tcMenu As TabControl
    Friend WithEvents tpCheque As TabPage
    Friend WithEvents tpPagos As TabPage
    Friend WithEvents tpPrecios As TabPage
    Friend WithEvents TpMantenimiento As TabPage
    Friend WithEvents TpProcesos As TabPage
    Friend WithEvents pbxNoti As PictureBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents PanelLab As Panel
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents PictureBox23 As PictureBox
    Friend WithEvents PictureBox21 As PictureBox
    Friend WithEvents PictureBox20 As PictureBox
    Friend WithEvents btnEntrega As Button
    Friend WithEvents btnHojaTrabajo As Button
    Friend WithEvents btnTrabajo As Button
    Friend WithEvents btnExamen As Button
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents btnSucursal As PictureBox
    Friend WithEvents btnSede As Button
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents btnMedico As Button
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnPaciente As Button
    Friend WithEvents PanelFactura As Panel
    Friend WithEvents btnSistema As Button
    Friend WithEvents btnTalentoHumano As Button
    Friend WithEvents PanelTalentoHumano As Panel
    Friend WithEvents TabControl4 As TabControl
    Friend WithEvents TabPage9 As TabPage
    Friend WithEvents TabControl3 As TabControl
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents PanelSistema As Panel
    Friend WithEvents btnAlmacen As Button
    Friend WithEvents PanelAlmacen As Panel
    Friend WithEvents TabControl5 As TabControl
    Friend WithEvents TabPage10 As TabPage
    Friend WithEvents TabPage11 As TabPage
    Friend WithEvents lblMiUser As Label
    Friend WithEvents lblUserCod As Label
    Friend WithEvents PictureBox48 As PictureBox
    Friend WithEvents btnValoresRef As Button
    Friend WithEvents PictureBox49 As PictureBox
    Friend WithEvents btnInformes As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Label6 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents btnEntradas As PictureBox
    Friend WithEvents btnFactCompra As PictureBox
    Friend WithEvents btnOrdeCompra As PictureBox
    Friend WithEvents btnSalidas As PictureBox
    Friend WithEvents btnAlmacenes As PictureBox
    Friend WithEvents btnCategoria As PictureBox
    Friend WithEvents btnProducto As PictureBox
    Friend WithEvents btnUnidadMedida As PictureBox
    Friend WithEvents btnSucursales As PictureBox
    Friend WithEvents btnEmpleados As PictureBox
    Friend WithEvents btnPermisos As PictureBox
    Friend WithEvents btnVacaciones As PictureBox
    Friend WithEvents btnPrecio As PictureBox
    Friend WithEvents btnListaPrecio As PictureBox
    Friend WithEvents btnDescuento As PictureBox
    Friend WithEvents btnPromociones As PictureBox
    Friend WithEvents btnPlanilla As PictureBox
    Friend WithEvents btnDepreciacion As PictureBox
    Friend WithEvents btnConsolidar As PictureBox
    Friend WithEvents btnCuentas As PictureBox
    Friend WithEvents btnDepto As PictureBox
    Friend WithEvents btnPuestoTrabajo As PictureBox
    Friend WithEvents btnHorarios As PictureBox
    Friend WithEvents btnContratos As PictureBox
    Friend WithEvents btnProfesion As PictureBox
    Friend WithEvents btnTipoDeducciones As PictureBox
    Friend WithEvents btnArea As PictureBox
    Friend WithEvents btnPeriodoContable As PictureBox
    Friend WithEvents btnAsientos As PictureBox
    Friend WithEvents btnCategoriaProveedor As PictureBox
    Friend WithEvents btnProveedor As PictureBox
    Friend WithEvents btnFormaPago As PictureBox
    Friend WithEvents btnCAI As PictureBox
    Friend WithEvents btnBancos As PictureBox
    Friend WithEvents btnCheques As PictureBox
    Friend WithEvents btnChequera As PictureBox
    Friend WithEvents btnDepoBanc As PictureBox
    Friend WithEvents btnFacturaCompra As PictureBox
    Friend WithEvents btnPagos As PictureBox
    Friend WithEvents PictureBox31 As PictureBox
    Friend WithEvents btnUsuarios As PictureBox
    Friend WithEvents btnPerfiles As PictureBox
    Friend WithEvents btnServidorCorreo As PictureBox
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblHora As Label
    Friend WithEvents bntCerrar As Button
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents btnSolicitudes As Button
    Friend WithEvents TabPage12 As TabPage
    Friend WithEvents btnEvaluacionServicio As PictureBox
    Friend WithEvents btnProveedores As PictureBox
    Friend WithEvents PictureBox54 As PictureBox
    Friend WithEvents PictureBox19 As PictureBox
    Friend WithEvents btnSubAreas As Button
    Friend WithEvents PictureBox76 As PictureBox
    Friend WithEvents btnGrupoExamenes As Button
    Friend WithEvents btnCandidatos As PictureBox
    Friend WithEvents btnConstanciaPlantillas As PictureBox
    Friend WithEvents btnReporteria As PictureBox
    Friend WithEvents btnTipoPermiso As PictureBox
    Friend WithEvents btnCapacitaciones As PictureBox
    Friend WithEvents btnFeriados As PictureBox
    Friend WithEvents Button1 As Button
    Friend WithEvents btnTipoMovimiento As PictureBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnEvaluacionP As PictureBox
    Friend WithEvents btnEvaluacionReactivo As PictureBox
    Friend WithEvents lblCajero As Label
    Friend WithEvents btnInventario As PictureBox
    Friend WithEvents btnBI As PictureBox
    Friend WithEvents PictureBox66 As PictureBox
    Friend WithEvents btnAutorizacion As PictureBox
    Friend WithEvents btnSede2 As PictureBox
    Friend WithEvents btnMedico2 As PictureBox
    Friend WithEvents btnPaciente2 As PictureBox
    Friend WithEvents pbxItemExamen As PictureBox
    Friend WithEvents btnConserjes As Button
    Friend WithEvents btnTomaDeMuestra As Button
    Friend WithEvents btnCerrarSesion As LinkLabel
    Friend WithEvents pbxTipoClasificacion As PictureBox
    Friend WithEvents pbxCategoriaCliente As PictureBox
    Friend WithEvents PictureBox52 As PictureBox
    Friend WithEvents PictureBox51 As PictureBox
    Friend WithEvents PictureBox50 As PictureBox
    Friend WithEvents PictureBox47 As PictureBox
    Friend WithEvents PictureBox46 As PictureBox
    Friend WithEvents PictureBox34 As PictureBox
    Friend WithEvents PictureBox33 As PictureBox
    Friend WithEvents PictureBox32 As PictureBox
    Friend WithEvents PictureBox30 As PictureBox
    Friend WithEvents PictureBox29 As PictureBox
    Friend WithEvents PictureBox28 As PictureBox
    Friend WithEvents PictureBox27 As PictureBox
    Friend WithEvents PanelTM As Panel
    Friend WithEvents TabControl6 As TabControl
    Friend WithEvents TabPage13 As TabPage
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label8 As Label
    Friend WithEvents btnTomaMuestra As PictureBox
    Friend WithEvents btnUsuariosTM As PictureBox
    Friend WithEvents btnImpresorasTM As PictureBox
    Friend WithEvents TabPage14 As TabPage
    Friend WithEvents TabPage15 As TabPage
    Friend WithEvents Label9 As Label
    Friend WithEvents Button9 As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents reporte As DataGridViewTextBoxColumn
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents TabPage16 As TabPage
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents btnIngresos As PictureBox
    Friend WithEvents btnFirmaDigital As PictureBox
    Friend WithEvents btnLogsExcepciones As PictureBox
    Friend WithEvents btnLogsLogin As PictureBox
    Friend WithEvents btnLogsCambios As PictureBox
    Friend WithEvents btnCorreoResultado As PictureBox
    Friend WithEvents TabPage17 As TabPage
    Friend WithEvents btnReenvio As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents btnCuentasAlmacen As PictureBox
    Friend WithEvents btnAreasLaboratorio As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents TabPage18 As TabPage
    Friend WithEvents TabPage19 As TabPage
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents btnCotizacionEmpresarial As PictureBox
    Friend WithEvents btnFacturaEmpresarial As PictureBox
    Friend WithEvents PictureBox38 As PictureBox
    Friend WithEvents PictureBox84 As PictureBox
    Friend WithEvents btnOrdenInterna As PictureBox
    Friend WithEvents btnClienteEmpresarial As PictureBox
    Friend WithEvents Label10 As Label
    Friend WithEvents PictureBox14 As PictureBox
    Friend WithEvents PictureBox15 As PictureBox
    Friend WithEvents PictureBox16 As PictureBox
    Friend WithEvents PictureBox17 As PictureBox
    Friend WithEvents PictureBox18 As PictureBox
    Friend WithEvents PictureBox22 As PictureBox
    Friend WithEvents PictureBox24 As PictureBox
    Friend WithEvents PictureBox26 As PictureBox
    Friend WithEvents TabPage20 As TabPage
    Friend WithEvents PictureBox36 As PictureBox
    Friend WithEvents PictureBox35 As PictureBox
    Friend WithEvents PictureBox37 As PictureBox
    Friend WithEvents PictureBox25 As PictureBox
    Friend WithEvents PictureBox39 As PictureBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents PictureBox40 As PictureBox
    Friend WithEvents PictureBox43 As PictureBox
    Friend WithEvents PictureBox42 As PictureBox
    Friend WithEvents PictureBox41 As PictureBox
    Friend WithEvents TabPage21 As TabPage
    Friend WithEvents PictureBox44 As PictureBox
End Class
