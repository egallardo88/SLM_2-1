﻿Public Class E_frm_CuadrarCentroCosto
    Private Sub E_frm_CuadrarCentroCosto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        alternarColoFilasDatagridview(dtDetalleAsiento)
        cargarData()
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub

    Public Sub cargarData()
        Try
            'datagridview

            Dim clsP As New ClsCentroCostos


            dtDetalleAsiento.DataSource = clsP.CuadrarCentrodeCosto().DefaultView
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub dtDetalleAsiento_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetalleAsiento.CellClick
        Try
            txtNombreexamen.Text = dtDetalleAsiento.Rows(e.RowIndex).Cells(1).Value
            txtCodInterno.Text = dtDetalleAsiento.Rows(e.RowIndex).Cells(2).Value
            txt_id_centro.Text = dtDetalleAsiento.Rows(e.RowIndex).Cells(3).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        txtNombreexamen.Clear()
        txtCodInterno.Clear()
        txt_id_centro.Clear()
        txtNombreexamen.ReadOnly = False
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            Dim clsC As New clsCentroCostoCuadrar

            With clsC
                .Examen1 = txtNombreexamen.Text
                .Id_examen1 = txtCodInterno.Text
                .Id_cc1 = txt_id_centro.Text
            End With
            If clsC.RegistrarNuevoCentroCosto = "1" Then
                MsgBox(mensaje_registro)
                cargarData()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            Dim clsC As New clsCentroCostoCuadrar

            With clsC
                .Examen1 = txtNombreexamen.Text
                .Id_examen1 = txtCodInterno.Text
                .Id_cc1 = txt_id_centro.Text
            End With
            If clsC.ActualizarNuevoCentroCosto = "1" Then
                MsgBox(mensaje_actualizacion)
                cargarData()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarToolStripMenuItem.Click
        Try
            Dim clsC As New clsCentroCostoCuadrar

            With clsC

                .Id_examen1 = txtCodInterno.Text

            End With
            If clsC.EliminarNuevoCentroCosto = "1" Then
                MsgBox(mensaje_dar_baja)
                cargarData()
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class