﻿Public Class E_DetalleExamenes

    'Objeto Item
    Dim Item As New ClsItemExamen

    'Arreglo de codigos a eliminar
    Dim codigoItemDetalle As ArrayList = New ArrayList()

    Private Sub E_DetalleExamenes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Limpiar()
            Me.Close()
        End If
    End Sub

    Sub Limpiar()

        Try
            txtCodExamen.Text = ""
            txtCodInterno.Text = ""
            txtCodBreve.Text = ""
            txtDescripcion.Text = ""
            txtGrupo.Text = ""
            'txtPrecioBase.Text = ""
            'txtClasificación.Text = ""
            'txtAbreviatura.Text = ""
            'txtComentario.Text = ""
            txtDias.Text = "0"
            dtpHora.Text = "16:00:00"
            chkEstado.Checked = False
            txtCodigoSubArea.ResetText()

            chkLU.Checked = False
            chkMA.Checked = False
            chkMI.Checked = False
            chkJU.Checked = False
            chkVI.Checked = False
            chkSA.Checked = False
            chkDO.Checked = False

            dtResultados.Rows.Clear()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)



    End Sub
    Private Sub dtResultados_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtResultados.CellClick
        Try
            If e.RowIndex >= 0 Then
                If e.ColumnIndex = 4 And Me.dtResultados.Rows(e.RowIndex).Cells(0).Value() = "" Then
                    E_ListarUnidades.lblFila.Text = e.RowIndex
                    E_ListarUnidades.lblNombre.Text = Me.dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString()
                    E_ListarUnidades.lbldescripcion.Text = Me.dtResultados.Rows(e.RowIndex).Cells(2).Value().ToString()
                    E_ListarUnidades.lblbandera.Text = 1
                    E_ListarUnidades.Show()
                    E_ListarUnidades.BringToFront()
                ElseIf e.ColumnIndex = 4 And Me.dtResultados.Rows(e.RowIndex).Cells(0).Value() <> "" Then
                    E_ListarUnidades.lblFila.Text = e.RowIndex
                    E_ListarUnidades.lblNombre.Text = Me.dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString()
                    E_ListarUnidades.lbldescripcion.Text = Me.dtResultados.Rows(e.RowIndex).Cells(2).Value().ToString()
                    E_ListarUnidades.lblbandera.Text = 0
                    E_ListarUnidades.lblcodeItemExamenDet.Text = Me.dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString()
                    E_ListarUnidades.Show()
                    E_ListarUnidades.BringToFront()



                ElseIf e.ColumnIndex = 5 Then 'SELECCION PARA VALORES DE REFERENCIA

                    'Variable
                    'Dim codigo As String = dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString()
                    Dim parametro As New ClsValoresReferencia
                    Dim objDetRef As New ClsDetalleValorReferencia

                    Dim parametro2 As New ClsValoresReferenciaTXT
                    Dim objDetRef2 As New ClsDetalleValorRefTxt

                    Dim dtseleccion As New DataTable
                    Dim dtDetalle As New DataTable
                    Dim fila As DataRow
                    Dim dfila As DataRow

                    If dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString() = "" Then
                        MsgBox("Se debe guardar el registro para agregar los Valores de Referencia.")
                    Else


                        'MsgBox("1")

                        ' parametro.Descripcion_ = Integer.Parse(dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString())
                        'A_ValoresRefPar.lblCodParametro.Text = Integer.Parse(dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString())
                        'MsgBox("Codigo " + dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString())
                        dtseleccion = parametro.buscarValorReferenciaNombreParametro(dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString())
                        ' fila = dtseleccion.Rows(0)
                        A_ValoresRefPar.limpiarcampostab0()

                        If dtseleccion.Rows.Count > 0 Then

                            fila = dtseleccion.Rows(0)

                            A_ValoresRefPar.lblCodValorRef.Text = fila("cod_ValorReferencia")
                            A_ValoresRefPar.lblCodParametro.Text = fila("codParametro")
                            A_ValoresRefPar.txtParametro.Text = fila("nombre")
                            A_ValoresRefPar.lblCodUnidad.Text = fila("codigoUnidad")
                            A_ValoresRefPar.txtUnidad.Text = fila("unidad_codigo_breve")
                            A_ValoresRefPar.txtDescripcion.Text = fila("descripcion")
                            If fila("genero") = "M" Then
                                A_ValoresRefPar.cbxGenero.SelectedIndex = 1
                            Else
                                A_ValoresRefPar.cbxGenero.SelectedIndex = 2
                            End If

                            objDetRef.Cod_ValorRef = Integer.Parse(A_ValoresRefPar.lblCodValorRef.Text)
                            dtDetalle = objDetRef.listarDetallesValores

                            For index As Integer = 0 To dtDetalle.Rows.Count - 1
                                dfila = dtDetalle.Rows(index)
                                A_ValoresRefPar.dtValoresRef.Rows.Add(New String() {CStr((dfila("cod_DetalleValorRef"))), (dfila("edadde")), dfila("edadhasta"), 0, dfila("valornormal"), dfila("hasta")})
                            Next
                            A_ValoresRefPar.btnGuardar.Enabled = False
                            A_ValoresRefPar.btnModificar.Enabled = True

                            'Mostrar form y seleccionar fila
                            A_ValoresRefPar.Show()
                            A_ValoresRefPar.tab = 0

                            'Seleccionar fila en datagrid
                            If A_ValoresRefPar.lblCodValorRef.Text <> "CodValorRef" Then

                                For k = 0 To A_ValoresRefPar.dtDataValoresRef.Rows.Count - 1

                                    If A_ValoresRefPar.dtDataValoresRef.Rows(k).Cells(0).Value = A_ValoresRefPar.lblCodValorRef.Text Then
                                        A_ValoresRefPar.dtDataValoresRef.CurrentCell = A_ValoresRefPar.dtDataValoresRef.Item(0, k)
                                        A_ValoresRefPar.dtDataValoresRef.Rows(k).Selected = True
                                    End If

                                Next

                            End If
                            A_ValoresRefPar.btnGuardar.Enabled = False
                            A_ValoresRefPar.btnModificar.Enabled = True
                        Else ' VALOR REFERENCIA TEXTO


                            dtseleccion = parametro2.buscarDatoValorTXT(dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString())
                            ' fila = dtseleccion.Rows(0)
                            A_ValoresRefPar.limpiarcampostab0()

                            If dtseleccion.Rows.Count > 0 Then
                                'MsgBox(dtseleccion.Rows.Count)
                                fila = dtseleccion.Rows(0)

                                A_ValoresRefPar.TabControl1.SelectedIndex = 1

                                A_ValoresRefPar.txtCod.Text = fila("cod_ValorReferenciatxt")
                                A_ValoresRefPar.lblCodParametro.Text = fila("cod_Parametro")
                                A_ValoresRefPar.txtParametro2.Text = fila("nombre")
                                A_ValoresRefPar.lblCodUnidad.Text = fila("codigoUnidad")
                                A_ValoresRefPar.txtunidad2.Text = fila("unidad_codigo_breve")
                                A_ValoresRefPar.txtDescripcion2.Text = fila("descripcion")
                                A_ValoresRefPar.txtEdadde.Text = fila("edadDe")
                                A_ValoresRefPar.txtEdadhasta.Text = fila("edadHasta")

                                If fila("genero") = "M" Then
                                    A_ValoresRefPar.cbxGenero2.SelectedIndex = 1
                                Else
                                    A_ValoresRefPar.cbxGenero2.SelectedIndex = 2
                                End If

                                objDetRef2.Cod_TXT = Integer.Parse(A_ValoresRefPar.txtCod.Text)
                                dtDetalle = objDetRef2.listarDetallesValorestxt


                                For index As Integer = 0 To dtDetalle.Rows.Count - 1
                                    dfila = dtDetalle.Rows(index)
                                    A_ValoresRefPar.dtDetalleTexto.Rows.Add(New String() {CStr((dfila("cod_DetalleValorReftxt"))), dfila("texto")})
                                Next

                                A_ValoresRefPar.btnGuardar.Enabled = False
                                A_ValoresRefPar.btnModificar.Enabled = True

                                'Mostrar form y seleccionar fila
                                A_ValoresRefPar.Show()
                                A_ValoresRefPar.tab = 1


                                'Seleccionar fila en datagrid
                                If A_ValoresRefPar.txtCod.Text <> "" Then

                                    For k = 0 To A_ValoresRefPar.dtDataValorRefTexto.Rows.Count - 1

                                        If A_ValoresRefPar.dtDataValorRefTexto.Rows(k).Cells(0).Value = A_ValoresRefPar.txtCod.Text Then
                                            A_ValoresRefPar.dtDataValorRefTexto.CurrentCell = A_ValoresRefPar.dtDataValorRefTexto.Item(0, k)
                                            A_ValoresRefPar.dtDataValorRefTexto.Rows(k).Selected = True
                                        End If

                                    Next

                                End If

                                A_ValoresRefPar.btnGuardar.Enabled = False
                                A_ValoresRefPar.btnModificar.Enabled = True

                                ' A_ValoresRefPar.TabControl1.SelectedIndex = 1
                            Else
                                A_ValoresRefPar.lblCodParametro.Text = Integer.Parse(dtResultados.Rows(e.RowIndex).Cells(0).Value().ToString())
                                A_ValoresRefPar.txtParametro.Text = dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString()
                                A_ValoresRefPar.lblCodUnidad.Text = Integer.Parse(dtResultados.Rows(e.RowIndex).Cells(3).Value().ToString())
                                A_ValoresRefPar.txtUnidad.Text = dtResultados.Rows(e.RowIndex).Cells(4).Value().ToString()

                                A_ValoresRefPar.txtParametro2.Text = dtResultados.Rows(e.RowIndex).Cells(1).Value().ToString()
                                A_ValoresRefPar.lblCodUnidad.Text = Integer.Parse(dtResultados.Rows(e.RowIndex).Cells(3).Value().ToString())
                                A_ValoresRefPar.txtunidad2.Text = dtResultados.Rows(e.RowIndex).Cells(4).Value().ToString()

                                A_ValoresRefPar.btnGuardar.Enabled = True
                                A_ValoresRefPar.btnModificar.Enabled = False
                                A_ValoresRefPar.TabControl1.SelectedIndex = 0
                                A_ValoresRefPar.Show()

                            End If
                            'MsgBox(fila("nombre"))

                            ' A_ValoresRefPar.dtDataValoresRef.Rows(Index).Cells(1).Value.ToString.Contains(txtBuscar.Text)

                            A_ValoresRefPar.BringToFront()
                            A_ValoresRefPar.WindowState = WindowState.Normal

                            'A_ValoresRefPar.txtParametro.Text = fila("nombre")
                            'A_ValoresRefPar.lblCodUnidad.Text = fila("codigoUnidad")
                            'A_ValoresRefPar.txtUnidad.Text = fila("unidad_codigo_breve")
                        End If
                    End If

                    'TERMINA VALORES DE REFERENCIA
                ElseIf e.ColumnIndex = 6 Then

                    If dtResultados.Rows(e.RowIndex).Cells(0).Value = "" Then
                        MsgBox("Se debe guardar el examen para poder ingresar resultados por defecto.")
                    Else

                        'Buscar si existen registros
                        Dim resultado As New ClsResultadoPorDefecto
                        Dim dtResult As New DataTable
                        Dim row As DataRow

                        resultado.nombreParametro_ = dtResultados.Rows(e.RowIndex).Cells(1).Value()
                        dtResult = resultado.BuscarResultado

                        If dtResult.Rows.Count <> 0 Then

                            row = dtResult.Rows(0)

                            MM_ResultadoPorDefecto.txtCod.Text = row("codigo")
                            MM_ResultadoPorDefecto.txtParametro.Text = dtResultados.Rows(e.RowIndex).Cells(1).Value
                            MM_ResultadoPorDefecto.txtUnidad.Text = dtResultados.Rows(e.RowIndex).Cells(3).Value
                            MM_ResultadoPorDefecto.Show()
                            MM_ResultadoPorDefecto.BringToFront()
                        Else
                            MM_ResultadoPorDefecto.txtParametro.Text = dtResultados.Rows(e.RowIndex).Cells(1).Value
                            MM_ResultadoPorDefecto.txtUnidad.Text = dtResultados.Rows(e.RowIndex).Cells(3).Value
                            MM_ResultadoPorDefecto.Show()
                            MM_ResultadoPorDefecto.BringToFront()
                        End If

                    End If

                ElseIf e.ColumnIndex = 7 And Me.dtResultados.Rows(e.RowIndex).Cells(0).Value() = "" Then
                    Dim n As String = MsgBox("¿Desea eliminar el resultado del examen?", MsgBoxStyle.YesNo, "Validación")
                    If n = vbYes Then
                        dtResultados.Rows.Remove(dtResultados.Rows(e.RowIndex.ToString))
                    End If
                ElseIf e.ColumnIndex = 7 And Me.dtResultados.Rows(e.RowIndex).Cells(0).Value() <> "" Then
                    Dim n As String = MsgBox("¿Desea eliminar el resultado del examen?", MsgBoxStyle.YesNo, "Validación")
                    If n = vbYes Then
                        codigoItemDetalle.Add(Me.dtResultados.Rows(e.RowIndex).Cells(0).Value())
                        dtResultados.Rows.Remove(dtResultados.Rows(e.RowIndex.ToString))
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox("dtResultados  " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub chkEstado_CheckedChanged(sender As Object, e As EventArgs) Handles chkEstado.CheckedChanged
        If chkEstado.Checked = True Then
            lblEstado.Text = "Activo"
            lblEstado.ForeColor = Color.Green
        Else
            lblEstado.Text = "Inactivo"
            lblEstado.ForeColor = Color.Red
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtItem.CellClick
        Try
            btnCrear.Enabled = True
            btnModificar.Enabled = True
            btnGuardar.Enabled = False

            txtCodExamen.Text = dtItem.Rows(e.RowIndex).Cells(0).Value
            txtCodInterno.Text = dtItem.Rows(e.RowIndex).Cells(1).Value
            txtCodBreve.Text = dtItem.Rows(e.RowIndex).Cells(2).Value
            txtDescripcion.Text = dtItem.Rows(e.RowIndex).Cells(3).Value

            'txtGrupo.Text = dtItem.Rows(e.RowIndex).Cells(4).Value
            lblcodigoGrupo.Text = dtItem.Rows(e.RowIndex).Cells(4).Value

            'txtPrecioBase.Text = dtItem.Rows(e.RowIndex).Cells(5).Value
            'txtClasificación.Text = dtItem.Rows(e.RowIndex).Cells(6).Value
            'txtAbreviatura.Text = dtItem.Rows(e.RowIndex).Cells(7).Value
            'txtComentario.Text = dtItem.Rows(e.RowIndex).Cells(8).Value
            chkEstado.Checked = dtItem.Rows(e.RowIndex).Cells(9).Value

            dtpHora.Text = dtItem.Rows(e.RowIndex).Cells(11).Value.ToString
            txtDias.Text = dtItem.Rows(e.RowIndex).Cells(12).Value

            chkLU.Checked = dtItem.Rows(e.RowIndex).Cells(13).Value
            chkMA.Checked = dtItem.Rows(e.RowIndex).Cells(14).Value
            chkMI.Checked = dtItem.Rows(e.RowIndex).Cells(15).Value
            chkJU.Checked = dtItem.Rows(e.RowIndex).Cells(16).Value
            chkVI.Checked = dtItem.Rows(e.RowIndex).Cells(17).Value
            chkSA.Checked = dtItem.Rows(e.RowIndex).Cells(18).Value
            chkDO.Checked = dtItem.Rows(e.RowIndex).Cells(19).Value


            'dias de entrega de resultado

            dtResultados.Rows.Clear()

            Dim dt As New DataTable
            Dim row As DataRow
            Dim objItemDet As New ClsItemExamenDetalle
            objItemDet.codigoItemExamen_ = dtItem.Rows(e.RowIndex).Cells(0).Value
            dt = objItemDet.BuscarItemExamenDetalle()
            For index As Integer = 0 To dt.Rows.Count - 1
                row = dt.Rows(index)
                dtResultados.Rows.Add(New String() {CStr(row("codigo")), CStr(row("nombre")), CStr(row("descripcion")), CStr(row("codigoUnidad")), CStr(row("unidad_codigo_breve"))})
            Next

            btnPreguntas.Enabled = True




            txtCodigoSubArea.Text = dtItem.Rows(e.RowIndex).Cells(20).Value
            lblcodeSubGrupo.Text = dtItem.Rows(e.RowIndex).Cells(10).Value


        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Sub

    Public Sub llenarSubAreas()
        Try
            'llenar el combobox tipo permiso
            Dim objSubAreas As New ClsSubArea
            Dim dt As New DataTable
            dt = objSubAreas.listarSubAreas()
            'cmbxTipoPermiso.DataSource = New BindingSource(dt, Nothing)
            cbxSubarea.DataSource = dt
            cbxSubarea.DisplayMember = "SubArea"
            cbxSubarea.ValueMember = "codSubArea"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Validación")
        End Try
    End Sub

    Private Sub E_DetalleExamenes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(dtItem)
        alternarColoFilasDatagridview(dtResultados)
        RegistrarVentanas(nombre_usurio, Me.Name)
        MenuStrip_slm(MenuStrip1)
        ColoresForm(Panel1, StatusStrip1)
        'TEST DE CAMBIOS PREGUNTAS
        Try
            'BackgroundWorker1.RunWorkerAsync()

            'llenar el combo box con las subareas
            llenarSubAreas()

            'Listar Item
            'BindingSource1.DataSource = Item.listarItemExamen
            'dtItem.DataSource = BindingSource1
            'BindingNavigator1.BindingSource = BindingSource1

            'OCULTAR COLUMNAS DE CONSULTA
            If dtItem.Columns.Contains("codItemExa") = True Then

                dtItem.Columns("codItemExa").Visible = False
                dtItem.Columns("codBreve").Visible = False
                dtItem.Columns("precioBase").Visible = False
                dtItem.Columns("grupo").Visible = False
                dtItem.Columns("clasificacion").Visible = False
                dtItem.Columns("abreviatura").Visible = False
                dtItem.Columns("comentario").Visible = False
                dtItem.Columns("estado").Visible = False
                dtItem.Columns("codigoSubArea").Visible = False
                dtItem.Columns("codInterno").HeaderText = "Cod. Interno"
                dtItem.Columns("descripcion").HeaderText = "Descrip. de Examen"
                dtItem.Columns("nombre").HeaderText = "Nombre de Subarea"

                dtItem.Columns("LU").Visible = False
                dtItem.Columns("MA").Visible = False
                dtItem.Columns("MI").Visible = False
                dtItem.Columns("JU").Visible = False
                dtItem.Columns("VI").Visible = False
                dtItem.Columns("SA").Visible = False
                dtItem.Columns("DO").Visible = False

            End If


            'agregar boton
            If dtResultados.Columns.Contains("btnValorRef") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dtResultados.Columns.Add(btn)
                btn.HeaderText = "V. Ref"
                btn.Text = "Valor Ref."
                btn.Name = "btnValor"
                btn.UseColumnTextForButtonValue = True
            End If

            'agregar boton resultado por defecto
            If dtResultados.Columns.Contains("btnResultDefecto") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dtResultados.Columns.Add(btn)
                btn.HeaderText = "Res. Defecto"
                btn.Text = "Resultado por defecto"
                btn.Name = "btnResult"
                btn.UseColumnTextForButtonValue = True
            End If


            If dtResultados.Columns.Contains("btnEliminar") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dtResultados.Columns.Add(btn)
                btn.HeaderText = "Eliminar"
                btn.Text = "Eliminar"
                btn.Name = "btnEliminar"
                btn.UseColumnTextForButtonValue = True
            End If

            btnCrear.Enabled = False
            btnModificar.Enabled = False

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgbtabla_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtItem.CellMouseDoubleClick
        Try
            codigoItemDetalle.Clear()

            Dim n As String = ""
            If (lblform.Text = "M_ListaPrecios") Then
                Dim temp As String = M_ListaPrecios.dgbtabla.Rows(lblFila.Text).Cells(4).Value()
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen seleccionado?", MsgBoxStyle.YesNo, "Validación")
                End If
                If n = vbYes Then
                    If (M_ListaPrecios.validarItem(dtItem.Rows(e.RowIndex).Cells(0).Value()) = 0) Then
                        If Convert.ToInt64(lblFila.Text) >= 0 And temp <> "" Then
                            M_ListaPrecios.dgbtabla.Rows.Remove(M_ListaPrecios.dgbtabla.Rows(lblFila.Text))
                        End If
                        M_ListaPrecios.dgbtabla.Rows.Insert(lblFila.Text, New String() {"", "", dtItem.Rows(e.RowIndex).Cells(0).Value(), dtItem.Rows(e.RowIndex).Cells(3).Value(), temp})
                        Me.Close()
                    Else
                        MsgBox("Ya a sido agregado anteriormente el examen o grupo de examen.")
                    End If
                End If
            ElseIf (lblform.Text = "M_Precio") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen seleccionado?", MsgBoxStyle.YesNo)
                End If
                If n = vbYes Then
                    M_Precio.txtcodigoItem.Text = txtCodExamen.Text
                    Me.Close()
                End If
            ElseIf (lblform.Text = "M_DiarioFacturacion") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen en el diario de facturación?", MsgBoxStyle.YesNo)
                End If
                If n = vbYes Then
                    'M_ListaPrecios.lblcodeT.Text = lblcode.Text
                    M_DiarioFacturacion.txtExamen.Text = txtDescripcion.Text
                    Me.Close()
                End If
            ElseIf (lblform.Text = "MM_TomaDeMuestras") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen en la toma de muestra?", MsgBoxStyle.YesNo)
                End If
                If n = vbYes Then
                    'MM_TomaDeMuestras.lblcodexamen.Text = txtCodExamen.Text
                    'MM_TomaDeMuestras.txtCodExamen.Text = txtCodInterno.Text
                    MM_TomaDeMuestras.txtExamen.Text = txtDescripcion.Text
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btnGrupoExamen_Click(sender As Object, e As EventArgs) Handles btnGrupoExamen.Click
        E_GrupoExamen.lblform.Text = "E_DetalleExamenes"
        E_GrupoExamen.Show()
    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtSubArea_CLICK(sender As Object, e As EventArgs) Handles txtCodigoSubArea.Click
        Try
            'validacion antes del llenado
            If Trim(lblcodigoGrupo.Text) <> "" And Trim(lblcodigoGrupo.Text) <> "label" Then
                E_ListarSubAreasXArea.lblcodeArea.Text = lblcodigoGrupo.Text
                E_ListarSubAreasXArea.lblform.Text = "E_DetalleExamenes"
                E_ListarSubAreasXArea.Show()
            Else
                MsgBox("Debe seleccionar el grupo de examen.", MsgBoxStyle.Information)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtGrupo_TextChanged(sender As Object, e As EventArgs) Handles txtGrupo.TextChanged
        If (Trim(txtGrupo.Text) <> "") Then
            Try
                Dim objGroup As New ClsGrupoExamen
                With objGroup
                    .codigoGrupoExamen_ = txtGrupo.Text
                End With
                Dim dt As New DataTable
                dt = objGroup.BuscarGrupoExamenCodigoBreve()
                Dim row As DataRow = dt.Rows(0)
                lblcodigoGrupo.Text = CStr(row("codigo"))
                txtGrupo.BackColor = Color.White
                If lblcodeSubGrupo.Text <> lblcodigoGrupo.Text Then
                    txtCodigoSubArea.Text = ""
                End If
            Catch ex As Exception
                lblcodigoGrupo.Text = ""
                txtGrupo.BackColor = Color.Red
            End Try
        Else
            lblcodigoGrupo.Text = ""
            txtGrupo.BackColor = Color.White
        End If
    End Sub
    Private Sub lblcodigoGrupo_TextChanged(sender As Object, e As EventArgs) Handles lblcodigoGrupo.TextChanged
        If (Trim(lblcodigoGrupo.Text) <> "") Then
            Try
                Dim objGroup As New ClsGrupoExamen
                With objGroup
                    .codigo_ = lblcodigoGrupo.Text
                End With
                Dim dt As New DataTable
                dt = objGroup.BuscarGrupoExamenCodigo()
                Dim row As DataRow = dt.Rows(0)
                txtGrupo.Text = CStr(row("codigoGrupoExamen"))
                txtGrupo.BackColor = Color.White
            Catch ex As Exception
                lblcodigoGrupo.Text = ""
                txtGrupo.BackColor = Color.Red
                'validacion
                lblcodeSubGrupo.Text = ""
                txtCodigoSubArea.Text = ""
            End Try
        Else
            lblcodigoGrupo.Text = ""
            txtGrupo.BackColor = Color.White
            'validacion
            lblcodeSubGrupo.Text = ""
            txtCodigoSubArea.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        'E_SubArea.lblform.Text = "M_BuscarCapacitaciones"
        'E_SubArea.Show()
        M_BuscarCapacitaciones.lblform.Text = "M_BuscarCapacitaciones"
        M_BuscarCapacitaciones.Show()
    End Sub


    Private Sub Button1_Click_1(sender As Object, e As EventArgs)
        E_Unidad.Show()
    End Sub

    Private Sub Button1_Click_2(sender As Object, e As EventArgs) Handles Button1.Click
        E_frmInventario.GridAExcel(dtItem)
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        Try
            Dim objItem As New ClsItemExamen
            With objItem
                .Descripcio_n = txtBuscar.Text
            End With
            BindingSource1.DataSource = objItem.BuscarItemExamenDescripcion
            dtItem.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1
            ocultarColumnasItem()
        Catch ex As Exception

        End Try

        'Dim dv As DataView = Item.listarItemExamen.DefaultView

        'dv.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", txtBuscar.Text)

        'dtItem.DataSource = dv
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Limpiar()
        btnCrear.Enabled = False
        btnModificar.Enabled = False
        btnGuardar.Enabled = True
        btnPreguntas.Enabled = False
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            'Modificacion de nuevo registro
            'If (dtResultados.Rows.Count <= 1) Then
            '    MsgBox("Debe ingresar un resultado de examen por lo menos.")
            '    Exit Sub
            'End If
            If (Trim(txtCodBreve.Text) = "") Then
                MsgBox("Debe ingresar el código breve.")
                Exit Sub
            End If
            If (Trim(txtGrupo.Text) = "") Then
                MsgBox("Debe ingresar o seleccionar el grupo de examen.")
                Exit Sub
            End If
            'If (Trim(txtCodigoSubArea.Text) = "") Then
            '    MsgBox("Debe seleccionar la subárea que pertenece el item.")
            '    Exit Sub
            'End If
            If (Trim(txtDescripcion.Text) = "") Then
                MsgBox("Debe ingresar una descripción del item facturable.")
                Exit Sub
            End If
            'If (Trim(txtPrecioBase.Text) = "") Then
            '    txtPrecioBase.Text = "0"
            '    Exit Sub
            'End If


            With Item
                .Cod_ItemExa = Integer.Parse(txtCodExamen.Text)
                .Cod_Interno = Integer.Parse(txtCodInterno.Text)
                .Cod_Breve = txtCodBreve.Text
                .Descripcio_n = txtDescripcion.Text
                .Grup_o = lblcodigoGrupo.Text
                .Precio_Base = 0.0
                .Clasificacio_n = ""
                .Abreviatur_a = ""
                .Comentari_o = ""
                .Estad_o = chkEstado.Checked
                .Dias_Entrega = Integer.Parse(txtDias.Text)
                .Hora_Max = dtpHora.Value
                If Trim(txtCodigoSubArea.Text) = "" Then
                    .codigoSubArea_ = 0
                Else
                    .codigoSubArea_ = lblcodeSubGrupo.Text

                End If
                'dias de entrega resultado
                .LUNES = chkLU.Checked
                .MARTES = chkMA.Checked
                .MIERCOLES = chkMI.Checked
                .JUEVES = chkJU.Checked
                .VIERNES = chkVI.Checked
                .SABADO = chkSA.Checked
                .DOMINGO = chkDO.Checked

                If .modificarItemExamen() = 0 Then
                    MsgBox("Error al querer actualizar el item.")
                    Exit Sub
                End If

                'BindingSource1.DataSource = .listarItemExamen()
                'dtItem.DataSource = BindingSource1
                'BindingNavigator1.BindingSource = BindingSource1
            End With
            '''''''
            Dim objItemDet As New ClsItemExamenDetalle
            For index As Integer = 0 To codigoItemDetalle.Count - 1
                'elimina
                objItemDet.codigo_ = Convert.ToInt64(codigoItemDetalle(index))
                If objItemDet.EliminarItemDetalle() <> 1 Then
                    MsgBox("Error al querer modificar el detalle del item.")
                End If
            Next
            codigoItemDetalle.Clear()
            For index As Integer = 0 To dtResultados.Rows.Count - 2
                If dtResultados.Rows(index).Cells(0).Value() = "" Then
                    'agrega
                    'MsgBox(dtResultados.Rows(index).Cells(1).Value())
                    With objItemDet
                        .codigoItemExamen_ = Convert.ToInt64(txtCodExamen.Text)
                        .Nombre_ = dtResultados.Rows(index).Cells(1).Value()
                        .Descrip = dtResultados.Rows(index).Cells(2).Value()
                        If dtResultados.Rows(index).Cells(3).Value() = "" Then
                            .codigoUnidad_ = 0
                        Else
                            .codigoUnidad_ = dtResultados.Rows(index).Cells(3).Value()
                        End If

                    End With
                    If objItemDet.RegistrarNuevoItemExamenDetalle() = 0 Then
                        MsgBox("Error al querer insertar el posible resultado.")
                    End If
                Else
                    'actualiza el detalle de los items
                    With objItemDet
                        .codigo_ = dtResultados.Rows(index).Cells(0).Value()
                        .codigoItemExamen_ = Convert.ToInt64(txtCodExamen.Text)
                        .Nombre_ = dtResultados.Rows(index).Cells(1).Value()
                        .Descrip = dtResultados.Rows(index).Cells(2).Value()
                        If dtResultados.Rows(index).Cells(3).Value() = "" Then
                            .codigoUnidad_ = 0
                        Else
                            .codigoUnidad_ = dtResultados.Rows(index).Cells(3).Value()
                        End If

                    End With
                    'MsgBox(dtResultados.Rows(index).Cells(1).Value())
                    If objItemDet.ModificarItemExamenDetalle() = 0 Then
                        MsgBox("Error al querer insertar el posible resultado.")
                    End If
                End If
            Next
            txtBuscar.Text = txtDescripcion.Text
            Limpiar()
            MessageBox.Show("El registro se ha modificado exitosamente.")

        Catch ex As Exception

            MessageBox.Show("Error al modificar." + ex.Message)


        End Try

    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            'Ingreso de nuevo registro
            'If (dtResultados.Rows.Count <= 1) Then
            '    MsgBox("Debe ingresar un resultado de examen por lo menos.")
            '    Exit Sub
            'End If
            If (Trim(txtCodBreve.Text) = "") Then
                MsgBox("Debe ingresar el código breve.")
                Exit Sub
            End If
            If (Trim(txtGrupo.Text) = "") Then
                MsgBox("Debe ingresar o seleccionar el grupo de examen.")
                Exit Sub
            End If
            'If (Trim(txtCodigoSubArea.Text) = "") Then
            '    MsgBox("Debe seleccionar la subárea que pertenece el item.")
            '    Exit Sub
            'End If
            If (Trim(txtDescripcion.Text) = "") Then
                MsgBox("Debe ingresar una descripción del item facturable.")
                Exit Sub
            End If

            If (Trim(txtCodInterno.Text) = "") Then
                MsgBox("Debe ingresar el código inteno para identificar el exámen.")
                Exit Sub
            End If

            'If (Trim(txtPrecioBase.Text) = "") Then
            '    txtPrecioBase.Text = "0"
            '    'Exit Sub
            'End If

            With Item
                .Cod_Interno = Integer.Parse(txtCodInterno.Text)
                .Cod_Breve = txtCodBreve.Text
                .Descripcio_n = txtDescripcion.Text
                .Grup_o = lblcodigoGrupo.Text
                .Precio_Base = 0.0
                .Clasificacio_n = ""
                .Abreviatur_a = ""
                .Comentari_o = ""
                .Estad_o = chkEstado.Checked
                .Dias_Entrega = Integer.Parse(txtDias.Text)
                .Hora_Max = dtpHora.Value

                If Trim(lblcodeSubGrupo.Text) = "" Then
                    .codigoSubArea_ = 0
                Else
                    .codigoSubArea_ = Integer.Parse(lblcodeSubGrupo.Text)

                End If

                'dias de entrega resultado
                .LUNES = chkLU.Checked
                .MARTES = chkMA.Checked
                .MIERCOLES = chkMI.Checked
                .JUEVES = chkJU.Checked
                .VIERNES = chkVI.Checked
                .SABADO = chkSA.Checked
                .DOMINGO = chkDO.Checked

                If .registrarNuevoItemExamen() = 0 Then
                    MsgBox("Error al querer insertar el item.")
                    Exit Sub
                End If
                'BindingSource1.DataSource = .listarItemExamen
                'dtItem.DataSource = BindingSource1
                'BindingNavigator1.BindingSource = BindingSource1
            End With

            Dim dt As New DataTable
            dt = Item.CapturarItem()
            Dim row As DataRow = dt.Rows(0)

            txtCodExamen.Text = CStr(row("codItemExa"))

            Dim objItemDet As New ClsItemExamenDetalle

            'se puede guardar si esta vacio.

            For index As Integer = 0 To dtResultados.Rows.Count - 2
                With objItemDet

                    .codigoItemExamen_ = Convert.ToInt64(txtCodExamen.Text)
                    .Nombre_ = dtResultados.Rows(index).Cells(1).Value()
                    .Descrip = dtResultados.Rows(index).Cells(2).Value()
                    If dtResultados.Rows(index).Cells(3).Value() = "" Then

                        .codigoUnidad_ = 0
                    Else
                        .codigoUnidad_ = dtResultados.Rows(index).Cells(3).Value()
                    End If



                End With
                If objItemDet.RegistrarNuevoItemExamenDetalle() = 0 Then
                    MsgBox("Error al querer insertar el posible resultado.")
                    Exit Sub
                End If
            Next
            txtBuscar.Text = txtDescripcion.Text
            Limpiar()
            MessageBox.Show("El registro se ha guardado exitosamente.")

        Catch ex As Exception

            MessageBox.Show("Error al guardar." + ex.Message)

        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub btnPreguntas_Click(sender As Object, e As EventArgs) Handles btnPreguntas.Click
        Try
            MM_TomaDeMuestras.Show()
            MM_TomaDeMuestras.BringToFront()
            MM_TomaDeMuestras.WindowState = WindowState.Normal

            'MM_TomaDeMuestras.txtCodExamen.Text = txtCodInterno.Text



        Catch ex As Exception

        End Try
    End Sub


    Private Function validarParametro(ByVal nombre As String)
        Dim cont As Integer = 0
        For index As Integer = 0 To dtResultados.Rows.Count - 2
            If (dtResultados.Rows(index).Cells(1).Value().ToString = nombre) Then
                cont += 1
            End If
            If (cont >= 2) Then
                Return 1
            End If
        Next
        Return 0
    End Function

    Private Sub dtResultados_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtResultados.CellEndEdit
        Try
            If validarParametro(dtResultados.Rows(e.RowIndex).Cells(1).Value.ToString) = 1 Then
                MsgBox("El parametro ya existe en la configuracion.", MsgBoxStyle.Critical)

                If dtResultados.Rows(e.RowIndex).Cells(0).Value.ToString = "" Then
                    dtResultados.Rows.Remove(dtResultados.Rows(e.RowIndex.ToString))
                Else
                    codigoItemDetalle.Add(dtResultados.Rows(e.RowIndex).Cells(0).Value)
                    dtResultados.Rows.Remove(dtResultados.Rows(e.RowIndex.ToString))
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try

            BindingSource1.DataSource = Item.listarItemExamen

        Catch ex As Exception

        End Try
    End Sub
    Private Sub ocultarColumnasItem()
        'OCULTAR COLUMNAS DE CONSULTA
        If dtItem.Columns.Contains("codItemExa") = True Then

            dtItem.Columns("codItemExa").Visible = False
            dtItem.Columns("codBreve").Visible = False
            dtItem.Columns("precioBase").Visible = False
            dtItem.Columns("grupo").Visible = False
            dtItem.Columns("clasificacion").Visible = False
            dtItem.Columns("abreviatura").Visible = False
            dtItem.Columns("comentario").Visible = False
            dtItem.Columns("estado").Visible = False
            dtItem.Columns("codigoSubArea").Visible = False
            dtItem.Columns("codInterno").HeaderText = "Cod. Interno"
            dtItem.Columns("descripcion").HeaderText = "Descrip. de Examen"
            dtItem.Columns("nombre").HeaderText = "Nombre de Subarea"

            dtItem.Columns("LU").Visible = False
            dtItem.Columns("MA").Visible = False
            dtItem.Columns("MI").Visible = False
            dtItem.Columns("JU").Visible = False
            dtItem.Columns("VI").Visible = False
            dtItem.Columns("SA").Visible = False
            dtItem.Columns("DO").Visible = False

        End If

    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted

        BindingSource1.DataSource = Item.listarItemExamen
        dtItem.DataSource = BindingSource1
        BindingNavigator1.BindingSource = BindingSource1
        If dtItem.Columns.Contains("codItemExa") = True Then

            dtItem.Columns("codItemExa").Visible = False
            dtItem.Columns("codBreve").Visible = False
            dtItem.Columns("precioBase").Visible = False
            dtItem.Columns("grupo").Visible = False
            dtItem.Columns("clasificacion").Visible = False
            dtItem.Columns("abreviatura").Visible = False
            dtItem.Columns("comentario").Visible = False
            dtItem.Columns("estado").Visible = False
            dtItem.Columns("codigoSubArea").Visible = False
            dtItem.Columns("codInterno").HeaderText = "Cod. Interno"
            dtItem.Columns("descripcion").HeaderText = "Descrip. de Examen"
            dtItem.Columns("nombre").HeaderText = "Nombre de Subarea"

            dtItem.Columns("LU").Visible = False
            dtItem.Columns("MA").Visible = False
            dtItem.Columns("MI").Visible = False
            dtItem.Columns("JU").Visible = False
            dtItem.Columns("VI").Visible = False
            dtItem.Columns("SA").Visible = False
            dtItem.Columns("DO").Visible = False

        End If

        'agregar boton
        If dtResultados.Columns.Contains("btnValorRef") = False Then
            Dim btn As New DataGridViewButtonColumn()
            dtResultados.Columns.Add(btn)
            btn.HeaderText = "V. Ref"
            btn.Text = "Valor Ref."
            btn.Name = "btnValor"
            btn.UseColumnTextForButtonValue = True
        End If

        'agregar boton resultado por defecto
        If dtResultados.Columns.Contains("btnResultDefecto") = False Then
            Dim btn As New DataGridViewButtonColumn()
            dtResultados.Columns.Add(btn)
            btn.HeaderText = "Res. Defecto"
            btn.Text = "Resultado por defecto"
            btn.Name = "btnResult"
            btn.UseColumnTextForButtonValue = True
        End If


        If dtResultados.Columns.Contains("btnEliminar") = False Then
            Dim btn As New DataGridViewButtonColumn()
            dtResultados.Columns.Add(btn)
            btn.HeaderText = "Eliminar"
            btn.Text = "Eliminar"
            btn.Name = "btnEliminar"
            btn.UseColumnTextForButtonValue = True
        End If



        btnCrear.Enabled = False
        btnModificar.Enabled = False
    End Sub

    Private Sub btnVer_Click(sender As Object, e As EventArgs) Handles btnVer.Click
        Try
            Dim objItem As New ClsItemExamen
            With objItem
                .codigoSubArea_ = Integer.Parse(cbxSubarea.SelectedValue)
            End With
            BindingSource1.DataSource = objItem.BuscarItemExamenSubarea
            dtItem.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1
            ocultarColumnasItem()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConfigurarEtiquetaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfigurarEtiquetaToolStripMenuItem.Click
        Try

            ConfigurarEtiqueta.Show()



        Catch ex As Exception

        End Try
    End Sub
End Class