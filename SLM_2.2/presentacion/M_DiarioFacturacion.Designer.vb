﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class M_DiarioFacturacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M_DiarioFacturacion))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtnumeroOficial = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cbxDomicilio = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnSede = New System.Windows.Forms.Button()
        Me.txtSede = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblsede = New System.Windows.Forms.Label()
        Me.lblcodPromo = New System.Windows.Forms.Label()
        Me.btnBuscarPromo = New System.Windows.Forms.Button()
        Me.txtPromocion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cbxOk = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnSucursal = New System.Windows.Forms.Button()
        Me.txtnombreSucursal = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblcodigoTipoClasif = New System.Windows.Forms.Label()
        Me.btnTipoClasificacion = New System.Windows.Forms.Button()
        Me.txtnombreClasificacion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnGrupoExamen = New System.Windows.Forms.Button()
        Me.btnExamen = New System.Windows.Forms.Button()
        Me.btnTerminoPago = New System.Windows.Forms.Button()
        Me.btnMedico = New System.Windows.Forms.Button()
        Me.btnUsuario = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtpFechaHasta = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpFechaDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtGrupo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtExamen = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDescripcionTermino = New System.Windows.Forms.TextBox()
        Me.txtNombreMedico = New System.Windows.Forms.TextBox()
        Me.cmbEstado = New System.Windows.Forms.ComboBox()
        Me.lblEsado = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtnumeroB = New System.Windows.Forms.TextBox()
        Me.lblnombreB = New System.Windows.Forms.Label()
        Me.lblForm = New System.Windows.Forms.Label()
        Me.txtnombreB = New System.Windows.Forms.TextBox()
        Me.lblcantidad = New System.Windows.Forms.Label()
        Me.lblnumeroB = New System.Windows.Forms.Label()
        Me.dgbtabla = New System.Windows.Forms.DataGridView()
        Me.lbltotalFacturas = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.BuscarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialCierreDeCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgbtabla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox1.Controls.Add(Me.txtnumeroOficial)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.cbxDomicilio)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.btnSede)
        Me.GroupBox1.Controls.Add(Me.txtSede)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.lblsede)
        Me.GroupBox1.Controls.Add(Me.lblcodPromo)
        Me.GroupBox1.Controls.Add(Me.btnBuscarPromo)
        Me.GroupBox1.Controls.Add(Me.txtPromocion)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cbxOk)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.btnSucursal)
        Me.GroupBox1.Controls.Add(Me.txtnombreSucursal)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.lblcodigoTipoClasif)
        Me.GroupBox1.Controls.Add(Me.btnTipoClasificacion)
        Me.GroupBox1.Controls.Add(Me.txtnombreClasificacion)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.btnGrupoExamen)
        Me.GroupBox1.Controls.Add(Me.btnExamen)
        Me.GroupBox1.Controls.Add(Me.btnTerminoPago)
        Me.GroupBox1.Controls.Add(Me.btnMedico)
        Me.GroupBox1.Controls.Add(Me.btnUsuario)
        Me.GroupBox1.Controls.Add(Me.lblTotal)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.dtpFechaHasta)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.dtpFechaDesde)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtGrupo)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtExamen)
        Me.GroupBox1.Controls.Add(Me.txtUsuario)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtDescripcionTermino)
        Me.GroupBox1.Controls.Add(Me.txtNombreMedico)
        Me.GroupBox1.Controls.Add(Me.cmbEstado)
        Me.GroupBox1.Controls.Add(Me.lblEsado)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dtpFecha)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.txtnumeroB)
        Me.GroupBox1.Controls.Add(Me.lblnombreB)
        Me.GroupBox1.Controls.Add(Me.lblForm)
        Me.GroupBox1.Controls.Add(Me.txtnombreB)
        Me.GroupBox1.Controls.Add(Me.lblcantidad)
        Me.GroupBox1.Controls.Add(Me.lblnumeroB)
        Me.GroupBox1.Controls.Add(Me.dgbtabla)
        Me.GroupBox1.Controls.Add(Me.lbltotalFacturas)
        Me.GroupBox1.Location = New System.Drawing.Point(20, 68)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(1065, 588)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Diario de Facturación"
        '
        'txtnumeroOficial
        '
        Me.txtnumeroOficial.Location = New System.Drawing.Point(465, 146)
        Me.txtnumeroOficial.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnumeroOficial.MaxLength = 20
        Me.txtnumeroOficial.Name = "txtnumeroOficial"
        Me.txtnumeroOficial.Size = New System.Drawing.Size(210, 20)
        Me.txtnumeroOficial.TabIndex = 191
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(361, 149)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 13)
        Me.Label16.TabIndex = 190
        Me.Label16.Text = "Número Oficial"
        '
        'cbxDomicilio
        '
        Me.cbxDomicilio.AutoSize = True
        Me.cbxDomicilio.Location = New System.Drawing.Point(998, 123)
        Me.cbxDomicilio.Name = "cbxDomicilio"
        Me.cbxDomicilio.Size = New System.Drawing.Size(15, 14)
        Me.cbxDomicilio.TabIndex = 189
        Me.cbxDomicilio.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(894, 123)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 188
        Me.Label15.Text = "DOMICILIO"
        '
        'btnSede
        '
        Me.btnSede.BackColor = System.Drawing.Color.Transparent
        Me.btnSede.BackgroundImage = CType(resources.GetObject("btnSede.BackgroundImage"), System.Drawing.Image)
        Me.btnSede.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSede.FlatAppearance.BorderSize = 0
        Me.btnSede.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSede.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSede.Location = New System.Drawing.Point(297, 147)
        Me.btnSede.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSede.Name = "btnSede"
        Me.btnSede.Size = New System.Drawing.Size(26, 17)
        Me.btnSede.TabIndex = 187
        Me.btnSede.Text = "..."
        Me.btnSede.UseVisualStyleBackColor = False
        '
        'txtSede
        '
        Me.txtSede.Location = New System.Drawing.Point(83, 147)
        Me.txtSede.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSede.MaxLength = 20
        Me.txtSede.Name = "txtSede"
        Me.txtSede.Size = New System.Drawing.Size(210, 20)
        Me.txtSede.TabIndex = 186
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(23, 149)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(32, 13)
        Me.Label14.TabIndex = 185
        Me.Label14.Text = "Sede"
        '
        'lblsede
        '
        Me.lblsede.AutoSize = True
        Me.lblsede.Location = New System.Drawing.Point(71, 150)
        Me.lblsede.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblsede.Name = "lblsede"
        Me.lblsede.Size = New System.Drawing.Size(0, 13)
        Me.lblsede.TabIndex = 184
        Me.lblsede.Visible = False
        '
        'lblcodPromo
        '
        Me.lblcodPromo.AutoSize = True
        Me.lblcodPromo.Location = New System.Drawing.Point(361, 133)
        Me.lblcodPromo.Name = "lblcodPromo"
        Me.lblcodPromo.Size = New System.Drawing.Size(65, 13)
        Me.lblcodPromo.TabIndex = 183
        Me.lblcodPromo.Text = "lblcodPromo"
        Me.lblcodPromo.Visible = False
        '
        'btnBuscarPromo
        '
        Me.btnBuscarPromo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscarPromo.BackColor = System.Drawing.Color.Transparent
        Me.btnBuscarPromo.BackgroundImage = CType(resources.GetObject("btnBuscarPromo.BackgroundImage"), System.Drawing.Image)
        Me.btnBuscarPromo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnBuscarPromo.FlatAppearance.BorderSize = 0
        Me.btnBuscarPromo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarPromo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarPromo.Location = New System.Drawing.Point(679, 118)
        Me.btnBuscarPromo.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscarPromo.Name = "btnBuscarPromo"
        Me.btnBuscarPromo.Size = New System.Drawing.Size(30, 17)
        Me.btnBuscarPromo.TabIndex = 182
        Me.btnBuscarPromo.Text = "..."
        Me.btnBuscarPromo.UseVisualStyleBackColor = False
        '
        'txtPromocion
        '
        Me.txtPromocion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPromocion.Location = New System.Drawing.Point(465, 118)
        Me.txtPromocion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPromocion.MaxLength = 20
        Me.txtPromocion.Name = "txtPromocion"
        Me.txtPromocion.Size = New System.Drawing.Size(210, 20)
        Me.txtPromocion.TabIndex = 181
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(361, 120)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(57, 13)
        Me.Label13.TabIndex = 180
        Me.Label13.Text = "Promocion"
        '
        'cbxOk
        '
        Me.cbxOk.AutoSize = True
        Me.cbxOk.Checked = True
        Me.cbxOk.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbxOk.Location = New System.Drawing.Point(834, 123)
        Me.cbxOk.Name = "cbxOk"
        Me.cbxOk.Size = New System.Drawing.Size(15, 14)
        Me.cbxOk.TabIndex = 179
        Me.cbxOk.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(730, 123)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(75, 13)
        Me.Label11.TabIndex = 178
        Me.Label11.Text = "OK FACTURA"
        '
        'btnSucursal
        '
        Me.btnSucursal.BackColor = System.Drawing.Color.Transparent
        Me.btnSucursal.BackgroundImage = CType(resources.GetObject("btnSucursal.BackgroundImage"), System.Drawing.Image)
        Me.btnSucursal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSucursal.FlatAppearance.BorderSize = 0
        Me.btnSucursal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSucursal.Location = New System.Drawing.Point(297, 120)
        Me.btnSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSucursal.Name = "btnSucursal"
        Me.btnSucursal.Size = New System.Drawing.Size(26, 17)
        Me.btnSucursal.TabIndex = 177
        Me.btnSucursal.Text = "..."
        Me.btnSucursal.UseVisualStyleBackColor = False
        '
        'txtnombreSucursal
        '
        Me.txtnombreSucursal.Location = New System.Drawing.Point(83, 120)
        Me.txtnombreSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreSucursal.MaxLength = 20
        Me.txtnombreSucursal.Name = "txtnombreSucursal"
        Me.txtnombreSucursal.Size = New System.Drawing.Size(210, 20)
        Me.txtnombreSucursal.TabIndex = 176
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(23, 122)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 13)
        Me.Label10.TabIndex = 175
        Me.Label10.Text = "Sucursal"
        '
        'lblcodigoTipoClasif
        '
        Me.lblcodigoTipoClasif.AutoSize = True
        Me.lblcodigoTipoClasif.Location = New System.Drawing.Point(186, 106)
        Me.lblcodigoTipoClasif.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodigoTipoClasif.Name = "lblcodigoTipoClasif"
        Me.lblcodigoTipoClasif.Size = New System.Drawing.Size(0, 13)
        Me.lblcodigoTipoClasif.TabIndex = 173
        Me.lblcodigoTipoClasif.Visible = False
        '
        'btnTipoClasificacion
        '
        Me.btnTipoClasificacion.BackColor = System.Drawing.Color.Transparent
        Me.btnTipoClasificacion.BackgroundImage = CType(resources.GetObject("btnTipoClasificacion.BackgroundImage"), System.Drawing.Image)
        Me.btnTipoClasificacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTipoClasificacion.FlatAppearance.BorderSize = 0
        Me.btnTipoClasificacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTipoClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTipoClasificacion.Location = New System.Drawing.Point(297, 92)
        Me.btnTipoClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.btnTipoClasificacion.Name = "btnTipoClasificacion"
        Me.btnTipoClasificacion.Size = New System.Drawing.Size(26, 17)
        Me.btnTipoClasificacion.TabIndex = 172
        Me.btnTipoClasificacion.Text = "..."
        Me.btnTipoClasificacion.UseVisualStyleBackColor = False
        '
        'txtnombreClasificacion
        '
        Me.txtnombreClasificacion.Location = New System.Drawing.Point(130, 92)
        Me.txtnombreClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreClasificacion.MaxLength = 20
        Me.txtnombreClasificacion.Name = "txtnombreClasificacion"
        Me.txtnombreClasificacion.ReadOnly = True
        Me.txtnombreClasificacion.Size = New System.Drawing.Size(163, 20)
        Me.txtnombreClasificacion.TabIndex = 171
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(23, 94)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 13)
        Me.Label8.TabIndex = 170
        Me.Label8.Text = "Tipo Clasificación"
        '
        'btnGrupoExamen
        '
        Me.btnGrupoExamen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGrupoExamen.BackColor = System.Drawing.Color.Transparent
        Me.btnGrupoExamen.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnGrupoExamen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnGrupoExamen.FlatAppearance.BorderSize = 0
        Me.btnGrupoExamen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrupoExamen.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrupoExamen.Location = New System.Drawing.Point(1019, 67)
        Me.btnGrupoExamen.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGrupoExamen.Name = "btnGrupoExamen"
        Me.btnGrupoExamen.Size = New System.Drawing.Size(30, 20)
        Me.btnGrupoExamen.TabIndex = 169
        Me.btnGrupoExamen.Text = "..."
        Me.btnGrupoExamen.UseVisualStyleBackColor = False
        '
        'btnExamen
        '
        Me.btnExamen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExamen.BackColor = System.Drawing.Color.Transparent
        Me.btnExamen.BackgroundImage = CType(resources.GetObject("btnExamen.BackgroundImage"), System.Drawing.Image)
        Me.btnExamen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExamen.FlatAppearance.BorderSize = 0
        Me.btnExamen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExamen.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExamen.Location = New System.Drawing.Point(679, 67)
        Me.btnExamen.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExamen.Name = "btnExamen"
        Me.btnExamen.Size = New System.Drawing.Size(30, 17)
        Me.btnExamen.TabIndex = 168
        Me.btnExamen.Text = "..."
        Me.btnExamen.UseVisualStyleBackColor = False
        '
        'btnTerminoPago
        '
        Me.btnTerminoPago.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTerminoPago.BackColor = System.Drawing.Color.Transparent
        Me.btnTerminoPago.BackgroundImage = CType(resources.GetObject("btnTerminoPago.BackgroundImage"), System.Drawing.Image)
        Me.btnTerminoPago.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTerminoPago.FlatAppearance.BorderSize = 0
        Me.btnTerminoPago.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTerminoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTerminoPago.Location = New System.Drawing.Point(679, 42)
        Me.btnTerminoPago.Margin = New System.Windows.Forms.Padding(2)
        Me.btnTerminoPago.Name = "btnTerminoPago"
        Me.btnTerminoPago.Size = New System.Drawing.Size(30, 17)
        Me.btnTerminoPago.TabIndex = 167
        Me.btnTerminoPago.Text = "..."
        Me.btnTerminoPago.UseVisualStyleBackColor = False
        '
        'btnMedico
        '
        Me.btnMedico.BackColor = System.Drawing.Color.Transparent
        Me.btnMedico.BackgroundImage = CType(resources.GetObject("btnMedico.BackgroundImage"), System.Drawing.Image)
        Me.btnMedico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMedico.FlatAppearance.BorderSize = 0
        Me.btnMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMedico.Location = New System.Drawing.Point(297, 42)
        Me.btnMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.btnMedico.Name = "btnMedico"
        Me.btnMedico.Size = New System.Drawing.Size(26, 17)
        Me.btnMedico.TabIndex = 166
        Me.btnMedico.Text = "..."
        Me.btnMedico.UseVisualStyleBackColor = False
        '
        'btnUsuario
        '
        Me.btnUsuario.BackColor = System.Drawing.Color.Transparent
        Me.btnUsuario.BackgroundImage = CType(resources.GetObject("btnUsuario.BackgroundImage"), System.Drawing.Image)
        Me.btnUsuario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnUsuario.FlatAppearance.BorderSize = 0
        Me.btnUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUsuario.Location = New System.Drawing.Point(297, 67)
        Me.btnUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.btnUsuario.Name = "btnUsuario"
        Me.btnUsuario.Size = New System.Drawing.Size(26, 17)
        Me.btnUsuario.TabIndex = 165
        Me.btnUsuario.Text = "..."
        Me.btnUsuario.UseVisualStyleBackColor = False
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(935, 564)
        Me.lblTotal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(25, 13)
        Me.lblTotal.TabIndex = 145
        Me.lblTotal.Text = "000"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(788, 564)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 144
        Me.Label9.Text = "TOTAL "
        '
        'dtpFechaHasta
        '
        Me.dtpFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaHasta.Location = New System.Drawing.Point(806, 92)
        Me.dtpFechaHasta.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpFechaHasta.Name = "dtpFechaHasta"
        Me.dtpFechaHasta.Size = New System.Drawing.Size(210, 20)
        Me.dtpFechaHasta.TabIndex = 143
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(732, 96)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 142
        Me.Label7.Text = "Fecha Hasta"
        '
        'dtpFechaDesde
        '
        Me.dtpFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDesde.Location = New System.Drawing.Point(465, 92)
        Me.dtpFechaDesde.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpFechaDesde.Name = "dtpFechaDesde"
        Me.dtpFechaDesde.Size = New System.Drawing.Size(210, 20)
        Me.dtpFechaDesde.TabIndex = 141
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(361, 95)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 13)
        Me.Label6.TabIndex = 140
        Me.Label6.Text = "Fecha Desde"
        '
        'txtGrupo
        '
        Me.txtGrupo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGrupo.Location = New System.Drawing.Point(805, 67)
        Me.txtGrupo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtGrupo.MaxLength = 20
        Me.txtGrupo.Name = "txtGrupo"
        Me.txtGrupo.Size = New System.Drawing.Size(210, 20)
        Me.txtGrupo.TabIndex = 139
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(723, 68)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 13)
        Me.Label5.TabIndex = 138
        Me.Label5.Text = "Nombre Area"
        '
        'txtExamen
        '
        Me.txtExamen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtExamen.Location = New System.Drawing.Point(465, 67)
        Me.txtExamen.Margin = New System.Windows.Forms.Padding(2)
        Me.txtExamen.MaxLength = 20
        Me.txtExamen.Name = "txtExamen"
        Me.txtExamen.Size = New System.Drawing.Size(210, 20)
        Me.txtExamen.TabIndex = 137
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(83, 67)
        Me.txtUsuario.Margin = New System.Windows.Forms.Padding(2)
        Me.txtUsuario.MaxLength = 20
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(210, 20)
        Me.txtUsuario.TabIndex = 136
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(361, 69)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 13)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "Nombre Examen"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 69)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 134
        Me.Label4.Text = "Cajero"
        '
        'txtDescripcionTermino
        '
        Me.txtDescripcionTermino.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescripcionTermino.Location = New System.Drawing.Point(465, 42)
        Me.txtDescripcionTermino.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDescripcionTermino.MaxLength = 20
        Me.txtDescripcionTermino.Name = "txtDescripcionTermino"
        Me.txtDescripcionTermino.Size = New System.Drawing.Size(210, 20)
        Me.txtDescripcionTermino.TabIndex = 132
        '
        'txtNombreMedico
        '
        Me.txtNombreMedico.Location = New System.Drawing.Point(83, 42)
        Me.txtNombreMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNombreMedico.MaxLength = 20
        Me.txtNombreMedico.Name = "txtNombreMedico"
        Me.txtNombreMedico.Size = New System.Drawing.Size(210, 20)
        Me.txtNombreMedico.TabIndex = 131
        '
        'cmbEstado
        '
        Me.cmbEstado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbEstado.FormattingEnabled = True
        Me.cmbEstado.Items.AddRange(New Object() {"Activa", "Anulada"})
        Me.cmbEstado.Location = New System.Drawing.Point(805, 40)
        Me.cmbEstado.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbEstado.Name = "cmbEstado"
        Me.cmbEstado.Size = New System.Drawing.Size(210, 21)
        Me.cmbEstado.TabIndex = 130
        '
        'lblEsado
        '
        Me.lblEsado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEsado.AutoSize = True
        Me.lblEsado.Location = New System.Drawing.Point(759, 46)
        Me.lblEsado.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblEsado.Name = "lblEsado"
        Me.lblEsado.Size = New System.Drawing.Size(40, 13)
        Me.lblEsado.TabIndex = 129
        Me.lblEsado.Text = "Estado"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(361, 44)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 127
        Me.Label2.Text = "Término de Pago"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 44)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 125
        Me.Label1.Text = "Médico"
        '
        'dtpFecha
        '
        Me.dtpFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(805, 15)
        Me.dtpFecha.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(210, 20)
        Me.dtpFecha.TabIndex = 124
        '
        'lblFecha
        '
        Me.lblFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(758, 19)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(37, 13)
        Me.lblFecha.TabIndex = 123
        Me.lblFecha.Text = "Fecha"
        '
        'txtnumeroB
        '
        Me.txtnumeroB.Location = New System.Drawing.Point(83, 17)
        Me.txtnumeroB.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnumeroB.MaxLength = 20
        Me.txtnumeroB.Name = "txtnumeroB"
        Me.txtnumeroB.Size = New System.Drawing.Size(210, 20)
        Me.txtnumeroB.TabIndex = 122
        '
        'lblnombreB
        '
        Me.lblnombreB.AutoSize = True
        Me.lblnombreB.Location = New System.Drawing.Point(322, 19)
        Me.lblnombreB.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblnombreB.Name = "lblnombreB"
        Me.lblnombreB.Size = New System.Drawing.Size(44, 13)
        Me.lblnombreB.TabIndex = 121
        Me.lblnombreB.Text = "Nombre"
        '
        'lblForm
        '
        Me.lblForm.AutoSize = True
        Me.lblForm.Location = New System.Drawing.Point(468, 0)
        Me.lblForm.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblForm.Name = "lblForm"
        Me.lblForm.Size = New System.Drawing.Size(37, 13)
        Me.lblForm.TabIndex = 119
        Me.lblForm.Text = "lblform"
        Me.lblForm.Visible = False
        '
        'txtnombreB
        '
        Me.txtnombreB.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreB.Location = New System.Drawing.Point(380, 16)
        Me.txtnombreB.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreB.MaxLength = 80
        Me.txtnombreB.Name = "txtnombreB"
        Me.txtnombreB.Size = New System.Drawing.Size(353, 20)
        Me.txtnombreB.TabIndex = 120
        '
        'lblcantidad
        '
        Me.lblcantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblcantidad.AutoSize = True
        Me.lblcantidad.Location = New System.Drawing.Point(210, 566)
        Me.lblcantidad.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcantidad.Name = "lblcantidad"
        Me.lblcantidad.Size = New System.Drawing.Size(25, 13)
        Me.lblcantidad.TabIndex = 118
        Me.lblcantidad.Text = "000"
        '
        'lblnumeroB
        '
        Me.lblnumeroB.AutoSize = True
        Me.lblnumeroB.Location = New System.Drawing.Point(23, 19)
        Me.lblnumeroB.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblnumeroB.Name = "lblnumeroB"
        Me.lblnumeroB.Size = New System.Drawing.Size(44, 13)
        Me.lblnumeroB.TabIndex = 119
        Me.lblnumeroB.Text = "Número"
        '
        'dgbtabla
        '
        Me.dgbtabla.AllowUserToAddRows = False
        Me.dgbtabla.AllowUserToDeleteRows = False
        Me.dgbtabla.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgbtabla.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgbtabla.BackgroundColor = System.Drawing.Color.White
        Me.dgbtabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbtabla.Location = New System.Drawing.Point(16, 184)
        Me.dgbtabla.Margin = New System.Windows.Forms.Padding(2)
        Me.dgbtabla.Name = "dgbtabla"
        Me.dgbtabla.ReadOnly = True
        Me.dgbtabla.RowHeadersVisible = False
        Me.dgbtabla.RowHeadersWidth = 51
        Me.dgbtabla.RowTemplate.Height = 24
        Me.dgbtabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgbtabla.Size = New System.Drawing.Size(1033, 371)
        Me.dgbtabla.TabIndex = 116
        '
        'lbltotalFacturas
        '
        Me.lbltotalFacturas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lbltotalFacturas.AutoSize = True
        Me.lbltotalFacturas.Location = New System.Drawing.Point(13, 566)
        Me.lbltotalFacturas.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbltotalFacturas.Name = "lbltotalFacturas"
        Me.lbltotalFacturas.Size = New System.Drawing.Size(140, 13)
        Me.lbltotalFacturas.TabIndex = 117
        Me.lbltotalFacturas.Text = "CANTIDAD DE FACTURAS"
        '
        'btnBuscar
        '
        Me.btnBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscar.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.ForeColor = System.Drawing.Color.White
        Me.btnBuscar.Location = New System.Drawing.Point(999, 5)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(86, 28)
        Me.btnBuscar.TabIndex = 133
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(102, Byte), Integer))
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 658)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1103, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BuscarToolStripMenuItem, Me.GenerarExcelToolStripMenuItem, Me.HistorialCierreDeCajaToolStripMenuItem, Me.ImprimirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1103, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'BuscarToolStripMenuItem
        '
        Me.BuscarToolStripMenuItem.Name = "BuscarToolStripMenuItem"
        Me.BuscarToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.BuscarToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.BuscarToolStripMenuItem.Text = "Buscar"
        '
        'GenerarExcelToolStripMenuItem
        '
        Me.GenerarExcelToolStripMenuItem.Name = "GenerarExcelToolStripMenuItem"
        Me.GenerarExcelToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
        Me.GenerarExcelToolStripMenuItem.Text = "Generar Excel"
        '
        'HistorialCierreDeCajaToolStripMenuItem
        '
        Me.HistorialCierreDeCajaToolStripMenuItem.Name = "HistorialCierreDeCajaToolStripMenuItem"
        Me.HistorialCierreDeCajaToolStripMenuItem.Size = New System.Drawing.Size(139, 20)
        Me.HistorialCierreDeCajaToolStripMenuItem.Text = "Historial Cierre de Caja"
        '
        'ImprimirToolStripMenuItem
        '
        Me.ImprimirToolStripMenuItem.Name = "ImprimirToolStripMenuItem"
        Me.ImprimirToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.ImprimirToolStripMenuItem.Text = "Imprimir"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.fondo
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.btnBuscar)
        Me.Panel1.Location = New System.Drawing.Point(0, 23)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1103, 40)
        Me.Panel1.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(13, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(194, 25)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Diario de Factura"
        '
        'M_DiarioFacturacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1103, 680)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "M_DiarioFacturacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SLM -  Módulo Facturación"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgbtabla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblcantidad As Label
    Friend WithEvents dgbtabla As DataGridView
    Friend WithEvents lbltotalFacturas As Label
    Friend WithEvents lblForm As Label
    Friend WithEvents txtnumeroB As TextBox
    Friend WithEvents lblnombreB As Label
    Friend WithEvents txtnombreB As TextBox
    Friend WithEvents lblnumeroB As Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents lblFecha As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cmbEstado As ComboBox
    Friend WithEvents lblEsado As Label
    Friend WithEvents txtDescripcionTermino As TextBox
    Friend WithEvents txtNombreMedico As TextBox
    Friend WithEvents btnBuscar As Button
    Friend WithEvents dtpFechaHasta As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents dtpFechaDesde As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents txtGrupo As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtExamen As TextBox
    Friend WithEvents txtUsuario As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents btnExamen As Button
    Friend WithEvents btnTerminoPago As Button
    Friend WithEvents btnMedico As Button
    Friend WithEvents btnUsuario As Button
    Friend WithEvents btnGrupoExamen As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lblcodigoTipoClasif As Label
    Friend WithEvents btnTipoClasificacion As Button
    Friend WithEvents txtnombreClasificacion As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btnSucursal As Button
    Friend WithEvents txtnombreSucursal As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cbxOk As CheckBox
    Friend WithEvents Label11 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HistorialCierreDeCajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents btnBuscarPromo As Button
    Friend WithEvents txtPromocion As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents BuscarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GenerarExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblcodPromo As Label
    Friend WithEvents btnSede As Button
    Friend WithEvents txtSede As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents lblsede As Label
    Friend WithEvents txtnumeroOficial As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cbxDomicilio As CheckBox
    Friend WithEvents Label15 As Label
    Friend WithEvents ImprimirToolStripMenuItem As ToolStripMenuItem
End Class
