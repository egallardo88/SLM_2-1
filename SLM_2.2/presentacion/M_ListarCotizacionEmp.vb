﻿Public Class M_ListarCotizacionEmp

    Dim objCotEmp As New ClsCotizacionEmpresarial
    Dim objDetCotEmp As New ClsDetalleCotizacionEmpresarial
    Private Sub M_ListarCotizacionEmp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        seleccionarCotizaciones()
        MenuStrip_slm(MenuStrip1)
        txtnombreB.Text = ""
        txtnumeroB.Text = ""
        Me.dgbtabla.Columns("codigoClienteEmpresarial").Visible = False
        Me.dgbtabla.Columns("codigoTerminoPago").Visible = False
        Me.dgbtabla.Columns("codigoRecepcionista").Visible = False
        Me.dgbtabla.Columns("codigoSucursal").Visible = False
        Me.dgbtabla.Columns("codigoCajero").Visible = False

        alternarColoFilasDatagridview(dgbtabla)
    End Sub

    Private Sub seleccionarCotizaciones()
        'alternarColoFilasDatagridview(dgbtabla)
        BindingSource1.DataSource = objCotEmp.SeleccionarCotizacionEmpresarial().DefaultView.ToTable
        BindingNavigator1.BindingSource = BindingSource1
        dgbtabla.DataSource = BindingSource1
    End Sub

    Private Sub NuevaCotizacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaCotizacionToolStripMenuItem.Click
        Try
            'Me.Close()
            txtnombreB.Text = ""
            txtnumeroB.Text = ""
            'Me.Hide()
            M_FacturaEmpresarial.limpiar()
            'M_FacturaEmpresarial.banderaTipo = False

            'M_FacturaEmpresarial.lbltipo.Text = "cotizacion"
            M_FacturaEmpresarial.deshabilitar()
            M_FacturaEmpresarial.HabilitarCotizacionEmp()
            MostrarForm(M_FacturaEmpresarial)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        seleccionarCotizacionesRangoFechas()
    End Sub

    Private Sub seleccionarCotizacionesRangoFechas()
        BindingSource1.DataSource = objCotEmp.SeleccionarCotizacionRangoFecha(dtpDesde.Value.Date, dtpHasta.Value.Date).DefaultView.ToTable
        BindingNavigator1.BindingSource = BindingSource1
        dgbtabla.DataSource = BindingSource1
    End Sub

    Private Sub txtnumeroB_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtnumeroB.KeyPress
        If Not (IsNumeric(e.KeyChar)) And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtnumeroB_TextChanged(sender As Object, e As EventArgs) Handles txtnumeroB.TextChanged
        If (txtnumeroB.Text <> "") Then
            Try
                objCotEmp.numero_ = txtnumeroB.Text
                'Dim dv As DataView = objCotEmp.BuscarCotizacionEmpresarialNumero.DefaultView
                'dgbtabla.DataSource = dv
                'Solucion
                BindingSource1.DataSource = objCotEmp.BuscarCotizacionEmpresarialNumero.DefaultView.ToTable
                BindingNavigator1.BindingSource = BindingSource1
                dgbtabla.DataSource = BindingSource1
            Catch ex As Exception
                MsgBox("No existe la cotización empresarial." + ex.Message, MsgBoxStyle.Critical, "Validación")
            End Try
        Else
            seleccionarCotizacionesRangoFechas()
        End If
    End Sub

    Private Sub txtnombreB_TextChanged(sender As Object, e As EventArgs) Handles txtnombreB.TextChanged
        If (txtnombreB.Text <> "") Then
            Try
                'objCotEmp.numero_ = txtnombreB.Text
                'Dim dv As DataView = objCotEmp.BuscarCotizacionEmpresarialCliente(txtnombreB.Text).DefaultView
                'dgbtabla.DataSource = dv
                BindingSource1.DataSource = objCotEmp.BuscarCotizacionEmpresarialCliente(txtnombreB.Text).DefaultView.ToTable
                BindingNavigator1.BindingSource = BindingSource1
                dgbtabla.DataSource = BindingSource1
            Catch ex As Exception
                MsgBox("No existe la cotización empresarial.", MsgBoxStyle.Critical, "Validación")
            End Try
        Else
            seleccionarCotizacionesRangoFechas()
        End If
    End Sub

    Private Sub dgbtabla_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgbtabla.CellMouseDoubleClick
        Try
            Dim n As String = ""
            If e.RowIndex >= 0 Then
                n = MsgBox("¿Desea ver la cotización empresarial?", MsgBoxStyle.YesNo, "Validación")
            End If
            If n = vbYes Then
                M_FacturaEmpresarial.limpiar()
                M_FacturaEmpresarial.banderaTipo = False
                M_FacturaEmpresarial.lbltipo.Text = "cotizacion"
                M_FacturaEmpresarial.txtcodigoCliente.Text = dgbtabla.Rows(e.RowIndex).Cells(2).Value()

                Dim dt As New DataTable
                'Dim precio As Double = 0
                objDetCotEmp.numeroCotizacion_ = Integer.Parse(dgbtabla.Rows(e.RowIndex).Cells(0).Value())
                M_FacturaEmpresarial.txtnumeroFactura.Text = dgbtabla.Rows(e.RowIndex).Cells(0).Value()
                M_FacturaEmpresarial.dtpfechaFactura.Value = dgbtabla.Rows(e.RowIndex).Cells(3).Value()
                M_FacturaEmpresarial.txtcodigoCajero.Text = dgbtabla.Rows(e.RowIndex).Cells(8).Value()
                M_FacturaEmpresarial.txtcodigoRecepecionista.Text = dgbtabla.Rows(e.RowIndex).Cells(5).Value()
                M_FacturaEmpresarial.lblcodeSucursal.Text = dgbtabla.Rows(e.RowIndex).Cells(6).Value()
                M_FacturaEmpresarial.lblcodeTerminoPago.Text = dgbtabla.Rows(e.RowIndex).Cells(4).Value()

                dt = objDetCotEmp.BuscarDetalleCotizacionEmp()
                Dim row As DataRow
                For index As Integer = 0 To dt.Rows.Count - 1
                    row = dt.Rows(index)
                    M_FacturaEmpresarial.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("precio")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal")), CStr(row("codigoSubArea")), CStr(row("numero")), CStr(row("codigoExamen")), CStr(row("id_centrocosto"))})
                    M_FacturaEmpresarial.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), ""})
                    M_FacturaEmpresarial.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), ""})
                    M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("precio")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal"))})
                Next
                txtnombreB.Text = ""
                txtnumeroB.Text = ""
                'Me.Close()

                'M_FacturaEmpresarial.deshabilitar()
                M_FacturaEmpresarial.HabilitarCotizacionEmp()
                M_FacturaEmpresarial.btnActualizar.Enabled = True
                M_FacturaEmpresarial.btnguardar.Enabled = False

                M_FacturaEmpresarial.totalFactura()
                MostrarForm(M_FacturaEmpresarial)

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


End Class