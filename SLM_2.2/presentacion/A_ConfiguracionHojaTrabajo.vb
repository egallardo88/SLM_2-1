﻿Public Class A_ConfiguracionHojaTrabajo

    Private Sub A_ConfiguracionHojaTrabajo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            RegistrarVentanas(nombre_usurio, Me.Name)
            Dim subarea As New ClsSubArea
            Dim dt As New DataTable
            Dim row As DataRow

            txtCodBreve.Text = E_SubArea.txtSubArea.Text
            txtNombreSubarea.Text = E_SubArea.txtNombre.Text
            lblcodigoSubarea.Text = E_SubArea.txtCodigo.Text

            With subarea

                .Cod = Integer.Parse(lblcodigoSubarea.Text)

                dt = .ConsultarParametros()

                For i = 0 To dt.Rows.Count - 1

                    row = dt.Rows(i)
                    dgvDatos.Rows.Add(New String() {(row("nombre")), row("orden")})

                Next

            End With

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Guardo datos hoja de trabajo" + txtCodBreve.Text)
            Dim n As String = MsgBox("¿Desea guardar el orden de los parametros?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then

                Dim parametro As New ClsItemExamenDetalle

                'For Each fila As DataGridViewRow In dgvDatos.Rows
                For i = 0 To dgvDatos.Rows.Count - 1

                    '//Validar que no esta vacia la celda
                    If dgvDatos.Rows(i).Cells(1).Value <> "" Then
                        'Actualizar datos
                        With parametro
                            .Nombre_ = dgvDatos.Rows(i).Cells(0).Value
                            .orden_ = dgvDatos.Rows(i).Cells(1).Value
                            .ActualizarOrdenHojaTrabajo()
                        End With

                    End If

                Next
                MsgBox("Se configuro el orden de los parametros en la hoja de trabajo.")
                Me.Close()

            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message)
        End Try
    End Sub
End Class