﻿Public Class E_frmOrdenInternaAutomatica
    Dim id_entrada_almacen, id_producto_, id_banera_dgv2, id_almacen_, cantidad_actual, id_salidaautomatica As Integer
    Dim codigobarra_ As String
    Private Sub txtCodOI_TextChanged(sender As Object, e As EventArgs) Handles txtCodOI.TextChanged
        CargarDataOI(txtCodOI.Text)
    End Sub

    Private Sub CargarDataOI(ByVal cod)

        Try
            'LISTAR ORDEN INTERNAS GENERADA AUTOMATICAMENTE
            Dim clsDeOC As New clsDetalleOI
            Dim dvOC As DataView = clsDeOC.listarOrdenesInternasAutomaticasConParametro(cod).DefaultView
            DataGridView1.DataSource = dvOC
        Catch ex As Exception

        End Try




        Try
            Dim objP As New ClsSalidaAlmacen

            Dim dt As New DataTable
            dt = objP.RecuperarDatosOrdenInterna(txtCodOI.Text)
            Dim row As DataRow = dt.Rows(0)
            'txtAreaSolicitante.Text = CStr(row("nombre"))
            'txtPersonaRecibe.Text = CStr(row("nombreCompleto"))
            txtAlmacenRecibe.Text = CStr(row("usuario"))


        Catch ex As Exception


        End Try
    End Sub

    Private Sub E_frmOrdenInternaAutomatica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        alternarColoFilasDatagridview(DataGridView1)
        alternarColoFilasDatagridview(DataGridView2)
        alternarColoFilasDatagridview(DataGridView3)
        alternarColoFilasDatagridview(DataGridView5)
        txtEntrega.Text = nombre_usurio
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            Dim clsOCOB As New clsDetalleOI
            id_banera_dgv2 = DataGridView1.Rows(e.RowIndex).Cells(2).Value.ToString
            Dim dvOC As DataView = clsOCOB.listarInventarioExistencias(DataGridView1.Rows(e.RowIndex).Cells(2).Value.ToString).DefaultView
            BindingSource1.DataSource = dvOC
            DataGridView2.DataSource = BindingSource1
            BindingNavigator3.BindingSource = BindingSource1

            If DataGridView1.Rows(0).Cells(2).Value.ToString = "" Then
                MsgBox("No existe inventario para el producto solicitado")
            End If

            id_entrada_almacen = DataGridView1.Rows(e.RowIndex).Cells(13).Value
            id_almacen_ = DataGridView1.Rows(e.RowIndex).Cells(10).Value
            codigobarra_ = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            txtCantidad.Text = DataGridView1.Rows(e.RowIndex).Cells(5).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        cargarDataListadoOrdenes()
    End Sub

    Public Sub cargarDataListadoOrdenes()

        Try
            'LISTAR ORDEN INTERNAS GENERADA AUTOMATICAMENTE
            Dim clsDeOC As New clsDetalleOI
            Dim dvOC As DataView = clsDeOC.listarOrdenesInternasAutomaticasID2(DateTimePicker1.Value.Date, DateTimePicker2.Value.Date).DefaultView
            DataGridView3.DataSource = dvOC
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView4_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView4.CellClick
        Try
            txtIdOrden.Text = DataGridView4.Rows(e.RowIndex).Cells(0).Value
            txtAlmacen.Text = DataGridView4.Rows(e.RowIndex).Cells(1).Value
            txtEstado.Text = DataGridView4.Rows(e.RowIndex).Cells(2).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView3_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView3.CellClick
        Try
            'LISTAR ORDEN INTERNAS GENERADA AUTOMATICAMENTE
            Dim clsDeOC As New clsDetalleOI
            id_salidaautomatica = DataGridView3.Rows(e.RowIndex).Cells(0).Value
            Dim dvOC As DataView = clsDeOC.listarOrdenesInternasAutomaticasDEtalle(id_salidaautomatica).DefaultView
            DataGridView4.DataSource = dvOC
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            '
            id_producto_ = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            txtidDetalleEntrada.Text = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            txtProducto.Text = DataGridView2.Rows(e.RowIndex).Cells(2).Value



            cantidad_actual = DataGridView2.Rows(e.RowIndex).Cells(3).Value

            'id_almacen_ = DataGridView2.Rows(e.RowIndex).Cells(5).Value
            'existenciaentrada = DataGridView2.Rows(e.RowIndex).Cells(3).Value
            'almacen_nombres = DataGridView2.Rows(e.RowIndex).Cells(5).Value
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargardata()
        Try
            Dim clsOCOB As New clsDetalleOI

            Dim dvOC As DataView = clsOCOB.listarInventarioExistencias(id_banera_dgv2).DefaultView
            BindingSource1.DataSource = dvOC
            DataGridView2.DataSource = BindingSource1
            BindingNavigator3.BindingSource = BindingSource1


        Catch ex As Exception

        End Try
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If validarGuardar(" registrar una salida ") = "1" Then


            Try
                'TRASLADOS DEL ALMACEN CENTRAL A OTROS ALMACENES

                Dim clsI As New clsEntradaAlmacen

                With clsI
                    .Id_entrada1 = id_entrada_almacen
                    .CantidadProducto = txtCantidadEntregada.Text
                    .IdAlmacen = id_almacen_
                    .Codigobarra1 = codigobarra_
                End With

                If Integer.Parse(txtCantidadEntregada.Text) <= Integer.Parse(cantidad_actual) Then
                    If clsI.TrasladarEntradaAlmacen() = "1" Then
                        MsgBox("Tralado de inventarios completado")
                        CargarDataOI(txtCodOI.Text)
                        ' cargarData()
                        'cargarData2()
                        'txtCodProducto.Text = ""
                        txtProducto.Text = ""
                        txtCantidadEntregada.Text = ""
                        txtLote.Text = ""
                    End If
                Else
                    MsgBox("La cantidad solicitada excede la existencia")

                End If
            Catch ex As Exception
                MsgBox("Debe seleccionar un producto del almacen de origen" + ex.ToString)
            End Try
        End If
    End Sub
End Class