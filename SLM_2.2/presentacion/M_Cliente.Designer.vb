﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class M_Cliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M_Cliente))
        Me.mtxtidentidadClienteB = New System.Windows.Forms.MaskedTextBox()
        Me.btnbuscarCliente = New System.Windows.Forms.Button()
        Me.lblidCliente = New System.Windows.Forms.Label()
        Me.gbxinfoCliente = New System.Windows.Forms.GroupBox()
        Me.lblForm = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkCambio = New System.Windows.Forms.CheckBox()
        Me.txtNombreAdicional = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtRTNAdicional = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.cmbxClasificacion = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtapellido2 = New System.Windows.Forms.TextBox()
        Me.txtnombre2 = New System.Windows.Forms.TextBox()
        Me.txtapellido1 = New System.Windows.Forms.TextBox()
        Me.txtnombre1 = New System.Windows.Forms.TextBox()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.lblcodeTerminoPago = New System.Windows.Forms.Label()
        Me.lblcodeCategoria = New System.Windows.Forms.Label()
        Me.txtnombreCategoria = New System.Windows.Forms.TextBox()
        Me.txtnombreClasificacion = New System.Windows.Forms.TextBox()
        Me.txtnombreConvenio = New System.Windows.Forms.TextBox()
        Me.txtnombreTerminos = New System.Windows.Forms.TextBox()
        Me.txtnombreAseguradora = New System.Windows.Forms.TextBox()
        Me.btnclasificacion = New System.Windows.Forms.Button()
        Me.txtcodigoClasificacion = New System.Windows.Forms.TextBox()
        Me.btnterminosPago = New System.Windows.Forms.Button()
        Me.txtcodigoTermino = New System.Windows.Forms.TextBox()
        Me.btnconvenio = New System.Windows.Forms.Button()
        Me.txtconvenio = New System.Windows.Forms.TextBox()
        Me.btnaseguradora = New System.Windows.Forms.Button()
        Me.txtaseguradora = New System.Windows.Forms.TextBox()
        Me.btncategoria = New System.Windows.Forms.Button()
        Me.dtpfechaNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.gbxgenero = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.rbtnmasculino = New System.Windows.Forms.RadioButton()
        Me.rbtnfemenino = New System.Windows.Forms.RadioButton()
        Me.lblclasificacion = New System.Windows.Forms.Label()
        Me.lblconvenio = New System.Windows.Forms.Label()
        Me.lblfechaNacimiento = New System.Windows.Forms.Label()
        Me.lblterminosPago = New System.Windows.Forms.Label()
        Me.lblaseguradora = New System.Windows.Forms.Label()
        Me.txtcodigoCategoria = New System.Windows.Forms.TextBox()
        Me.lblcategoria = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.rtxtdireccion = New System.Windows.Forms.RichTextBox()
        Me.txtcorreo = New System.Windows.Forms.TextBox()
        Me.lbldireccion = New System.Windows.Forms.Label()
        Me.lblcodigo = New System.Windows.Forms.Label()
        Me.txttelefonoCasa = New System.Windows.Forms.TextBox()
        Me.txtscanId = New System.Windows.Forms.TextBox()
        Me.txtnombreCompleto = New System.Windows.Forms.TextBox()
        Me.mtxtidentidad = New System.Windows.Forms.MaskedTextBox()
        Me.lbltelefonoCasa = New System.Windows.Forms.Label()
        Me.lblcorreo = New System.Windows.Forms.Label()
        Me.lblscanId = New System.Windows.Forms.Label()
        Me.txtcorreo2 = New System.Windows.Forms.TextBox()
        Me.lblcorreo2 = New System.Windows.Forms.Label()
        Me.txtcelular = New System.Windows.Forms.TextBox()
        Me.lblnombre = New System.Windows.Forms.Label()
        Me.lblcelular = New System.Windows.Forms.Label()
        Me.lblidentidadCliente = New System.Windows.Forms.Label()
        Me.txttelefonoTrabajo = New System.Windows.Forms.TextBox()
        Me.lbltelefonoTrabajo = New System.Windows.Forms.Label()
        Me.txtrtn = New System.Windows.Forms.TextBox()
        Me.lblrtn = New System.Windows.Forms.Label()
        Me.lblapellido2 = New System.Windows.Forms.Label()
        Me.lblnombre2 = New System.Windows.Forms.Label()
        Me.lblapellido1 = New System.Windows.Forms.Label()
        Me.lblnombre1 = New System.Windows.Forms.Label()
        Me.btnseleccionarCliente = New System.Windows.Forms.Button()
        Me.btnpaciente = New System.Windows.Forms.Button()
        Me.btnbuscarPorNombre = New System.Windows.Forms.Button()
        Me.lblNombreB = New System.Windows.Forms.Label()
        Me.txtnombreB = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgbtabla = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BúsquedaAvanzadaCrtlBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarEscToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnnuevo = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnguardarCliente = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnactualizarCliente = New System.Windows.Forms.ToolStripMenuItem()
        Me.gbxinfoCliente.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbxgenero.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgbtabla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mtxtidentidadClienteB
        '
        Me.mtxtidentidadClienteB.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.mtxtidentidadClienteB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mtxtidentidadClienteB.Location = New System.Drawing.Point(83, 14)
        Me.mtxtidentidadClienteB.Margin = New System.Windows.Forms.Padding(2)
        Me.mtxtidentidadClienteB.Name = "mtxtidentidadClienteB"
        Me.mtxtidentidadClienteB.Size = New System.Drawing.Size(156, 21)
        Me.mtxtidentidadClienteB.TabIndex = 0
        Me.mtxtidentidadClienteB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnbuscarCliente
        '
        Me.btnbuscarCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarCliente.BackgroundImage = CType(resources.GetObject("btnbuscarCliente.BackgroundImage"), System.Drawing.Image)
        Me.btnbuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarCliente.FlatAppearance.BorderSize = 0
        Me.btnbuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarCliente.Location = New System.Drawing.Point(243, 15)
        Me.btnbuscarCliente.Name = "btnbuscarCliente"
        Me.btnbuscarCliente.Size = New System.Drawing.Size(22, 21)
        Me.btnbuscarCliente.TabIndex = 1
        Me.btnbuscarCliente.UseVisualStyleBackColor = True
        '
        'lblidCliente
        '
        Me.lblidCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblidCliente.AutoSize = True
        Me.lblidCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblidCliente.Location = New System.Drawing.Point(18, 18)
        Me.lblidCliente.Name = "lblidCliente"
        Me.lblidCliente.Size = New System.Drawing.Size(60, 15)
        Me.lblidCliente.TabIndex = 18
        Me.lblidCliente.Text = "ID Cliente"
        '
        'gbxinfoCliente
        '
        Me.gbxinfoCliente.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxinfoCliente.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.gbxinfoCliente.Controls.Add(Me.lblForm)
        Me.gbxinfoCliente.Controls.Add(Me.GroupBox1)
        Me.gbxinfoCliente.Controls.Add(Me.Label12)
        Me.gbxinfoCliente.Controls.Add(Me.Label62)
        Me.gbxinfoCliente.Controls.Add(Me.Label61)
        Me.gbxinfoCliente.Controls.Add(Me.Label60)
        Me.gbxinfoCliente.Controls.Add(Me.Label11)
        Me.gbxinfoCliente.Controls.Add(Me.Label10)
        Me.gbxinfoCliente.Controls.Add(Me.Label9)
        Me.gbxinfoCliente.Controls.Add(Me.Label8)
        Me.gbxinfoCliente.Controls.Add(Me.Label59)
        Me.gbxinfoCliente.Controls.Add(Me.cmbxClasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.Label7)
        Me.gbxinfoCliente.Controls.Add(Me.Label5)
        Me.gbxinfoCliente.Controls.Add(Me.Label4)
        Me.gbxinfoCliente.Controls.Add(Me.Label3)
        Me.gbxinfoCliente.Controls.Add(Me.Label2)
        Me.gbxinfoCliente.Controls.Add(Me.Label1)
        Me.gbxinfoCliente.Controls.Add(Me.txtapellido2)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombre2)
        Me.gbxinfoCliente.Controls.Add(Me.txtapellido1)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombre1)
        Me.gbxinfoCliente.Controls.Add(Me.txtEdad)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodeTerminoPago)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodeCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreClasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreConvenio)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreTerminos)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreAseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.btnclasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoClasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.btnterminosPago)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoTermino)
        Me.gbxinfoCliente.Controls.Add(Me.btnconvenio)
        Me.gbxinfoCliente.Controls.Add(Me.txtconvenio)
        Me.gbxinfoCliente.Controls.Add(Me.btnaseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.txtaseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.btncategoria)
        Me.gbxinfoCliente.Controls.Add(Me.dtpfechaNacimiento)
        Me.gbxinfoCliente.Controls.Add(Me.gbxgenero)
        Me.gbxinfoCliente.Controls.Add(Me.lblclasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.lblconvenio)
        Me.gbxinfoCliente.Controls.Add(Me.lblfechaNacimiento)
        Me.gbxinfoCliente.Controls.Add(Me.lblterminosPago)
        Me.gbxinfoCliente.Controls.Add(Me.lblaseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.lblcategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigo)
        Me.gbxinfoCliente.Controls.Add(Me.rtxtdireccion)
        Me.gbxinfoCliente.Controls.Add(Me.txtcorreo)
        Me.gbxinfoCliente.Controls.Add(Me.lbldireccion)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodigo)
        Me.gbxinfoCliente.Controls.Add(Me.txttelefonoCasa)
        Me.gbxinfoCliente.Controls.Add(Me.txtscanId)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreCompleto)
        Me.gbxinfoCliente.Controls.Add(Me.mtxtidentidad)
        Me.gbxinfoCliente.Controls.Add(Me.lbltelefonoCasa)
        Me.gbxinfoCliente.Controls.Add(Me.lblcorreo)
        Me.gbxinfoCliente.Controls.Add(Me.lblscanId)
        Me.gbxinfoCliente.Controls.Add(Me.txtcorreo2)
        Me.gbxinfoCliente.Controls.Add(Me.lblcorreo2)
        Me.gbxinfoCliente.Controls.Add(Me.txtcelular)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre)
        Me.gbxinfoCliente.Controls.Add(Me.lblcelular)
        Me.gbxinfoCliente.Controls.Add(Me.lblidentidadCliente)
        Me.gbxinfoCliente.Controls.Add(Me.txttelefonoTrabajo)
        Me.gbxinfoCliente.Controls.Add(Me.lbltelefonoTrabajo)
        Me.gbxinfoCliente.Controls.Add(Me.txtrtn)
        Me.gbxinfoCliente.Controls.Add(Me.lblrtn)
        Me.gbxinfoCliente.Controls.Add(Me.lblapellido2)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre2)
        Me.gbxinfoCliente.Controls.Add(Me.lblapellido1)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre1)
        Me.gbxinfoCliente.Location = New System.Drawing.Point(11, 220)
        Me.gbxinfoCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxinfoCliente.Name = "gbxinfoCliente"
        Me.gbxinfoCliente.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxinfoCliente.Size = New System.Drawing.Size(768, 352)
        Me.gbxinfoCliente.TabIndex = 54
        Me.gbxinfoCliente.TabStop = False
        Me.gbxinfoCliente.Text = "Información de Cliente/Paciente"
        Me.gbxinfoCliente.Visible = False
        '
        'lblForm
        '
        Me.lblForm.AutoSize = True
        Me.lblForm.Location = New System.Drawing.Point(494, 330)
        Me.lblForm.Name = "lblForm"
        Me.lblForm.Size = New System.Drawing.Size(37, 13)
        Me.lblForm.TabIndex = 216
        Me.lblForm.Text = "lblform"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.chkCambio)
        Me.GroupBox1.Controls.Add(Me.txtNombreAdicional)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtRTNAdicional)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Location = New System.Drawing.Point(462, 95)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(295, 92)
        Me.GroupBox1.TabIndex = 132
        Me.GroupBox1.TabStop = False
        '
        'chkCambio
        '
        Me.chkCambio.AutoSize = True
        Me.chkCambio.Location = New System.Drawing.Point(17, 14)
        Me.chkCambio.Margin = New System.Windows.Forms.Padding(2)
        Me.chkCambio.Name = "chkCambio"
        Me.chkCambio.Size = New System.Drawing.Size(195, 17)
        Me.chkCambio.TabIndex = 17
        Me.chkCambio.Text = "Cambiar Nombre y RTN en Factura "
        Me.chkCambio.UseVisualStyleBackColor = True
        '
        'txtNombreAdicional
        '
        Me.txtNombreAdicional.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreAdicional.Enabled = False
        Me.txtNombreAdicional.Location = New System.Drawing.Point(62, 39)
        Me.txtNombreAdicional.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNombreAdicional.MaxLength = 200
        Me.txtNombreAdicional.Name = "txtNombreAdicional"
        Me.txtNombreAdicional.Size = New System.Drawing.Size(223, 20)
        Me.txtNombreAdicional.TabIndex = 18
        Me.txtNombreAdicional.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 41)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(44, 13)
        Me.Label15.TabIndex = 218
        Me.Label15.Text = "Nombre"
        '
        'txtRTNAdicional
        '
        Me.txtRTNAdicional.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRTNAdicional.Enabled = False
        Me.txtRTNAdicional.Location = New System.Drawing.Point(62, 60)
        Me.txtRTNAdicional.Margin = New System.Windows.Forms.Padding(2)
        Me.txtRTNAdicional.MaxLength = 30
        Me.txtRTNAdicional.Name = "txtRTNAdicional"
        Me.txtRTNAdicional.Size = New System.Drawing.Size(223, 20)
        Me.txtRTNAdicional.TabIndex = 19
        Me.txtRTNAdicional.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(14, 62)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 13)
        Me.Label14.TabIndex = 216
        Me.Label14.Text = "RTN"
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(464, 271)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(17, 13)
        Me.Label12.TabIndex = 215
        Me.Label12.Text = "(*)"
        '
        'Label62
        '
        Me.Label62.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(606, 330)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(153, 13)
        Me.Label62.TabIndex = 213
        Me.Label62.Text = "Debe llenar uno de los campos"
        '
        'Label61
        '
        Me.Label61.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(606, 313)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(101, 13)
        Me.Label61.TabIndex = 212
        Me.Label61.Text = "Campos obligatorios"
        '
        'Label60
        '
        Me.Label60.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label60.AutoSize = True
        Me.Label60.ForeColor = System.Drawing.Color.Red
        Me.Label60.Location = New System.Drawing.Point(593, 315)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(11, 13)
        Me.Label60.TabIndex = 211
        Me.Label60.Text = "*"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(468, 271)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(17, 13)
        Me.Label11.TabIndex = 214
        Me.Label11.Text = "(*)"
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Red
        Me.Label10.Location = New System.Drawing.Point(516, 248)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(17, 13)
        Me.Label10.TabIndex = 213
        Me.Label10.Text = "(*)"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(468, 226)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(17, 13)
        Me.Label9.TabIndex = 212
        Me.Label9.Text = "(*)"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(480, 201)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(17, 13)
        Me.Label8.TabIndex = 211
        Me.Label8.Text = "(*)"
        '
        'Label59
        '
        Me.Label59.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label59.AutoSize = True
        Me.Label59.ForeColor = System.Drawing.Color.Red
        Me.Label59.Location = New System.Drawing.Point(590, 330)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(17, 13)
        Me.Label59.TabIndex = 210
        Me.Label59.Text = "(*)"
        '
        'cmbxClasificacion
        '
        Me.cmbxClasificacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbxClasificacion.FormattingEnabled = True
        Me.cmbxClasificacion.Location = New System.Drawing.Point(146, 311)
        Me.cmbxClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbxClasificacion.Name = "cmbxClasificacion"
        Me.cmbxClasificacion.Size = New System.Drawing.Size(290, 21)
        Me.cmbxClasificacion.TabIndex = 133
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.Red
        Me.Label7.Location = New System.Drawing.Point(255, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(11, 13)
        Me.Label7.TabIndex = 131
        Me.Label7.Text = "*"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(1, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(11, 13)
        Me.Label5.TabIndex = 130
        Me.Label5.Text = "*"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(7, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 13)
        Me.Label4.TabIndex = 129
        Me.Label4.Text = "*"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(5, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(11, 13)
        Me.Label3.TabIndex = 128
        Me.Label3.Text = "*"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(37, 317)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(11, 13)
        Me.Label2.TabIndex = 127
        Me.Label2.Text = "*"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(11, 268)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(11, 13)
        Me.Label1.TabIndex = 126
        Me.Label1.Text = "*"
        '
        'txtapellido2
        '
        Me.txtapellido2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtapellido2.Location = New System.Drawing.Point(328, 88)
        Me.txtapellido2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtapellido2.MaxLength = 20
        Me.txtapellido2.Name = "txtapellido2"
        Me.txtapellido2.Size = New System.Drawing.Size(109, 20)
        Me.txtapellido2.TabIndex = 12
        Me.txtapellido2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnombre2
        '
        Me.txtnombre2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombre2.Location = New System.Drawing.Point(327, 65)
        Me.txtnombre2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombre2.MaxLength = 20
        Me.txtnombre2.Name = "txtnombre2"
        Me.txtnombre2.Size = New System.Drawing.Size(109, 20)
        Me.txtnombre2.TabIndex = 10
        Me.txtnombre2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtapellido1
        '
        Me.txtapellido1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtapellido1.Location = New System.Drawing.Point(127, 87)
        Me.txtapellido1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtapellido1.MaxLength = 20
        Me.txtapellido1.Name = "txtapellido1"
        Me.txtapellido1.Size = New System.Drawing.Size(118, 20)
        Me.txtapellido1.TabIndex = 11
        Me.txtapellido1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnombre1
        '
        Me.txtnombre1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombre1.Location = New System.Drawing.Point(127, 65)
        Me.txtnombre1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombre1.MaxLength = 20
        Me.txtnombre1.Name = "txtnombre1"
        Me.txtnombre1.Size = New System.Drawing.Size(118, 20)
        Me.txtnombre1.TabIndex = 9
        Me.txtnombre1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtEdad
        '
        Me.txtEdad.Location = New System.Drawing.Point(252, 131)
        Me.txtEdad.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(55, 20)
        Me.txtEdad.TabIndex = 121
        '
        'lblcodeTerminoPago
        '
        Me.lblcodeTerminoPago.AutoSize = True
        Me.lblcodeTerminoPago.Location = New System.Drawing.Point(15, 298)
        Me.lblcodeTerminoPago.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeTerminoPago.Name = "lblcodeTerminoPago"
        Me.lblcodeTerminoPago.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeTerminoPago.TabIndex = 120
        Me.lblcodeTerminoPago.Visible = False
        '
        'lblcodeCategoria
        '
        Me.lblcodeCategoria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblcodeCategoria.AutoSize = True
        Me.lblcodeCategoria.Location = New System.Drawing.Point(263, 22)
        Me.lblcodeCategoria.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeCategoria.Name = "lblcodeCategoria"
        Me.lblcodeCategoria.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeCategoria.TabIndex = 119
        Me.lblcodeCategoria.Visible = False
        '
        'txtnombreCategoria
        '
        Me.txtnombreCategoria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreCategoria.Location = New System.Drawing.Point(471, 19)
        Me.txtnombreCategoria.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreCategoria.Name = "txtnombreCategoria"
        Me.txtnombreCategoria.ReadOnly = True
        Me.txtnombreCategoria.Size = New System.Drawing.Size(287, 20)
        Me.txtnombreCategoria.TabIndex = 114
        '
        'txtnombreClasificacion
        '
        Me.txtnombreClasificacion.Location = New System.Drawing.Point(237, 311)
        Me.txtnombreClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreClasificacion.Name = "txtnombreClasificacion"
        Me.txtnombreClasificacion.ReadOnly = True
        Me.txtnombreClasificacion.Size = New System.Drawing.Size(205, 20)
        Me.txtnombreClasificacion.TabIndex = 113
        Me.txtnombreClasificacion.Visible = False
        '
        'txtnombreConvenio
        '
        Me.txtnombreConvenio.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreConvenio.Location = New System.Drawing.Point(237, 288)
        Me.txtnombreConvenio.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreConvenio.Name = "txtnombreConvenio"
        Me.txtnombreConvenio.ReadOnly = True
        Me.txtnombreConvenio.Size = New System.Drawing.Size(200, 20)
        Me.txtnombreConvenio.TabIndex = 112
        '
        'txtnombreTerminos
        '
        Me.txtnombreTerminos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreTerminos.Location = New System.Drawing.Point(237, 264)
        Me.txtnombreTerminos.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreTerminos.Name = "txtnombreTerminos"
        Me.txtnombreTerminos.ReadOnly = True
        Me.txtnombreTerminos.Size = New System.Drawing.Size(200, 20)
        Me.txtnombreTerminos.TabIndex = 111
        '
        'txtnombreAseguradora
        '
        Me.txtnombreAseguradora.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreAseguradora.Location = New System.Drawing.Point(237, 241)
        Me.txtnombreAseguradora.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreAseguradora.Name = "txtnombreAseguradora"
        Me.txtnombreAseguradora.ReadOnly = True
        Me.txtnombreAseguradora.Size = New System.Drawing.Size(200, 20)
        Me.txtnombreAseguradora.TabIndex = 110
        '
        'btnclasificacion
        '
        Me.btnclasificacion.BackColor = System.Drawing.Color.Transparent
        Me.btnclasificacion.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnclasificacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnclasificacion.FlatAppearance.BorderSize = 0
        Me.btnclasificacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnclasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclasificacion.Location = New System.Drawing.Point(118, 311)
        Me.btnclasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.btnclasificacion.Name = "btnclasificacion"
        Me.btnclasificacion.Size = New System.Drawing.Size(22, 21)
        Me.btnclasificacion.TabIndex = 108
        Me.btnclasificacion.Text = "..."
        Me.btnclasificacion.UseVisualStyleBackColor = False
        '
        'txtcodigoClasificacion
        '
        Me.txtcodigoClasificacion.Location = New System.Drawing.Point(165, 311)
        Me.txtcodigoClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoClasificacion.Name = "txtcodigoClasificacion"
        Me.txtcodigoClasificacion.ReadOnly = True
        Me.txtcodigoClasificacion.Size = New System.Drawing.Size(67, 20)
        Me.txtcodigoClasificacion.TabIndex = 107
        Me.txtcodigoClasificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcodigoClasificacion.Visible = False
        '
        'btnterminosPago
        '
        Me.btnterminosPago.BackColor = System.Drawing.Color.Transparent
        Me.btnterminosPago.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnterminosPago.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnterminosPago.FlatAppearance.BorderSize = 0
        Me.btnterminosPago.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnterminosPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnterminosPago.Location = New System.Drawing.Point(118, 261)
        Me.btnterminosPago.Margin = New System.Windows.Forms.Padding(2)
        Me.btnterminosPago.Name = "btnterminosPago"
        Me.btnterminosPago.Size = New System.Drawing.Size(22, 21)
        Me.btnterminosPago.TabIndex = 106
        Me.btnterminosPago.Text = "..."
        Me.btnterminosPago.UseVisualStyleBackColor = False
        '
        'txtcodigoTermino
        '
        Me.txtcodigoTermino.Location = New System.Drawing.Point(146, 264)
        Me.txtcodigoTermino.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoTermino.Name = "txtcodigoTermino"
        Me.txtcodigoTermino.Size = New System.Drawing.Size(86, 20)
        Me.txtcodigoTermino.TabIndex = 105
        Me.txtcodigoTermino.Text = "CO"
        Me.txtcodigoTermino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnconvenio
        '
        Me.btnconvenio.BackColor = System.Drawing.Color.Transparent
        Me.btnconvenio.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnconvenio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnconvenio.Enabled = False
        Me.btnconvenio.FlatAppearance.BorderSize = 0
        Me.btnconvenio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnconvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnconvenio.Location = New System.Drawing.Point(118, 286)
        Me.btnconvenio.Margin = New System.Windows.Forms.Padding(2)
        Me.btnconvenio.Name = "btnconvenio"
        Me.btnconvenio.Size = New System.Drawing.Size(22, 21)
        Me.btnconvenio.TabIndex = 104
        Me.btnconvenio.UseVisualStyleBackColor = False
        '
        'txtconvenio
        '
        Me.txtconvenio.Location = New System.Drawing.Point(146, 288)
        Me.txtconvenio.Margin = New System.Windows.Forms.Padding(2)
        Me.txtconvenio.Name = "txtconvenio"
        Me.txtconvenio.ReadOnly = True
        Me.txtconvenio.Size = New System.Drawing.Size(86, 20)
        Me.txtconvenio.TabIndex = 103
        Me.txtconvenio.Text = "x"
        Me.txtconvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnaseguradora
        '
        Me.btnaseguradora.BackColor = System.Drawing.Color.Transparent
        Me.btnaseguradora.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnaseguradora.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnaseguradora.Enabled = False
        Me.btnaseguradora.FlatAppearance.BorderSize = 0
        Me.btnaseguradora.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnaseguradora.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnaseguradora.Location = New System.Drawing.Point(118, 238)
        Me.btnaseguradora.Margin = New System.Windows.Forms.Padding(2)
        Me.btnaseguradora.Name = "btnaseguradora"
        Me.btnaseguradora.Size = New System.Drawing.Size(22, 21)
        Me.btnaseguradora.TabIndex = 102
        Me.btnaseguradora.UseVisualStyleBackColor = False
        '
        'txtaseguradora
        '
        Me.txtaseguradora.Location = New System.Drawing.Point(146, 241)
        Me.txtaseguradora.Margin = New System.Windows.Forms.Padding(2)
        Me.txtaseguradora.Name = "txtaseguradora"
        Me.txtaseguradora.ReadOnly = True
        Me.txtaseguradora.Size = New System.Drawing.Size(86, 20)
        Me.txtaseguradora.TabIndex = 101
        Me.txtaseguradora.Text = "x"
        Me.txtaseguradora.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btncategoria
        '
        Me.btncategoria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btncategoria.BackColor = System.Drawing.Color.Transparent
        Me.btncategoria.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btncategoria.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btncategoria.FlatAppearance.BorderSize = 0
        Me.btncategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btncategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncategoria.Location = New System.Drawing.Point(441, 17)
        Me.btncategoria.Margin = New System.Windows.Forms.Padding(2)
        Me.btncategoria.Name = "btncategoria"
        Me.btncategoria.Size = New System.Drawing.Size(22, 21)
        Me.btncategoria.TabIndex = 100
        Me.btncategoria.Text = "..."
        Me.btncategoria.UseVisualStyleBackColor = False
        '
        'dtpfechaNacimiento
        '
        Me.dtpfechaNacimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpfechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpfechaNacimiento.Location = New System.Drawing.Point(126, 131)
        Me.dtpfechaNacimiento.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpfechaNacimiento.Name = "dtpfechaNacimiento"
        Me.dtpfechaNacimiento.Size = New System.Drawing.Size(119, 19)
        Me.dtpfechaNacimiento.TabIndex = 13
        '
        'gbxgenero
        '
        Me.gbxgenero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxgenero.BackColor = System.Drawing.Color.White
        Me.gbxgenero.Controls.Add(Me.Label6)
        Me.gbxgenero.Controls.Add(Me.rbtnmasculino)
        Me.gbxgenero.Controls.Add(Me.rbtnfemenino)
        Me.gbxgenero.Location = New System.Drawing.Point(462, 47)
        Me.gbxgenero.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Name = "gbxgenero"
        Me.gbxgenero.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Size = New System.Drawing.Size(295, 44)
        Me.gbxgenero.TabIndex = 98
        Me.gbxgenero.TabStop = False
        Me.gbxgenero.Text = "Género"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(8, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 13)
        Me.Label6.TabIndex = 131
        Me.Label6.Text = "*"
        '
        'rbtnmasculino
        '
        Me.rbtnmasculino.AutoSize = True
        Me.rbtnmasculino.Location = New System.Drawing.Point(31, 17)
        Me.rbtnmasculino.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnmasculino.Name = "rbtnmasculino"
        Me.rbtnmasculino.Size = New System.Drawing.Size(73, 17)
        Me.rbtnmasculino.TabIndex = 15
        Me.rbtnmasculino.TabStop = True
        Me.rbtnmasculino.Text = "Masculino"
        Me.rbtnmasculino.UseVisualStyleBackColor = True
        '
        'rbtnfemenino
        '
        Me.rbtnfemenino.AutoSize = True
        Me.rbtnfemenino.Location = New System.Drawing.Point(116, 16)
        Me.rbtnfemenino.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnfemenino.Name = "rbtnfemenino"
        Me.rbtnfemenino.Size = New System.Drawing.Size(71, 17)
        Me.rbtnfemenino.TabIndex = 16
        Me.rbtnfemenino.TabStop = True
        Me.rbtnfemenino.Text = "Femenino"
        Me.rbtnfemenino.UseVisualStyleBackColor = True
        '
        'lblclasificacion
        '
        Me.lblclasificacion.AutoSize = True
        Me.lblclasificacion.Location = New System.Drawing.Point(49, 315)
        Me.lblclasificacion.Name = "lblclasificacion"
        Me.lblclasificacion.Size = New System.Drawing.Size(66, 13)
        Me.lblclasificacion.TabIndex = 96
        Me.lblclasificacion.Text = "Clasificación"
        '
        'lblconvenio
        '
        Me.lblconvenio.AutoSize = True
        Me.lblconvenio.Location = New System.Drawing.Point(61, 294)
        Me.lblconvenio.Name = "lblconvenio"
        Me.lblconvenio.Size = New System.Drawing.Size(52, 13)
        Me.lblconvenio.TabIndex = 94
        Me.lblconvenio.Text = "Convenio"
        '
        'lblfechaNacimiento
        '
        Me.lblfechaNacimiento.AutoSize = True
        Me.lblfechaNacimiento.Location = New System.Drawing.Point(12, 132)
        Me.lblfechaNacimiento.Name = "lblfechaNacimiento"
        Me.lblfechaNacimiento.Size = New System.Drawing.Size(93, 13)
        Me.lblfechaNacimiento.TabIndex = 93
        Me.lblfechaNacimiento.Text = "Fecha Nacimiento"
        '
        'lblterminosPago
        '
        Me.lblterminosPago.AutoSize = True
        Me.lblterminosPago.Location = New System.Drawing.Point(21, 268)
        Me.lblterminosPago.Name = "lblterminosPago"
        Me.lblterminosPago.Size = New System.Drawing.Size(93, 13)
        Me.lblterminosPago.TabIndex = 91
        Me.lblterminosPago.Text = "Términos de Pago"
        '
        'lblaseguradora
        '
        Me.lblaseguradora.AutoSize = True
        Me.lblaseguradora.Location = New System.Drawing.Point(46, 247)
        Me.lblaseguradora.Name = "lblaseguradora"
        Me.lblaseguradora.Size = New System.Drawing.Size(67, 13)
        Me.lblaseguradora.TabIndex = 89
        Me.lblaseguradora.Text = "Aseguradora"
        '
        'txtcodigoCategoria
        '
        Me.txtcodigoCategoria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcodigoCategoria.Location = New System.Drawing.Point(327, 18)
        Me.txtcodigoCategoria.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoCategoria.MaxLength = 20
        Me.txtcodigoCategoria.Name = "txtcodigoCategoria"
        Me.txtcodigoCategoria.Size = New System.Drawing.Size(109, 20)
        Me.txtcodigoCategoria.TabIndex = 88
        Me.txtcodigoCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcategoria
        '
        Me.lblcategoria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblcategoria.AutoSize = True
        Me.lblcategoria.Location = New System.Drawing.Point(270, 19)
        Me.lblcategoria.Name = "lblcategoria"
        Me.lblcategoria.Size = New System.Drawing.Size(54, 13)
        Me.lblcategoria.TabIndex = 87
        Me.lblcategoria.Text = "Categoría"
        '
        'txtcodigo
        '
        Me.txtcodigo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcodigo.Location = New System.Drawing.Point(127, 19)
        Me.txtcodigo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.ReadOnly = True
        Me.txtcodigo.Size = New System.Drawing.Size(118, 20)
        Me.txtcodigo.TabIndex = 86
        Me.txtcodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'rtxtdireccion
        '
        Me.rtxtdireccion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtxtdireccion.Location = New System.Drawing.Point(125, 177)
        Me.rtxtdireccion.Margin = New System.Windows.Forms.Padding(2)
        Me.rtxtdireccion.MaxLength = 200
        Me.rtxtdireccion.Name = "rtxtdireccion"
        Me.rtxtdireccion.Size = New System.Drawing.Size(312, 53)
        Me.rtxtdireccion.TabIndex = 14
        Me.rtxtdireccion.Text = ""
        '
        'txtcorreo
        '
        Me.txtcorreo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcorreo.Location = New System.Drawing.Point(587, 269)
        Me.txtcorreo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcorreo.MaxLength = 100
        Me.txtcorreo.Name = "txtcorreo"
        Me.txtcorreo.Size = New System.Drawing.Size(158, 20)
        Me.txtcorreo.TabIndex = 23
        Me.txtcorreo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbldireccion
        '
        Me.lbldireccion.AutoSize = True
        Me.lbldireccion.Location = New System.Drawing.Point(11, 183)
        Me.lbldireccion.Name = "lbldireccion"
        Me.lbldireccion.Size = New System.Drawing.Size(111, 13)
        Me.lbldireccion.TabIndex = 60
        Me.lbldireccion.Text = "Dirección Facturación"
        '
        'lblcodigo
        '
        Me.lblcodigo.AutoSize = True
        Me.lblcodigo.Location = New System.Drawing.Point(20, 20)
        Me.lblcodigo.Name = "lblcodigo"
        Me.lblcodigo.Size = New System.Drawing.Size(40, 13)
        Me.lblcodigo.TabIndex = 85
        Me.lblcodigo.Text = "Código"
        '
        'txttelefonoCasa
        '
        Me.txttelefonoCasa.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttelefonoCasa.Location = New System.Drawing.Point(587, 201)
        Me.txttelefonoCasa.Margin = New System.Windows.Forms.Padding(2)
        Me.txttelefonoCasa.MaxLength = 20
        Me.txttelefonoCasa.Name = "txttelefonoCasa"
        Me.txttelefonoCasa.Size = New System.Drawing.Size(158, 20)
        Me.txttelefonoCasa.TabIndex = 20
        Me.txttelefonoCasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtscanId
        '
        Me.txtscanId.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtscanId.Location = New System.Drawing.Point(127, 42)
        Me.txtscanId.Margin = New System.Windows.Forms.Padding(2)
        Me.txtscanId.MaxLength = 50
        Me.txtscanId.Name = "txtscanId"
        Me.txtscanId.Size = New System.Drawing.Size(118, 20)
        Me.txtscanId.TabIndex = 7
        Me.txtscanId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnombreCompleto
        '
        Me.txtnombreCompleto.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnombreCompleto.Location = New System.Drawing.Point(125, 153)
        Me.txtnombreCompleto.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreCompleto.MaxLength = 80
        Me.txtnombreCompleto.Name = "txtnombreCompleto"
        Me.txtnombreCompleto.ReadOnly = True
        Me.txtnombreCompleto.Size = New System.Drawing.Size(312, 20)
        Me.txtnombreCompleto.TabIndex = 62
        Me.txtnombreCompleto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'mtxtidentidad
        '
        Me.mtxtidentidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mtxtidentidad.BackColor = System.Drawing.Color.White
        Me.mtxtidentidad.Location = New System.Drawing.Point(327, 42)
        Me.mtxtidentidad.Margin = New System.Windows.Forms.Padding(2)
        Me.mtxtidentidad.Name = "mtxtidentidad"
        Me.mtxtidentidad.Size = New System.Drawing.Size(109, 20)
        Me.mtxtidentidad.TabIndex = 8
        Me.mtxtidentidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbltelefonoCasa
        '
        Me.lbltelefonoCasa.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbltelefonoCasa.AutoSize = True
        Me.lbltelefonoCasa.Location = New System.Drawing.Point(502, 201)
        Me.lbltelefonoCasa.Name = "lbltelefonoCasa"
        Me.lbltelefonoCasa.Size = New System.Drawing.Size(76, 13)
        Me.lbltelefonoCasa.TabIndex = 59
        Me.lbltelefonoCasa.Text = "Teléfono Casa"
        '
        'lblcorreo
        '
        Me.lblcorreo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblcorreo.AutoSize = True
        Me.lblcorreo.Location = New System.Drawing.Point(492, 271)
        Me.lblcorreo.Name = "lblcorreo"
        Me.lblcorreo.Size = New System.Drawing.Size(82, 13)
        Me.lblcorreo.TabIndex = 58
        Me.lblcorreo.Text = "Correo Personal"
        '
        'lblscanId
        '
        Me.lblscanId.AutoSize = True
        Me.lblscanId.Location = New System.Drawing.Point(20, 47)
        Me.lblscanId.Name = "lblscanId"
        Me.lblscanId.Size = New System.Drawing.Size(50, 13)
        Me.lblscanId.TabIndex = 83
        Me.lblscanId.Text = "SCAN ID"
        '
        'txtcorreo2
        '
        Me.txtcorreo2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcorreo2.Location = New System.Drawing.Point(587, 292)
        Me.txtcorreo2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcorreo2.MaxLength = 100
        Me.txtcorreo2.Name = "txtcorreo2"
        Me.txtcorreo2.Size = New System.Drawing.Size(158, 20)
        Me.txtcorreo2.TabIndex = 24
        Me.txtcorreo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcorreo2
        '
        Me.lblcorreo2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblcorreo2.AutoSize = True
        Me.lblcorreo2.Location = New System.Drawing.Point(482, 294)
        Me.lblcorreo2.Name = "lblcorreo2"
        Me.lblcorreo2.Size = New System.Drawing.Size(97, 13)
        Me.lblcorreo2.TabIndex = 81
        Me.lblcorreo2.Text = "Correo (Alternativo)"
        '
        'txtcelular
        '
        Me.txtcelular.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtcelular.Location = New System.Drawing.Point(587, 246)
        Me.txtcelular.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcelular.MaxLength = 20
        Me.txtcelular.Name = "txtcelular"
        Me.txtcelular.Size = New System.Drawing.Size(158, 20)
        Me.txtcelular.TabIndex = 22
        Me.txtcelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblnombre
        '
        Me.lblnombre.AutoSize = True
        Me.lblnombre.Location = New System.Drawing.Point(17, 157)
        Me.lblnombre.Name = "lblnombre"
        Me.lblnombre.Size = New System.Drawing.Size(91, 13)
        Me.lblnombre.TabIndex = 56
        Me.lblnombre.Text = "Nombre Completo"
        '
        'lblcelular
        '
        Me.lblcelular.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblcelular.AutoSize = True
        Me.lblcelular.Location = New System.Drawing.Point(535, 248)
        Me.lblcelular.Name = "lblcelular"
        Me.lblcelular.Size = New System.Drawing.Size(39, 13)
        Me.lblcelular.TabIndex = 79
        Me.lblcelular.Text = "Celular"
        '
        'lblidentidadCliente
        '
        Me.lblidentidadCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblidentidadCliente.AutoSize = True
        Me.lblidentidadCliente.Location = New System.Drawing.Point(274, 45)
        Me.lblidentidadCliente.Name = "lblidentidadCliente"
        Me.lblidentidadCliente.Size = New System.Drawing.Size(51, 13)
        Me.lblidentidadCliente.TabIndex = 57
        Me.lblidentidadCliente.Text = "Identidad"
        '
        'txttelefonoTrabajo
        '
        Me.txttelefonoTrabajo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttelefonoTrabajo.Location = New System.Drawing.Point(587, 223)
        Me.txttelefonoTrabajo.Margin = New System.Windows.Forms.Padding(2)
        Me.txttelefonoTrabajo.MaxLength = 20
        Me.txttelefonoTrabajo.Name = "txttelefonoTrabajo"
        Me.txttelefonoTrabajo.Size = New System.Drawing.Size(158, 20)
        Me.txttelefonoTrabajo.TabIndex = 21
        Me.txttelefonoTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbltelefonoTrabajo
        '
        Me.lbltelefonoTrabajo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbltelefonoTrabajo.AutoSize = True
        Me.lbltelefonoTrabajo.Location = New System.Drawing.Point(488, 226)
        Me.lbltelefonoTrabajo.Name = "lbltelefonoTrabajo"
        Me.lbltelefonoTrabajo.Size = New System.Drawing.Size(88, 13)
        Me.lbltelefonoTrabajo.TabIndex = 77
        Me.lbltelefonoTrabajo.Text = "Teléfono Trabajo"
        '
        'txtrtn
        '
        Me.txtrtn.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtrtn.Location = New System.Drawing.Point(127, 109)
        Me.txtrtn.Margin = New System.Windows.Forms.Padding(2)
        Me.txtrtn.MaxLength = 20
        Me.txtrtn.Name = "txtrtn"
        Me.txtrtn.Size = New System.Drawing.Size(118, 20)
        Me.txtrtn.TabIndex = 76
        Me.txtrtn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblrtn
        '
        Me.lblrtn.AutoSize = True
        Me.lblrtn.Location = New System.Drawing.Point(21, 110)
        Me.lblrtn.Name = "lblrtn"
        Me.lblrtn.Size = New System.Drawing.Size(30, 13)
        Me.lblrtn.TabIndex = 75
        Me.lblrtn.Text = "RTN"
        '
        'lblapellido2
        '
        Me.lblapellido2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblapellido2.AutoSize = True
        Me.lblapellido2.Location = New System.Drawing.Point(260, 90)
        Me.lblapellido2.Name = "lblapellido2"
        Me.lblapellido2.Size = New System.Drawing.Size(65, 13)
        Me.lblapellido2.TabIndex = 73
        Me.lblapellido2.Text = "2do Apellido"
        '
        'lblnombre2
        '
        Me.lblnombre2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblnombre2.AutoSize = True
        Me.lblnombre2.Location = New System.Drawing.Point(262, 67)
        Me.lblnombre2.Name = "lblnombre2"
        Me.lblnombre2.Size = New System.Drawing.Size(65, 13)
        Me.lblnombre2.TabIndex = 71
        Me.lblnombre2.Text = "2do Nombre"
        '
        'lblapellido1
        '
        Me.lblapellido1.AutoSize = True
        Me.lblapellido1.Location = New System.Drawing.Point(17, 90)
        Me.lblapellido1.Name = "lblapellido1"
        Me.lblapellido1.Size = New System.Drawing.Size(62, 13)
        Me.lblapellido1.TabIndex = 69
        Me.lblapellido1.Text = "1er Apellido"
        '
        'lblnombre1
        '
        Me.lblnombre1.AutoSize = True
        Me.lblnombre1.Location = New System.Drawing.Point(17, 67)
        Me.lblnombre1.Name = "lblnombre1"
        Me.lblnombre1.Size = New System.Drawing.Size(62, 13)
        Me.lblnombre1.TabIndex = 67
        Me.lblnombre1.Text = "1er Nombre"
        '
        'btnseleccionarCliente
        '
        Me.btnseleccionarCliente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnseleccionarCliente.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnseleccionarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnseleccionarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnseleccionarCliente.ForeColor = System.Drawing.Color.Black
        Me.btnseleccionarCliente.Location = New System.Drawing.Point(572, 5)
        Me.btnseleccionarCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.btnseleccionarCliente.Name = "btnseleccionarCliente"
        Me.btnseleccionarCliente.Size = New System.Drawing.Size(82, 31)
        Me.btnseleccionarCliente.TabIndex = 4
        Me.btnseleccionarCliente.Text = "Seleccionar"
        Me.btnseleccionarCliente.UseVisualStyleBackColor = False
        '
        'btnpaciente
        '
        Me.btnpaciente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnpaciente.BackColor = System.Drawing.Color.SandyBrown
        Me.btnpaciente.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnpaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnpaciente.ForeColor = System.Drawing.Color.Black
        Me.btnpaciente.Location = New System.Drawing.Point(661, 5)
        Me.btnpaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.btnpaciente.Name = "btnpaciente"
        Me.btnpaciente.Size = New System.Drawing.Size(106, 31)
        Me.btnpaciente.TabIndex = 5
        Me.btnpaciente.Text = "Crear Paciente"
        Me.btnpaciente.UseVisualStyleBackColor = False
        '
        'btnbuscarPorNombre
        '
        Me.btnbuscarPorNombre.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarPorNombre.BackgroundImage = CType(resources.GetObject("btnbuscarPorNombre.BackgroundImage"), System.Drawing.Image)
        Me.btnbuscarPorNombre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarPorNombre.FlatAppearance.BorderSize = 0
        Me.btnbuscarPorNombre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarPorNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarPorNombre.Location = New System.Drawing.Point(517, 16)
        Me.btnbuscarPorNombre.Name = "btnbuscarPorNombre"
        Me.btnbuscarPorNombre.Size = New System.Drawing.Size(22, 21)
        Me.btnbuscarPorNombre.TabIndex = 3
        Me.btnbuscarPorNombre.UseVisualStyleBackColor = True
        '
        'lblNombreB
        '
        Me.lblNombreB.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblNombreB.AutoSize = True
        Me.lblNombreB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreB.Location = New System.Drawing.Point(300, 18)
        Me.lblNombreB.Name = "lblNombreB"
        Me.lblNombreB.Size = New System.Drawing.Size(52, 15)
        Me.lblNombreB.TabIndex = 119
        Me.lblNombreB.Text = "Nombre"
        '
        'txtnombreB
        '
        Me.txtnombreB.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombreB.Location = New System.Drawing.Point(357, 16)
        Me.txtnombreB.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreB.MaxLength = 80
        Me.txtnombreB.Name = "txtnombreB"
        Me.txtnombreB.Size = New System.Drawing.Size(156, 20)
        Me.txtnombreB.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.Panel1.Controls.Add(Me.btnseleccionarCliente)
        Me.Panel1.Controls.Add(Me.dgbtabla)
        Me.Panel1.Controls.Add(Me.btnbuscarPorNombre)
        Me.Panel1.Controls.Add(Me.btnpaciente)
        Me.Panel1.Controls.Add(Me.txtnombreB)
        Me.Panel1.Controls.Add(Me.lblidCliente)
        Me.Panel1.Controls.Add(Me.mtxtidentidadClienteB)
        Me.Panel1.Controls.Add(Me.lblNombreB)
        Me.Panel1.Controls.Add(Me.btnbuscarCliente)
        Me.Panel1.Location = New System.Drawing.Point(11, 62)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(768, 152)
        Me.Panel1.TabIndex = 120
        '
        'dgbtabla
        '
        Me.dgbtabla.AllowUserToAddRows = False
        Me.dgbtabla.AllowUserToDeleteRows = False
        Me.dgbtabla.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgbtabla.BackgroundColor = System.Drawing.Color.White
        Me.dgbtabla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbtabla.Location = New System.Drawing.Point(4, 49)
        Me.dgbtabla.Margin = New System.Windows.Forms.Padding(2)
        Me.dgbtabla.Name = "dgbtabla"
        Me.dgbtabla.ReadOnly = True
        Me.dgbtabla.RowHeadersWidth = 51
        Me.dgbtabla.RowTemplate.Height = 24
        Me.dgbtabla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgbtabla.Size = New System.Drawing.Size(760, 97)
        Me.dgbtabla.TabIndex = 6
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(102, Byte), Integer))
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 583)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(791, 22)
        Me.StatusStrip1.TabIndex = 121
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.fondo
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Location = New System.Drawing.Point(0, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(791, 44)
        Me.Panel2.TabIndex = 122
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(12, 12)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(98, 25)
        Me.Label13.TabIndex = 215
        Me.Label13.Text = "Clientes"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.MenuStrip1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.btnnuevo, Me.btnguardarCliente, Me.btnactualizarCliente})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(791, 24)
        Me.MenuStrip1.TabIndex = 124
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BúsquedaAvanzadaCrtlBToolStripMenuItem, Me.CerrarEscToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.ForeColor = System.Drawing.Color.Black
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'BúsquedaAvanzadaCrtlBToolStripMenuItem
        '
        Me.BúsquedaAvanzadaCrtlBToolStripMenuItem.BackColor = System.Drawing.Color.White
        Me.BúsquedaAvanzadaCrtlBToolStripMenuItem.Name = "BúsquedaAvanzadaCrtlBToolStripMenuItem"
        Me.BúsquedaAvanzadaCrtlBToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.BúsquedaAvanzadaCrtlBToolStripMenuItem.Text = "Búsqueda Avanzada          Ctrl+B"
        '
        'CerrarEscToolStripMenuItem
        '
        Me.CerrarEscToolStripMenuItem.Name = "CerrarEscToolStripMenuItem"
        Me.CerrarEscToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.CerrarEscToolStripMenuItem.Text = "Cerrar                                       Esc"
        '
        'btnnuevo
        '
        Me.btnnuevo.Name = "btnnuevo"
        Me.btnnuevo.Size = New System.Drawing.Size(54, 20)
        Me.btnnuevo.Text = "Nuevo"
        '
        'btnguardarCliente
        '
        Me.btnguardarCliente.Name = "btnguardarCliente"
        Me.btnguardarCliente.Size = New System.Drawing.Size(61, 20)
        Me.btnguardarCliente.Text = "Guardar"
        '
        'btnactualizarCliente
        '
        Me.btnactualizarCliente.Name = "btnactualizarCliente"
        Me.btnactualizarCliente.Size = New System.Drawing.Size(70, 20)
        Me.btnactualizarCliente.Text = "Modificar"
        '
        'M_Cliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(791, 605)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.gbxinfoCliente)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "M_Cliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SLM -  Módulo Facturación"
        Me.gbxinfoCliente.ResumeLayout(False)
        Me.gbxinfoCliente.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxgenero.ResumeLayout(False)
        Me.gbxgenero.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgbtabla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents mtxtidentidadClienteB As MaskedTextBox
    Friend WithEvents btnbuscarCliente As Button
    Friend WithEvents lblidCliente As Label
    Friend WithEvents gbxinfoCliente As GroupBox
    Friend WithEvents dtpfechaNacimiento As DateTimePicker
    Friend WithEvents gbxgenero As GroupBox
    Friend WithEvents rbtnmasculino As RadioButton
    Friend WithEvents rbtnfemenino As RadioButton
    Friend WithEvents lblclasificacion As Label
    Friend WithEvents lblconvenio As Label
    Friend WithEvents lblfechaNacimiento As Label
    Friend WithEvents lblterminosPago As Label
    Friend WithEvents lblaseguradora As Label
    Friend WithEvents lblcategoria As Label
    Friend WithEvents txtcodigo As TextBox
    Friend WithEvents rtxtdireccion As RichTextBox
    Friend WithEvents txtcorreo As TextBox
    Friend WithEvents lbldireccion As Label
    Friend WithEvents lblcodigo As Label
    Friend WithEvents txttelefonoCasa As TextBox
    Friend WithEvents txtscanId As TextBox
    Friend WithEvents txtnombreCompleto As TextBox
    Friend WithEvents mtxtidentidad As MaskedTextBox
    Friend WithEvents lbltelefonoCasa As Label
    Friend WithEvents lblcorreo As Label
    Friend WithEvents lblscanId As Label
    Friend WithEvents txtcorreo2 As TextBox
    Friend WithEvents lblcorreo2 As Label
    Friend WithEvents txtcelular As TextBox
    Friend WithEvents lblnombre As Label
    Friend WithEvents lblcelular As Label
    Friend WithEvents lblidentidadCliente As Label
    Friend WithEvents txttelefonoTrabajo As TextBox
    Friend WithEvents lbltelefonoTrabajo As Label
    Friend WithEvents txtrtn As TextBox
    Friend WithEvents lblrtn As Label
    Friend WithEvents lblapellido2 As Label
    Friend WithEvents lblnombre2 As Label
    Friend WithEvents lblapellido1 As Label
    Friend WithEvents lblnombre1 As Label
    Friend WithEvents txtcodigoCategoria As TextBox
    Friend WithEvents btnclasificacion As Button
    Friend WithEvents txtcodigoClasificacion As TextBox
    Friend WithEvents btnterminosPago As Button
    Friend WithEvents txtcodigoTermino As TextBox
    Friend WithEvents btnconvenio As Button
    Friend WithEvents txtconvenio As TextBox
    Friend WithEvents btnaseguradora As Button
    Friend WithEvents txtaseguradora As TextBox
    Friend WithEvents btncategoria As Button
    Friend WithEvents txtnombreCategoria As TextBox
    Friend WithEvents txtnombreClasificacion As TextBox
    Friend WithEvents txtnombreConvenio As TextBox
    Friend WithEvents txtnombreTerminos As TextBox
    Friend WithEvents txtnombreAseguradora As TextBox
    Friend WithEvents btnseleccionarCliente As Button
    Friend WithEvents btnpaciente As Button
    Friend WithEvents btnbuscarPorNombre As Button
    Friend WithEvents lblNombreB As Label
    Friend WithEvents txtnombreB As TextBox
    Friend WithEvents lblcodeCategoria As Label
    Friend WithEvents lblcodeTerminoPago As Label
    Friend WithEvents txtEdad As TextBox
    Friend WithEvents txtapellido2 As TextBox
    Friend WithEvents txtnombre2 As TextBox
    Friend WithEvents txtapellido1 As TextBox
    Friend WithEvents txtnombre1 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cmbxClasificacion As ComboBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label61 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents dgbtabla As DataGridView
    Friend WithEvents Label13 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BúsquedaAvanzadaCrtlBToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CerrarEscToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnnuevo As ToolStripMenuItem
    Friend WithEvents btnguardarCliente As ToolStripMenuItem
    Friend WithEvents btnactualizarCliente As ToolStripMenuItem
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents chkCambio As CheckBox
    Friend WithEvents txtNombreAdicional As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtRTNAdicional As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents lblForm As Label
End Class
