﻿Public Class M_ListadoResultadosParametro
    Public fila As Integer
    Dim objOrd As New ClsOrdenDeTrabajo

    Private Sub M_ListadoResultadosParametro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'LLENAR EL DATA GRID VIEW
            Dim dv As DataView = objOrd.BuscarResultadosPorDefectoParametro(E_HojaTrabajo.txtParametro.Text).DefaultView
            dgvResultados.DataSource = dv

            'AGREGARLE COLOR AL DATAGRIDVIEW
            alternarColoFilasDatagridview(dgvResultados)

            'CAMBIO DE NOMBRE COLUMNAS
            dgvResultados.Columns("resultado").HeaderText = "Resultado"

            'OCULTAR COLUMNAS
            Me.dgvResultados.Columns("nombre").Visible = False

            Dim colColl As DataColumnCollection = E_HojaTrabajo.ds.Tables("HojaTrabajo").Columns
            If E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString = "*" Or Trim(E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString) = "" Then
                rbtnReemplazar.Checked = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvResultados_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvResultados.CellMouseDoubleClick
        Try
            Dim n As String = ""
            If IsDBNull(Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()) = True Or Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value() = "" Then
                Me.Close()
            End If
            'If e.RowIndex >= 0 Then
            '    n = MsgBox("¿Desea utilizar el resultado seleccionado?", MsgBoxStyle.YesNo)
            'End If
            'If n = vbYes Then
            '    Dim colColl As DataColumnCollection = E_HojaTrabajo.ds.Tables("HojaTrabajo").Columns
            '    If E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString = "*" Then
            '        E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '    ElseIf trim(E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString) = "" Then
            '        E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '    Else
            '        'E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) + ", " + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()

            '        If rbtnMantener.Checked = True Then
            '            E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) + ", " + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '        Else
            '            E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '        End If
            '        'E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '    End If
            '    E_HojaTrabajo.dgvHojaTrab.DataSource = E_HojaTrabajo.ds.Tables(0)
            '    Dim objOrdDet As New ClsOrdenTrabajoDetalle
            '    With objOrdDet
            '        .cod_orden_trabajo_ = Integer.Parse(E_HojaTrabajo.txtOrden.Text)
            '        '.resultado_ = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
            '        .resultado_ = E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value
            '        .nombreItemDetalle_ = E_HojaTrabajo.txtParametro.Text
            '        .estado_ = "Procesado"
            '    End With
            '    If objOrdDet.ModificarOrdenTrabajoDetalleTecnico() <> 1 Then
            '        'En caso que no exista el detalle de orden de trabajo entonces le asigna un valor nulo o vacio
            '        MsgBox("Error al modificar el parametro de examen de la orden de trabajo.", MsgBoxStyle.Critical)
            '    End If
            '    Me.Close()
            'End If


            If e.RowIndex >= 0 Then
                Dim colColl As DataColumnCollection = E_HojaTrabajo.ds.Tables("HojaTrabajo").Columns
                If E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString = "*" Then
                    E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                ElseIf Trim(E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString) = "" Then
                    E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                Else
                    'E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) + ", " + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()

                    If rbtnMantener.Checked = True Then
                        E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) + ", " + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                    Else
                        E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                    End If
                    'E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value + Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                End If
                E_HojaTrabajo.dgvHojaTrab.DataSource = E_HojaTrabajo.ds.Tables(0)
                Dim objOrdDet As New ClsOrdenTrabajoDetalle
                With objOrdDet
                    .cod_orden_trabajo_ = Integer.Parse(E_HojaTrabajo.txtOrden.Text)
                    '.resultado_ = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                    .resultado_ = E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value
                    .nombreItemDetalle_ = E_HojaTrabajo.txtParametro.Text
                    .estado_ = "Procesado"
                End With
                'MsgBox("guarda")
                If objOrdDet.ModificarOrdenTrabajoDetalleTecnico() <> 1 Then
                    'En caso que no exista el detalle de orden de trabajo entonces le asigna un valor nulo o vacio
                    MsgBox("Error al modificar el parametro de examen de la orden de trabajo.", MsgBoxStyle.Critical)
                End If
                E_HojaTrabajo.ValidarResultadosIngresados()
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("No se selecciono el resultado correctamente.", MsgBoxStyle.Information)
            Me.Close()
        End Try
    End Sub
    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try
            If (e.KeyCode = Keys.Escape) Then
                Me.Close()
            End If

            If e.KeyCode = Keys.Enter Then
                Dim filaAct As Integer = Convert.ToInt64(dgvResultados.CurrentCell.RowIndex.ToString)

                Dim colColl As DataColumnCollection = E_HojaTrabajo.ds.Tables("HojaTrabajo").Columns
                If E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString = "*" Then
                    E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(filaAct).Cells(1).Value()
                ElseIf Trim(E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value.ToString) = "" Then
                    E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value = Me.dgvResultados.Rows(filaAct).Cells(1).Value()
                Else
                    If rbtnMantener.Checked = True Then
                        E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) + ", " + Me.dgvResultados.Rows(filaAct).Cells(1).Value()
                    Else
                        E_HojaTrabajo.ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)) = Me.dgvResultados.Rows(filaAct).Cells(1).Value()
                    End If
                End If
                E_HojaTrabajo.dgvHojaTrab.DataSource = E_HojaTrabajo.ds.Tables(0)
                Dim objOrdDet As New ClsOrdenTrabajoDetalle
                With objOrdDet
                    .cod_orden_trabajo_ = Integer.Parse(E_HojaTrabajo.txtOrden.Text)
                    '.resultado_ = Me.dgvResultados.Rows(e.RowIndex).Cells(1).Value()
                    .resultado_ = E_HojaTrabajo.dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(E_HojaTrabajo.txtParametro.Text)).Value
                    .nombreItemDetalle_ = E_HojaTrabajo.txtParametro.Text
                    .estado_ = "Procesado"
                End With
                'MsgBox("guarda")
                If objOrdDet.ModificarOrdenTrabajoDetalleTecnico() <> 1 Then
                    'En caso que no exista el detalle de orden de trabajo entonces le asigna un valor nulo o vacio
                    MsgBox("Error al modificar el parametro de examen de la orden de trabajo.", MsgBoxStyle.Critical)
                End If
                E_HojaTrabajo.ValidarResultadosIngresados()

                Me.Close()
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class