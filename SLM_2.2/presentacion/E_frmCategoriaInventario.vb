﻿Public Class E_frmCategoriaInventario
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        GridAExcel_global(DataGridView1)
    End Sub
    Public Sub limpiar()
        txtCodigo.Clear()
        txtNombre.Clear()
        RichTextBox1.Clear()
    End Sub
    Public Sub Habilitar()
        txtNombre.ReadOnly = False

    End Sub
    Public Sub Cancelar()
        txtNombre.ReadOnly = True
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        limpiar()
        Habilitar()
        RadioButton1.Checked = True

    End Sub
    Public Sub guardarRegistro()
        Dim cls As New cls_CategoriaInventario
        With cls
            .Nombre1 = txtNombre.Text
            .Descripcion1 = RichTextBox1.Text
        End With

        If cls.RegistrarCategoriaInventario() = "1" Then
            MsgBox(mensaje_registro)
            cargarData()
        End If
    End Sub

    Public Sub ActualizarRegistro()
        Dim cls As New cls_CategoriaInventario
        Dim estadoCI As String
        If RadioButton1.Checked = True Then
            estadoCI = "Habilitado"
        Else
            estadoCI = "DesHabilitado"
        End If
        With cls
            .Id1 = txtCodigo.Text
            .Nombre1 = txtNombre.Text
            .Descripcion1 = RichTextBox1.Text
            .Estado1 = estadoCI
        End With

        If cls.ActualizarCategoriaInventario() = "1" Then
            MsgBox(mensaje_actualizacion)
            cargarData()
        End If
    End Sub

    Public Sub EliminarRegistro()
        Dim cls As New cls_CategoriaInventario
        With cls
            .Id1 = txtCodigo.Text

        End With

        If cls.ActualizarCategoriaInventario() = "1" Then
            MsgBox(mensaje_actualizacion)
            cargarData()
        End If
    End Sub
    Private Sub cargarData()
        Try
            'datagridview
            Dim TableUM As New DataTable
            Dim clsP As New cls_CategoriaInventario
            TableUM.Load(clsP.RecuperarRegistros)
            BindingSource1.DataSource = TableUM

            DataGridView1.DataSource = BindingSource1
        Catch ex As Exception
            RegistrarExcepciones(codigo_usuario, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub E_frmCategoriaInventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarData()
        alternarColoFilasDatagridview(DataGridView1)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtCodigo.Text <> "" Then
            ActualizarRegistro()
        ElseIf txtCodigo.Text = "" Then
            guardarRegistro()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            Habilitar()
            txtCodigo.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtNombre.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            RichTextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value

            If DataGridView1.Rows(e.RowIndex).Cells(3).Value.ToString = "Habilitado" Then
                RadioButton1.Checked = True
            Else
                RadioButton2.Checked = True
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        limpiar()
        Cancelar()
    End Sub
End Class