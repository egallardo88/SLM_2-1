﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class A_PlanillaCalculo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(A_PlanillaCalculo))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTechoRap = New System.Windows.Forms.TextBox()
        Me.chkDiasFaltados = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtIHSS = New System.Windows.Forms.TextBox()
        Me.txtDescripcionPlanilla = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCodPlanilla = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnExportarExcel = New System.Windows.Forms.Button()
        Me.dtData = New System.Windows.Forms.DataGridView()
        Me.Empleado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SueldoBase = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adicional = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.T_Extra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiasFaltados = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDev = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IHSS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RetISR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Embargos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prest_Cofinter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AdelantoSueldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrestamoRAP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RetOptica = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RetPrestamo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpVecinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RAPVolunt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RetRAP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SueldoNeto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodEmpleado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CargarEmpleadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarAdelantosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarVistaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dtData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtTechoRap)
        Me.GroupBox1.Controls.Add(Me.chkDiasFaltados)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtIHSS)
        Me.GroupBox1.Controls.Add(Me.txtDescripcionPlanilla)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtCodPlanilla)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 72)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1059, 79)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de Empleado"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(653, 46)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 19
        Me.Button2.Text = "Calcular"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(461, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Techo RAP:"
        '
        'txtTechoRap
        '
        Me.txtTechoRap.Location = New System.Drawing.Point(529, 47)
        Me.txtTechoRap.Name = "txtTechoRap"
        Me.txtTechoRap.Size = New System.Drawing.Size(118, 20)
        Me.txtTechoRap.TabIndex = 17
        '
        'chkDiasFaltados
        '
        Me.chkDiasFaltados.AutoSize = True
        Me.chkDiasFaltados.Location = New System.Drawing.Point(80, 50)
        Me.chkDiasFaltados.Name = "chkDiasFaltados"
        Me.chkDiasFaltados.Size = New System.Drawing.Size(168, 17)
        Me.chkDiasFaltados.TabIndex = 16
        Me.chkDiasFaltados.Text = "Ingreso Manual Días Faltados"
        Me.chkDiasFaltados.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(314, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "IHSS:"
        '
        'txtIHSS
        '
        Me.txtIHSS.Location = New System.Drawing.Point(351, 47)
        Me.txtIHSS.Name = "txtIHSS"
        Me.txtIHSS.Size = New System.Drawing.Size(100, 20)
        Me.txtIHSS.TabIndex = 14
        '
        'txtDescripcionPlanilla
        '
        Me.txtDescripcionPlanilla.Enabled = False
        Me.txtDescripcionPlanilla.Location = New System.Drawing.Point(350, 22)
        Me.txtDescripcionPlanilla.Name = "txtDescripcionPlanilla"
        Me.txtDescripcionPlanilla.Size = New System.Drawing.Size(459, 20)
        Me.txtDescripcionPlanilla.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(209, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Descrip. Planilla Adelantos:"
        '
        'txtCodPlanilla
        '
        Me.txtCodPlanilla.Enabled = False
        Me.txtCodPlanilla.Location = New System.Drawing.Point(80, 22)
        Me.txtCodPlanilla.Name = "txtCodPlanilla"
        Me.txtCodPlanilla.Size = New System.Drawing.Size(119, 20)
        Me.txtCodPlanilla.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Cód. Planilla:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.btnExportarExcel)
        Me.GroupBox2.Controls.Add(Me.dtData)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 157)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1059, 495)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.Location = New System.Drawing.Point(833, 9)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(107, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Cargar Excel"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExportarExcel.BackColor = System.Drawing.Color.Green
        Me.btnExportarExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportarExcel.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.btnExportarExcel.Location = New System.Drawing.Point(946, 9)
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(107, 23)
        Me.btnExportarExcel.TabIndex = 4
        Me.btnExportarExcel.Text = "Exportar Excel"
        Me.btnExportarExcel.UseVisualStyleBackColor = False
        '
        'dtData
        '
        Me.dtData.AllowUserToAddRows = False
        Me.dtData.AllowUserToDeleteRows = False
        Me.dtData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtData.BackgroundColor = System.Drawing.Color.White
        Me.dtData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Empleado, Me.SueldoBase, Me.Adicional, Me.T_Extra, Me.Column1, Me.Column2, Me.Column3, Me.DiasFaltados, Me.Column4, Me.TotalDev, Me.IHSS, Me.RetISR, Me.Embargos, Me.Prest_Cofinter, Me.AdelantoSueldo, Me.PrestamoRAP, Me.RetOptica, Me.RetPrestamo, Me.ImpVecinal, Me.RAPVolunt, Me.RetRAP, Me.SueldoNeto, Me.CodEmpleado, Me.codDetalle})
        Me.dtData.Location = New System.Drawing.Point(6, 35)
        Me.dtData.Name = "dtData"
        Me.dtData.Size = New System.Drawing.Size(1047, 454)
        Me.dtData.TabIndex = 0
        '
        'Empleado
        '
        Me.Empleado.Frozen = True
        Me.Empleado.HeaderText = "Nombre"
        Me.Empleado.Name = "Empleado"
        Me.Empleado.ReadOnly = True
        '
        'SueldoBase
        '
        Me.SueldoBase.HeaderText = "Sueldo Base"
        Me.SueldoBase.Name = "SueldoBase"
        Me.SueldoBase.ReadOnly = True
        '
        'Adicional
        '
        Me.Adicional.HeaderText = "Adicional (+)"
        Me.Adicional.Name = "Adicional"
        '
        'T_Extra
        '
        Me.T_Extra.HeaderText = "T. Extra"
        Me.T_Extra.Name = "T_Extra"
        '
        'Column1
        '
        Me.Column1.HeaderText = "T. Extra Doble"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "T. Extra Mixto"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "T. Extra Nocturno"
        Me.Column3.Name = "Column3"
        '
        'DiasFaltados
        '
        Me.DiasFaltados.HeaderText = "Días Faltados"
        Me.DiasFaltados.Name = "DiasFaltados"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Total Salario Extraor."
        Me.Column4.Name = "Column4"
        '
        'TotalDev
        '
        Me.TotalDev.HeaderText = "Total Devengado"
        Me.TotalDev.Name = "TotalDev"
        Me.TotalDev.ReadOnly = True
        '
        'IHSS
        '
        Me.IHSS.HeaderText = "I.H.S.S."
        Me.IHSS.Name = "IHSS"
        '
        'RetISR
        '
        Me.RetISR.HeaderText = "Retención ISR"
        Me.RetISR.Name = "RetISR"
        '
        'Embargos
        '
        Me.Embargos.HeaderText = "Embargos"
        Me.Embargos.Name = "Embargos"
        '
        'Prest_Cofinter
        '
        Me.Prest_Cofinter.HeaderText = "Prest. Cofinter"
        Me.Prest_Cofinter.Name = "Prest_Cofinter"
        '
        'AdelantoSueldo
        '
        Me.AdelantoSueldo.HeaderText = "Adelantos Sueldo"
        Me.AdelantoSueldo.Name = "AdelantoSueldo"
        '
        'PrestamoRAP
        '
        Me.PrestamoRAP.HeaderText = "Prestamo RAP"
        Me.PrestamoRAP.Name = "PrestamoRAP"
        '
        'RetOptica
        '
        Me.RetOptica.HeaderText = "Retención Optica"
        Me.RetOptica.Name = "RetOptica"
        '
        'RetPrestamo
        '
        Me.RetPrestamo.HeaderText = "Ret. Prestamo"
        Me.RetPrestamo.Name = "RetPrestamo"
        '
        'ImpVecinal
        '
        Me.ImpVecinal.HeaderText = "Imp. Vecinal"
        Me.ImpVecinal.Name = "ImpVecinal"
        '
        'RAPVolunt
        '
        Me.RAPVolunt.HeaderText = "RAP Volunt."
        Me.RAPVolunt.Name = "RAPVolunt"
        '
        'RetRAP
        '
        Me.RetRAP.HeaderText = "Ret. RAP"
        Me.RetRAP.Name = "RetRAP"
        '
        'SueldoNeto
        '
        Me.SueldoNeto.HeaderText = "Sueldo Neto"
        Me.SueldoNeto.Name = "SueldoNeto"
        Me.SueldoNeto.ReadOnly = True
        '
        'CodEmpleado
        '
        Me.CodEmpleado.HeaderText = "CodEmpleado"
        Me.CodEmpleado.Name = "CodEmpleado"
        Me.CodEmpleado.Visible = False
        '
        'codDetalle
        '
        Me.codDetalle.HeaderText = "codDetalle"
        Me.codDetalle.Name = "codDetalle"
        Me.codDetalle.Visible = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CargarEmpleadosToolStripMenuItem, Me.CargarAdelantosToolStripMenuItem, Me.GenerarVistaToolStripMenuItem, Me.CerrarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1083, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CargarEmpleadosToolStripMenuItem
        '
        Me.CargarEmpleadosToolStripMenuItem.Name = "CargarEmpleadosToolStripMenuItem"
        Me.CargarEmpleadosToolStripMenuItem.Size = New System.Drawing.Size(115, 20)
        Me.CargarEmpleadosToolStripMenuItem.Text = "Cargar Empleados"
        '
        'CargarAdelantosToolStripMenuItem
        '
        Me.CargarAdelantosToolStripMenuItem.Name = "CargarAdelantosToolStripMenuItem"
        Me.CargarAdelantosToolStripMenuItem.Size = New System.Drawing.Size(110, 20)
        Me.CargarAdelantosToolStripMenuItem.Text = "Cargar Adelantos"
        Me.CargarAdelantosToolStripMenuItem.Visible = False
        '
        'GenerarVistaToolStripMenuItem
        '
        Me.GenerarVistaToolStripMenuItem.Name = "GenerarVistaToolStripMenuItem"
        Me.GenerarVistaToolStripMenuItem.Size = New System.Drawing.Size(88, 20)
        Me.GenerarVistaToolStripMenuItem.Text = "Generar Vista"
        '
        'CerrarToolStripMenuItem
        '
        Me.CerrarToolStripMenuItem.Name = "CerrarToolStripMenuItem"
        Me.CerrarToolStripMenuItem.Size = New System.Drawing.Size(51, 20)
        Me.CerrarToolStripMenuItem.Text = "Cerrar"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.fondo
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(-1, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1088, 47)
        Me.Panel1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(14, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(209, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cálculo de Planilla"
        '
        'A_PlanillaCalculo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1083, 671)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "A_PlanillaCalculo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cálculo de Planilla"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dtData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dtData As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDescripcionPlanilla As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCodPlanilla As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtIHSS As TextBox
    Friend WithEvents chkDiasFaltados As CheckBox
    Friend WithEvents btnExportarExcel As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents CargarEmpleadosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargarAdelantosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GenerarVistaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button1 As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTechoRap As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Empleado As DataGridViewTextBoxColumn
    Friend WithEvents SueldoBase As DataGridViewTextBoxColumn
    Friend WithEvents Adicional As DataGridViewTextBoxColumn
    Friend WithEvents T_Extra As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents DiasFaltados As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents TotalDev As DataGridViewTextBoxColumn
    Friend WithEvents IHSS As DataGridViewTextBoxColumn
    Friend WithEvents RetISR As DataGridViewTextBoxColumn
    Friend WithEvents Embargos As DataGridViewTextBoxColumn
    Friend WithEvents Prest_Cofinter As DataGridViewTextBoxColumn
    Friend WithEvents AdelantoSueldo As DataGridViewTextBoxColumn
    Friend WithEvents PrestamoRAP As DataGridViewTextBoxColumn
    Friend WithEvents RetOptica As DataGridViewTextBoxColumn
    Friend WithEvents RetPrestamo As DataGridViewTextBoxColumn
    Friend WithEvents ImpVecinal As DataGridViewTextBoxColumn
    Friend WithEvents RAPVolunt As DataGridViewTextBoxColumn
    Friend WithEvents RetRAP As DataGridViewTextBoxColumn
    Friend WithEvents SueldoNeto As DataGridViewTextBoxColumn
    Friend WithEvents CodEmpleado As DataGridViewTextBoxColumn
    Friend WithEvents codDetalle As DataGridViewTextBoxColumn
    Friend WithEvents CerrarToolStripMenuItem As ToolStripMenuItem
End Class
