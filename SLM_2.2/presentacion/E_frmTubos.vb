﻿Imports System.Data.SqlClient

Public Class E_frmTubos
    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click

    End Sub
    Public Sub limpiar()
        TextBox1.Clear()
        TextBox2.Clear()

    End Sub

    Public Sub cargarData()
        Try


            BindingSource1.DataSource = listarTubos()

            DataGridView1.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar los bancos. " + ex.Message)
        End Try
    End Sub

    Private Sub E_frmTubos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MenuStrip_slm(MenuStrip1)
        ColoresForm(Panel1, StatusStrip1)

        cargarData()
    End Sub

    Public Function listarTubos() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("E_slmListarTubos", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
            da.Dispose()
            objCon.cerrarConexion()
        End Using
        objCon.cerrarConexion()
    End Function

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            If registrarTubo() = "1" Then
                MsgBox(mensaje_registro)
                cargarData()
            End If
        Catch ex As Exception
            MsgBox("Campos vacios")
        End Try

    End Sub
    Public Function registrarTubo() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        'PROCEDIMIENTO ALMACENADO
        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarTubo"

        'VARIABLES 
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "color"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre"
        sqlpar.Value = TextBox2.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion
        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function ActualizarTubo() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        'PROCEDIMIENTO ALMACENADO
        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarTubo"

        'VARIABLES 
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "color"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre"
        sqlpar.Value = TextBox2.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id"
        sqlpar.Value = TextBox3.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion
        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            TextBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            TextBox3.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        If TextBox3.Text <> "" Then
            Try
                If ActualizarTubo() = "1" Then
                    MsgBox(mensaje_actualizacion)
                    cargarData()
                End If
            Catch ex As Exception
                MsgBox("Erro al actualiza, pregunte a sistemas")
            End Try
        End If
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        End
    End Sub
End Class