﻿Imports System.Data.SqlClient
Imports System.IO

Public Class E_frmSalidaAlmacenesSLM
    Dim cod_id_entrada, cod_id_producto, cod_id_producto2, contador_existencias, contador_existencias_devolver As Integer
    Dim cantidad As Integer
    Dim codigoBarra, producto_correo, codigo_barra_correo As String
    Dim id_traslado, nuevo_alma, nueva_existencia As Integer
    Private Sub E_frmSalidaAlmacenesSLM_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet9.historialSucursales' Puede moverla o quitarla según sea necesario.
        Me.HistorialSucursalesTableAdapter.Fill(Me.Slm_testDataSet9.historialSucursales)
        ColoresForm(Panel2, StatusStrip1)
        'MenuStrip_slm(MenuStrip1)
        alternarColoFilasDatagridview(DataGridView1)
        alternarColoFilasDatagridview(DataGridView2)
        alternarColoFilasDatagridview(DataGridView5)
        alternarColoFilasDatagridview(DataGridView3)
        cargarData()
        cargarInventarioNoRecibido()
        TextBox1.Text = nombre_usurio
        Txtusuariod.Text = nombre_usurio
        Dim nombre_almacend As String = RetornarNombreAlmacenErick(nombre_usurio)
        TextBox2.Text = nombre_almacend
        txtAlmacend.Text = nombre_almacend
        Button4.Enabled = False
    End Sub
    Private Sub cargarData()
        Dim clsA As New clsInventario
        Dim usuario As String
        usuario = RetornarAlmacenErick(nombre_usurio)

        Try

            Dim dvOC As DataView = clsA.ListarInventarioAlmacenSalidasSLM(usuario).DefaultView

            BindingSource1.DataSource = dvOC
            BindingNavigator1.BindingSource = BindingSource1
            DataGridView1.DataSource = BindingSource1
            DataGridView2.DataSource = BindingSource1
            ' sumarData1()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'registrar la salida
        If validarGuardar("realizar una salida del inventario del producto " + txtProducto.Text + " ") = "1" Then


            If txtCantidad.Text = "" Then
                MsgBox("No puede dejar la cantidad vacia")
                Exit Sub
            End If
            If txtCantidad.Text > cantidad Then
                MsgBox("La cantidad a ingresar es mayor a la existencia del inventario")
                Exit Sub
            End If
            If txtProducto.Text = "" Then
                MsgBox("Debe seleccionar un producto")
                Exit Sub
            End If
            Try
                If RegistrarSalida() = "1" Then
                    MsgBox(mensaje_registro)
                    If CInt(contador_existencias) - CInt(txtCantidad.Text) = 0 Then
                        enviarMailAlmacen(RetornarNombreAlmacenErick(nombre_usurio), producto_correo.ToString + " - " + codigo_barra_correo.ToString)

                    End If
                    Button1.Enabled = False
                    txtCantidad.Clear()
                    txtProducto.Clear()
                    cargarData()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            cod_id_producto = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            codigo_barra_correo = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            txtProducto.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            producto_correo = txtProducto.Text
            cantidad = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            contador_existencias = DataGridView1.Rows(e.RowIndex).Cells(3).Value

            If contador_existencias = 0 Then
                Button1.Enabled = False

            Else
                Button1.Enabled = True

            End If

            Dim clsp As New ClsProducto

            Dim datos As Byte() = clsp.RecuperarImagenProducto(cod_id_producto)
            Dim ms = New MemoryStream(datos)

            PictureBox1.Image = Image.FromStream(ms)

        Catch ex As Exception

        End Try

    End Sub

    Public Function RegistrarSalida() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmSalidaSucursalInventario"


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_producto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = cod_id_producto
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nueva_existencia" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = txtCantidad.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nuevo_almacen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = RetornarAlmacenErick(nombre_usurio)
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function RecuperarUsuario() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmRecuperarUsuarioAlmacenSLM"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = nombre_usurio
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub DataGridView5_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellClick
        Try
            txtIdEntradaRecibe.Text = DataGridView5.Rows(e.RowIndex).Cells(0).Value
            codigoBarra = DataGridView5.Rows(e.RowIndex).Cells(1).Value
            txtProductorecibe.Text = DataGridView5.Rows(e.RowIndex).Cells(2).Value
            tctCantidadRecibe.Text = DataGridView5.Rows(e.RowIndex).Cells(3).Value
            tctEstadoRecibe.Text = DataGridView5.Rows(e.RowIndex).Cells(5).Value
            txtFechaEnvioreicbe.Text = DataGridView5.Rows(e.RowIndex).Cells(4).Value
            'txtloterecibe.Text = DataGridView5.Rows(e.RowIndex).Cells(5).Value
            nuevo_alma = DataGridView5.Rows(e.RowIndex).Cells(6).Value

        Catch ex As Exception

        End Try

    End Sub

    Private Sub GroupBox4_Enter(sender As Object, e As EventArgs) Handles GroupBox4.Enter

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim clsOCOB As New clsEntradaAlmacen
            Dim dvOC As DataView = clsOCOB.ListarHistorialFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date, RetornarAlmacenErick(nombre_usurio)).DefaultView
            DataGridView3.DataSource = dvOC
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub ToolStripTextBox4_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox4.TextChanged
        Try
            BindingSource1.Filter = String.Format("CONVERT(nombre_producto+codigoBarra, System.String) LIKE '%{0}%'", ToolStripTextBox4.Text)
            DataGridView1.DataSource = BindingSource1

        Catch ex As Exception

        End Try
        ' .RowFilter = String.Format("CONVERT(lote+nombre_producto+id_producto, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
        ' DataGridView1.DataSource = dv
    End Sub

    Private Sub ToolStripButton10_Click(sender As Object, e As EventArgs) Handles ToolStripButton10.Click
        cargarData()
        cargarInventarioNoRecibido()
    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs) Handles TxtCantidadD.TextChanged

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        cargarDataOrdenAutomatica()
    End Sub
    Public Sub cargarDataOrdenAutomatica()
        Try
            Dim clsA As New clsInventario
            Dim dvOC As DataView = clsA.ListarInventarioOrdenAutomatica(TextBox4.Text).DefaultView

            BindingSource4.DataSource = dvOC
            BindingNavigator5.BindingSource = BindingSource4
            DataGridView4.DataSource = BindingSource4

            ' sumarData1()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        'ACTUALIZAR LA TABLA DE ORDEN AUTOMATICA
        'CARGAR EL INVENTARIO 
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If validarGuardar("realzar una devolucion del producto " + txtProductod.Text + " ") = "1" Then
            If txtProductod.Text = "" Then
                MsgBox("Debe seleccionar un producto")
                Exit Sub
            End If
            If TxtCantidadD.Text = "" Then
                MsgBox("Debe agregar la cantidad a devolver")
                Exit Sub
            End If

            If TxtCantidadD.Text > contador_existencias_devolver Then
                MsgBox("No puede devolver cantidades mayores a su inventario")
                Exit Sub
            End If
            Try
                If SalidaDevolucion() = "1" Then
                    MsgBox(mensaje_registro)
                    cargarData()
                    TxtCantidadD.Clear()
                    txtProductod.Clear()
                    RichTextBox1.Clear()
                    Button4.Enabled = False
                End If



            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ToolStripButton9_Click(sender As Object, e As EventArgs) Handles ToolStripButton9.Click
        GridAExcel_global(DataGridView1)
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            cod_id_producto2 = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtProductod.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            contador_existencias_devolver = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            Button4.Enabled = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FillByid_almacenToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.HistorialSucursalesTableAdapter.FillByid_almacen(Me.Slm_testDataSet9.historialSucursales)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub cargarInventarioNoRecibido()
        'crear funcion para recuperar el usuario del almacen
        Dim usuario As String
        usuario = RetornarAlmacenErick(nombre_usurio)

        Try
            'datagridview
            Dim TableUM As New DataTable
            Dim clsP As New clsEntradaAlmacen
            TableUM.Load(clsP.ListarTrasladosIdAlmacen(usuario))
            BindingSource2.DataSource = TableUM

            DataGridView5.DataSource = BindingSource2
        Catch ex As Exception

        End Try
    End Sub

    Public Sub cargarInventarioNoRecibidoAutomatico()
        'crear funcion para recuperar el usuario del almacen
        Dim usuario As String
        usuario = RetornarAlmacenErick(nombre_usurio)

        Try
            'datagridview
            Dim TableUM As New DataTable
            Dim clsP As New clsEntradaAlmacen
            TableUM.Load(clsP.ListarTrasladosIdAlmacen(usuario))
            BindingSource4.DataSource = TableUM

            DataGridView4.DataSource = BindingSource2
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Recibir_Click(sender As Object, e As EventArgs) Handles Recibir.Click
        If validarGuardar("aceptar producto a su inventario") = "1" Then


            Dim clsI As New clsEntradaAlmacen
            With clsI
                .Id_entrada1 = txtIdEntradaRecibe.Text
                .Codigobarra1 = codigoBarra
                .CantidadProducto = tctCantidadRecibe.Text
                .IdAlmacen = nuevo_alma

            End With

            If clsI.AceptarTraslado() = "1" Then
                MsgBox("Producto Aceptado")
                cargarData()
                cargarInventarioNoRecibido()
            End If
        End If
    End Sub

    Public Function SalidaDevolucion() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmDevolucionStock"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "almacen_devuelve"
        sqlpar.Value = RetornarAlmacenErick(nombre_usurio)
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_producto"
        sqlpar.Value = cod_id_producto2
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "stock"
        sqlpar.Value = TxtCantidadD.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "comentario"
        sqlpar.Value = RichTextBox1.Text
        sqlcom.Parameters.Add(sqlpar)





        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

End Class