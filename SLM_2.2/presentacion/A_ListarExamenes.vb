﻿Public Class A_ListarExamenes

    Dim item As New ClsItemExamen
    Dim fila As DataRow
    Private Sub A_ListarExamenes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        Try
            alternarColoFilasDatagridview(dtExamenes)
            dtExamenes.DataSource = item.ConsultaParaPromociones

            dtExamenes.Columns("codItemExa").Visible = False
            dtExamenes.Columns("codInterno").HeaderText = "Código"
            dtExamenes.Columns("descripcion").HeaderText = "Descripción"
            dtExamenes.Columns("precio").HeaderText = "Precio PG"

        Catch ex As Exception

        End Try

    End Sub

    'Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
    '    Try
    '        Dim i, rows As Integer
    '        rows = dtExamenes.Rows.Count - 1

    '        For i = 0 To rows
    '            If dtExamenes.Rows(i).Cells(0).Value = True Then
    '                'Agregar colección de exámenes

    '                If (A_Promociones.validarDetalle(dtExamenes.Rows(i).Cells(1).Value)) = 0 Then
    '                    A_Promociones.dtDetallePromo.Rows.Add(dtExamenes.Rows(i).Cells(1).Value, dtExamenes.Rows(i).Cells(2).Value, dtExamenes.Rows(i).Cells(3).Value, dtExamenes.Rows(i).Cells(4).Value, 0, dtExamenes.Rows(i).Cells(4).Value)
    '                End If

    '            End If
    '        Next
    '        A_Promociones.CalcularTotal()
    '        Me.Close()
    '    Catch ex As Exception

    '    End Try


    'End Sub






    Private Sub txtbusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtbusqueda.TextChanged

        Try
            Dim dv As New DataView
            Dim dt As New DataTable

            dt = dtExamenes.DataSource

            dv = dt.DefaultView
            dv.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", txtbusqueda.Text)

            If dv.Count = "0" Then
                MsgBox("No existe el item facturable.", MsgBoxStyle.Exclamation)
                txtbusqueda.Text = ""
                Me.Refresh()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub dtExamenes_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtExamenes.CellMouseDoubleClick

        Try

            Dim code As Integer = Integer.Parse(dtExamenes.Rows(e.RowIndex).Cells(0).Value)

            Dim n As String = MsgBox("¿Desea agregar el examen a la factura?", MsgBoxStyle.YesNo, "Validación")

            If n = vbYes And A_Promociones.validarExamen(code) = 0 Then
                MsgBox("entra")
                A_Promociones.dtDetallePromo.Rows.Add(New String() {dtExamenes.Rows(e.RowIndex).Cells(0).Value, dtExamenes.Rows(e.RowIndex).Cells(1).Value, dtExamenes.Rows(e.RowIndex).Cells(2).Value, dtExamenes.Rows(e.RowIndex).Cells(3).Value, 0, dtExamenes.Rows(e.RowIndex).Cells(3).Value})
                A_Promociones.CalcularTotal()
                'Me.Close()
            Else
                MsgBox("El examen ya a sido registrado en la factura.", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class