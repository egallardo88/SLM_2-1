﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class E_EspecificarHojaTrabajo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(E_EspecificarHojaTrabajo))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblCodeSubArea = New System.Windows.Forms.Label()
        Me.lblCodeSucursal = New System.Windows.Forms.Label()
        Me.btnSubarea = New System.Windows.Forms.Button()
        Me.btnSucursal = New System.Windows.Forms.Button()
        Me.txtSubArea = New System.Windows.Forms.TextBox()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.btnAbrir = New System.Windows.Forms.Button()
        Me.txtDescripcionSubArea = New System.Windows.Forms.TextBox()
        Me.txtDescripcionSucursal = New System.Windows.Forms.TextBox()
        Me.lblCodigoGrupo = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 95)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Subárea"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 136)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Sucursal"
        '
        'lblCodeSubArea
        '
        Me.lblCodeSubArea.AutoSize = True
        Me.lblCodeSubArea.Location = New System.Drawing.Point(22, 81)
        Me.lblCodeSubArea.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodeSubArea.Name = "lblCodeSubArea"
        Me.lblCodeSubArea.Size = New System.Drawing.Size(39, 13)
        Me.lblCodeSubArea.TabIndex = 3
        Me.lblCodeSubArea.Text = "Label4"
        Me.lblCodeSubArea.Visible = False
        '
        'lblCodeSucursal
        '
        Me.lblCodeSucursal.AutoSize = True
        Me.lblCodeSucursal.Location = New System.Drawing.Point(18, 122)
        Me.lblCodeSucursal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodeSucursal.Name = "lblCodeSucursal"
        Me.lblCodeSucursal.Size = New System.Drawing.Size(39, 13)
        Me.lblCodeSucursal.TabIndex = 5
        Me.lblCodeSucursal.Text = "Label6"
        Me.lblCodeSucursal.Visible = False
        '
        'btnSubarea
        '
        Me.btnSubarea.BackColor = System.Drawing.Color.Transparent
        Me.btnSubarea.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnSubarea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSubarea.FlatAppearance.BorderSize = 0
        Me.btnSubarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubarea.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubarea.Location = New System.Drawing.Point(89, 92)
        Me.btnSubarea.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSubarea.Name = "btnSubarea"
        Me.btnSubarea.Size = New System.Drawing.Size(24, 18)
        Me.btnSubarea.TabIndex = 118
        Me.btnSubarea.Text = "..."
        Me.btnSubarea.UseVisualStyleBackColor = False
        '
        'btnSucursal
        '
        Me.btnSucursal.BackColor = System.Drawing.Color.Transparent
        Me.btnSucursal.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnSucursal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSucursal.FlatAppearance.BorderSize = 0
        Me.btnSucursal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSucursal.Location = New System.Drawing.Point(89, 132)
        Me.btnSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSucursal.Name = "btnSucursal"
        Me.btnSucursal.Size = New System.Drawing.Size(24, 18)
        Me.btnSucursal.TabIndex = 120
        Me.btnSucursal.Text = "..."
        Me.btnSucursal.UseVisualStyleBackColor = False
        '
        'txtSubArea
        '
        Me.txtSubArea.Location = New System.Drawing.Point(117, 91)
        Me.txtSubArea.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSubArea.MaxLength = 20
        Me.txtSubArea.Name = "txtSubArea"
        Me.txtSubArea.Size = New System.Drawing.Size(89, 20)
        Me.txtSubArea.TabIndex = 121
        Me.txtSubArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSucursal
        '
        Me.txtSucursal.Location = New System.Drawing.Point(117, 132)
        Me.txtSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSucursal.MaxLength = 50
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(89, 20)
        Me.txtSucursal.TabIndex = 123
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnAbrir
        '
        Me.btnAbrir.BackColor = System.Drawing.Color.LimeGreen
        Me.btnAbrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAbrir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbrir.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnAbrir.Location = New System.Drawing.Point(117, 176)
        Me.btnAbrir.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAbrir.Name = "btnAbrir"
        Me.btnAbrir.Size = New System.Drawing.Size(264, 28)
        Me.btnAbrir.TabIndex = 130
        Me.btnAbrir.Text = "Ingresar al laboratorio"
        Me.btnAbrir.UseVisualStyleBackColor = False
        '
        'txtDescripcionSubArea
        '
        Me.txtDescripcionSubArea.Location = New System.Drawing.Point(209, 91)
        Me.txtDescripcionSubArea.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDescripcionSubArea.Name = "txtDescripcionSubArea"
        Me.txtDescripcionSubArea.ReadOnly = True
        Me.txtDescripcionSubArea.Size = New System.Drawing.Size(172, 20)
        Me.txtDescripcionSubArea.TabIndex = 133
        '
        'txtDescripcionSucursal
        '
        Me.txtDescripcionSucursal.Location = New System.Drawing.Point(209, 132)
        Me.txtDescripcionSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDescripcionSucursal.Name = "txtDescripcionSucursal"
        Me.txtDescripcionSucursal.ReadOnly = True
        Me.txtDescripcionSucursal.Size = New System.Drawing.Size(172, 20)
        Me.txtDescripcionSucursal.TabIndex = 135
        '
        'lblCodigoGrupo
        '
        Me.lblCodigoGrupo.AutoSize = True
        Me.lblCodigoGrupo.Location = New System.Drawing.Point(18, 64)
        Me.lblCodigoGrupo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodigoGrupo.Name = "lblCodigoGrupo"
        Me.lblCodigoGrupo.Size = New System.Drawing.Size(39, 13)
        Me.lblCodigoGrupo.TabIndex = 136
        Me.lblCodigoGrupo.Text = "Label7"
        Me.lblCodigoGrupo.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 214)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(466, 22)
        Me.StatusStrip1.TabIndex = 137
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(0, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(466, 48)
        Me.Panel1.TabIndex = 138
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.SLM_2._2.My.Resources.Resources.temporal
        Me.PictureBox1.Location = New System.Drawing.Point(174, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(87, 41)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'E_EspecificarHojaTrabajo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(466, 236)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.lblCodigoGrupo)
        Me.Controls.Add(Me.txtDescripcionSucursal)
        Me.Controls.Add(Me.txtDescripcionSubArea)
        Me.Controls.Add(Me.btnAbrir)
        Me.Controls.Add(Me.txtSucursal)
        Me.Controls.Add(Me.txtSubArea)
        Me.Controls.Add(Me.btnSucursal)
        Me.Controls.Add(Me.btnSubarea)
        Me.Controls.Add(Me.lblCodeSucursal)
        Me.Controls.Add(Me.lblCodeSubArea)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "E_EspecificarHojaTrabajo"
        Me.Text = "Especificación Hoja de Trabajo"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblCodeSubArea As Label
    Friend WithEvents lblCodeSucursal As Label
    Friend WithEvents btnSubarea As Button
    Friend WithEvents btnSucursal As Button
    Friend WithEvents txtSubArea As TextBox
    Friend WithEvents txtSucursal As TextBox
    Friend WithEvents btnAbrir As Button
    Friend WithEvents txtDescripcionSubArea As TextBox
    Friend WithEvents txtDescripcionSucursal As TextBox
    Friend WithEvents lblCodigoGrupo As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox1 As PictureBox
End Class
