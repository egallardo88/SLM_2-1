﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class M_ListadoResultadosParametro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M_ListadoResultadosParametro))
        Me.dgvResultados = New System.Windows.Forms.DataGridView()
        Me.gbxgenero = New System.Windows.Forms.GroupBox()
        Me.rbtnMantener = New System.Windows.Forms.RadioButton()
        Me.rbtnReemplazar = New System.Windows.Forms.RadioButton()
        CType(Me.dgvResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxgenero.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvResultados
        '
        Me.dgvResultados.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvResultados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResultados.Location = New System.Drawing.Point(12, 51)
        Me.dgvResultados.Name = "dgvResultados"
        Me.dgvResultados.Size = New System.Drawing.Size(296, 225)
        Me.dgvResultados.TabIndex = 0
        '
        'gbxgenero
        '
        Me.gbxgenero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxgenero.BackColor = System.Drawing.Color.White
        Me.gbxgenero.Controls.Add(Me.rbtnMantener)
        Me.gbxgenero.Controls.Add(Me.rbtnReemplazar)
        Me.gbxgenero.Location = New System.Drawing.Point(12, 6)
        Me.gbxgenero.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Name = "gbxgenero"
        Me.gbxgenero.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Size = New System.Drawing.Size(297, 40)
        Me.gbxgenero.TabIndex = 156
        Me.gbxgenero.TabStop = False
        Me.gbxgenero.Text = "¿Desea mantener o reemplazar el resultado?"
        '
        'rbtnMantener
        '
        Me.rbtnMantener.AutoSize = True
        Me.rbtnMantener.Checked = True
        Me.rbtnMantener.Location = New System.Drawing.Point(39, 16)
        Me.rbtnMantener.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnMantener.Name = "rbtnMantener"
        Me.rbtnMantener.Size = New System.Drawing.Size(70, 17)
        Me.rbtnMantener.TabIndex = 15
        Me.rbtnMantener.TabStop = True
        Me.rbtnMantener.Text = "Mantener"
        Me.rbtnMantener.UseVisualStyleBackColor = True
        '
        'rbtnReemplazar
        '
        Me.rbtnReemplazar.AutoSize = True
        Me.rbtnReemplazar.Location = New System.Drawing.Point(179, 16)
        Me.rbtnReemplazar.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnReemplazar.Name = "rbtnReemplazar"
        Me.rbtnReemplazar.Size = New System.Drawing.Size(81, 17)
        Me.rbtnReemplazar.TabIndex = 16
        Me.rbtnReemplazar.Text = "Reemplazar"
        Me.rbtnReemplazar.UseVisualStyleBackColor = True
        '
        'M_ListadoResultadosParametro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(320, 288)
        Me.Controls.Add(Me.gbxgenero)
        Me.Controls.Add(Me.dgvResultados)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "M_ListadoResultadosParametro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SLM - Módulo Laboratorio"
        CType(Me.dgvResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxgenero.ResumeLayout(False)
        Me.gbxgenero.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvResultados As DataGridView
    Friend WithEvents gbxgenero As GroupBox
    Friend WithEvents rbtnMantener As RadioButton
    Friend WithEvents rbtnReemplazar As RadioButton
End Class
