﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class M_FacturaEmpresarial
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M_FacturaEmpresarial))
        Me.txtTarjeta = New System.Windows.Forms.TextBox()
        Me.lblPromocion = New System.Windows.Forms.Label()
        Me.tbpExamenes = New System.Windows.Forms.TabPage()
        Me.dgblistadoExamenes = New System.Windows.Forms.DataGridView()
        Me.codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEntrega = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Subtotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codeDetFact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codeItemExam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_centrocosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.disponibles = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgbObservaciones = New System.Windows.Forms.DataGridView()
        Me.tbpObservaciones = New System.Windows.Forms.TabPage()
        Me.txtDeposito = New System.Windows.Forms.TextBox()
        Me.txtsubtotal = New System.Windows.Forms.TextBox()
        Me.txtPorcentaje = New System.Windows.Forms.TextBox()
        Me.lblSubtotal = New System.Windows.Forms.Label()
        Me.txtvuelto2 = New System.Windows.Forms.TextBox()
        Me.txtCheque = New System.Windows.Forms.TextBox()
        Me.txtTransferencia = New System.Windows.Forms.TextBox()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.lblVuelto2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.lblvuelto = New System.Windows.Forms.Label()
        Me.txttotal = New System.Windows.Forms.TextBox()
        Me.txtvuelto = New System.Windows.Forms.TextBox()
        Me.lblpagoPaciente = New System.Windows.Forms.Label()
        Me.cbxAnular = New System.Windows.Forms.CheckBox()
        Me.txtpagoPaciente = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnestadoFactura = New System.Windows.Forms.Button()
        Me.btnimprimirComprobante = New System.Windows.Forms.Button()
        Me.btnentregarExamen = New System.Windows.Forms.Button()
        Me.btnbusquedaExamen = New System.Windows.Forms.Button()
        Me.btnmuestrasPendientes = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.chkSegundoOK = New System.Windows.Forms.CheckBox()
        Me.txtEfectivo = New System.Windows.Forms.TextBox()
        Me.lblEfectivo = New System.Windows.Forms.Label()
        Me.lblTarjeta = New System.Windows.Forms.Label()
        Me.lblCheque = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblTransferencia = New System.Windows.Forms.Label()
        Me.cbxok = New System.Windows.Forms.CheckBox()
        Me.txtNombreCajero = New System.Windows.Forms.TextBox()
        Me.txtNombreRecepcionista = New System.Windows.Forms.TextBox()
        Me.txtCodigoBreveMaquina = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtnombrePaciente = New System.Windows.Forms.TextBox()
        Me.lbltipo = New System.Windows.Forms.Label()
        Me.lblPaciente = New System.Windows.Forms.Label()
        Me.btnBuscarPaciente = New System.Windows.Forms.Button()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.txtcodigoTerminal = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkCola = New System.Windows.Forms.CheckBox()
        Me.cbxentregarMedico = New System.Windows.Forms.CheckBox()
        Me.cbxentregarPaciente = New System.Windows.Forms.CheckBox()
        Me.cbxenviarCorreo = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblcliente = New System.Windows.Forms.Label()
        Me.lblmedico = New System.Windows.Forms.Label()
        Me.txtdescripcionTermino = New System.Windows.Forms.TextBox()
        Me.lblterminosPago = New System.Windows.Forms.Label()
        Me.txtcodigoCajero = New System.Windows.Forms.TextBox()
        Me.btnbuscarSucursal = New System.Windows.Forms.Button()
        Me.txtcodigoRecepecionista = New System.Windows.Forms.TextBox()
        Me.txtnombreSucursal = New System.Windows.Forms.TextBox()
        Me.dtpfechaFactura = New System.Windows.Forms.DateTimePicker()
        Me.txtnombreMedico = New System.Windows.Forms.TextBox()
        Me.btnbuscarTerminosPago = New System.Windows.Forms.Button()
        Me.lblfechaVto = New System.Windows.Forms.Label()
        Me.btnbuscarSede = New System.Windows.Forms.Button()
        Me.txtnombreCliente = New System.Windows.Forms.TextBox()
        Me.btnbuscarCliente = New System.Windows.Forms.Button()
        Me.txtnumeroOficial = New System.Windows.Forms.TextBox()
        Me.txtcodigoTerminosPago = New System.Windows.Forms.TextBox()
        Me.txtcodigoSede = New System.Windows.Forms.TextBox()
        Me.lblpoliza = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtpfechaVto = New System.Windows.Forms.DateTimePicker()
        Me.txtnumeroPoliza = New System.Windows.Forms.TextBox()
        Me.txtnombreSede = New System.Windows.Forms.TextBox()
        Me.txtcodigoMedico = New System.Windows.Forms.TextBox()
        Me.txtcodigoSucursal = New System.Windows.Forms.TextBox()
        Me.btnbuscarMedico = New System.Windows.Forms.Button()
        Me.txtcodigoCliente = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtnumeroFactura = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblcodigo = New System.Windows.Forms.Label()
        Me.lblcodEmpresa = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnsalir = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnImprimir = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnguardar = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnActualizar = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnNuevaCotizacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnnueva = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaEmpresarialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnGenerarOrdenTrabajo = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.lblRTN = New System.Windows.Forms.Label()
        Me.lblOKAY = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgbObservaciones2 = New System.Windows.Forms.DataGridView()
        Me.tbpMuestra = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.lblFechaNacimiento = New System.Windows.Forms.Label()
        Me.lblcodePriceList = New System.Windows.Forms.Label()
        Me.lblcodeTerminoPago = New System.Windows.Forms.Label()
        Me.lblcodeSucursal = New System.Windows.Forms.Label()
        Me.tbpExamenes.SuspendLayout()
        CType(Me.dgblistadoExamenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgbObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgbObservaciones2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpMuestra.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTarjeta
        '
        Me.txtTarjeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTarjeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarjeta.Location = New System.Drawing.Point(97, 47)
        Me.txtTarjeta.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTarjeta.MaxLength = 20
        Me.txtTarjeta.Name = "txtTarjeta"
        Me.txtTarjeta.Size = New System.Drawing.Size(140, 19)
        Me.txtTarjeta.TabIndex = 102
        Me.txtTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblPromocion
        '
        Me.lblPromocion.AutoSize = True
        Me.lblPromocion.Location = New System.Drawing.Point(372, 8)
        Me.lblPromocion.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPromocion.Name = "lblPromocion"
        Me.lblPromocion.Size = New System.Drawing.Size(13, 13)
        Me.lblPromocion.TabIndex = 105
        Me.lblPromocion.Text = "0"
        Me.lblPromocion.Visible = False
        '
        'tbpExamenes
        '
        Me.tbpExamenes.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.tbpExamenes.Controls.Add(Me.lblPromocion)
        Me.tbpExamenes.Controls.Add(Me.dgblistadoExamenes)
        Me.tbpExamenes.Controls.Add(Me.MenuStrip2)
        Me.tbpExamenes.Location = New System.Drawing.Point(4, 22)
        Me.tbpExamenes.Margin = New System.Windows.Forms.Padding(2)
        Me.tbpExamenes.Name = "tbpExamenes"
        Me.tbpExamenes.Padding = New System.Windows.Forms.Padding(2)
        Me.tbpExamenes.Size = New System.Drawing.Size(976, 233)
        Me.tbpExamenes.TabIndex = 0
        Me.tbpExamenes.Text = "Exámenes"
        '
        'dgblistadoExamenes
        '
        Me.dgblistadoExamenes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgblistadoExamenes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgblistadoExamenes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgblistadoExamenes.BackgroundColor = System.Drawing.Color.White
        Me.dgblistadoExamenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgblistadoExamenes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.codigo, Me.Cantidad, Me.Precio, Me.Descripcion, Me.FechaEntrega, Me.Descuento, Me.Subtotal, Me.subArea, Me.codeDetFact, Me.codeItemExam, Me.id_centrocosto, Me.disponibles})
        Me.dgblistadoExamenes.Location = New System.Drawing.Point(5, 28)
        Me.dgblistadoExamenes.Margin = New System.Windows.Forms.Padding(2)
        Me.dgblistadoExamenes.Name = "dgblistadoExamenes"
        Me.dgblistadoExamenes.RowHeadersVisible = False
        Me.dgblistadoExamenes.RowHeadersWidth = 51
        Me.dgblistadoExamenes.RowTemplate.Height = 24
        Me.dgblistadoExamenes.Size = New System.Drawing.Size(964, 201)
        Me.dgblistadoExamenes.TabIndex = 0
        '
        'codigo
        '
        Me.codigo.FillWeight = 61.71429!
        Me.codigo.HeaderText = "Código"
        Me.codigo.MinimumWidth = 6
        Me.codigo.Name = "codigo"
        '
        'Cantidad
        '
        Me.Cantidad.FillWeight = 78.78874!
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.MinimumWidth = 6
        Me.Cantidad.Name = "Cantidad"
        '
        'Precio
        '
        Me.Precio.FillWeight = 70.54861!
        Me.Precio.HeaderText = "Precio"
        Me.Precio.MinimumWidth = 6
        Me.Precio.Name = "Precio"
        '
        'Descripcion
        '
        Me.Descripcion.FillWeight = 110.2328!
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.MinimumWidth = 100
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'FechaEntrega
        '
        Me.FechaEntrega.FillWeight = 122.8278!
        Me.FechaEntrega.HeaderText = "Fecha Entrega"
        Me.FechaEntrega.MinimumWidth = 6
        Me.FechaEntrega.Name = "FechaEntrega"
        Me.FechaEntrega.ReadOnly = True
        '
        'Descuento
        '
        Me.Descuento.FillWeight = 111.2212!
        Me.Descuento.HeaderText = "Descuento"
        Me.Descuento.MinimumWidth = 6
        Me.Descuento.Name = "Descuento"
        '
        'Subtotal
        '
        Me.Subtotal.FillWeight = 97.66737!
        Me.Subtotal.HeaderText = "Subtotal"
        Me.Subtotal.MinimumWidth = 6
        Me.Subtotal.Name = "Subtotal"
        Me.Subtotal.ReadOnly = True
        '
        'subArea
        '
        Me.subArea.HeaderText = "subArea"
        Me.subArea.MinimumWidth = 6
        Me.subArea.Name = "subArea"
        Me.subArea.Visible = False
        '
        'codeDetFact
        '
        Me.codeDetFact.HeaderText = "codeDetFact"
        Me.codeDetFact.MinimumWidth = 6
        Me.codeDetFact.Name = "codeDetFact"
        Me.codeDetFact.Visible = False
        '
        'codeItemExam
        '
        Me.codeItemExam.FillWeight = 146.9992!
        Me.codeItemExam.HeaderText = "codeItemExam"
        Me.codeItemExam.MinimumWidth = 6
        Me.codeItemExam.Name = "codeItemExam"
        Me.codeItemExam.Visible = False
        '
        'id_centrocosto
        '
        Me.id_centrocosto.HeaderText = "id_centrocosto"
        Me.id_centrocosto.MinimumWidth = 6
        Me.id_centrocosto.Name = "id_centrocosto"
        Me.id_centrocosto.Visible = False
        '
        'disponibles
        '
        Me.disponibles.HeaderText = "Pruebas Realizadas"
        Me.disponibles.Name = "disponibles"
        '
        'MenuStrip2
        '
        Me.MenuStrip2.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.MenuStrip2.Location = New System.Drawing.Point(2, 2)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Padding = New System.Windows.Forms.Padding(4, 1, 0, 1)
        Me.MenuStrip2.Size = New System.Drawing.Size(972, 24)
        Me.MenuStrip2.TabIndex = 106
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "codeDetFact"
        Me.DataGridViewTextBoxColumn9.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.FillWeight = 176.1905!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Observación"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.FillWeight = 23.80952!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Código Examen"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'dgbObservaciones
        '
        Me.dgbObservaciones.AllowUserToAddRows = False
        Me.dgbObservaciones.AllowUserToDeleteRows = False
        Me.dgbObservaciones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgbObservaciones.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgbObservaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgbObservaciones.BackgroundColor = System.Drawing.Color.White
        Me.dgbObservaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbObservaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn9})
        Me.dgbObservaciones.Location = New System.Drawing.Point(4, 4)
        Me.dgbObservaciones.Margin = New System.Windows.Forms.Padding(2)
        Me.dgbObservaciones.Name = "dgbObservaciones"
        Me.dgbObservaciones.RowHeadersVisible = False
        Me.dgbObservaciones.RowHeadersWidth = 51
        Me.dgbObservaciones.RowTemplate.Height = 24
        Me.dgbObservaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgbObservaciones.Size = New System.Drawing.Size(968, 227)
        Me.dgbObservaciones.TabIndex = 1
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.tbpObservaciones.Controls.Add(Me.dgbObservaciones)
        Me.tbpObservaciones.Location = New System.Drawing.Point(4, 22)
        Me.tbpObservaciones.Margin = New System.Windows.Forms.Padding(2)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Padding = New System.Windows.Forms.Padding(2)
        Me.tbpObservaciones.Size = New System.Drawing.Size(976, 233)
        Me.tbpObservaciones.TabIndex = 1
        Me.tbpObservaciones.Text = "Nota Técnico"
        '
        'txtDeposito
        '
        Me.txtDeposito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDeposito.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeposito.Location = New System.Drawing.Point(97, 70)
        Me.txtDeposito.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDeposito.MaxLength = 20
        Me.txtDeposito.Name = "txtDeposito"
        Me.txtDeposito.Size = New System.Drawing.Size(140, 19)
        Me.txtDeposito.TabIndex = 108
        Me.txtDeposito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtsubtotal
        '
        Me.txtsubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsubtotal.Location = New System.Drawing.Point(115, 71)
        Me.txtsubtotal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtsubtotal.Name = "txtsubtotal"
        Me.txtsubtotal.ReadOnly = True
        Me.txtsubtotal.Size = New System.Drawing.Size(108, 19)
        Me.txtsubtotal.TabIndex = 115
        Me.txtsubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPorcentaje
        '
        Me.txtPorcentaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcentaje.Location = New System.Drawing.Point(115, 48)
        Me.txtPorcentaje.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPorcentaje.Name = "txtPorcentaje"
        Me.txtPorcentaje.ReadOnly = True
        Me.txtPorcentaje.Size = New System.Drawing.Size(108, 19)
        Me.txtPorcentaje.TabIndex = 117
        Me.txtPorcentaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubtotal.Location = New System.Drawing.Point(65, 74)
        Me.lblSubtotal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(46, 13)
        Me.lblSubtotal.TabIndex = 114
        Me.lblSubtotal.Text = "Subtotal"
        '
        'txtvuelto2
        '
        Me.txtvuelto2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtvuelto2.Location = New System.Drawing.Point(115, 23)
        Me.txtvuelto2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtvuelto2.Name = "txtvuelto2"
        Me.txtvuelto2.ReadOnly = True
        Me.txtvuelto2.Size = New System.Drawing.Size(108, 19)
        Me.txtvuelto2.TabIndex = 119
        Me.txtvuelto2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCheque
        '
        Me.txtCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCheque.Location = New System.Drawing.Point(97, 116)
        Me.txtCheque.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCheque.MaxLength = 20
        Me.txtCheque.Name = "txtCheque"
        Me.txtCheque.Size = New System.Drawing.Size(140, 19)
        Me.txtCheque.TabIndex = 112
        Me.txtCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTransferencia
        '
        Me.txtTransferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransferencia.Location = New System.Drawing.Point(97, 93)
        Me.txtTransferencia.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTransferencia.MaxLength = 20
        Me.txtTransferencia.Name = "txtTransferencia"
        Me.txtTransferencia.Size = New System.Drawing.Size(140, 19)
        Me.txtTransferencia.TabIndex = 110
        Me.txtTransferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPorcentaje.Location = New System.Drawing.Point(13, 51)
        Me.lblPorcentaje.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(98, 13)
        Me.lblPorcentaje.TabIndex = 116
        Me.lblPorcentaje.Text = "Porcentaje a Pagar"
        '
        'lblVuelto2
        '
        Me.lblVuelto2.AutoSize = True
        Me.lblVuelto2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVuelto2.Location = New System.Drawing.Point(74, 29)
        Me.lblVuelto2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblVuelto2.Name = "lblVuelto2"
        Me.lblVuelto2.Size = New System.Drawing.Size(37, 13)
        Me.lblVuelto2.TabIndex = 118
        Me.lblVuelto2.Text = "Vuelto"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel2.Controls.Add(Me.txtsubtotal)
        Me.Panel2.Controls.Add(Me.lbltotal)
        Me.Panel2.Controls.Add(Me.lblvuelto)
        Me.Panel2.Controls.Add(Me.txttotal)
        Me.Panel2.Controls.Add(Me.txtvuelto)
        Me.Panel2.Controls.Add(Me.lblpagoPaciente)
        Me.Panel2.Controls.Add(Me.txtPorcentaje)
        Me.Panel2.Controls.Add(Me.lblSubtotal)
        Me.Panel2.Controls.Add(Me.txtvuelto2)
        Me.Panel2.Controls.Add(Me.cbxAnular)
        Me.Panel2.Controls.Add(Me.lblPorcentaje)
        Me.Panel2.Controls.Add(Me.txtpagoPaciente)
        Me.Panel2.Controls.Add(Me.lblVuelto2)
        Me.Panel2.Location = New System.Drawing.Point(261, 14)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(714, 108)
        Me.Panel2.TabIndex = 120
        '
        'lbltotal
        '
        Me.lbltotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal.Location = New System.Drawing.Point(506, 88)
        Me.lbltotal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(44, 16)
        Me.lbltotal.TabIndex = 38
        Me.lbltotal.Text = "Total"
        '
        'lblvuelto
        '
        Me.lblvuelto.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblvuelto.AutoSize = True
        Me.lblvuelto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblvuelto.Location = New System.Drawing.Point(531, 57)
        Me.lblvuelto.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblvuelto.Name = "lblvuelto"
        Me.lblvuelto.Size = New System.Drawing.Size(52, 16)
        Me.lblvuelto.TabIndex = 37
        Me.lblvuelto.Text = "Vuelto"
        '
        'txttotal
        '
        Me.txttotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttotal.BackColor = System.Drawing.Color.Khaki
        Me.txttotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttotal.ForeColor = System.Drawing.Color.OrangeRed
        Me.txttotal.Location = New System.Drawing.Point(558, 82)
        Me.txttotal.Margin = New System.Windows.Forms.Padding(2)
        Me.txttotal.Name = "txttotal"
        Me.txttotal.ReadOnly = True
        Me.txttotal.Size = New System.Drawing.Size(147, 22)
        Me.txttotal.TabIndex = 84
        Me.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtvuelto
        '
        Me.txtvuelto.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtvuelto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtvuelto.Location = New System.Drawing.Point(597, 51)
        Me.txtvuelto.Margin = New System.Windows.Forms.Padding(2)
        Me.txtvuelto.Name = "txtvuelto"
        Me.txtvuelto.ReadOnly = True
        Me.txtvuelto.Size = New System.Drawing.Size(108, 22)
        Me.txtvuelto.TabIndex = 65
        Me.txtvuelto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblpagoPaciente
        '
        Me.lblpagoPaciente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblpagoPaciente.AutoSize = True
        Me.lblpagoPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpagoPaciente.Location = New System.Drawing.Point(420, 24)
        Me.lblpagoPaciente.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblpagoPaciente.Name = "lblpagoPaciente"
        Me.lblpagoPaciente.Size = New System.Drawing.Size(173, 16)
        Me.lblpagoPaciente.TabIndex = 36
        Me.lblpagoPaciente.Text = "A Pagar por el Paciente"
        '
        'cbxAnular
        '
        Me.cbxAnular.AutoSize = True
        Me.cbxAnular.Location = New System.Drawing.Point(12, 4)
        Me.cbxAnular.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxAnular.Name = "cbxAnular"
        Me.cbxAnular.Size = New System.Drawing.Size(95, 17)
        Me.cbxAnular.TabIndex = 107
        Me.cbxAnular.Text = "Anular Factura"
        Me.cbxAnular.UseVisualStyleBackColor = True
        '
        'txtpagoPaciente
        '
        Me.txtpagoPaciente.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtpagoPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpagoPaciente.Location = New System.Drawing.Point(597, 18)
        Me.txtpagoPaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtpagoPaciente.MaxLength = 20
        Me.txtpagoPaciente.Name = "txtpagoPaciente"
        Me.txtpagoPaciente.ReadOnly = True
        Me.txtpagoPaciente.Size = New System.Drawing.Size(108, 22)
        Me.txtpagoPaciente.TabIndex = 83
        Me.txtpagoPaciente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox3.Controls.Add(Me.btnestadoFactura)
        Me.GroupBox3.Controls.Add(Me.btnimprimirComprobante)
        Me.GroupBox3.Controls.Add(Me.btnentregarExamen)
        Me.GroupBox3.Controls.Add(Me.btnbusquedaExamen)
        Me.GroupBox3.Controls.Add(Me.btnmuestrasPendientes)
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox3.Location = New System.Drawing.Point(11, 476)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Size = New System.Drawing.Size(980, 162)
        Me.GroupBox3.TabIndex = 138
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de pago"
        '
        'btnestadoFactura
        '
        Me.btnestadoFactura.BackColor = System.Drawing.Color.Yellow
        Me.btnestadoFactura.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnestadoFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnestadoFactura.Location = New System.Drawing.Point(262, 127)
        Me.btnestadoFactura.Margin = New System.Windows.Forms.Padding(2)
        Me.btnestadoFactura.Name = "btnestadoFactura"
        Me.btnestadoFactura.Size = New System.Drawing.Size(94, 29)
        Me.btnestadoFactura.TabIndex = 88
        Me.btnestadoFactura.Text = "Estado Factura"
        Me.btnestadoFactura.UseVisualStyleBackColor = False
        '
        'btnimprimirComprobante
        '
        Me.btnimprimirComprobante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnimprimirComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnimprimirComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnimprimirComprobante.Location = New System.Drawing.Point(615, 127)
        Me.btnimprimirComprobante.Margin = New System.Windows.Forms.Padding(2)
        Me.btnimprimirComprobante.Name = "btnimprimirComprobante"
        Me.btnimprimirComprobante.Size = New System.Drawing.Size(121, 29)
        Me.btnimprimirComprobante.TabIndex = 70
        Me.btnimprimirComprobante.Text = "Imprimir Comprobante Examen"
        Me.btnimprimirComprobante.UseVisualStyleBackColor = False
        '
        'btnentregarExamen
        '
        Me.btnentregarExamen.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnentregarExamen.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnentregarExamen.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnentregarExamen.Location = New System.Drawing.Point(740, 127)
        Me.btnentregarExamen.Margin = New System.Windows.Forms.Padding(2)
        Me.btnentregarExamen.Name = "btnentregarExamen"
        Me.btnentregarExamen.Size = New System.Drawing.Size(111, 29)
        Me.btnentregarExamen.TabIndex = 85
        Me.btnentregarExamen.Text = "Entregar Examen"
        Me.btnentregarExamen.UseVisualStyleBackColor = False
        '
        'btnbusquedaExamen
        '
        Me.btnbusquedaExamen.BackColor = System.Drawing.Color.Yellow
        Me.btnbusquedaExamen.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnbusquedaExamen.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbusquedaExamen.Location = New System.Drawing.Point(490, 127)
        Me.btnbusquedaExamen.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbusquedaExamen.Name = "btnbusquedaExamen"
        Me.btnbusquedaExamen.Size = New System.Drawing.Size(121, 29)
        Me.btnbusquedaExamen.TabIndex = 86
        Me.btnbusquedaExamen.Text = "Búsqueda de Examen"
        Me.btnbusquedaExamen.UseVisualStyleBackColor = False
        '
        'btnmuestrasPendientes
        '
        Me.btnmuestrasPendientes.BackColor = System.Drawing.Color.Yellow
        Me.btnmuestrasPendientes.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnmuestrasPendientes.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnmuestrasPendientes.Location = New System.Drawing.Point(360, 127)
        Me.btnmuestrasPendientes.Margin = New System.Windows.Forms.Padding(2)
        Me.btnmuestrasPendientes.Name = "btnmuestrasPendientes"
        Me.btnmuestrasPendientes.Size = New System.Drawing.Size(126, 29)
        Me.btnmuestrasPendientes.TabIndex = 87
        Me.btnmuestrasPendientes.Text = "Muestras Pendientes"
        Me.btnmuestrasPendientes.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.Panel3.Controls.Add(Me.chkSegundoOK)
        Me.Panel3.Controls.Add(Me.txtCheque)
        Me.Panel3.Controls.Add(Me.txtEfectivo)
        Me.Panel3.Controls.Add(Me.txtTransferencia)
        Me.Panel3.Controls.Add(Me.txtTarjeta)
        Me.Panel3.Controls.Add(Me.txtDeposito)
        Me.Panel3.Controls.Add(Me.lblEfectivo)
        Me.Panel3.Controls.Add(Me.lblTarjeta)
        Me.Panel3.Controls.Add(Me.lblCheque)
        Me.Panel3.Controls.Add(Me.lblDeposito)
        Me.Panel3.Controls.Add(Me.lblTransferencia)
        Me.Panel3.Controls.Add(Me.cbxok)
        Me.Panel3.Location = New System.Drawing.Point(10, 14)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(246, 142)
        Me.Panel3.TabIndex = 121
        '
        'chkSegundoOK
        '
        Me.chkSegundoOK.AutoSize = True
        Me.chkSegundoOK.Location = New System.Drawing.Point(97, 3)
        Me.chkSegundoOK.Margin = New System.Windows.Forms.Padding(2)
        Me.chkSegundoOK.Name = "chkSegundoOK"
        Me.chkSegundoOK.Size = New System.Drawing.Size(93, 17)
        Me.chkSegundoOK.TabIndex = 114
        Me.chkSegundoOK.Text = "Cerrar Factura"
        Me.chkSegundoOK.UseVisualStyleBackColor = True
        '
        'txtEfectivo
        '
        Me.txtEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEfectivo.Location = New System.Drawing.Point(97, 24)
        Me.txtEfectivo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEfectivo.MaxLength = 20
        Me.txtEfectivo.Name = "txtEfectivo"
        Me.txtEfectivo.Size = New System.Drawing.Size(140, 19)
        Me.txtEfectivo.TabIndex = 103
        Me.txtEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEfectivo
        '
        Me.lblEfectivo.AutoSize = True
        Me.lblEfectivo.Location = New System.Drawing.Point(9, 27)
        Me.lblEfectivo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblEfectivo.Name = "lblEfectivo"
        Me.lblEfectivo.Size = New System.Drawing.Size(46, 13)
        Me.lblEfectivo.TabIndex = 104
        Me.lblEfectivo.Text = "Efectivo"
        '
        'lblTarjeta
        '
        Me.lblTarjeta.AutoSize = True
        Me.lblTarjeta.Location = New System.Drawing.Point(9, 50)
        Me.lblTarjeta.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTarjeta.Name = "lblTarjeta"
        Me.lblTarjeta.Size = New System.Drawing.Size(40, 13)
        Me.lblTarjeta.TabIndex = 105
        Me.lblTarjeta.Text = "Tarjeta"
        '
        'lblCheque
        '
        Me.lblCheque.AutoSize = True
        Me.lblCheque.Location = New System.Drawing.Point(9, 119)
        Me.lblCheque.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(44, 13)
        Me.lblCheque.TabIndex = 113
        Me.lblCheque.Text = "Cheque"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(9, 73)
        Me.lblDeposito.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(49, 13)
        Me.lblDeposito.TabIndex = 109
        Me.lblDeposito.Text = "Depósito"
        '
        'lblTransferencia
        '
        Me.lblTransferencia.AutoSize = True
        Me.lblTransferencia.Location = New System.Drawing.Point(9, 96)
        Me.lblTransferencia.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTransferencia.Name = "lblTransferencia"
        Me.lblTransferencia.Size = New System.Drawing.Size(72, 13)
        Me.lblTransferencia.TabIndex = 111
        Me.lblTransferencia.Text = "Transferencia"
        '
        'cbxok
        '
        Me.cbxok.AutoSize = True
        Me.cbxok.Enabled = False
        Me.cbxok.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxok.Location = New System.Drawing.Point(12, 5)
        Me.cbxok.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxok.Name = "cbxok"
        Me.cbxok.Size = New System.Drawing.Size(41, 17)
        Me.cbxok.TabIndex = 67
        Me.cbxok.Text = "OK"
        Me.cbxok.UseVisualStyleBackColor = True
        '
        'txtNombreCajero
        '
        Me.txtNombreCajero.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtNombreCajero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreCajero.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreCajero.Location = New System.Drawing.Point(622, 63)
        Me.txtNombreCajero.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNombreCajero.Name = "txtNombreCajero"
        Me.txtNombreCajero.ReadOnly = True
        Me.txtNombreCajero.Size = New System.Drawing.Size(178, 19)
        Me.txtNombreCajero.TabIndex = 112
        Me.txtNombreCajero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombreRecepcionista
        '
        Me.txtNombreRecepcionista.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtNombreRecepcionista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreRecepcionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreRecepcionista.Location = New System.Drawing.Point(622, 40)
        Me.txtNombreRecepcionista.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNombreRecepcionista.Name = "txtNombreRecepcionista"
        Me.txtNombreRecepcionista.ReadOnly = True
        Me.txtNombreRecepcionista.Size = New System.Drawing.Size(178, 19)
        Me.txtNombreRecepcionista.TabIndex = 111
        Me.txtNombreRecepcionista.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCodigoBreveMaquina
        '
        Me.txtCodigoBreveMaquina.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtCodigoBreveMaquina.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoBreveMaquina.Location = New System.Drawing.Point(619, 139)
        Me.txtCodigoBreveMaquina.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCodigoBreveMaquina.Name = "txtCodigoBreveMaquina"
        Me.txtCodigoBreveMaquina.ReadOnly = True
        Me.txtCodigoBreveMaquina.Size = New System.Drawing.Size(180, 19)
        Me.txtCodigoBreveMaquina.TabIndex = 50
        Me.txtCodigoBreveMaquina.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox2.Controls.Add(Me.txtnombrePaciente)
        Me.GroupBox2.Controls.Add(Me.lbltipo)
        Me.GroupBox2.Controls.Add(Me.lblPaciente)
        Me.GroupBox2.Controls.Add(Me.btnBuscarPaciente)
        Me.GroupBox2.Controls.Add(Me.txtPaciente)
        Me.GroupBox2.Controls.Add(Me.txtNombreCajero)
        Me.GroupBox2.Controls.Add(Me.txtNombreRecepcionista)
        Me.GroupBox2.Controls.Add(Me.txtCodigoBreveMaquina)
        Me.GroupBox2.Controls.Add(Me.txtcodigoTerminal)
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.lblcliente)
        Me.GroupBox2.Controls.Add(Me.lblmedico)
        Me.GroupBox2.Controls.Add(Me.txtdescripcionTermino)
        Me.GroupBox2.Controls.Add(Me.lblterminosPago)
        Me.GroupBox2.Controls.Add(Me.txtcodigoCajero)
        Me.GroupBox2.Controls.Add(Me.btnbuscarSucursal)
        Me.GroupBox2.Controls.Add(Me.txtcodigoRecepecionista)
        Me.GroupBox2.Controls.Add(Me.txtnombreSucursal)
        Me.GroupBox2.Controls.Add(Me.dtpfechaFactura)
        Me.GroupBox2.Controls.Add(Me.txtnombreMedico)
        Me.GroupBox2.Controls.Add(Me.btnbuscarTerminosPago)
        Me.GroupBox2.Controls.Add(Me.lblfechaVto)
        Me.GroupBox2.Controls.Add(Me.btnbuscarSede)
        Me.GroupBox2.Controls.Add(Me.txtnombreCliente)
        Me.GroupBox2.Controls.Add(Me.btnbuscarCliente)
        Me.GroupBox2.Controls.Add(Me.txtnumeroOficial)
        Me.GroupBox2.Controls.Add(Me.txtcodigoTerminosPago)
        Me.GroupBox2.Controls.Add(Me.txtcodigoSede)
        Me.GroupBox2.Controls.Add(Me.lblpoliza)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.dtpfechaVto)
        Me.GroupBox2.Controls.Add(Me.txtnumeroPoliza)
        Me.GroupBox2.Controls.Add(Me.txtnombreSede)
        Me.GroupBox2.Controls.Add(Me.txtcodigoMedico)
        Me.GroupBox2.Controls.Add(Me.txtcodigoSucursal)
        Me.GroupBox2.Controls.Add(Me.btnbuscarMedico)
        Me.GroupBox2.Controls.Add(Me.txtcodigoCliente)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtnumeroFactura)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.lblcodigo)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 40)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(979, 169)
        Me.GroupBox2.TabIndex = 137
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detalle Cliente"
        '
        'txtnombrePaciente
        '
        Me.txtnombrePaciente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombrePaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombrePaciente.Location = New System.Drawing.Point(298, 63)
        Me.txtnombrePaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombrePaciente.Name = "txtnombrePaciente"
        Me.txtnombrePaciente.ReadOnly = True
        Me.txtnombrePaciente.Size = New System.Drawing.Size(236, 19)
        Me.txtnombrePaciente.TabIndex = 137
        '
        'lbltipo
        '
        Me.lbltipo.AutoSize = True
        Me.lbltipo.Location = New System.Drawing.Point(471, -15)
        Me.lbltipo.Name = "lbltipo"
        Me.lbltipo.Size = New System.Drawing.Size(24, 13)
        Me.lbltipo.TabIndex = 136
        Me.lbltipo.Text = "tipo"
        '
        'lblPaciente
        '
        Me.lblPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaciente.Location = New System.Drawing.Point(27, 67)
        Me.lblPaciente.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(49, 13)
        Me.lblPaciente.TabIndex = 133
        Me.lblPaciente.Text = "Paciente"
        '
        'btnBuscarPaciente
        '
        Me.btnBuscarPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnBuscarPaciente.BackColor = System.Drawing.Color.Transparent
        Me.btnBuscarPaciente.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnBuscarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnBuscarPaciente.FlatAppearance.BorderSize = 0
        Me.btnBuscarPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarPaciente.Location = New System.Drawing.Point(87, 61)
        Me.btnBuscarPaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscarPaciente.Name = "btnBuscarPaciente"
        Me.btnBuscarPaciente.Size = New System.Drawing.Size(26, 18)
        Me.btnBuscarPaciente.TabIndex = 134
        Me.btnBuscarPaciente.Text = "..."
        Me.btnBuscarPaciente.UseVisualStyleBackColor = False
        '
        'txtPaciente
        '
        Me.txtPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaciente.Location = New System.Drawing.Point(120, 63)
        Me.txtPaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaciente.MaxLength = 20
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.Size = New System.Drawing.Size(157, 19)
        Me.txtPaciente.TabIndex = 135
        Me.txtPaciente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodigoTerminal
        '
        Me.txtcodigoTerminal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoTerminal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoTerminal.Location = New System.Drawing.Point(803, 139)
        Me.txtcodigoTerminal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoTerminal.Name = "txtcodigoTerminal"
        Me.txtcodigoTerminal.ReadOnly = True
        Me.txtcodigoTerminal.Size = New System.Drawing.Size(103, 19)
        Me.txtcodigoTerminal.TabIndex = 49
        Me.txtcodigoTerminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcodigoTerminal.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.chkCola)
        Me.GroupBox1.Controls.Add(Me.cbxentregarMedico)
        Me.GroupBox1.Controls.Add(Me.cbxentregarPaciente)
        Me.GroupBox1.Controls.Add(Me.cbxenviarCorreo)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(813, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(152, 114)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Resultados"
        '
        'chkCola
        '
        Me.chkCola.AutoSize = True
        Me.chkCola.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCola.Location = New System.Drawing.Point(13, 87)
        Me.chkCola.Margin = New System.Windows.Forms.Padding(2)
        Me.chkCola.Name = "chkCola"
        Me.chkCola.Size = New System.Drawing.Size(120, 19)
        Me.chkCola.TabIndex = 64
        Me.chkCola.Text = "Agregar a la Cola"
        Me.chkCola.UseVisualStyleBackColor = True
        '
        'cbxentregarMedico
        '
        Me.cbxentregarMedico.AutoSize = True
        Me.cbxentregarMedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxentregarMedico.Location = New System.Drawing.Point(13, 19)
        Me.cbxentregarMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxentregarMedico.Name = "cbxentregarMedico"
        Me.cbxentregarMedico.Size = New System.Drawing.Size(127, 19)
        Me.cbxentregarMedico.TabIndex = 50
        Me.cbxentregarMedico.Text = "Entregar a Médico"
        Me.cbxentregarMedico.UseVisualStyleBackColor = True
        '
        'cbxentregarPaciente
        '
        Me.cbxentregarPaciente.AutoSize = True
        Me.cbxentregarPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxentregarPaciente.Location = New System.Drawing.Point(13, 42)
        Me.cbxentregarPaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxentregarPaciente.Name = "cbxentregarPaciente"
        Me.cbxentregarPaciente.Size = New System.Drawing.Size(134, 19)
        Me.cbxentregarPaciente.TabIndex = 62
        Me.cbxentregarPaciente.Text = "Entregar a Paciente"
        Me.cbxentregarPaciente.UseVisualStyleBackColor = True
        '
        'cbxenviarCorreo
        '
        Me.cbxenviarCorreo.AutoSize = True
        Me.cbxenviarCorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxenviarCorreo.Location = New System.Drawing.Point(13, 64)
        Me.cbxenviarCorreo.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxenviarCorreo.Name = "cbxenviarCorreo"
        Me.cbxenviarCorreo.Size = New System.Drawing.Size(121, 19)
        Me.cbxenviarCorreo.TabIndex = 63
        Me.cbxenviarCorreo.Text = "Enviar por Correo"
        Me.cbxenviarCorreo.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(537, 142)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 13)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "Terminal"
        '
        'lblcliente
        '
        Me.lblcliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblcliente.AutoSize = True
        Me.lblcliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcliente.Location = New System.Drawing.Point(27, 44)
        Me.lblcliente.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcliente.Name = "lblcliente"
        Me.lblcliente.Size = New System.Drawing.Size(48, 13)
        Me.lblcliente.TabIndex = 21
        Me.lblcliente.Text = "Empresa"
        '
        'lblmedico
        '
        Me.lblmedico.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblmedico.AutoSize = True
        Me.lblmedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmedico.Location = New System.Drawing.Point(27, 92)
        Me.lblmedico.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblmedico.Name = "lblmedico"
        Me.lblmedico.Size = New System.Drawing.Size(42, 13)
        Me.lblmedico.TabIndex = 22
        Me.lblmedico.Text = "Médico"
        '
        'txtdescripcionTermino
        '
        Me.txtdescripcionTermino.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtdescripcionTermino.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdescripcionTermino.Location = New System.Drawing.Point(299, 114)
        Me.txtdescripcionTermino.Margin = New System.Windows.Forms.Padding(2)
        Me.txtdescripcionTermino.Name = "txtdescripcionTermino"
        Me.txtdescripcionTermino.ReadOnly = True
        Me.txtdescripcionTermino.Size = New System.Drawing.Size(235, 19)
        Me.txtdescripcionTermino.TabIndex = 132
        '
        'lblterminosPago
        '
        Me.lblterminosPago.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblterminosPago.AutoSize = True
        Me.lblterminosPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblterminosPago.Location = New System.Drawing.Point(4, 117)
        Me.lblterminosPago.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblterminosPago.Name = "lblterminosPago"
        Me.lblterminosPago.Size = New System.Drawing.Size(62, 13)
        Me.lblterminosPago.TabIndex = 25
        Me.lblterminosPago.Text = "Term. Pago"
        '
        'txtcodigoCajero
        '
        Me.txtcodigoCajero.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoCajero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcodigoCajero.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoCajero.Location = New System.Drawing.Point(621, 66)
        Me.txtcodigoCajero.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoCajero.Name = "txtcodigoCajero"
        Me.txtcodigoCajero.Size = New System.Drawing.Size(30, 19)
        Me.txtcodigoCajero.TabIndex = 128
        Me.txtcodigoCajero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcodigoCajero.Visible = False
        '
        'btnbuscarSucursal
        '
        Me.btnbuscarSucursal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarSucursal.BackColor = System.Drawing.Color.Transparent
        Me.btnbuscarSucursal.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnbuscarSucursal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarSucursal.FlatAppearance.BorderSize = 0
        Me.btnbuscarSucursal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarSucursal.Location = New System.Drawing.Point(589, 114)
        Me.btnbuscarSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbuscarSucursal.Name = "btnbuscarSucursal"
        Me.btnbuscarSucursal.Size = New System.Drawing.Size(25, 18)
        Me.btnbuscarSucursal.TabIndex = 92
        Me.btnbuscarSucursal.Text = "..."
        Me.btnbuscarSucursal.UseVisualStyleBackColor = False
        '
        'txtcodigoRecepecionista
        '
        Me.txtcodigoRecepecionista.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoRecepecionista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcodigoRecepecionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoRecepecionista.Location = New System.Drawing.Point(621, 42)
        Me.txtcodigoRecepecionista.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoRecepecionista.Name = "txtcodigoRecepecionista"
        Me.txtcodigoRecepecionista.Size = New System.Drawing.Size(30, 19)
        Me.txtcodigoRecepecionista.TabIndex = 127
        Me.txtcodigoRecepecionista.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcodigoRecepecionista.Visible = False
        '
        'txtnombreSucursal
        '
        Me.txtnombreSucursal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombreSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombreSucursal.Location = New System.Drawing.Point(697, 113)
        Me.txtnombreSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreSucursal.Name = "txtnombreSucursal"
        Me.txtnombreSucursal.ReadOnly = True
        Me.txtnombreSucursal.Size = New System.Drawing.Size(103, 19)
        Me.txtnombreSucursal.TabIndex = 94
        '
        'dtpfechaFactura
        '
        Me.dtpfechaFactura.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.dtpfechaFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpfechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpfechaFactura.Location = New System.Drawing.Point(621, 16)
        Me.dtpfechaFactura.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpfechaFactura.Name = "dtpfechaFactura"
        Me.dtpfechaFactura.Size = New System.Drawing.Size(178, 19)
        Me.dtpfechaFactura.TabIndex = 124
        '
        'txtnombreMedico
        '
        Me.txtnombreMedico.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombreMedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombreMedico.Location = New System.Drawing.Point(299, 88)
        Me.txtnombreMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreMedico.Name = "txtnombreMedico"
        Me.txtnombreMedico.ReadOnly = True
        Me.txtnombreMedico.Size = New System.Drawing.Size(235, 19)
        Me.txtnombreMedico.TabIndex = 129
        '
        'btnbuscarTerminosPago
        '
        Me.btnbuscarTerminosPago.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarTerminosPago.BackColor = System.Drawing.Color.Transparent
        Me.btnbuscarTerminosPago.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnbuscarTerminosPago.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarTerminosPago.FlatAppearance.BorderSize = 0
        Me.btnbuscarTerminosPago.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarTerminosPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarTerminosPago.Location = New System.Drawing.Point(87, 113)
        Me.btnbuscarTerminosPago.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbuscarTerminosPago.Name = "btnbuscarTerminosPago"
        Me.btnbuscarTerminosPago.Size = New System.Drawing.Size(26, 18)
        Me.btnbuscarTerminosPago.TabIndex = 131
        Me.btnbuscarTerminosPago.Text = "..."
        Me.btnbuscarTerminosPago.UseVisualStyleBackColor = False
        '
        'lblfechaVto
        '
        Me.lblfechaVto.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblfechaVto.AutoSize = True
        Me.lblfechaVto.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfechaVto.Location = New System.Drawing.Point(5, 139)
        Me.lblfechaVto.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblfechaVto.Name = "lblfechaVto"
        Me.lblfechaVto.Size = New System.Drawing.Size(59, 13)
        Me.lblfechaVto.TabIndex = 27
        Me.lblfechaVto.Text = "Fecha Vto."
        '
        'btnbuscarSede
        '
        Me.btnbuscarSede.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarSede.BackColor = System.Drawing.Color.Transparent
        Me.btnbuscarSede.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnbuscarSede.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarSede.FlatAppearance.BorderSize = 0
        Me.btnbuscarSede.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarSede.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarSede.Location = New System.Drawing.Point(589, 91)
        Me.btnbuscarSede.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbuscarSede.Name = "btnbuscarSede"
        Me.btnbuscarSede.Size = New System.Drawing.Size(26, 18)
        Me.btnbuscarSede.TabIndex = 80
        Me.btnbuscarSede.Text = "..."
        Me.btnbuscarSede.UseVisualStyleBackColor = False
        '
        'txtnombreCliente
        '
        Me.txtnombreCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombreCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombreCliente.Location = New System.Drawing.Point(298, 41)
        Me.txtnombreCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreCliente.Name = "txtnombreCliente"
        Me.txtnombreCliente.ReadOnly = True
        Me.txtnombreCliente.Size = New System.Drawing.Size(236, 19)
        Me.txtnombreCliente.TabIndex = 122
        '
        'btnbuscarCliente
        '
        Me.btnbuscarCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarCliente.BackColor = System.Drawing.Color.Transparent
        Me.btnbuscarCliente.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnbuscarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarCliente.FlatAppearance.BorderSize = 0
        Me.btnbuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarCliente.Location = New System.Drawing.Point(87, 38)
        Me.btnbuscarCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbuscarCliente.Name = "btnbuscarCliente"
        Me.btnbuscarCliente.Size = New System.Drawing.Size(26, 18)
        Me.btnbuscarCliente.TabIndex = 118
        Me.btnbuscarCliente.Text = "..."
        Me.btnbuscarCliente.UseVisualStyleBackColor = False
        '
        'txtnumeroOficial
        '
        Me.txtnumeroOficial.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnumeroOficial.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnumeroOficial.Location = New System.Drawing.Point(362, 17)
        Me.txtnumeroOficial.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnumeroOficial.Name = "txtnumeroOficial"
        Me.txtnumeroOficial.ReadOnly = True
        Me.txtnumeroOficial.Size = New System.Drawing.Size(173, 19)
        Me.txtnumeroOficial.TabIndex = 120
        Me.txtnumeroOficial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodigoTerminosPago
        '
        Me.txtcodigoTerminosPago.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoTerminosPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcodigoTerminosPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoTerminosPago.Location = New System.Drawing.Point(119, 113)
        Me.txtcodigoTerminosPago.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoTerminosPago.MaxLength = 20
        Me.txtcodigoTerminosPago.Name = "txtcodigoTerminosPago"
        Me.txtcodigoTerminosPago.Size = New System.Drawing.Size(158, 19)
        Me.txtcodigoTerminosPago.TabIndex = 130
        Me.txtcodigoTerminosPago.Text = "CO"
        Me.txtcodigoTerminosPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodigoSede
        '
        Me.txtcodigoSede.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoSede.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoSede.Location = New System.Drawing.Point(619, 90)
        Me.txtcodigoSede.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoSede.MaxLength = 20
        Me.txtcodigoSede.Name = "txtcodigoSede"
        Me.txtcodigoSede.Size = New System.Drawing.Size(74, 19)
        Me.txtcodigoSede.TabIndex = 60
        Me.txtcodigoSede.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblpoliza
        '
        Me.lblpoliza.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblpoliza.AutoSize = True
        Me.lblpoliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpoliza.Location = New System.Drawing.Point(298, 138)
        Me.lblpoliza.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblpoliza.Name = "lblpoliza"
        Me.lblpoliza.Size = New System.Drawing.Size(58, 13)
        Me.lblpoliza.TabIndex = 32
        Me.lblpoliza.Text = "Nro. Poliza"
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(553, 96)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(32, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Sede"
        '
        'dtpfechaVto
        '
        Me.dtpfechaVto.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.dtpfechaVto.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpfechaVto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpfechaVto.Location = New System.Drawing.Point(70, 136)
        Me.dtpfechaVto.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpfechaVto.Name = "dtpfechaVto"
        Me.dtpfechaVto.Size = New System.Drawing.Size(208, 19)
        Me.dtpfechaVto.TabIndex = 125
        '
        'txtnumeroPoliza
        '
        Me.txtnumeroPoliza.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnumeroPoliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnumeroPoliza.Location = New System.Drawing.Point(360, 136)
        Me.txtnumeroPoliza.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnumeroPoliza.Name = "txtnumeroPoliza"
        Me.txtnumeroPoliza.Size = New System.Drawing.Size(176, 19)
        Me.txtnumeroPoliza.TabIndex = 45
        Me.txtnumeroPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnombreSede
        '
        Me.txtnombreSede.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnombreSede.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombreSede.Location = New System.Drawing.Point(697, 90)
        Me.txtnombreSede.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreSede.Name = "txtnombreSede"
        Me.txtnombreSede.ReadOnly = True
        Me.txtnombreSede.Size = New System.Drawing.Size(103, 19)
        Me.txtnombreSede.TabIndex = 77
        '
        'txtcodigoMedico
        '
        Me.txtcodigoMedico.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoMedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoMedico.Location = New System.Drawing.Point(119, 88)
        Me.txtcodigoMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoMedico.MaxLength = 20
        Me.txtcodigoMedico.Name = "txtcodigoMedico"
        Me.txtcodigoMedico.Size = New System.Drawing.Size(158, 19)
        Me.txtcodigoMedico.TabIndex = 123
        Me.txtcodigoMedico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodigoSucursal
        '
        Me.txtcodigoSucursal.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcodigoSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoSucursal.Location = New System.Drawing.Point(619, 113)
        Me.txtcodigoSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoSucursal.MaxLength = 20
        Me.txtcodigoSucursal.Name = "txtcodigoSucursal"
        Me.txtcodigoSucursal.Size = New System.Drawing.Size(74, 19)
        Me.txtcodigoSucursal.TabIndex = 53
        Me.txtcodigoSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnbuscarMedico
        '
        Me.btnbuscarMedico.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnbuscarMedico.BackColor = System.Drawing.Color.Transparent
        Me.btnbuscarMedico.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnbuscarMedico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnbuscarMedico.FlatAppearance.BorderSize = 0
        Me.btnbuscarMedico.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnbuscarMedico.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscarMedico.Location = New System.Drawing.Point(87, 85)
        Me.btnbuscarMedico.Margin = New System.Windows.Forms.Padding(2)
        Me.btnbuscarMedico.Name = "btnbuscarMedico"
        Me.btnbuscarMedico.Size = New System.Drawing.Size(26, 18)
        Me.btnbuscarMedico.TabIndex = 81
        Me.btnbuscarMedico.Text = "..."
        Me.btnbuscarMedico.UseVisualStyleBackColor = False
        '
        'txtcodigoCliente
        '
        Me.txtcodigoCliente.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtcodigoCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcodigoCliente.Location = New System.Drawing.Point(120, 40)
        Me.txtcodigoCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoCliente.MaxLength = 20
        Me.txtcodigoCliente.Name = "txtcodigoCliente"
        Me.txtcodigoCliente.Size = New System.Drawing.Size(158, 19)
        Me.txtcodigoCliente.TabIndex = 121
        Me.txtcodigoCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(537, 117)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 13)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "Sucursal"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(299, 20)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Nro. Oficial"
        '
        'txtnumeroFactura
        '
        Me.txtnumeroFactura.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtnumeroFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnumeroFactura.Location = New System.Drawing.Point(70, 18)
        Me.txtnumeroFactura.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnumeroFactura.Name = "txtnumeroFactura"
        Me.txtnumeroFactura.ReadOnly = True
        Me.txtnumeroFactura.Size = New System.Drawing.Size(208, 19)
        Me.txtnumeroFactura.TabIndex = 119
        Me.txtnumeroFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(543, 22)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Fecha Factura"
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(544, 44)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 13)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Recepcionista"
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(581, 69)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Cajero"
        '
        'lblcodigo
        '
        Me.lblcodigo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblcodigo.AutoSize = True
        Me.lblcodigo.BackColor = System.Drawing.Color.Transparent
        Me.lblcodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcodigo.Location = New System.Drawing.Point(36, 22)
        Me.lblcodigo.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodigo.Name = "lblcodigo"
        Me.lblcodigo.Size = New System.Drawing.Size(27, 13)
        Me.lblcodigo.TabIndex = 0
        Me.lblcodigo.Text = "Nro."
        '
        'lblcodEmpresa
        '
        Me.lblcodEmpresa.AutoSize = True
        Me.lblcodEmpresa.Location = New System.Drawing.Point(384, 25)
        Me.lblcodEmpresa.Name = "lblcodEmpresa"
        Me.lblcodEmpresa.Size = New System.Drawing.Size(66, 13)
        Me.lblcodEmpresa.TabIndex = 137
        Me.lblcodEmpresa.Text = "codEmpresa"
        Me.lblcodEmpresa.Visible = False
        '
        'Timer1
        '
        '
        'Button1
        '
        Me.Button1.Name = "Button1"
        Me.Button1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B), System.Windows.Forms.Keys)
        Me.Button1.Size = New System.Drawing.Size(99, 22)
        Me.Button1.Text = "Buscar Examen"
        '
        'btnsalir
        '
        Me.btnsalir.Name = "btnsalir"
        Me.btnsalir.Size = New System.Drawing.Size(41, 22)
        Me.btnsalir.Text = "Salir"
        '
        'btnImprimir
        '
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(65, 22)
        Me.btnImprimir.Text = "Imprimir"
        '
        'btnguardar
        '
        Me.btnguardar.Name = "btnguardar"
        Me.btnguardar.Size = New System.Drawing.Size(61, 22)
        Me.btnguardar.Text = "Guardar"
        '
        'btnActualizar
        '
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(71, 22)
        Me.btnActualizar.Text = "Actualizar"
        '
        'btnNuevaCotizacion
        '
        Me.btnNuevaCotizacion.Name = "btnNuevaCotizacion"
        Me.btnNuevaCotizacion.Size = New System.Drawing.Size(112, 22)
        Me.btnNuevaCotizacion.Text = "Nueva Cotizacion"
        '
        'btnnueva
        '
        Me.btnnueva.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturaEmpresarialToolStripMenuItem, Me.btnGenerarOrdenTrabajo})
        Me.btnnueva.Name = "btnnueva"
        Me.btnnueva.Size = New System.Drawing.Size(95, 22)
        Me.btnnueva.Text = "Nueva Factura"
        '
        'FacturaEmpresarialToolStripMenuItem
        '
        Me.FacturaEmpresarialToolStripMenuItem.Name = "FacturaEmpresarialToolStripMenuItem"
        Me.FacturaEmpresarialToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.FacturaEmpresarialToolStripMenuItem.Text = "Factura Empresarial"
        '
        'btnGenerarOrdenTrabajo
        '
        Me.btnGenerarOrdenTrabajo.Name = "btnGenerarOrdenTrabajo"
        Me.btnGenerarOrdenTrabajo.Size = New System.Drawing.Size(208, 22)
        Me.btnGenerarOrdenTrabajo.Text = "Generar Orden de Trabajo"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnnueva, Me.btnNuevaCotizacion, Me.btnActualizar, Me.btnguardar, Me.btnImprimir, Me.Button1, Me.btnsalir})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 1, 0, 1)
        Me.MenuStrip1.Size = New System.Drawing.Size(1012, 24)
        Me.MenuStrip1.TabIndex = 142
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'lblRTN
        '
        Me.lblRTN.AutoSize = True
        Me.lblRTN.Location = New System.Drawing.Point(216, 25)
        Me.lblRTN.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblRTN.Name = "lblRTN"
        Me.lblRTN.Size = New System.Drawing.Size(35, 13)
        Me.lblRTN.TabIndex = 141
        Me.lblRTN.Text = "label1"
        Me.lblRTN.Visible = False
        '
        'lblOKAY
        '
        Me.lblOKAY.AutoSize = True
        Me.lblOKAY.Location = New System.Drawing.Point(134, 25)
        Me.lblOKAY.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblOKAY.Name = "lblOKAY"
        Me.lblOKAY.Size = New System.Drawing.Size(39, 13)
        Me.lblOKAY.TabIndex = 140
        Me.lblOKAY.Text = "Label1"
        Me.lblOKAY.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "codeDetFact"
        Me.DataGridViewTextBoxColumn5.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.FillWeight = 176.1905!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Observación"
        Me.DataGridViewTextBoxColumn4.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.FillWeight = 23.80952!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Código Examen"
        Me.DataGridViewTextBoxColumn3.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'dgbObservaciones2
        '
        Me.dgbObservaciones2.AllowUserToAddRows = False
        Me.dgbObservaciones2.AllowUserToDeleteRows = False
        Me.dgbObservaciones2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgbObservaciones2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgbObservaciones2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgbObservaciones2.BackgroundColor = System.Drawing.Color.White
        Me.dgbObservaciones2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbObservaciones2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.dgbObservaciones2.Location = New System.Drawing.Point(6, 2)
        Me.dgbObservaciones2.Margin = New System.Windows.Forms.Padding(2)
        Me.dgbObservaciones2.Name = "dgbObservaciones2"
        Me.dgbObservaciones2.RowHeadersVisible = False
        Me.dgbObservaciones2.RowHeadersWidth = 51
        Me.dgbObservaciones2.RowTemplate.Height = 24
        Me.dgbObservaciones2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgbObservaciones2.Size = New System.Drawing.Size(968, 229)
        Me.dgbObservaciones2.TabIndex = 2
        '
        'tbpMuestra
        '
        Me.tbpMuestra.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.tbpMuestra.Controls.Add(Me.dgbObservaciones2)
        Me.tbpMuestra.Location = New System.Drawing.Point(4, 22)
        Me.tbpMuestra.Margin = New System.Windows.Forms.Padding(2)
        Me.tbpMuestra.Name = "tbpMuestra"
        Me.tbpMuestra.Size = New System.Drawing.Size(976, 233)
        Me.tbpMuestra.TabIndex = 2
        Me.tbpMuestra.Text = "Nota Procesamiento Muestra"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tbpExamenes)
        Me.TabControl1.Controls.Add(Me.tbpObservaciones)
        Me.TabControl1.Controls.Add(Me.tbpMuestra)
        Me.TabControl1.Location = New System.Drawing.Point(11, 213)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(984, 259)
        Me.TabControl1.TabIndex = 139
        '
        'lblFechaNacimiento
        '
        Me.lblFechaNacimiento.AutoSize = True
        Me.lblFechaNacimiento.Location = New System.Drawing.Point(76, 25)
        Me.lblFechaNacimiento.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblFechaNacimiento.Name = "lblFechaNacimiento"
        Me.lblFechaNacimiento.Size = New System.Drawing.Size(39, 13)
        Me.lblFechaNacimiento.TabIndex = 136
        Me.lblFechaNacimiento.Text = "Label1"
        Me.lblFechaNacimiento.Visible = False
        '
        'lblcodePriceList
        '
        Me.lblcodePriceList.AutoSize = True
        Me.lblcodePriceList.Location = New System.Drawing.Point(17, 25)
        Me.lblcodePriceList.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodePriceList.Name = "lblcodePriceList"
        Me.lblcodePriceList.Size = New System.Drawing.Size(39, 13)
        Me.lblcodePriceList.TabIndex = 135
        Me.lblcodePriceList.Text = "Label1"
        Me.lblcodePriceList.Visible = False
        '
        'lblcodeTerminoPago
        '
        Me.lblcodeTerminoPago.AutoSize = True
        Me.lblcodeTerminoPago.Location = New System.Drawing.Point(9, 142)
        Me.lblcodeTerminoPago.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeTerminoPago.Name = "lblcodeTerminoPago"
        Me.lblcodeTerminoPago.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeTerminoPago.TabIndex = 134
        Me.lblcodeTerminoPago.Visible = False
        '
        'lblcodeSucursal
        '
        Me.lblcodeSucursal.AutoSize = True
        Me.lblcodeSucursal.Location = New System.Drawing.Point(297, 175)
        Me.lblcodeSucursal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeSucursal.Name = "lblcodeSucursal"
        Me.lblcodeSucursal.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeSucursal.TabIndex = 133
        Me.lblcodeSucursal.Visible = False
        '
        'M_FacturaEmpresarial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1012, 651)
        Me.Controls.Add(Me.lblcodEmpresa)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.lblRTN)
        Me.Controls.Add(Me.lblOKAY)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lblFechaNacimiento)
        Me.Controls.Add(Me.lblcodePriceList)
        Me.Controls.Add(Me.lblcodeTerminoPago)
        Me.Controls.Add(Me.lblcodeSucursal)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "M_FacturaEmpresarial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SLM -  Módulo Facturación"
        Me.tbpExamenes.ResumeLayout(False)
        Me.tbpExamenes.PerformLayout()
        CType(Me.dgblistadoExamenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgbObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgbObservaciones2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpMuestra.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtTarjeta As TextBox
    Friend WithEvents lblPromocion As Label
    Friend WithEvents tbpExamenes As TabPage
    Friend WithEvents dgblistadoExamenes As DataGridView
    Friend WithEvents MenuStrip2 As MenuStrip
    Friend WithEvents DataGridViewTextBoxColumn9 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents dgbObservaciones As DataGridView
    Friend WithEvents tbpObservaciones As TabPage
    Friend WithEvents txtDeposito As TextBox
    Friend WithEvents txtsubtotal As TextBox
    Friend WithEvents txtPorcentaje As TextBox
    Friend WithEvents lblSubtotal As Label
    Friend WithEvents txtvuelto2 As TextBox
    Friend WithEvents txtCheque As TextBox
    Friend WithEvents txtTransferencia As TextBox
    Friend WithEvents lblPorcentaje As Label
    Friend WithEvents lblVuelto2 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents lbltotal As Label
    Friend WithEvents lblvuelto As Label
    Friend WithEvents txttotal As TextBox
    Friend WithEvents txtvuelto As TextBox
    Friend WithEvents lblpagoPaciente As Label
    Friend WithEvents txtpagoPaciente As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnestadoFactura As Button
    Friend WithEvents btnimprimirComprobante As Button
    Friend WithEvents btnentregarExamen As Button
    Friend WithEvents btnbusquedaExamen As Button
    Friend WithEvents btnmuestrasPendientes As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents txtEfectivo As TextBox
    Friend WithEvents lblEfectivo As Label
    Friend WithEvents lblTarjeta As Label
    Friend WithEvents lblCheque As Label
    Friend WithEvents cbxAnular As CheckBox
    Friend WithEvents lblDeposito As Label
    Friend WithEvents lblTransferencia As Label
    Friend WithEvents cbxok As CheckBox
    Friend WithEvents txtNombreCajero As TextBox
    Friend WithEvents txtNombreRecepcionista As TextBox
    Friend WithEvents txtCodigoBreveMaquina As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtcodigoTerminal As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cbxentregarMedico As CheckBox
    Friend WithEvents cbxentregarPaciente As CheckBox
    Friend WithEvents cbxenviarCorreo As CheckBox
    Friend WithEvents Label16 As Label
    Friend WithEvents lblcliente As Label
    Friend WithEvents lblmedico As Label
    Friend WithEvents lblterminosPago As Label
    Friend WithEvents btnbuscarSucursal As Button
    Friend WithEvents txtnombreSucursal As TextBox
    Friend WithEvents lblfechaVto As Label
    Friend WithEvents btnbuscarSede As Button
    Friend WithEvents txtcodigoSede As TextBox
    Friend WithEvents lblpoliza As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtnumeroPoliza As TextBox
    Friend WithEvents txtnombreSede As TextBox
    Friend WithEvents txtcodigoSucursal As TextBox
    Friend WithEvents btnbuscarMedico As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblcodigo As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Button1 As ToolStripMenuItem
    Friend WithEvents btnsalir As ToolStripMenuItem
    Friend WithEvents btnImprimir As ToolStripMenuItem
    Friend WithEvents btnguardar As ToolStripMenuItem
    Friend WithEvents btnActualizar As ToolStripMenuItem
    Friend WithEvents btnNuevaCotizacion As ToolStripMenuItem
    Friend WithEvents btnnueva As ToolStripMenuItem
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents lblRTN As Label
    Friend WithEvents lblOKAY As Label
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents dgbObservaciones2 As DataGridView
    Friend WithEvents tbpMuestra As TabPage
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents lblFechaNacimiento As Label
    Friend WithEvents lblcodePriceList As Label
    Friend WithEvents txtdescripcionTermino As TextBox
    Friend WithEvents lblcodeTerminoPago As Label
    Friend WithEvents lblcodeSucursal As Label
    Friend WithEvents btnbuscarTerminosPago As Button
    Friend WithEvents txtcodigoCajero As TextBox
    Friend WithEvents btnbuscarCliente As Button
    Friend WithEvents txtcodigoTerminosPago As TextBox
    Friend WithEvents txtnombreMedico As TextBox
    Friend WithEvents txtcodigoRecepecionista As TextBox
    Friend WithEvents dtpfechaVto As DateTimePicker
    Friend WithEvents dtpfechaFactura As DateTimePicker
    Friend WithEvents txtcodigoMedico As TextBox
    Friend WithEvents txtnombreCliente As TextBox
    Friend WithEvents txtcodigoCliente As TextBox
    Friend WithEvents txtnumeroOficial As TextBox
    Friend WithEvents txtnumeroFactura As TextBox
    Friend WithEvents lblPaciente As Label
    Friend WithEvents btnBuscarPaciente As Button
    Friend WithEvents txtPaciente As TextBox
    Friend WithEvents chkSegundoOK As CheckBox
    Friend WithEvents lbltipo As Label
    Friend WithEvents chkCola As CheckBox
    Friend WithEvents FacturaEmpresarialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnGenerarOrdenTrabajo As ToolStripMenuItem
    Friend WithEvents lblcodEmpresa As Label
    Friend WithEvents txtnombrePaciente As TextBox
    Friend WithEvents codigo As DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DataGridViewTextBoxColumn
    Friend WithEvents Precio As DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As DataGridViewTextBoxColumn
    Friend WithEvents FechaEntrega As DataGridViewTextBoxColumn
    Friend WithEvents Descuento As DataGridViewTextBoxColumn
    Friend WithEvents Subtotal As DataGridViewTextBoxColumn
    Friend WithEvents subArea As DataGridViewTextBoxColumn
    Friend WithEvents codeDetFact As DataGridViewTextBoxColumn
    Friend WithEvents codeItemExam As DataGridViewTextBoxColumn
    Friend WithEvents id_centrocosto As DataGridViewTextBoxColumn
    Friend WithEvents disponibles As DataGridViewTextBoxColumn
End Class
