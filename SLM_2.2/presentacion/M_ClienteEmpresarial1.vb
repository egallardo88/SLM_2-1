﻿Public Class M_ClienteEmpresarial
    Dim cliente As New ClsClienteEmpresarial
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

            If btnGuardar.Text = "Guardar" Then
                Dim n As String = MsgBox("¿Desea guardar el registro?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then

                    GuardarCliente()

                End If

            ElseIf btnGuardar.Text = "Modificar" Then
                Dim n As String = MsgBox("¿Desea modificar el registro?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then

                    ActualizarRegistro()

                End If

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    'Funcion para listar Clientes Empresariales
    Private Sub CargarData()
        Dim dt As New DataTable
        dt = cliente.listarClienteEmpresarial
        dgvData.DataSource = dt
    End Sub

    'Funcion para guardar Cliente Empresarial
    Private Sub GuardarCliente()
        If txtNombreEmpresa.Text <> "" Then

            With cliente
                'variables
                .nombreEmpresa_ = txtNombreEmpresa.Text
                .RTN_ = txtRTN.Text
                .nombreContacto_ = txtNombreContacto.Text
                .telefono_ = txtTelefono.Text
                .correo_ = txtCorreo.Text

                'Registrar datos
                If .registrarClienteEmpresarial = 1 Then
                    MsgBox("Se registro un nuevo Cliente Empresarial.")
                    CargarData()
                    limpiar()
                    BloquearCampos()


                End If
            End With
        Else
            MsgBox("Debe ingresar el nombre de la empresa.")
        End If
    End Sub

    'Funcion para actualizar registros
    Private Sub ActualizarRegistro()
        If txtNombreEmpresa.Text <> "" Then

            With cliente
                'variables
                .Cod = Integer.Parse(txtCodigo.Text)
                .nombreEmpresa_ = txtNombreEmpresa.Text
                .RTN_ = txtRTN.Text
                .nombreContacto_ = txtNombreContacto.Text
                .telefono_ = txtTelefono.Text
                .correo_ = txtCorreo.Text

                'Registrar datos
                If .modificarClienteEmpresarial = 1 Then
                    MsgBox("Se modifico el Cliente Empresarial.")
                    btnGuardar.Text = "Guardar"
                    CargarData()
                    limpiar()
                    BloquearCampos()
                End If
            End With
        Else
            MsgBox("Debe ingresar el nombre de la empresa.")
        End If
    End Sub

    Private Sub M_ClienteEmpresarial1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            CargarData()
            alternarColoFilasDatagridview(dgvData)
            BloquearCampos()

            If dgvData.Columns.Contains("codigo") = True Then

                dgvData.Columns("codigo").Visible = False
                dgvData.Columns("nombreEmpresa").HeaderText = "Nombre de Empresa"
                dgvData.Columns("nombreContacto").HeaderText = "Nombre de Contacto"
                dgvData.Columns("telefono").HeaderText = "Tel."
                dgvData.Columns("correo").HeaderText = "Correo"

            End If

        Catch ex As Exception

        End Try
    End Sub



    Private Sub dgvData_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvData.CellDoubleClick
        Try

            If lblform.Text = "empresa" Then
                Dim n As String = MsgBox("¿Desea seleccionar el registro?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then

                    M_FacturaEmpresarial.txtcodigoCliente.Text = dgvData.Rows(e.RowIndex).Cells(0).Value
                    Me.Close()

                End If

            Else
                txtCodigo.Text = dgvData.Rows(e.RowIndex).Cells(0).Value
                txtNombreEmpresa.Text = dgvData.Rows(e.RowIndex).Cells(1).Value
                txtRTN.Text = dgvData.Rows(e.RowIndex).Cells(2).Value
                txtNombreContacto.Text = dgvData.Rows(e.RowIndex).Cells(3).Value
                txtTelefono.Text = dgvData.Rows(e.RowIndex).Cells(4).Value
                txtCorreo.Text = dgvData.Rows(e.RowIndex).Cells(5).Value

                btnGuardar.Text = "Modificar"
                DesbloquearCampos()
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        DesbloquearCampos()

    End Sub

    Sub limpiar()
        txtCodigo.Clear()
        txtNombreContacto.Clear()
        txtNombreEmpresa.Clear()
        txtTelefono.Clear()
        txtRTN.Clear()
        txtCorreo.Clear()
    End Sub

    Sub BloquearCampos()
        txtNombreEmpresa.Enabled = False
        txtNombreContacto.Enabled = False
        txtTelefono.Enabled = False
        txtCorreo.Enabled = False
        txtRTN.Enabled = False
    End Sub

    Sub DesbloquearCampos()
        txtNombreEmpresa.Enabled = True
        txtNombreContacto.Enabled = True
        txtTelefono.Enabled = True
        txtCorreo.Enabled = True
        txtRTN.Enabled = True
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        limpiar()
        BloquearCampos()
        btnGuardar.Text = "Guardar"

    End Sub

    Private Sub CrearCotizacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CrearCotizacionToolStripMenuItem.Click

        If lblform.Text = "empresa" And txtCodigo.Text <> "" Then
            M_FacturaEmpresarial.txtcodigoCliente.Text = txtCodigo.Text
            Me.Close()
        End If

    End Sub
End Class