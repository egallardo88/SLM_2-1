﻿Imports System.Data.SqlClient

Public Class E_frmActualizarFechasDomicilio
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        cargarData()
    End Sub
    Private Sub cargarData()

        Try
            'datagridview

            DataGridView1.DataSource = BuscarFactura().DefaultView

        Catch ex As Exception

        End Try
    End Sub
    Public Function BuscarFactura() As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion
        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "E_slmBuscarFactura"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = TextBox2.Text
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    Return dt
                End Using
            End Using
        End Using
    End Function
    Private Sub E_frmActualizarFechasDomicilio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If ActualizarFecha() = "1" Then
                MsgBox(mensaje_actualizacion)
            End If
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al actualizar los registros")
        End Try
        cargarData()
    End Sub
    Public Function ActualizarFecha() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarFechasFactura"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numero" 'nombre del almacen 
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fechaFactura" 'nombre del almacen 
        sqlpar.Value = DateTimePicker1.Value.ToString("yyyyMMdd")
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "vencimiento" 'nombre del almacen 
        sqlpar.Value = DateTimePicker1.Value.ToString("yyyyMMdd")
        sqlcom.Parameters.Add(sqlpar)



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
End Class