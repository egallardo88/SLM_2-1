﻿Public Class HT_DescripcionResultado
    Dim DescripcionResultado As New ClsDescripcionResultado

    Private Sub btnBuscarPlantilla_Click(sender As Object, e As EventArgs) Handles btnBuscarPlantilla.Click
        Try

            'Listar Plantillas de area
            MostrarForm(A_ListadoPlantillas)


        Catch ex As Exception

        End Try
    End Sub


    Private Sub txtCodigoPlantilla_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigoPlantilla.KeyDown
        Try
            Dim plantilla As New ClsPlantillaResultado
            Dim dt As New DataTable
            Dim row As DataRow

            If e.KeyCode = Keys.Enter Then
                plantilla.simbolo_ = Trim(txtCodigoPlantilla.Text)
                dt = plantilla.BuscarPlantillaAreaSimbolo(E_HojaTrabajo.txtArea.Text)
                row = dt.Rows(0)
                rtxtDescripcionResultado.Text = rtxtDescripcionResultado.Text + System.Environment.NewLine + row("descripcion")
                txtCodigoPlantilla.Clear()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            Dim detResult As New ClsDescripcionResultado

            If GuardarToolStripMenuItem.Text = "Guardar" Then

                With detResult

                    .Descripcion_Resultado = rtxtDescripcionResultado.Text
                    .Codigo_OTrabajo = Integer.Parse(txtOrdenTrabajo.Text)
                    .parametro_ = txtParametro.Text

                    If .registrarNuevaDescripcionResultado = 1 Then
                        MsgBox("Se registro la descripcion del resultado correctamente.")
                        E_HojaTrabajo.lblcodDescrip.Text = txtOrdenTrabajo.Text
                    End If

                End With

            ElseIf GuardarToolStripMenuItem.Text = "Modificar" Then

                With detResult

                    .Codigo = Integer.Parse(txtCodigo.Text)
                    .Descripcion_Resultado = rtxtDescripcionResultado.Text
                    .Codigo_OTrabajo = Integer.Parse(txtOrdenTrabajo.Text)
                    .parametro_ = txtParametro.Text


                    If .modificarDescripcionResultado = 1 Then
                        MsgBox("Se modifico la descripcion del resultado correctamente.")
                    End If

                End With

            End If



        Catch ex As Exception

        End Try
    End Sub
    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub
    Private Sub HT_DescripcionResultado_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtCodigoPlantilla_TextChanged(sender As Object, e As EventArgs) Handles txtCodigoPlantilla.TextChanged

    End Sub
End Class