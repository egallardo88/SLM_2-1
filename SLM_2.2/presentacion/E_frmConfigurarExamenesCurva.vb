﻿Imports System.Data.SqlClient

Public Class E_frmConfigurarExamenesCurva
    Dim cod_examen As Integer
    Dim genero_local As String = ""
    Private Sub E_frmConfigurarExamenesCurva_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        cargarData()
    End Sub

    Public Function listarExamenes() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter(" E_slmListarExamenesTubos", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function

    Public Function listarExamenes2() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("E_slmRecuperarexamenesGraficos " + txtCodigo.Text + " ", cn)

            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Sub cargarData()
        Try


            BindingSource1.DataSource = listarExamenes()

            DataGridView1.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar. " + ex.Message)
        End Try
    End Sub
    Public Function RegistrarExamen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCrearExamenGrafica"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_examen"
        sqlpar.Value = txtCodigo.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "parametro"
        sqlpar.Value = txtParametroh.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "correlativo"
        sqlpar.Value = txtCorrela.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "valor_referencia"
        sqlpar.Value = txtValoR.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "unidad"
        sqlpar.Value = txtUnid.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "edad_desde"
        sqlpar.Value = txtEdaddesde.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "edad_hasta"
        sqlpar.Value = txtEdadHasta.Text
        sqlcom.Parameters.Add(sqlpar)

        If RadioHombre.Checked = True Then

            genero_local = "Masculino"
        ElseIf RadioMujer.Checked = True Then
            genero_local = "Femenino"
        End If
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "genero"
        sqlpar.Value = genero_local
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "descripcion"
        sqlpar.Value = txtDescripcion.Text
        sqlcom.Parameters.Add(sqlpar)

        'sqlpar = New SqlParameter
        'sqlpar.ParameterName = "categoria"
        'sqlpar.Value = ComboBox1.Text
        'sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function EliminarParametro(ByVal id As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmEliminarParametroExamenGrafico"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_par"
        sqlpar.Value = id
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs)


    End Sub
    Public Sub limpiar()
        txtParametroh.Clear()
        txtCorrela.Clear()
        txtIdParamet.Clear()
        txtCodigo.Clear()
        txtUnid.Clear()
        txtValoR.Clear()
        txtDescripcion.Clear()
        txtExamen.Clear()
    End Sub
    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            limpiar()
            txtCodigo.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtExamen.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

            cargarData2()
        Catch ex As Exception

        End Try

    End Sub

    Public Sub cargarData2()
        Try


            BindingSource2.DataSource = listarExamenes2()

            DataGridView2.DataSource = BindingSource2


            BindingNavigator2.BindingSource = BindingSource2



        Catch ex As Exception
            MsgBox("Hubo un error al consultar los bancos. " + ex.Message)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            If RegistrarExamen() = "1" Then
                MsgBox(mensaje_registro)
                txtParametroh.Clear()
                txtCorrela.Clear()
                txtIdParamet.Clear()
                txtValoR.Clear()
                txtUnid.Clear()
                txtDescripcion.Clear()
                cargarData2()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            txtIdParamet.Text = DataGridView2.Rows(e.RowIndex).Cells(1).Value
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(0).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(1).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(2).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(3).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(4).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(5).Value)
            'MsgBox(DataGridView2.Rows(e.RowIndex).Cells(6).Value)
            txtParametroh.Text = DataGridView2.Rows(e.RowIndex).Cells(3).Value
            txtCorrela.Text = DataGridView2.Rows(e.RowIndex).Cells(2).Value
            txtDescripcion.Text = DataGridView2.Rows(e.RowIndex).Cells(4).Value

            txtValoR.Text = DataGridView2.Rows(e.RowIndex).Cells(6).Value

            txtUnid.Text = DataGridView2.Rows(e.RowIndex).Cells(5).Value
            txtEdaddesde.Text = DataGridView2.Rows(e.RowIndex).Cells(7).Value
            txtEdadHasta.Text = DataGridView2.Rows(e.RowIndex).Cells(8).Value
            If DataGridView2.Rows(e.RowIndex).Cells(5).Value.ToString = "Hombre" Then
                RadioHombre.Checked = True
                genero_local = "Masculino"
            Else
                RadioMujer.Checked = True
                genero_local = "Femenino"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        Try
            If EliminarParametro(DataGridView2.Rows(e.RowIndex).Cells(1).Value) Then
                MsgBox(mensaje_dar_baja)
                cargarData2()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles TextBox4.TextChanged
        Try

            'BindingSource1.DataSource = listarExamenes()

            'DataGridView1.DataSource = BindingSource1


            'BindingNavigator1.BindingSource = BindingSource1


            Dim dvOC As DataView = listarExamenes().DefaultView


            dvOC.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", TextBox4.Text)
            DataGridView1.DataSource = dvOC
            BindingSource1.DataSource = dvOC
            BindingNavigator1.BindingSource = BindingSource1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        If ActualizarExamen() = "1" Then
            MsgBox(mensaje_actualizacion)
            cargarData2()
        End If
    End Sub

    Public Function ActualizarExamen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarExamenGrafica"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_eg"
        sqlpar.Value = txtIdParamet.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "parametro"
        sqlpar.Value = txtParametroh.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "correlativo"
        sqlpar.Value = txtCorrela.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "valor_referencia"
        sqlpar.Value = txtValoR.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "unidad"
        sqlpar.Value = txtUnid.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "edad_desde"
        sqlpar.Value = txtEdaddesde.Text
        sqlcom.Parameters.Add(sqlpar)

        If RadioHombre.Checked = True Then

            genero_local = "Masculino"
        ElseIf RadioMujer.Checked = True Then
            genero_local = "Femenino"
        End If

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "edad_hasta"
        sqlpar.Value = txtEdadHasta.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "genero"
        sqlpar.Value = genero_local
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "descripcion"
        sqlpar.Value = txtDescripcion.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
End Class