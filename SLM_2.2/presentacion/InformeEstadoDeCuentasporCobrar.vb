﻿Public Class InformeEstadoDeCuentasporCobrar
    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvData)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Try
            M_Categoria.lblform.Text = "EstadodeCuenta"
            M_Categoria.Show()
        Catch ex As Exception

        End Try

    End Sub


    Private Sub llenarConvenios()
        Try
            'MsgBox(lblcodeCategoria.Text)
            'llenar el combobox tipo termino
            Dim objTipoCls As New ClsListaPrecios
            'objTipoCls.codigoCategoriaCliente_ = Integer.Parse(lblcodCategoria.Text)
            Dim dt As New DataTable
            dt = objTipoCls.SeleccionarConvenios()
            cbxListaPrecio.DataSource = dt
            cbxListaPrecio.DisplayMember = "descripcion"
            cbxListaPrecio.ValueMember = "codigo"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Validación")
        End Try
    End Sub

    Private Sub InformeEstadoDeCuentasporCobrar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            llenarConvenios()
            alternarColoFilasDatagridview(dgvData)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkTodo_CheckedChanged(sender As Object, e As EventArgs) Handles chkTodo.CheckedChanged
        Try
            If chkTodo.Checked = True Then

                cbxListaPrecio.Enabled = False

            Else

                cbxListaPrecio.Enabled = True

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarToolStripMenuItem.Click
        Try

            Dim fechadesde, fechahasta As Date
            Dim codCate, codLista As System.Nullable(Of Integer)

            fechadesde = dtpDesde.Value
            fechahasta = dtpHasta.Value



            If lblcodCategoria.Text <> "codCategoria" Then
                codCate = Integer.Parse(lblcodCategoria.Text)
            End If

            If chkTodo.Checked <> True Then
                codLista = cbxListaPrecio.SelectedValue
            End If

            Dim consulta As New ClsListaPrecios
            Dim data As New DataTable

            data = consulta.EstadoCuentasPorCobrar(fechadesde, fechahasta, codCate, codLista)

            dgvData.DataSource = data

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ImprimirReporteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirReporteToolStripMenuItem.Click
        Try




            Dim objReporte As New CuentasPorCobrar



            Dim fechadesde, fechahasta As Date
            Dim codCate, codLista As System.Nullable(Of Integer)

            fechadesde = dtpDesde.Value
            fechahasta = dtpHasta.Value



            If lblcodCategoria.Text <> "codCategoria" Then
                codCate = Integer.Parse(lblcodCategoria.Text)
            End If

            If chkTodo.Checked <> True Then
                codLista = cbxListaPrecio.SelectedValue
            End If

            objReporte.SetParameterValue("@fechaDesde", fechadesde)
            objReporte.SetParameterValue("@fechaHasta", fechahasta)
            objReporte.SetParameterValue("@codigoCategoria", codCate)
            objReporte.SetParameterValue("@codigoConvenio", codLista)

            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            M_ImpresionFacturaEmpresarial.CrystalReportViewer2.ReportSource = objReporte
            M_ImpresionFacturaEmpresarial.Text = "Estado Cuentas por Cobrar"

            M_ImpresionFacturaEmpresarial.Show()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class