﻿Public Class MM_ResultadoPorDefecto
    Dim resultadosarray As New ArrayList
    Private Sub E_DetalleExamenes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub
    Private Sub MM_ResultadoPorDefecto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            alternarColoFilasDatagridview(dgvResultados)

            'Agregar boton de eliminar a grid
            If dgvResultados.Columns.Contains("btnEliminar") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dgvResultados.Columns.Add(btn)
                btn.HeaderText = "Eliminar"
                btn.Text = "Eliminar"
                btn.Name = "btnEliminar"
                btn.UseColumnTextForButtonValue = True
            End If

            If txtCod.Text <> "" Then

                Dim detalle As New ClsDetalleResultadoPorDefecto
                Dim dt As New DataTable
                Dim row As DataRow

                detalle.codigo_ = txtCod.Text
                dt = detalle.BuscarDetalleResultado

                For i = 0 To dt.Rows.Count - 1
                    row = dt.Rows(i)
                    dgvResultados.Rows.Add(New String() {CStr(row("codigoDetalle")), (row("resultado")), (row("pordefecto"))})

                Next

                btnGuardar.Text = "Modificar"

            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            'Objetos
            Dim resultado As New ClsResultadoPorDefecto
            Dim DetalleResultado As New ClsDetalleResultadoPorDefecto
            Dim codigoResultado As String

            'Guardar info
            If btnGuardar.Text = "Guardar" Then 'If boton de guardado

                If txtParametro.Text <> "" Then 'if validacion para guardar

                    With resultado

                        .nombreParametro_ = RTrim(txtParametro.Text)
                        codigoResultado = .registrarResultadoPorParametro()
                        txtCod.Text = codigoResultado

                        'Registro de detalle
                        With DetalleResultado
                            'FALTA REGISTRAR EL DETALLE Y ENVIAR DATOS DESDE PANTALLA EXAMEN

                            For i = 0 To dgvResultados.Rows.Count - 2

                                .codigo_ = Integer.Parse(codigoResultado)
                                .resultado_ = dgvResultados.Rows(i).Cells(1).Value
                                .porDefecto_ = dgvResultados.Rows(i).Cells(2).Value
                                .registrarDetalleResultadoPorDefecto()

                            Next

                        End With
                        MsgBox("Se registro el resultado por defecto para " + txtParametro.Text + ".", MsgBoxStyle.Information)

                    End With
                Else

                End If ' fin if validacion para guardar


            Else 'Modificar info


                '::::::::::::::::::::::::::::Modificacion
                For index As Integer = 0 To resultadosarray.Count - 1
                        'elimina
                        DetalleResultado.Cod = Convert.ToInt64(resultadosarray(index))
                        If DetalleResultado.EliminarDetalleResultado() <> 1 Then
                            MsgBox("Error al querer modificar la toma de muestra.", MsgBoxStyle.Critical, "Validación.")
                        End If
                    Next
                resultadosarray.Clear()


                For index As Integer = 0 To dgvResultados.Rows.Count - 2
                    If Trim(dgvResultados.Rows(index).Cells(0).Value()) = "" Then

                        'agrega
                        With DetalleResultado

                            .codigo_ = Integer.Parse(txtCod.Text)
                            .resultado_ = dgvResultados.Rows(index).Cells(1).Value
                            .porDefecto_ = dgvResultados.Rows(index).Cells(2).Value
                            .registrarDetalleResultadoPorDefecto()
                        End With

                    Else

                        'actualiza 
                        With DetalleResultado
                            .Cod = Integer.Parse(dgvResultados.Rows(index).Cells(0).Value)
                            .resultado_ = dgvResultados.Rows(index).Cells(1).Value
                            .porDefecto_ = dgvResultados.Rows(index).Cells(2).Value
                        End With
                        If DetalleResultado.ActualizarDetalleResultadoPorDefecto() = 0 Then
                            MsgBox("Error al querer modificar la pregunta.", MsgBoxStyle.Critical)
                        End If
                    End If
                Next

                MsgBox("Se actualizo el registro de resultados.")
                Me.Close()
            End If ' fin if de guardado

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgvResultados_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvResultados.CellContentClick
        Try

            If e.ColumnIndex = 3 Then
                Dim n As String = MsgBox("¿Desea eliminar el resultado por defecto?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then
                    resultadosarray.Add(Me.dgvResultados.Rows(e.RowIndex).Cells(0).Value())
                    dgvResultados.Rows.Remove(dgvResultados.Rows(e.RowIndex.ToString))
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class