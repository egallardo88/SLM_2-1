﻿Public Class ConfigurarEtiqueta
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Try

            M_ListadoItemExamenes.lblform.Text = "etiqueta"
            MostrarForm(M_ListadoItemExamenes)


        Catch ex As Exception

        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try

            Dim codigoCantidad As Integer


            If txtCantidad.Text <> "" And lblCodItemExa.Text <> "Label4" Then

                Dim etiqueta As New ClsCantidadEtiquetas

                With etiqueta
                    .codigo_examen = Integer.Parse(lblCodItemExa.Text)
                    .cantidad_ = Integer.Parse(txtCantidad.Text)
                    codigoCantidad = .registrar
                    If codigoCantidad > 1 Then

                        'REGISTRO DE DETALLE

                        Dim detalleCantidad As New ClsDetalleCantidadEtiquetas

                        For i = 0 To dgvDetalle.Rows.Count - 2

                            With detalleCantidad

                                .codigoCantidaddeEtiquetas_ = codigoCantidad
                                .Descripcion_ = dgvDetalle.Rows(i).Cells(1).Value
                                .registrar()
                            End With

                        Next

                        MsgBox("Se realizo el registro de configuracion.")
                    End If
                End With
                limpiar()
                cargarData()
            Else
                MsgBox("Falta cantidad o seleccionar un examen.")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        Try

            If txtCodigo.Text <> "" Then
                Dim etiqueta As New ClsCantidadEtiquetas

                With etiqueta
                    .Cod = Integer.Parse(txtCodigo.Text)
                    .codigo_examen = Integer.Parse(lblCodItemExa.Text)
                    .cantidad_ = Integer.Parse(txtCantidad.Text)
                    If .modificar() = 1 Then
                        'REGISTRO DE DETALLE

                        Dim detalleCantidad As New ClsDetalleCantidadEtiquetas

                        detalleCantidad.codigoCantidaddeEtiquetas_ = Integer.Parse(txtCodigo.Text)
                        detalleCantidad.eliminar()

                        For i = 0 To dgvDetalle.Rows.Count - 2

                            With detalleCantidad
                                .codigoCantidaddeEtiquetas_ = Integer.Parse(txtCodigo.Text)
                                .Descripcion_ = dgvDetalle.Rows(i).Cells(1).Value
                                .registrar()
                            End With

                        Next
                        MsgBox("Se realizo la modificacion de configuracion.")
                    End If
                End With
                limpiar()
                cargarData()
            Else
                MsgBox("Debe seleccionar un examen para modificar.")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConfigurarEtiqueta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            alternarColoFilasDatagridview(dgvData)
            alternarColoFilasDatagridview(dgvDetalle)
            cargarData()


            If dgvDetalle.Columns.Contains("btnEliminar") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dgvDetalle.Columns.Add(btn)
                btn.HeaderText = "Eliminar"
                btn.Text = "Eliminar"
                btn.Name = "btnEliminar"
                btn.UseColumnTextForButtonValue = True
            End If

            If dgvData.Columns.Contains("cod") = True Then

                dgvData.Columns("codItemExa").Visible = False
                dgvData.Columns("cantidad").Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvData_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvData.CellDoubleClick
        Try


            txtCodigo.Text = dgvData.Rows(e.RowIndex).Cells(0).Value
            lblCodItemExa.Text = dgvData.Rows(e.RowIndex).Cells(1).Value
            txtNombreExamen.Text = dgvData.Rows(e.RowIndex).Cells(2).Value
            ' txtCantidad.Text = dgvData.Rows(e.RowIndex).Cells(3).Value

            'DETALLE
            Dim detalle As New ClsDetalleCantidadEtiquetas
            detalle.codigoCantidaddeEtiquetas_ = Integer.Parse(txtCodigo.Text)
            Dim dt As New DataTable
            dt = detalle.BuscarDetalleCantidad()
            Dim row As DataRow
            dgvDetalle.Rows.Clear()

            For index = 0 To dt.Rows.Count - 1
                row = dt.Rows(index)
                dgvDetalle.Rows.Add(New String() {row("codigo"), row("descripcion")})
            Next


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Sub cargarData()


        Dim etiqueta As New ClsCantidadEtiquetas
        Dim dt As New DataTable

        dt = etiqueta.ListarConfiguracion
        dgvData.DataSource = dt

        If dgvData.Columns.Contains("cod") = True Then

            dgvData.Columns("codItemExa").Visible = False
            dgvData.Columns("cantidad").Visible = False
        End If


    End Sub

    Sub limpiar()
        Try

            txtCodigo.Clear()
            'txtCantidad.Clear()
            txtNombreExamen.Clear()
            dgvDetalle.Rows.Clear()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        limpiar()
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ConfigurarEtiqueta_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub dgvDetalle_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetalle.CellClick
        If e.ColumnIndex = 2 Then
            Try
                dgvDetalle.Rows.Remove(dgvDetalle.Rows(e.RowIndex.ToString))

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub

    Private Sub txtCodigo_TextChanged(sender As Object, e As EventArgs) Handles txtCodigo.TextChanged
        If Trim(txtCodigo.Text) = "" Then
            GuardarToolStripMenuItem.Enabled = True
            ModificarToolStripMenuItem.Enabled = False
        Else
            GuardarToolStripMenuItem.Enabled = False
            ModificarToolStripMenuItem.Enabled = True
        End If
    End Sub

    Private Sub txtExamen_TextChanged(sender As Object, e As EventArgs) Handles txtExamen.TextChanged
        Try
            Dim dv As New DataView
            Dim dt As New DataTable

            dt = dgvData.DataSource

            dv = dt.DefaultView
            dv.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", txtExamen.Text)

            If dv.Count = "0" Then
                MsgBox("No existe el item facturable.", MsgBoxStyle.Exclamation)
                txtExamen.Text = ""
                cargarData()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class