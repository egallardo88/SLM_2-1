﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms.DataVisualization.Charting
'ESTA VENTANA TIENE BITACORA
Public Class E_frmGraficaEstatica
    Dim sCommand As SqlCommand
    Dim sAdapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim sDs As DataSet
    Dim sTable As DataTable


    Public respuesta As SqlDataReader

    Public enunciado As SqlCommand


    Public Sub crearOrdenes()
        Dim clsG As New ClsGrafica
        RegistrarAcciones(nombre_usurio, Me.Name, "Registro orden de trabajo" + TextBox2.Text)
        Try
            With clsG
                .Id_orden1 = TextBox1.Text
            End With
            If clsG.RegistrarProducto() = "1" Then

                Exit Sub
            End If
            If clsG.RegistrarProducto() = "2" Then

                Exit Sub
            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub
    Public Sub cargarDatosPaciente()
        Dim clsC As New ClsConnection
        Try
            enunciado = New SqlCommand("select distinct c.nombreCompleto,c.fechaNacimiento,c.genero,s.nombre from  factura f , OrdenDeTrabajo o,OrdenTrabajoDetalle od, Cliente c,Sucursal s
where  o.cod_orden_trabajo = od.cod_orden_trabajo and f.numero = o.cod_factura and s.codigo = f.codigoSucursal
and c.codigo = f.codigoCliente and f.numero ='" + id_facturatrabajo_grafico + "'

", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()
            While respuesta.Read
                TextBox2.Text = respuesta.Item("nombreCompleto")
                TextBox5.Text = respuesta.Item("genero")
                TextBox6.Text = DateDiff("yyyy", CDate(respuesta.Item("fechaNacimiento")), Date.Now)
                TextBox4.Text = respuesta.Item("nombre")
            End While
            respuesta.Close()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Sub cargarDatosOrden()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_tecnico = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox8.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub E_frmGraficaEstatica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MenuStrip_slm(MenuStrip1)
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet3._DATOS_' Puede moverla o quitarla según sea necesario.
        ' Me.DATOS_TableAdapter.Fill(Me.Slm_testDataSet3._DATOS_)
        RegistrarVentanas(nombre_usurio, Me.Name)
        'otros datos
        'alternarColoFilasDatagridview(DataGridView3)
        ColoresForm(Panel1, StatusStrip1)
        TextBox1.Text = id_ordentrabajo_grafico
        TextBox10.Text = id_facturatrabajo_grafico
        Try
            crearOrdenes()
            CargarData()
            cargarDatosPaciente()
            cargarDatosOrden()

            cargarDatosOrdenValidador()

        Catch ex As Exception

        End Try

        If estado_paragrafico_osmosis = "Procesado" Then
            RadioButton1.Checked = True
        End If

        If estado_paragrafico_osmosis = "Validado" Then
            RadioButton2.Checked = True
        End If
        For xx As Integer = 0 To DataGridView1.Rows.Count - 1

            'Chart1.Series("Series1").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(1).Value)

            'Chart1.Series("Series1").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine
            'Chart1.Series("Series1").Color = Color.Black

            'Chart1.Series("Series2").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(2).Value)
            'Chart1.Series("Series2").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
            'Chart1.Series("Series2").Color = Color.Black

            'Chart1.Series("Series3").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(3).Value)
            'Chart1.Series("Series3").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
            'Chart1.Series("Series3").Color = Color.Black

            'Chart1.Series("Series4").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(4).Value)
            'Chart1.Series("Series4").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
            'Chart1.Series("Series4").Color = Color.Black

            'Chart1.Series("Series5").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(5).Value)
            'Chart1.Series("Series5").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
            'Chart1.Series("Series5").Color = Color.Blue



            'Chart1.Series("Series6").Points.AddXY(DataGridView1.Columns(0).HeaderText, DataGridView1.Rows(xx).Cells(6).Value)
            'Chart1.Series("Series6").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline
            'Chart1.Series("Series6").Color = Color.Red
            'Chart1.Series("Series6").Font = Font.Bold = True

        Next

        alternarColoFilasDatagridview(DataGridView1)

        Try
            Dim file As String
            Dim imagen_g As New Bitmap(Chart1.Width, Chart1.Height)
            Chart1.DrawToBitmap(imagen_g, Chart1.DisplayRectangle)
            PictureBox3.Image = imagen_g
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDatosOrdenValidador()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_validador = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox9.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CargarData()
        Dim clsc As New ClsConnection
        'Dim connectionString As String = "Data Source=.;Initial Catalog=pubs;Integrated Security=True"
        Dim sql As String = "SELECT * FROM DatosGrafica where id_orden ='" + TextBox1.Text + "'"
        Dim connection As New SqlConnection(clsc.str_con)
        connection.Open()
        sCommand = New SqlCommand(sql, connection)
        sAdapter = New SqlDataAdapter(sCommand)
        sBuilder = New SqlCommandBuilder(sAdapter)
        sDs = New DataSet()
        sAdapter.Fill(sDs, "DatosGrafica$")
        sTable = sDs.Tables("DatosGrafica$")
        connection.Close()
        DataGridView1.DataSource = sDs.Tables("DatosGrafica$")
        DataGridView1.ReadOnly = False
        save_btn.Enabled = True
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect


        DataGridView1.Columns(8).Visible = False
        DataGridView1.Columns(9).Visible = False
        DataGridView1.Columns(10).Visible = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub new_btn_Click(sender As Object, e As EventArgs)
        DataGridView1.[ReadOnly] = False
        save_btn.Enabled = True

        ' delete_btn.Enabled = False
    End Sub
    Public Sub cargarGrafico()

        Dim clsA As New ClsGrafica
        Dim dvOC As DataTable = clsA.DatosGrafica(TextBox1.Text)


        Chart1.DataSource = dvOC
        Chart1.Titles.Clear()

        Chart1.Series("Series1").YValueMembers = "f2"
        Chart1.Series("Series2").YValueMembers = "f3"
        Chart1.Series("Series3").YValueMembers = "f4"
        Chart1.Series("Series4").YValueMembers = "f5"
        Chart1.Series("Series5").YValueMembers = "f6"
        Chart1.Series("Series6").YValueMembers = "f7"

        Chart1.Series("Series1").XValueMember = "f1"
        Chart1.Series("Series2").XValueMember = "f1"
        Chart1.Series("Series3").XValueMember = "f1"
        Chart1.Series("Series4").XValueMember = "f1"
        Chart1.Series("Series5").XValueMember = "f1"
        Chart1.Series("Series6").XValueMember = "f1"

        Chart1.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Series3").ChartType = DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Series4").ChartType = DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Series5").ChartType = DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Series6").ChartType = DataVisualization.Charting.SeriesChartType.Line

        Chart1.Series("Series2").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.Dash
        Chart1.Series("Series3").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.Dash

        'colores
        Chart1.Series("Series1").Color = Color.Black
        Chart1.Series("Series2").Color = Color.Black
        Chart1.Series("Series3").Color = Color.Black
        Chart1.Series("Series4").Color = Color.Black
        Chart1.Series("Series5").Color = Color.Cyan
        Chart1.Series("Series6").Color = Color.Red
        'bordes
        Chart1.Series("Series1").BorderWidth = 3
        Chart1.Series("Series2").BorderWidth = 3
        Chart1.Series("Series3").BorderWidth = 3
        Chart1.Series("Series4").BorderWidth = 3
        Chart1.Series("Series5").BorderWidth = 3
        Chart1.Series("Series6").BorderWidth = 3

        Chart1.DataBind()
        Chart1.Titles.Add("FRAGILIDAD OSMOTICA")
        Chart1.ChartAreas(0).AxisX.Title = "% NaCL"
        Chart1.ChartAreas(0).AxisY.Title = "% HEMOLISIS"
        DataGridView1.Columns(0).Visible = False
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Try
            CargarData()
            cargarGrafico()
            RegistrarAcciones(nombre_usurio, Me.Name, "Busco orden de trabajo" + TextBox1.Text)
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs)
        Dim file As String
        Dim imagen_g As New Bitmap(Chart1.Width, Chart1.Height)
        Chart1.DrawToBitmap(imagen_g, Chart1.DisplayRectangle)
        PictureBox3.Image = imagen_g
        'imagen_g.Save("test.bmp")
        'Process.Start("test.bmp")

    End Sub

    Public Sub guardarGrafico()
        Dim ms As New System.IO.MemoryStream()
        PictureBox3.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png)

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        Try
            CargarData()
            cargarGrafico()
            RegistrarAcciones(nombre_usurio, Me.Name, "Busco orden de trabajo" + TextBox1.Text)
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Public Function RegistrarImagen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer
        Dim ms As New System.IO.MemoryStream()
        PictureBox3.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp)

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarImagenOrdenTrabajo"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_orden"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "es_doble"
        sqlpar.Value = False
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "imagen"
        sqlpar.Value = ms.GetBuffer()
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        E_frmPDFOsmosis.Show()
    End Sub

    Public Function CambiarEstadoOrdendetrabajo(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficas"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function CambiarEstadoOrdendetrabajoValidaro(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficasValidador"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Validar_Click(sender As Object, e As EventArgs) Handles Validar.Click
        If RadioButton1.Checked = True Then
            'IF PARA ACTUALIZAR QUIEN FUE EL QUE PROCESO
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                MsgBox("Se actualizo el estado de la orden")
            End If
        ElseIf RadioButton2.Checked = True Then
            'IF PARA VALIDAR QUIEN FUE EL QUE VALIDO
            If CambiarEstadoOrdendetrabajoValidaro("Validado") = "1" Then
                MsgBox("Se actualizo el estado de la orden")
                BackgroundWorker1.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub GuardarDatosHojaDeTrabajoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarDatosHojaDeTrabajoToolStripMenuItem.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Guardo examen osmosis id orden" + TextBox1.Text)
            sAdapter.Update(sTable)
            DataGridView1.[ReadOnly] = False
            save_btn.Enabled = True
            CargarData()
            cargarGrafico()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

        'new_btn.Enabled = True
        'delete_btn.Enabled = True

        Try
            Dim file As String
            Dim imagen_g As New Bitmap(Chart1.Width, Chart1.Height)
            Chart1.DrawToBitmap(imagen_g, Chart1.DisplayRectangle)
            PictureBox3.Image = imagen_g
            If RegistrarImagen() = "1" Then
                MsgBox(mensaje_registro)
            End If
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                MsgBox("Se actualizaron los datos de la orden")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub E_frmGraficaEstatica_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        E_frmListadoFragilidadOsmotica.Show()
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim clsr As New ClsResultados
        ' E_frmInsulinaIndividual.Show()
        clsr.GraficaOsmosisEnviarCorreo(TextBox10.Text)
    End Sub
End Class