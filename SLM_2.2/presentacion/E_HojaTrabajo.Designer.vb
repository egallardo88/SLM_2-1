﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class E_HojaTrabajo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(E_HojaTrabajo))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.txtArea = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSubarea = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtsucursal = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFecha = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtHora = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvHojaTrab = New System.Windows.Forms.DataGridView()
        Me.rbtnUrgentes = New System.Windows.Forms.RadioButton()
        Me.rbtnCortesia = New System.Windows.Forms.RadioButton()
        Me.rbtnNombrePaciente = New System.Windows.Forms.RadioButton()
        Me.rbtnNroOrdTrab = New System.Windows.Forms.RadioButton()
        Me.btnActualizarVista = New System.Windows.Forms.Button()
        Me.btnDetalleResultado = New System.Windows.Forms.Button()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtParametro = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtValorActual = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtInstrTecnico = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtHora2 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtFecha2 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtValidador = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtValoresRef = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbxValidado = New System.Windows.Forms.CheckBox()
        Me.cbxProcesado = New System.Windows.Forms.CheckBox()
        Me.cbxEnProceso = New System.Windows.Forms.CheckBox()
        Me.cbxNoProcesado = New System.Windows.Forms.CheckBox()
        Me.cbxPendMuestra = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValidarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenDeTrabajoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirExcelCtrlPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResultadosPorDefectoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResultadosParametroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarCambiosEnOTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PlantillasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenimientoDePlantillasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImrpimirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirSinValoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImprimirConValoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblCodeSubArea = New System.Windows.Forms.Label()
        Me.lblCodeSucursal = New System.Windows.Forms.Label()
        Me.cbxPlantillas = New UIDC.UI_ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnValoresRef = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.gbxgenero = New System.Windows.Forms.GroupBox()
        Me.rbtnMantener = New System.Windows.Forms.RadioButton()
        Me.rbtnQuitar = New System.Windows.Forms.RadioButton()
        Me.btnValidarUnicoResultado = New System.Windows.Forms.Button()
        Me.lblcodDescrip = New System.Windows.Forms.Label()
        Me.btnValidarResultado = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.pbProgreso = New System.Windows.Forms.ProgressBar()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.PrevisualizarResultadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.dgvHojaTrab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.gbxgenero.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 7)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nro."
        '
        'txtNumero
        '
        Me.txtNumero.BackColor = System.Drawing.Color.White
        Me.txtNumero.Location = New System.Drawing.Point(37, 4)
        Me.txtNumero.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.ReadOnly = True
        Me.txtNumero.Size = New System.Drawing.Size(76, 20)
        Me.txtNumero.TabIndex = 1
        '
        'txtArea
        '
        Me.txtArea.BackColor = System.Drawing.Color.White
        Me.txtArea.Location = New System.Drawing.Point(147, 4)
        Me.txtArea.Margin = New System.Windows.Forms.Padding(2)
        Me.txtArea.Name = "txtArea"
        Me.txtArea.ReadOnly = True
        Me.txtArea.Size = New System.Drawing.Size(76, 20)
        Me.txtArea.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(117, 7)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Area"
        '
        'txtSubarea
        '
        Me.txtSubarea.BackColor = System.Drawing.Color.White
        Me.txtSubarea.Location = New System.Drawing.Point(274, 4)
        Me.txtSubarea.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSubarea.Name = "txtSubarea"
        Me.txtSubarea.ReadOnly = True
        Me.txtSubarea.Size = New System.Drawing.Size(76, 20)
        Me.txtSubarea.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(226, 8)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Subárea"
        '
        'txtsucursal
        '
        Me.txtsucursal.BackColor = System.Drawing.Color.White
        Me.txtsucursal.Location = New System.Drawing.Point(405, 4)
        Me.txtsucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtsucursal.Name = "txtsucursal"
        Me.txtsucursal.ReadOnly = True
        Me.txtsucursal.Size = New System.Drawing.Size(76, 20)
        Me.txtsucursal.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(355, 7)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Sucursal"
        '
        'txtFecha
        '
        Me.txtFecha.BackColor = System.Drawing.Color.White
        Me.txtFecha.Location = New System.Drawing.Point(523, 5)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.ReadOnly = True
        Me.txtFecha.Size = New System.Drawing.Size(76, 20)
        Me.txtFecha.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(485, 8)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Fecha"
        '
        'txtHora
        '
        Me.txtHora.BackColor = System.Drawing.Color.White
        Me.txtHora.Location = New System.Drawing.Point(637, 5)
        Me.txtHora.Margin = New System.Windows.Forms.Padding(2)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.ReadOnly = True
        Me.txtHora.Size = New System.Drawing.Size(76, 20)
        Me.txtHora.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(603, 8)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Hora"
        '
        'dgvHojaTrab
        '
        Me.dgvHojaTrab.AllowUserToAddRows = False
        Me.dgvHojaTrab.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHojaTrab.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvHojaTrab.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHojaTrab.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvHojaTrab.ColumnHeadersHeight = 29
        Me.dgvHojaTrab.GridColor = System.Drawing.Color.White
        Me.dgvHojaTrab.Location = New System.Drawing.Point(9, 252)
        Me.dgvHojaTrab.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvHojaTrab.Name = "dgvHojaTrab"
        Me.dgvHojaTrab.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvHojaTrab.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvHojaTrab.RowHeadersWidth = 51
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvHojaTrab.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvHojaTrab.RowTemplate.Height = 24
        Me.dgvHojaTrab.Size = New System.Drawing.Size(1097, 332)
        Me.dgvHojaTrab.TabIndex = 12
        '
        'rbtnUrgentes
        '
        Me.rbtnUrgentes.AutoSize = True
        Me.rbtnUrgentes.BackColor = System.Drawing.Color.Transparent
        Me.rbtnUrgentes.Location = New System.Drawing.Point(5, 18)
        Me.rbtnUrgentes.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnUrgentes.Name = "rbtnUrgentes"
        Me.rbtnUrgentes.Size = New System.Drawing.Size(106, 17)
        Me.rbtnUrgentes.TabIndex = 19
        Me.rbtnUrgentes.TabStop = True
        Me.rbtnUrgentes.Text = "Urgentes Primero"
        Me.rbtnUrgentes.UseVisualStyleBackColor = False
        '
        'rbtnCortesia
        '
        Me.rbtnCortesia.AutoSize = True
        Me.rbtnCortesia.BackColor = System.Drawing.Color.Transparent
        Me.rbtnCortesia.Location = New System.Drawing.Point(119, 18)
        Me.rbtnCortesia.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnCortesia.Name = "rbtnCortesia"
        Me.rbtnCortesia.Size = New System.Drawing.Size(101, 17)
        Me.rbtnCortesia.TabIndex = 20
        Me.rbtnCortesia.TabStop = True
        Me.rbtnCortesia.Text = "Cortesia Primero"
        Me.rbtnCortesia.UseVisualStyleBackColor = False
        '
        'rbtnNombrePaciente
        '
        Me.rbtnNombrePaciente.AutoSize = True
        Me.rbtnNombrePaciente.BackColor = System.Drawing.Color.Transparent
        Me.rbtnNombrePaciente.Location = New System.Drawing.Point(5, 40)
        Me.rbtnNombrePaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnNombrePaciente.Name = "rbtnNombrePaciente"
        Me.rbtnNombrePaciente.Size = New System.Drawing.Size(107, 17)
        Me.rbtnNombrePaciente.TabIndex = 21
        Me.rbtnNombrePaciente.TabStop = True
        Me.rbtnNombrePaciente.Text = "Nombre Paciente"
        Me.rbtnNombrePaciente.UseVisualStyleBackColor = False
        '
        'rbtnNroOrdTrab
        '
        Me.rbtnNroOrdTrab.AutoSize = True
        Me.rbtnNroOrdTrab.BackColor = System.Drawing.Color.Transparent
        Me.rbtnNroOrdTrab.Location = New System.Drawing.Point(119, 39)
        Me.rbtnNroOrdTrab.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnNroOrdTrab.Name = "rbtnNroOrdTrab"
        Me.rbtnNroOrdTrab.Size = New System.Drawing.Size(96, 17)
        Me.rbtnNroOrdTrab.TabIndex = 22
        Me.rbtnNroOrdTrab.TabStop = True
        Me.rbtnNroOrdTrab.Text = "Nro. Ord. Trab."
        Me.rbtnNroOrdTrab.UseVisualStyleBackColor = False
        '
        'btnActualizarVista
        '
        Me.btnActualizarVista.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActualizarVista.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnActualizarVista.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnActualizarVista.ForeColor = System.Drawing.Color.White
        Me.btnActualizarVista.Location = New System.Drawing.Point(6, 77)
        Me.btnActualizarVista.Margin = New System.Windows.Forms.Padding(2)
        Me.btnActualizarVista.Name = "btnActualizarVista"
        Me.btnActualizarVista.Size = New System.Drawing.Size(97, 34)
        Me.btnActualizarVista.TabIndex = 23
        Me.btnActualizarVista.Text = "Actualizar Vista"
        Me.btnActualizarVista.UseVisualStyleBackColor = False
        '
        'btnDetalleResultado
        '
        Me.btnDetalleResultado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDetalleResultado.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnDetalleResultado.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnDetalleResultado.ForeColor = System.Drawing.Color.White
        Me.btnDetalleResultado.Location = New System.Drawing.Point(6, 116)
        Me.btnDetalleResultado.Margin = New System.Windows.Forms.Padding(2)
        Me.btnDetalleResultado.Name = "btnDetalleResultado"
        Me.btnDetalleResultado.Size = New System.Drawing.Size(97, 35)
        Me.btnDetalleResultado.TabIndex = 24
        Me.btnDetalleResultado.Text = "Detalle Resultado"
        Me.btnDetalleResultado.UseVisualStyleBackColor = False
        '
        'txtOrden
        '
        Me.txtOrden.BackColor = System.Drawing.Color.White
        Me.txtOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrden.Location = New System.Drawing.Point(64, 6)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(2)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.ReadOnly = True
        Me.txtOrden.Size = New System.Drawing.Size(76, 24)
        Me.txtOrden.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 10)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(36, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Orden"
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.Color.White
        Me.txtPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaciente.Location = New System.Drawing.Point(203, 7)
        Me.txtPaciente.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(243, 24)
        Me.txtPaciente.TabIndex = 29
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(150, 11)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "Paciente"
        '
        'txtParametro
        '
        Me.txtParametro.BackColor = System.Drawing.Color.White
        Me.txtParametro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtParametro.Location = New System.Drawing.Point(508, 6)
        Me.txtParametro.Margin = New System.Windows.Forms.Padding(2)
        Me.txtParametro.Name = "txtParametro"
        Me.txtParametro.ReadOnly = True
        Me.txtParametro.Size = New System.Drawing.Size(76, 24)
        Me.txtParametro.TabIndex = 31
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(450, 13)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Parametro"
        '
        'txtValorActual
        '
        Me.txtValorActual.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtValorActual.BackColor = System.Drawing.Color.White
        Me.txtValorActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValorActual.Location = New System.Drawing.Point(658, 7)
        Me.txtValorActual.Margin = New System.Windows.Forms.Padding(2)
        Me.txtValorActual.Name = "txtValorActual"
        Me.txtValorActual.ReadOnly = True
        Me.txtValorActual.Size = New System.Drawing.Size(179, 24)
        Me.txtValorActual.TabIndex = 33
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(601, 14)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 13)
        Me.Label12.TabIndex = 32
        Me.Label12.Text = "Valor Act."
        '
        'txtInstrTecnico
        '
        Me.txtInstrTecnico.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInstrTecnico.BackColor = System.Drawing.Color.White
        Me.txtInstrTecnico.Location = New System.Drawing.Point(82, 66)
        Me.txtInstrTecnico.Margin = New System.Windows.Forms.Padding(2)
        Me.txtInstrTecnico.Name = "txtInstrTecnico"
        Me.txtInstrTecnico.ReadOnly = True
        Me.txtInstrTecnico.Size = New System.Drawing.Size(755, 20)
        Me.txtInstrTecnico.TabIndex = 35
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(2, 73)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 13)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Instr. Técnico"
        '
        'txtBuscar
        '
        Me.txtBuscar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBuscar.Location = New System.Drawing.Point(82, 94)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(426, 20)
        Me.txtBuscar.TabIndex = 37
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(34, 97)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(40, 13)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "Buscar"
        '
        'btnBuscar
        '
        Me.btnBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscar.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnBuscar.ForeColor = System.Drawing.Color.White
        Me.btnBuscar.Location = New System.Drawing.Point(512, 94)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(68, 20)
        Me.btnBuscar.TabIndex = 38
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(13, 20)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(349, 13)
        Me.Label15.TabIndex = 39
        Me.Label15.Text = "Usar Comandos en Resultados; -V p/Validar, R- p/Objetar, **p/Confirmar"
        '
        'txtHora2
        '
        Me.txtHora2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtHora2.Location = New System.Drawing.Point(772, 17)
        Me.txtHora2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtHora2.Name = "txtHora2"
        Me.txtHora2.ReadOnly = True
        Me.txtHora2.Size = New System.Drawing.Size(76, 20)
        Me.txtHora2.TabIndex = 45
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(738, 22)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(30, 13)
        Me.Label16.TabIndex = 44
        Me.Label16.Text = "Hora"
        '
        'txtFecha2
        '
        Me.txtFecha2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtFecha2.Location = New System.Drawing.Point(626, 17)
        Me.txtFecha2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFecha2.Name = "txtFecha2"
        Me.txtFecha2.ReadOnly = True
        Me.txtFecha2.Size = New System.Drawing.Size(76, 20)
        Me.txtFecha2.TabIndex = 43
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(585, 22)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(37, 13)
        Me.Label17.TabIndex = 42
        Me.Label17.Text = "Fecha"
        '
        'txtValidador
        '
        Me.txtValidador.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtValidador.Location = New System.Drawing.Point(477, 17)
        Me.txtValidador.Margin = New System.Windows.Forms.Padding(2)
        Me.txtValidador.Name = "txtValidador"
        Me.txtValidador.ReadOnly = True
        Me.txtValidador.Size = New System.Drawing.Size(76, 20)
        Me.txtValidador.TabIndex = 41
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(403, 22)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 13)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Validador Por"
        '
        'txtValoresRef
        '
        Me.txtValoresRef.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtValoresRef.BackColor = System.Drawing.Color.White
        Me.txtValoresRef.Location = New System.Drawing.Point(82, 38)
        Me.txtValoresRef.Margin = New System.Windows.Forms.Padding(2)
        Me.txtValoresRef.Name = "txtValoresRef"
        Me.txtValoresRef.ReadOnly = True
        Me.txtValoresRef.Size = New System.Drawing.Size(726, 20)
        Me.txtValoresRef.TabIndex = 47
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(8, 45)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 13)
        Me.Label19.TabIndex = 46
        Me.Label19.Text = "Valores Ref."
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox1.Controls.Add(Me.rbtnNroOrdTrab)
        Me.GroupBox1.Controls.Add(Me.rbtnUrgentes)
        Me.GroupBox1.Controls.Add(Me.rbtnCortesia)
        Me.GroupBox1.Controls.Add(Me.rbtnNombrePaciente)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(220, 68)
        Me.GroupBox1.TabIndex = 48
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ordenado Por"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.GroupBox2.Controls.Add(Me.dtpHasta)
        Me.GroupBox2.Controls.Add(Me.dtpDesde)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.cbxValidado)
        Me.GroupBox2.Controls.Add(Me.cbxProcesado)
        Me.GroupBox2.Controls.Add(Me.cbxEnProceso)
        Me.GroupBox2.Controls.Add(Me.cbxNoProcesado)
        Me.GroupBox2.Controls.Add(Me.cbxPendMuestra)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 47)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(850, 30)
        Me.GroupBox2.TabIndex = 49
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Estado de Orden de Trabajo"
        '
        'dtpHasta
        '
        Me.dtpHasta.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(740, 7)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(109, 20)
        Me.dtpHasta.TabIndex = 26
        '
        'dtpDesde
        '
        Me.dtpDesde.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(578, 7)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(109, 20)
        Me.dtpDesde.TabIndex = 25
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(698, 11)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(38, 13)
        Me.Label20.TabIndex = 24
        Me.Label20.Text = "Hasta:"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(533, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Desde:"
        '
        'cbxValidado
        '
        Me.cbxValidado.AutoSize = True
        Me.cbxValidado.BackColor = System.Drawing.Color.Transparent
        Me.cbxValidado.Location = New System.Drawing.Point(450, 13)
        Me.cbxValidado.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxValidado.Name = "cbxValidado"
        Me.cbxValidado.Size = New System.Drawing.Size(67, 17)
        Me.cbxValidado.TabIndex = 22
        Me.cbxValidado.Text = "Validado"
        Me.cbxValidado.UseVisualStyleBackColor = False
        '
        'cbxProcesado
        '
        Me.cbxProcesado.AutoSize = True
        Me.cbxProcesado.BackColor = System.Drawing.Color.Transparent
        Me.cbxProcesado.Location = New System.Drawing.Point(364, 13)
        Me.cbxProcesado.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxProcesado.Name = "cbxProcesado"
        Me.cbxProcesado.Size = New System.Drawing.Size(77, 17)
        Me.cbxProcesado.TabIndex = 21
        Me.cbxProcesado.Text = "Procesado"
        Me.cbxProcesado.UseVisualStyleBackColor = False
        '
        'cbxEnProceso
        '
        Me.cbxEnProceso.AutoSize = True
        Me.cbxEnProceso.BackColor = System.Drawing.Color.Transparent
        Me.cbxEnProceso.Location = New System.Drawing.Point(271, 13)
        Me.cbxEnProceso.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxEnProceso.Name = "cbxEnProceso"
        Me.cbxEnProceso.Size = New System.Drawing.Size(81, 17)
        Me.cbxEnProceso.TabIndex = 20
        Me.cbxEnProceso.Text = "En Proceso"
        Me.cbxEnProceso.UseVisualStyleBackColor = False
        '
        'cbxNoProcesado
        '
        Me.cbxNoProcesado.AutoSize = True
        Me.cbxNoProcesado.BackColor = System.Drawing.Color.Transparent
        Me.cbxNoProcesado.Location = New System.Drawing.Point(166, 13)
        Me.cbxNoProcesado.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxNoProcesado.Name = "cbxNoProcesado"
        Me.cbxNoProcesado.Size = New System.Drawing.Size(94, 17)
        Me.cbxNoProcesado.TabIndex = 19
        Me.cbxNoProcesado.Text = "No Procesado"
        Me.cbxNoProcesado.UseVisualStyleBackColor = False
        '
        'cbxPendMuestra
        '
        Me.cbxPendMuestra.AutoSize = True
        Me.cbxPendMuestra.BackColor = System.Drawing.Color.Transparent
        Me.cbxPendMuestra.Location = New System.Drawing.Point(26, 13)
        Me.cbxPendMuestra.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxPendMuestra.Name = "cbxPendMuestra"
        Me.cbxPendMuestra.Size = New System.Drawing.Size(130, 17)
        Me.cbxPendMuestra.TabIndex = 18
        Me.cbxPendMuestra.Text = "Pendiente de Muestra"
        Me.cbxPendMuestra.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.PlantillasToolStripMenuItem, Me.ImrpimirToolStripMenuItem, Me.PrevisualizarResultadoToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1106, 24)
        Me.MenuStrip1.TabIndex = 50
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ValidarDatosToolStripMenuItem, Me.OrdenDeTrabajoToolStripMenuItem, Me.ImprimirExcelCtrlPToolStripMenuItem, Me.ResultadosPorDefectoToolStripMenuItem, Me.ResultadosParametroToolStripMenuItem, Me.GuardarCambiosEnOTToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'ValidarDatosToolStripMenuItem
        '
        Me.ValidarDatosToolStripMenuItem.Name = "ValidarDatosToolStripMenuItem"
        Me.ValidarDatosToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.ValidarDatosToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.ValidarDatosToolStripMenuItem.Text = "Validar Datos"
        '
        'OrdenDeTrabajoToolStripMenuItem
        '
        Me.OrdenDeTrabajoToolStripMenuItem.Name = "OrdenDeTrabajoToolStripMenuItem"
        Me.OrdenDeTrabajoToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.OrdenDeTrabajoToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.OrdenDeTrabajoToolStripMenuItem.Text = "Orden de Trabajo"
        '
        'ImprimirExcelCtrlPToolStripMenuItem
        '
        Me.ImprimirExcelCtrlPToolStripMenuItem.Name = "ImprimirExcelCtrlPToolStripMenuItem"
        Me.ImprimirExcelCtrlPToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.ImprimirExcelCtrlPToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.ImprimirExcelCtrlPToolStripMenuItem.Text = "Imprimir Excel"
        '
        'ResultadosPorDefectoToolStripMenuItem
        '
        Me.ResultadosPorDefectoToolStripMenuItem.Name = "ResultadosPorDefectoToolStripMenuItem"
        Me.ResultadosPorDefectoToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.ResultadosPorDefectoToolStripMenuItem.Text = "Resultados Por Defecto"
        '
        'ResultadosParametroToolStripMenuItem
        '
        Me.ResultadosParametroToolStripMenuItem.Name = "ResultadosParametroToolStripMenuItem"
        Me.ResultadosParametroToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Space), System.Windows.Forms.Keys)
        Me.ResultadosParametroToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.ResultadosParametroToolStripMenuItem.Text = "Resultados Parametro"
        '
        'GuardarCambiosEnOTToolStripMenuItem
        '
        Me.GuardarCambiosEnOTToolStripMenuItem.Name = "GuardarCambiosEnOTToolStripMenuItem"
        Me.GuardarCambiosEnOTToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.GuardarCambiosEnOTToolStripMenuItem.Text = "Guardar Cambios En OT"
        '
        'PlantillasToolStripMenuItem
        '
        Me.PlantillasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MantenimientoDePlantillasToolStripMenuItem})
        Me.PlantillasToolStripMenuItem.Name = "PlantillasToolStripMenuItem"
        Me.PlantillasToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.PlantillasToolStripMenuItem.Text = "Plantillas"
        '
        'MantenimientoDePlantillasToolStripMenuItem
        '
        Me.MantenimientoDePlantillasToolStripMenuItem.Name = "MantenimientoDePlantillasToolStripMenuItem"
        Me.MantenimientoDePlantillasToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.MantenimientoDePlantillasToolStripMenuItem.Text = "Mantenimiento de Plantillas"
        '
        'ImrpimirToolStripMenuItem
        '
        Me.ImrpimirToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImprimirSinValoresToolStripMenuItem, Me.ImprimirConValoresToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ImrpimirToolStripMenuItem.Name = "ImrpimirToolStripMenuItem"
        Me.ImrpimirToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.ImrpimirToolStripMenuItem.Text = "Imprimir"
        '
        'ImprimirSinValoresToolStripMenuItem
        '
        Me.ImprimirSinValoresToolStripMenuItem.Name = "ImprimirSinValoresToolStripMenuItem"
        Me.ImprimirSinValoresToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.ImprimirSinValoresToolStripMenuItem.Text = "Imprimir Sin Valores"
        '
        'ImprimirConValoresToolStripMenuItem
        '
        Me.ImprimirConValoresToolStripMenuItem.Name = "ImprimirConValoresToolStripMenuItem"
        Me.ImprimirConValoresToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.ImprimirConValoresToolStripMenuItem.Text = "Imprimir Con Valores"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(185, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'lblCodeSubArea
        '
        Me.lblCodeSubArea.AutoSize = True
        Me.lblCodeSubArea.Location = New System.Drawing.Point(535, 101)
        Me.lblCodeSubArea.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodeSubArea.Name = "lblCodeSubArea"
        Me.lblCodeSubArea.Size = New System.Drawing.Size(47, 13)
        Me.lblCodeSubArea.TabIndex = 51
        Me.lblCodeSubArea.Text = "Subárea"
        Me.lblCodeSubArea.Visible = False
        '
        'lblCodeSucursal
        '
        Me.lblCodeSucursal.AutoSize = True
        Me.lblCodeSucursal.Location = New System.Drawing.Point(601, 101)
        Me.lblCodeSucursal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCodeSucursal.Name = "lblCodeSucursal"
        Me.lblCodeSucursal.Size = New System.Drawing.Size(48, 13)
        Me.lblCodeSucursal.TabIndex = 52
        Me.lblCodeSucursal.Text = "Sucursal"
        Me.lblCodeSucursal.Visible = False
        '
        'cbxPlantillas
        '
        Me.cbxPlantillas.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxPlantillas.ArrowColour = System.Drawing.Color.DodgerBlue
        Me.cbxPlantillas.BackColor = System.Drawing.Color.Transparent
        Me.cbxPlantillas.BaseColour = System.Drawing.Color.White
        Me.cbxPlantillas.BorderColour = System.Drawing.Color.Gray
        Me.cbxPlantillas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbxPlantillas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPlantillas.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.cbxPlantillas.FontColour = System.Drawing.Color.Black
        Me.cbxPlantillas.FormattingEnabled = True
        Me.cbxPlantillas.LineColour = System.Drawing.Color.DodgerBlue
        Me.cbxPlantillas.Location = New System.Drawing.Point(6, 156)
        Me.cbxPlantillas.Name = "cbxPlantillas"
        Me.cbxPlantillas.Size = New System.Drawing.Size(107, 26)
        Me.cbxPlantillas.SqaureColour = System.Drawing.Color.Gainsboro
        Me.cbxPlantillas.SqaureHoverColour = System.Drawing.Color.Gray
        Me.cbxPlantillas.StartIndex = 0
        Me.cbxPlantillas.TabIndex = 53
        Me.cbxPlantillas.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtNumero)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtArea)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtSubarea)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtsucursal)
        Me.Panel1.Controls.Add(Me.txtHora)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtFecha)
        Me.Panel1.Location = New System.Drawing.Point(6, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(850, 29)
        Me.Panel1.TabIndex = 148
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.HighlightText
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.btnBuscar)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.btnValoresRef)
        Me.Panel2.Controls.Add(Me.lblCodeSubArea)
        Me.Panel2.Controls.Add(Me.lblCodeSucursal)
        Me.Panel2.Controls.Add(Me.txtValoresRef)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.txtValorActual)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.txtBuscar)
        Me.Panel2.Controls.Add(Me.txtParametro)
        Me.Panel2.Controls.Add(Me.txtInstrTecnico)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtPaciente)
        Me.Panel2.Controls.Add(Me.txtOrden)
        Me.Panel2.Location = New System.Drawing.Point(6, 81)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(850, 128)
        Me.Panel2.TabIndex = 149
        '
        'btnValoresRef
        '
        Me.btnValoresRef.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnValoresRef.BackColor = System.Drawing.Color.White
        Me.btnValoresRef.BackgroundImage = Global.SLM_2._2.My.Resources.Resources.SeekPng_com_lupa_png_872219
        Me.btnValoresRef.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnValoresRef.FlatAppearance.BorderSize = 0
        Me.btnValoresRef.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnValoresRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValoresRef.Location = New System.Drawing.Point(811, 36)
        Me.btnValoresRef.Margin = New System.Windows.Forms.Padding(2)
        Me.btnValoresRef.Name = "btnValoresRef"
        Me.btnValoresRef.Size = New System.Drawing.Size(25, 23)
        Me.btnValoresRef.TabIndex = 147
        Me.btnValoresRef.Text = "..."
        Me.btnValoresRef.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.AliceBlue
        Me.Panel3.Controls.Add(Me.txtFecha2)
        Me.Panel3.Controls.Add(Me.txtHora2)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.txtValidador)
        Me.Panel3.Controls.Add(Me.Label17)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Location = New System.Drawing.Point(9, 647)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1270, 43)
        Me.Panel3.TabIndex = 150
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(102, Byte), Integer))
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 586)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1106, 22)
        Me.StatusStrip1.TabIndex = 151
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.GroupBox3.Controls.Add(Me.GroupBox2)
        Me.GroupBox3.Controls.Add(Me.Panel1)
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 35)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(862, 212)
        Me.GroupBox3.TabIndex = 152
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de la orden"
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.Controls.Add(Me.gbxgenero)
        Me.Panel4.Controls.Add(Me.btnValidarUnicoResultado)
        Me.Panel4.Controls.Add(Me.lblcodDescrip)
        Me.Panel4.Controls.Add(Me.btnValidarResultado)
        Me.Panel4.Controls.Add(Me.GroupBox1)
        Me.Panel4.Controls.Add(Me.btnDetalleResultado)
        Me.Panel4.Controls.Add(Me.cbxPlantillas)
        Me.Panel4.Controls.Add(Me.btnActualizarVista)
        Me.Panel4.Location = New System.Drawing.Point(877, 35)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(229, 212)
        Me.Panel4.TabIndex = 153
        '
        'gbxgenero
        '
        Me.gbxgenero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxgenero.BackColor = System.Drawing.Color.White
        Me.gbxgenero.Controls.Add(Me.rbtnMantener)
        Me.gbxgenero.Controls.Add(Me.rbtnQuitar)
        Me.gbxgenero.Location = New System.Drawing.Point(8, 157)
        Me.gbxgenero.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Name = "gbxgenero"
        Me.gbxgenero.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Size = New System.Drawing.Size(206, 44)
        Me.gbxgenero.TabIndex = 155
        Me.gbxgenero.TabStop = False
        Me.gbxgenero.Text = "Al Validar La Orden De Trabajo"
        '
        'rbtnMantener
        '
        Me.rbtnMantener.AutoSize = True
        Me.rbtnMantener.Location = New System.Drawing.Point(25, 17)
        Me.rbtnMantener.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnMantener.Name = "rbtnMantener"
        Me.rbtnMantener.Size = New System.Drawing.Size(70, 17)
        Me.rbtnMantener.TabIndex = 15
        Me.rbtnMantener.Text = "Mantener"
        Me.rbtnMantener.UseVisualStyleBackColor = True
        '
        'rbtnQuitar
        '
        Me.rbtnQuitar.AutoSize = True
        Me.rbtnQuitar.Checked = True
        Me.rbtnQuitar.Location = New System.Drawing.Point(126, 17)
        Me.rbtnQuitar.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnQuitar.Name = "rbtnQuitar"
        Me.rbtnQuitar.Size = New System.Drawing.Size(53, 17)
        Me.rbtnQuitar.TabIndex = 16
        Me.rbtnQuitar.TabStop = True
        Me.rbtnQuitar.Text = "Quitar"
        Me.rbtnQuitar.UseVisualStyleBackColor = True
        '
        'btnValidarUnicoResultado
        '
        Me.btnValidarUnicoResultado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnValidarUnicoResultado.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnValidarUnicoResultado.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnValidarUnicoResultado.ForeColor = System.Drawing.Color.White
        Me.btnValidarUnicoResultado.Location = New System.Drawing.Point(118, 118)
        Me.btnValidarUnicoResultado.Margin = New System.Windows.Forms.Padding(2)
        Me.btnValidarUnicoResultado.Name = "btnValidarUnicoResultado"
        Me.btnValidarUnicoResultado.Size = New System.Drawing.Size(96, 35)
        Me.btnValidarUnicoResultado.TabIndex = 150
        Me.btnValidarUnicoResultado.Text = "Validar Resultado"
        Me.btnValidarUnicoResultado.UseVisualStyleBackColor = False
        '
        'lblcodDescrip
        '
        Me.lblcodDescrip.AutoSize = True
        Me.lblcodDescrip.Location = New System.Drawing.Point(114, 156)
        Me.lblcodDescrip.Name = "lblcodDescrip"
        Me.lblcodDescrip.Size = New System.Drawing.Size(123, 13)
        Me.lblcodDescrip.TabIndex = 149
        Me.lblcodDescrip.Text = "codigoResultadoDescrip"
        Me.lblcodDescrip.Visible = False
        '
        'btnValidarResultado
        '
        Me.btnValidarResultado.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnValidarResultado.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnValidarResultado.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnValidarResultado.ForeColor = System.Drawing.Color.White
        Me.btnValidarResultado.Location = New System.Drawing.Point(117, 77)
        Me.btnValidarResultado.Margin = New System.Windows.Forms.Padding(2)
        Me.btnValidarResultado.Name = "btnValidarResultado"
        Me.btnValidarResultado.Size = New System.Drawing.Size(97, 35)
        Me.btnValidarResultado.TabIndex = 148
        Me.btnValidarResultado.Text = "Validar Orden de Trabajo"
        Me.btnValidarResultado.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Navy
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(483, 696)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(181, 13)
        Me.Label7.TabIndex = 154
        Me.Label7.Text = "Seleccionar fila para cargar el detalle"
        '
        'pbProgreso
        '
        Me.pbProgreso.Location = New System.Drawing.Point(0, 21)
        Me.pbProgreso.Name = "pbProgreso"
        Me.pbProgreso.Size = New System.Drawing.Size(1106, 11)
        Me.pbProgreso.TabIndex = 155
        '
        'PrevisualizarResultadoToolStripMenuItem
        '
        Me.PrevisualizarResultadoToolStripMenuItem.Name = "PrevisualizarResultadoToolStripMenuItem"
        Me.PrevisualizarResultadoToolStripMenuItem.Size = New System.Drawing.Size(139, 20)
        Me.PrevisualizarResultadoToolStripMenuItem.Text = "Previsualizar Resultado"
        '
        'E_HojaTrabajo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1106, 608)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.dgvHojaTrab)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.pbProgreso)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "E_HojaTrabajo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SLM - Módulo Laboratorio"
        CType(Me.dgvHojaTrab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.gbxgenero.ResumeLayout(False)
        Me.gbxgenero.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumero As TextBox
    Friend WithEvents txtArea As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtSubarea As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtsucursal As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtFecha As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtHora As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents dgvHojaTrab As DataGridView
    Friend WithEvents rbtnUrgentes As RadioButton
    Friend WithEvents rbtnCortesia As RadioButton
    Friend WithEvents rbtnNombrePaciente As RadioButton
    Friend WithEvents rbtnNroOrdTrab As RadioButton
    Friend WithEvents btnActualizarVista As Button
    Friend WithEvents btnDetalleResultado As Button
    Friend WithEvents txtOrden As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtPaciente As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtParametro As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtValorActual As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtInstrTecnico As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtBuscar As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents Label15 As Label
    Friend WithEvents txtHora2 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtFecha2 As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtValidador As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtValoresRef As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cbxValidado As CheckBox
    Friend WithEvents cbxProcesado As CheckBox
    Friend WithEvents cbxEnProceso As CheckBox
    Friend WithEvents cbxNoProcesado As CheckBox
    Friend WithEvents cbxPendMuestra As CheckBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ValidarDatosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblCodeSubArea As Label
    Friend WithEvents lblCodeSucursal As Label
    Friend WithEvents cbxPlantillas As UIDC.UI_ComboBox
    Friend WithEvents btnValoresRef As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents btnValidarResultado As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents lblcodDescrip As Label
    Friend WithEvents PlantillasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MantenimientoDePlantillasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OrdenDeTrabajoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnValidarUnicoResultado As Button
    Friend WithEvents ImprimirExcelCtrlPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResultadosPorDefectoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResultadosParametroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents Label20 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents ImrpimirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImprimirSinValoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImprimirConValoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GuardarCambiosEnOTToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents gbxgenero As GroupBox
    Friend WithEvents rbtnMantener As RadioButton
    Friend WithEvents rbtnQuitar As RadioButton
    Friend WithEvents pbProgreso As ProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents PrevisualizarResultadoToolStripMenuItem As ToolStripMenuItem
End Class
