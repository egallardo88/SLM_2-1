﻿Public Class A_Bancos

    'Objeto Banco de la clase Cls Bancos
    Dim Banco As New ClsBancos
    '::::::::::::::::: Limpiar Campos ::::::::::::::::::
    Sub Limpiar()
        txtCodBreve.Text = ""
        txtNombreBanco.Text = ""
        txtCodigo.Text = ""
        chkEstado.Checked = False
        txtBusquedaBanco.Text = ""
    End Sub
    ':::::::::::::::::::::::::::::::::::::::::::::::::::
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)



    End Sub
    Private Sub btnCrear_Click(sender As Object, e As EventArgs)

    End Sub
    Private Sub btnModificar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub dtBancos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtBancos.CellClick
        Try
            'Capturar variables de banco seleccionado

            txtCodigo.Text = dtBancos.Rows(e.RowIndex).Cells(0).Value
            txtCodBreve.Text = dtBancos.Rows(e.RowIndex).Cells(1).Value
            txtNombreBanco.Text = dtBancos.Rows(e.RowIndex).Cells(2).Value
            chkEstado.Checked = dtBancos.Rows(e.RowIndex).Cells(3).Value

            'Habilitar botones de edicion
            btnCrear.Enabled = True
            btnModificar.Enabled = True
            btnGuardar.Enabled = False

        Catch ex As Exception

        End Try
    End Sub
    Private Sub chkEstado_CheckedChanged(sender As Object, e As EventArgs) Handles chkEstado.CheckedChanged
        'Cambio de color estado de cuenta.
        If chkEstado.Checked = True Then
            lblEstado.Text = "Activa"
            lblEstado.ForeColor = Color.Green
        Else


            lblEstado.Text = "Inactiva"
            lblEstado.ForeColor = Color.Red


        End If
    End Sub

    Private Sub A_Bancos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RegistrarVentanas(nombre_usurio, Me.Name)
        MenuStrip_slm(MenuStrip1)
        ColoresForm(Panel1, StatusStrip1)
        '´Prueba de cambios git 2255

        Try


            BindingSource1.DataSource = Banco.listarBancos

            dtBancos.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1

            alternarColoFilasDatagridview(dtBancos)
            dtBancos.Columns("nombreBanco").Width = 170

            dtBancos.Columns("codBanco").Visible = False
            dtBancos.Columns("codBreve").HeaderText = "COD.BREVE"
            dtBancos.Columns("nombreBanco").HeaderText = "NOMBRE DE BANCO"
            dtBancos.Columns("estado").HeaderText = "ESTADO"

            'botones
            btnCrear.Enabled = False
            btnModificar.Enabled = False
            btnGuardar.Enabled = True

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox("Hubo un error al consultar los bancos. " + ex.Message)
        End Try

    End Sub

    Private Sub txtBusquedaBanco_TextChanged(sender As Object, e As EventArgs) Handles txtBusquedaBanco.TextChanged

        Dim Dato As New DataView

        Try
            'Actualizar datos en datagrid con textbox
            With Banco

                .Nombre_Banco = txtBusquedaBanco.Text

                BindingSource1.DataSource = Banco.buscarBanco.DefaultView.Table

                dtBancos.DataSource = BindingSource1

                BindingNavigator1.BindingSource = BindingSource1

            End With

            If txtBusquedaBanco.Text = "" Then

                BindingSource1.DataSource = Banco.listarBancos

                dtBancos.DataSource = BindingSource1


                BindingNavigator1.BindingSource = BindingSource1
            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs)
        Limpiar()
        btnCrear.Enabled = False
        btnModificar.Enabled = False
        btnGuardar.Enabled = True
        txtBusquedaBanco.Text = ""
        dtBancos.DataSource = Banco.listarBancos
    End Sub

    Private Sub A_Bancos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
            'frmMenuConta.Show()
        End If
    End Sub

    Private Sub txtNombreBanco_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombreBanco.KeyPress
        'Solo letras para el campo nombre
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtBusquedaBanco_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBusquedaBanco.KeyPress
        'Solo letras para el campo nombre
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub A_Bancos_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        'frmMenuConta.Show()
    End Sub

    Private Sub txtCodBreve_TextChanged(sender As Object, e As EventArgs) Handles txtCodBreve.TextChanged
        If txtCodBreve.BackColor = Color.Red Then
            txtCodBreve.BackColor = Color.White
        End If
    End Sub

    Private Sub txtNombreBanco_TextChanged(sender As Object, e As EventArgs) Handles txtNombreBanco.TextChanged
        If txtNombreBanco.BackColor = Color.Red Then
            txtNombreBanco.BackColor = Color.White
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        txtBusquedaBanco.Clear()
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Limpiar()
        RegistrarAcciones(nombre_usurio, Me.Name, "Creo")
        btnCrear.Enabled = False
        btnModificar.Enabled = False
        btnGuardar.Enabled = True
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar" + txtCodigo.Text)
            Dim n As String = MsgBox("¿Desea modificar el registro de banco?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then

                'Captura de variables
                With Banco
                    .Cod = Convert.ToInt32(txtCodigo.Text)
                    .cod_breve = txtCodBreve.Text
                    .Nombre_Banco = txtNombreBanco.Text
                    .Estad_o = chkEstado.Checked

                    'Modificación de Banco


                End With
                If Banco.modificarBanco() = "1" Then
                    MsgBox(mensaje_actualizacion)
                    Limpiar()
                    BindingSource1.DataSource = Banco.listarBancos

                    dtBancos.DataSource = BindingSource1


                    BindingNavigator1.BindingSource = BindingSource1
                    btnCrear.Enabled = False
                    btnModificar.Enabled = False
                    btnGuardar.Enabled = True
                End If
            End If
        Catch ex As Exception

            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)

        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim n As String = MsgBox("¿Desea guardar el nuevo banco?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then

            RegistrarAcciones(nombre_usurio, Me.Name, "Guardar" + txtCodigo.Text)
            If txtCodBreve.Text <> "" And txtNombreBanco.Text <> "" Then

                Try
                    'Captura de variables
                    With Banco
                        .cod_breve = txtCodBreve.Text
                        .Nombre_Banco = txtNombreBanco.Text
                        .Estad_o = chkEstado.Checked
                    End With

                    'Registro de Banco
                    If Banco.registrarNuevoBanco() = 1 Then
                        BindingSource1.DataSource = Banco.listarBancos

                        dtBancos.DataSource = BindingSource1


                        BindingNavigator1.BindingSource = BindingSource1
                        MsgBox("El registro se guardo exitosamente")
                        Limpiar()

                    End If

                Catch ex As Exception
                    MsgBox("Error al guardar el registro. Detalle: " + ex.Message)
                End Try
            Else

                MsgBox("Existen campos vacíos.")

                If txtCodBreve.Text = "" Then
                    txtCodBreve.BackColor = Color.Red
                ElseIf txtNombreBanco.Text = "" Then
                    txtNombreBanco.BackColor = Color.Red
                End If

            End If
        End If
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub FactToolStripMenuItem_Click(sender As Object, e As EventArgs) 
        Try
            With M_ListarFacturasEmpresarial
                .Show()
                .BringToFront()
                .WindowState = WindowState.Normal
            End With
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub PruebaImprimirFactToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Try


            'le asigno un valor a los parametros del procedimiento almacenado
            Dim objReporte As New MM_FacturaFinal

            objReporte.SetParameterValue("@numero", Convert.ToInt64(1003280))
            objReporte.SetParameterValue("@numeroFactura", Convert.ToInt64(1003280))
            objReporte.SetParameterValue("@fechaNacimiento", Convert.ToDateTime("25 / 8 / 1990"))

            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            M_ImprimirFacturaReport.CrystalReportViewer1.ReportSource = objReporte
            'M_ImprimirFacturaReport.CrystalReportViewer1.Refresh()
            M_ImprimirFacturaReport.Show()

        Catch ex As Exception
            MsgBox(ex.Message)
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub FacturaEmpreToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Try
            MostrarForm(M_ListarFacturasEmpresarial)
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub TestToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Try
            MostrarForm(ConfigurarEtiqueta)
        Catch ex As Exception

        End Try
    End Sub
End Class