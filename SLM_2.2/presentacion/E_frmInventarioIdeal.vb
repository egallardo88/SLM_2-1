﻿Imports System.Data.SqlClient

Public Class E_frmInventarioIdeal
    Dim sCommand As SqlCommand
    Dim sAdapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim sDs As DataSet
    Dim sTable As DataTable
    Dim id_almacen_global As Integer
    Dim id_elimnar As Integer
    Private Sub E_frmInventarioIdeal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarDataProducto()
        ComboAlmacen()
        MenuStrip_slm(MenuStrip1)
        alternarColoFilasDatagridview(DataGridView1)
        alternarColoFilasDatagridview(DataGridView2)
        ColoresForm(Panel2, StatusStrip1)
    End Sub
    Private Sub ComboAlmacen()
        Dim clsD As New ClsAlmacen

        Dim ds As New DataTable


        ds.Load(clsD.RecuperarAlmacenes())

        ComboBox1.DataSource = ds
        ComboBox1.DisplayMember = "nombre_almacen"
        ComboBox1.ValueMember = "id_almacen"
    End Sub
    Private Sub cargarDataProducto()
        Try
            'datagridview
            Dim TableUM As New DataTable
            Dim clsP As New ClsProducto
            TableUM.Load(clsP.RecuperarProductoOC())
            BindingSource1.DataSource = TableUM

            DataGridView2.DataSource = BindingSource1

            BindingNavigator1.BindingSource = BindingSource1
        Catch ex As Exception

            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub
    Private Sub CargarData()
        Try


            Dim clsc As New ClsConnection
            'Dim connectionString As String = "Data Source=.;Initial Catalog=pubs;Integrated Security=True"
            Dim sql As String = "SELECT * FROM InventarioIdeal where id_almacen ='" + ComboBox1.SelectedValue.ToString + "'"
            Dim connection As New SqlConnection(clsc.str_con)
            connection.Open()
            sCommand = New SqlCommand(sql, connection)
            sAdapter = New SqlDataAdapter(sCommand)
            sBuilder = New SqlCommandBuilder(sAdapter)
            sDs = New DataSet()
            sAdapter.Fill(sDs, "InventarioIdeal$")
            sTable = sDs.Tables("InventarioIdeal$")
            connection.Close()
            DataGridView1.DataSource = sDs.Tables("InventarioIdeal$")
            DataGridView1.ReadOnly = False
            'save_btn.Enabled = True
            DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect


            DataGridView1.Columns(4).Visible = False
        Catch ex As Exception

        End Try
        'DataGridView1.Columns(9).Visible = False
        'DataGridView1.Columns(10).Visible = False
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        Try

            If validarGuardar("Agregar producto") = 1 Then




                Dim clsD As New clsInventario
                With clsD
                    .Id_producto1 = CInt(DataGridView2.Rows(e.RowIndex).Cells(1).Value)
                    .Nombre_producto1 = DataGridView2.Rows(e.RowIndex).Cells(2).Value
                    .Cantidad_ideal1 = 0
                    .Id_almacen1 = CInt(ComboBox1.SelectedValue)
                    .Nombre_almacen1 = ComboBox1.Text
                End With

                If clsD.RegistrarInventarioDetalleIdeal = "1" Then
                    MsgBox("Se agrego fila")
                    CargarData()
                End If

            End If
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        CargarData()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Try
            Dim objOrd As New ClsProducto


            Dim dv As DataView = objOrd.RecuperarProducto2.DefaultView

            dv.RowFilter = String.Format("CONVERT(nombre_producto, System.String) LIKE '%{0}%'", TextBox1.Text)

            DataGridView2.DataSource = dv
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            ' RegistrarAcciones(nombre_usurio, Me.Name, "Guardo examen osmosis id orden" + TextBox1.Text)
            sAdapter.Update(sTable)
            DataGridView1.[ReadOnly] = False
            'save_btn.Enabled = True
            CargarData()
            'cargarGrafico()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        'Try

        '    If validarGuardar("Eliminar producto") = 1 Then




        '        Dim clsD As New clsInventario
        '        With clsD
        '            .Id_producto1 = CInt(DataGridView1.Rows(e.RowIndex).Cells(1).Value)

        '        End With

        '        If clsD.EliminarProductoIdeal = "1" Then
        '            MsgBox("Se elimino fila")
        '            CargarData()
        '        End If

        '    End If
        'Catch ex As Exception
        '    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        'End Try
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EliminarToolStripMenuItem.Click
        Try

            If validarGuardar("Eliminar producto") = 1 Then




                Dim clsD As New clsInventario
                With clsD
                    .Id_producto1 = id_elimnar

                End With

                If clsD.EliminarProductoIdeal = "1" Then
                    MsgBox("Se elimino fila")
                    CargarData()
                End If

            End If
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            id_elimnar = CInt(DataGridView1.Rows(e.RowIndex).Cells(0).Value)
        Catch ex As Exception

        End Try

    End Sub
End Class