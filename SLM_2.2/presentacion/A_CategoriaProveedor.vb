﻿Public Class A_CategoriaProveedor

    Dim Categoria As New ClsCategoriaProveedor
    Private Sub A_CategoriaProveedor_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub A_CategoriaProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(dtCategorias)
        ColoresForm(Panel1, StatusStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
        Panel1.BackColor = Color.FromArgb(94, 141, 194)
        StatusStrip1.BackColor = Color.FromArgb(64, 180, 229)
        Panel1.BackgroundImage = Nothing
        Try
            'Listar categorias de proveedor

            BindingSource1.DataSource = Categoria.listarCategoriasProveedor

            dtCategorias.DataSource = BindingSource1

            BindingNavigator1.BindingSource = BindingSource1

            'BOTONES
            btnCrear.Enabled = False
            btnModificar.Enabled = False
            btnGuardar.Enabled = True

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

            If txtCodBreve.Text <> "" And txtDescrip.Text <> "" And txtClasifica.Text <> "" Then

                'Comprobar existencia de Clasificacion
                Dim dt As New DataTable
                Dim Cate As New ClsTipoClasificacion

                Cate.Codigo1 = Convert.ToInt32(txtClasifica.Text)

                dt = Cate.BuscarTipoClasificacionCode

                If dt.Rows.Count > 0 Then
                    'Ingresar nueva categoria en base de datos
                    With Categoria
                        .Codig_o = txtCodBreve.Text
                        .Descripcio_n = txtDescrip.Text
                        .Cta_Acreedor = txtAcreedores.Text
                        .Cta_Anticipos = txtAnticipos.Text
                        .Cod_Clasi = Convert.ToInt32(txtClasifica.Text)
                        If .registrarNuevaCategoria() = 1 Then
                            MsgBox("Se registro una nueva categoria para proveedores.")
                            Limpiar()

                        End If
                    End With
                Else
                    MsgBox("El Tipo de Clasificación no existe o hubo un error al seleccionarla.")

                End If


                BindingSource1.DataSource = Categoria.listarCategoriasProveedor

                dtCategorias.DataSource = BindingSource1

                BindingNavigator1.BindingSource = BindingSource1

            ElseIf txtCodBreve.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtCodBreve.BackColor = Color.Red

            ElseIf txtDescrip.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtDescrip.BackColor = Color.Red

            ElseIf txtClasifica.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtClasifica.BackColor = Color.Red

            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub dtCategorias_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtCategorias.CellClick
        Try


            lblCodCat.Text = dtCategorias.Rows(e.RowIndex).Cells(0).Value
            txtCodBreve.Text = dtCategorias.Rows(e.RowIndex).Cells(1).Value
            txtDescrip.Text = dtCategorias.Rows(e.RowIndex).Cells(2).Value
            txtAcreedores.Text = dtCategorias.Rows(e.RowIndex).Cells(3).Value
            txtAnticipos.Text = dtCategorias.Rows(e.RowIndex).Cells(4).Value
            txtClasifica.Text = dtCategorias.Rows(e.RowIndex).Cells(5).Value

            'Habilitar edicion
            btnCrear.Enabled = True
            btnModificar.Enabled = True
            btnGuardar.Enabled = False


        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Limpiar()
        btnModificar.Enabled = False
        btnGuardar.Enabled = True
        btnCrear.Enabled = False
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            'Comprobar existencia de clasificacion
            Dim dt As New DataTable
            Dim Cate As New ClsTipoClasificacion

            Cate.Codigo1 = Convert.ToInt32(txtClasifica.Text)

            dt = Cate.BuscarTipoClasificacionCode

            If dt.Rows.Count > 0 Then
                'Modificacion de categoria en base de datos
                With Categoria
                    'Capturar variables editadas
                    .Cod = Convert.ToInt32(lblCodCat.Text) 'campo oculto
                    .Codig_o = txtCodBreve.Text
                    .Descripcio_n = txtDescrip.Text
                    .Cta_Acreedor = txtAcreedores.Text
                    .Cta_Anticipos = txtAnticipos.Text
                    .Cod_Clasi = Convert.ToInt32(txtClasifica.Text)

                    .modificarCategoria()
                End With

                'Actualizar listado

                BindingSource1.DataSource = Categoria.listarCategoriasProveedor

                dtCategorias.DataSource = BindingSource1

                BindingNavigator1.BindingSource = BindingSource1
                'Limpiar campos
                Limpiar()

                btnCrear.Enabled = False
                btnModificar.Enabled = False
                btnGuardar.Enabled = True

            Else
                MsgBox("El Tipo de Clasificación no existe o hubo un error al seleccionarla.")

            End If
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    '::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    Sub Limpiar()
        txtCodBreve.Text = ""
        txtDescrip.Text = ""
        txtAcreedores.Text = ""
        txtAnticipos.Text = ""
        txtClasifica.Text = ""

        txtCodBreve.BackColor = Color.White
        txtDescrip.BackColor = Color.White
        txtClasifica.BackColor = Color.White

    End Sub

    Private Sub btnBuscarClas_Click(sender As Object, e As EventArgs) Handles btnBuscarClas.Click
        A_ListarTipoClasificacion.Show()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs)

        Limpiar()
        btnCrear.Enabled = False
        btnModificar.Enabled = False
        btnGuardar.Enabled = True
        dtCategorias.DataSource = Categoria.listarCategoriasProveedor

    End Sub

    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged

        Try

            If txtBusqueda.Text <> "" Then

                Categoria.Descripcio_n = txtBusqueda.Text

                dtCategorias.DataSource = Categoria.buscar()
            Else

                dtCategorias.DataSource = Categoria.listarCategoriasProveedor

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtCodBreve_TextChanged(sender As Object, e As EventArgs) Handles txtCodBreve.TextChanged
        If txtCodBreve.BackColor = Color.Red Then
            txtCodBreve.BackColor = Color.White
        End If
    End Sub

    Private Sub txtDescrip_TextChanged(sender As Object, e As EventArgs) Handles txtDescrip.TextChanged
        If txtDescrip.BackColor = Color.Red Then
            txtDescrip.BackColor = Color.White
        End If
    End Sub

    Private Sub txtClasifica_TextChanged(sender As Object, e As EventArgs) Handles txtClasifica.TextChanged
        If txtClasifica.BackColor = Color.Red Then
            txtClasifica.BackColor = Color.White
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()

    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        txtBusqueda.Clear()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        GridAExcel_global(dtCategorias)
    End Sub
End Class