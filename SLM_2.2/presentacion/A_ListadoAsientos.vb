﻿Public Class A_ListadoAsientos

    Dim asiento As New ClsAsientoContable

    Private Sub A_ListadoAsientos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ColoresForm(Panel1, StatusStrip1)
            MenuStrip_slm(MenuStrip1)
            BindingSource1.DataSource = asiento.listarAsientos().DefaultView
            dtAsientos.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1

            alternarColoFilasDatagridview(dtAsientos)
            dtAsientos.Columns("cod_asiento").Visible = False
            dtAsientos.Columns("codPeriodo").Visible = False
            dtAsientos.Columns("fecha").Visible = False

            dtAsientos.Columns("campoLlave").HeaderText = "Transf."
            dtAsientos.Columns("descripcion").HeaderText = "Descripción"
            dtAsientos.Columns("descripcion").Width = 320


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub txtCod_TextChanged(sender As Object, e As EventArgs) Handles txtCod.TextChanged

        Dim Dato As New DataTable
        Dim asi As New ClsAsientoContable

        Try

            If txtCod.Text = "" Then

                BindingSource1.DataSource = asiento.listarAsientos().DefaultView
                dtAsientos.DataSource = BindingSource1
                BindingNavigator1.BindingSource = BindingSource1

                dtAsientos.Columns("cod_asiento").Visible = False
                dtAsientos.Columns("codPeriodo").Visible = False
                dtAsientos.Columns("fecha").Visible = False

                dtAsientos.Columns("descripcion").Width = 320
            Else

                Dim campoLlave As Integer = Convert.ToInt32(txtCod.Text)

                asi.Campo_Llave = Convert.ToInt32(campoLlave)


                BindingSource1.DataSource = asi.buscarAsiento()
                dtAsientos.DataSource = BindingSource1
                BindingNavigator1.BindingSource = BindingSource1

                dtAsientos.Columns("cod_asiento").Visible = False
                dtAsientos.Columns("codPeriodo").Visible = False
                dtAsientos.Columns("fecha").Visible = False
                dtAsientos.Columns("descripcion").Width = 320

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub dtAsientos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtAsientos.CellDoubleClick

        Try
            Dim dt As DataTable
            Dim row As DataRow



            dt = asiento.listarAsientos()
            row = dt.Rows(e.RowIndex)

            frmAsientos.txtNro.Text = dtAsientos.Rows(e.RowIndex).Cells(4).Value
            frmAsientos.dtpFecha.Value = dtAsientos.Rows(e.RowIndex).Cells(3).Value
            frmAsientos.txtTexto.Text = dtAsientos.Rows(e.RowIndex).Cells(2).Value
            frmAsientos.lblCodAsiento.Text = dtAsientos.Rows(e.RowIndex).Cells(0).Value
            frmAsientos.chkAnular.Checked = dtAsientos.Rows(e.RowIndex).Cells(5).Value
            frmAsientos.lblForm.Text = "Listado"
            frmAsientos.lblOrigen.Text = dtAsientos.Rows(e.RowIndex).Cells(6).Value
            Me.Close()
            frmAsientos.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub txtCod_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCod.KeyPress
        'Solo acepta numeros.
        If (Char.IsNumber(e.KeyChar)) Then
            e.Handled = False
        ElseIf (Char.IsControl(e.KeyChar)) Then
            e.Handled = False
        ElseIf (Char.IsPunctuation(e.KeyChar)) Then
            e.Handled = False
        End If
    End Sub

    Private Sub btnIngresarAsiento_Click(sender As Object, e As EventArgs) Handles btnIngresarAsiento.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Ingreso el asiento manual " + txtCod.Text)
            frmAsientos.Show()
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub MantenimientoCCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MantenimientoCCToolStripMenuItem.Click
        Try

            A_CentrodeCosto.Show()
            A_CentrodeCosto.BringToFront()
            A_CentrodeCosto.WindowState = WindowState.Normal

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ReporteCCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteCCToolStripMenuItem.Click
        Try

            reporteCentroCostos.Show()
            reporteCentroCostos.BringToFront()
            reporteCentroCostos.WindowState = WindowState.Normal

        Catch ex As Exception

        End Try
    End Sub

    Private Sub IngresosPorAreaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IngresosPorAreaToolStripMenuItem.Click
        Try
            Me.Close()
            MostrarForm(A_IngresosDetallados)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub InformeAsientosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InformeAsientosToolStripMenuItem.Click
        Try
            A_InformeAsientos.Show()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MayorDeCuentasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MayorDeCuentasToolStripMenuItem.Click
        Try
            reporteMayorCuentas.Show()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

        Try

            BindingSource1.DataSource = asiento.listarAsientos()
            dtAsientos.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1

            dtAsientos.Columns("cod_asiento").Visible = False
            dtAsientos.Columns("codPeriodo").Visible = False
            dtAsientos.Columns("fecha").Visible = False
            dtAsientos.Columns("descripcion").Width = 320

        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindingNavigator2_RefreshItems(sender As Object, e As EventArgs)

    End Sub
End Class