﻿Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Text

Public Class E_HojaTrabajo
    'Dim dv As DataView
    Public ds As New DataSet  'Orden de los examenes por grupo o laboratorio
    Dim celda, fila As Integer 'capturar columna y fila para agregar plantilla
    Dim cadenaIngresada As String = ""

    Public code1 As System.Nullable(Of Integer) = Nothing
    Public code2 As System.Nullable(Of Integer) = Nothing
    Public code3 As System.Nullable(Of Integer) = Nothing
    Public code4 As System.Nullable(Of Integer) = Nothing
    Public code5 As System.Nullable(Of Integer) = Nothing
    Public code6 As System.Nullable(Of Integer) = Nothing

    Private Sub E_HojaTrabajo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            MenuStrip_slm(MenuStrip1)
            ColoresForm(Panel1, StatusStrip1)
            alternarColoFilasDatagridviewHT(dgvHojaTrab)
            txtHora.Text = Date.Now.ToLongTimeString
            txtFecha.Text = Date.Today
            dtpDesde.Value = Date.Today
            dtpHasta.Value = Date.Today
            Me.Text = "Hoja de Trabajo - " + txtArea.Text + "|" + txtSubarea.Text
            'CARGA DE PLANTILLAS :::::::::::::::::::::::::::::::::::::::::...

            Dim plantilla As New ClsPlantillaResultado

            Dim dt As New DataTable

            dt = plantilla.BuscarPlantillaXSubarea(Integer.Parse(lblCodeSubArea.Text))

            cbxPlantillas.DataSource = dt
            cbxPlantillas.DisplayMember = "simbolo"
            cbxPlantillas.SelectedIndex = 0

            '...::::::::::::::::::::::::::::::::::::::::::::::::::::::::::...

        Catch ex As Exception

        End Try

    End Sub
    Private Sub dgvHojaTrab_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHojaTrab.CellEndEdit

        Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
        'Actualizar el detalle de orden de trabajo 
        If e.ColumnIndex > 4 And dgvHojaTrab.Rows(e.RowIndex).Cells(colColl.IndexOf("Estado")).Value <> "ANULADA" Then

            Try
                If (Trim(dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value()) <> "") Then

                    Dim objOrdDet As New ClsOrdenTrabajoDetalle
                    With objOrdDet
                        .cod_orden_trabajo_ = dgvHojaTrab.Rows(e.RowIndex).Cells(0).Value()
                        .resultado_ = dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value()
                        .nombreItemDetalle_ = dgvHojaTrab.Columns.Item(e.ColumnIndex).Name
                        .estado_ = "Procesado"
                    End With
                    If objOrdDet.ModificarOrdenTrabajoDetalleTecnico() <> 1 Then
                        'En caso que no exista el detalle de orden de trabajo entonces le asigna un valor nulo o vacio
                        dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value() = ""
                    End If
                End If
                ValidarResultadosIngresados()


                'celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
                'fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                txtOrden.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(0).Value()
                txtPaciente.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(1).Value()
                txtParametro.Text = dgvHojaTrab.Columns.Item(e.ColumnIndex).Name
                txtValorActual.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value()
            Catch ex As Exception
                Try
                    dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value() = ""
                Catch ex2 As Exception

                End Try
            End Try
        End If
    End Sub

    Private Sub dgvHojaTrab_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHojaTrab.CellClick
        Try
            If e.ColumnIndex > 4 And e.RowIndex >= 0 Then
                'Mostrar valores de la fila seleccionada
                txtOrden.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(0).Value()
                txtPaciente.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(1).Value()
                txtParametro.Text = dgvHojaTrab.Columns.Item(e.ColumnIndex).Name
                txtValorActual.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(e.ColumnIndex).Value()
                celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
                fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)



                'buscar valores referencia 
                If (Trim(txtParametro.Text) <> "") Then
                    Try
                        Dim objCat As New ClsValoresReferencia
                        Dim dt As New DataTable
                        dt = objCat.buscarValorReferenciaHojaTrabajo(txtParametro.Text, Integer.Parse(txtOrden.Text), dgvHojaTrab.Rows(e.RowIndex).Cells(3).Value().ToString.First)
                        Dim row As DataRow = dt.Rows(0)
                        txtValoresRef.Text = CStr(row("valoresReferencia"))
                    Catch ex As Exception
                        txtValoresRef.Text = ""
                    End Try
                End If

                'buscar observaciones hoja de trabajo
                ' BuscarObservacionesHojaTrabajo()
                'ObservacionesOT()

                'Buscar Descripcion de Resultado::::::::::::::::::::::::::::::::::::::::::::::

                Dim plantillaResult As New ClsDescripcionResultado
                Dim dtPlantillasResult As New DataTable
                Dim rowPlantillasResult As DataRow

                With plantillaResult
                    .Codigo_OTrabajo = txtOrden.Text
                    .parametro_ = txtParametro.Text
                    dtPlantillasResult = .buscarDescripcionResultado
                    If dtPlantillasResult.Rows.Count > 0 Then
                        rowPlantillasResult = dtPlantillasResult.Rows(0)
                    End If

                    If dtPlantillasResult.Rows.Count <= 0 Then
                        lblcodDescrip.Text = "codigoResultadoDescrip"
                    Else
                        lblcodDescrip.Text = rowPlantillasResult("cod_orden_trabajo")
                    End If

                End With

                ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

                'buscar valores referencia 
                'If (Trim(txtParametro.Text) <> "") Then
                '    Try
                '        Dim objCat As New ClsValoresReferencia
                '        Dim dt As New DataTable
                '        dt = objCat.buscarValorReferenciaParametro(txtParametro.Text)
                '        Dim row As DataRow = dt.Rows(0)
                '        txtValoresRef.Text = CStr(row("ValoresReferencia"))
                '    Catch ex As Exception
                '        txtValoresRef.Text = ""
                '    End Try
                'End If


            Else
                txtOrden.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(0).Value()
                txtPaciente.Text = dgvHojaTrab.Rows(e.RowIndex).Cells(1).Value()
                txtParametro.Text = ""
                txtValorActual.Text = ""
                txtInstrTecnico.Text = ""
                txtValoresRef.Text = ""
            End If

            'BLOQUEO DE FILA ANULADA
            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
            If dgvHojaTrab.Rows(e.RowIndex).Cells(colColl.IndexOf("Estado")).Value = "ANULADA" Then

                dgvHojaTrab.Rows(fila).ReadOnly = True

            End If



            ObservacionesOT()
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnActualizarVista_Click(sender As Object, e As EventArgs) Handles btnActualizarVista.Click

        'BackgroundWorker1.RunWorkerAsync()

        Try
            pbProgreso.Value += 5
            If cbxPendMuestra.Checked = False And cbxNoProcesado.Checked = False And cbxEnProceso.Checked = False And cbxProcesado.Checked = False And cbxValidado.Checked = False Then
                MsgBox("Debe seleccionar un estado de orden de trabajo.", MsgBoxStyle.Information)
                Exit Sub
            End If

            'If dgvHojaTrab.Rows.Count > 1 Then
            ds.Tables(0).Rows.Clear()
            ' End If

            Dim pendienteMuestra, noProcesado, enProceso, procesado, validado As String
            Dim codeSuc As System.Nullable(Of Integer)

            If cbxPendMuestra.Checked Then
                pendienteMuestra = "Pendiente Muestra"
            Else
                pendienteMuestra = Nothing
            End If
            If cbxNoProcesado.Checked Then
                noProcesado = "No Procesado"
            Else
                noProcesado = Nothing
            End If
            If cbxEnProceso.Checked Then
                enProceso = "En Proceso"
            Else
                enProceso = Nothing
            End If
            If cbxProcesado.Checked Then
                procesado = "Procesado"
            Else
                procesado = Nothing
            End If
            If cbxValidado.Checked Then
                validado = "Validado"
            Else
                validado = Nothing
            End If

            Dim edad As String
            Dim dt As New DataTable
            Dim row As DataRow


            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns

            'orden de trabajo
            Dim objOrdTrab As New ClsOrdenDeTrabajo
            'Dim dt As New DataTable ' ordenes de trabajo
            Dim rowO As DataRow ' fila orden de trabajo

            'detalle orden de trabajo
            Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
            Dim dtDet As New DataTable ' detalle orden de trabajo
            Dim rowDet As DataRow ' fila detalle orden de trabajo
            'parametros de busqueda
            objOrdTrab.codigoSubArea_ = Convert.ToInt64(lblCodeSubArea.Text)
            If Trim(lblCodeSucursal.Text) <> "" Then
                codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
            Else
                codeSuc = Nothing
            End If

            'If code1 Is Nothing Then
            '    code1 = Nothing
            'End If

            'If code2 Is Nothing Then
            '    code2 = Nothing
            'End If

            'If code3 Is Nothing Then
            '    code3 = Nothing
            'End If
            pbProgreso.Value += 15
            'objOrdTrab.codigoSucursal_ = Convert.ToInt64(lblCodeSucursal.Text)
            Dim dv As DataView
            If Trim(lblCodeSucursal.Text) <> "" Then
                codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
                dv = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value).DefaultView
                ' MsgBox("E1")
            ElseIf code1 IsNot Nothing Then
                ' MsgBox("E2")
                codeSuc = Nothing
                dv = objOrdTrab.ActualizarListadoHojaDeTrabajoSucursales(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value, code1, code2, code3, code4, code5, code6).DefaultView

            ElseIf E_EspecificarHojaTrabajo.txtSucursal.Text = "" Then
                codeSuc = Nothing
                dv = objOrdTrab.ActualizarListadoHojaDeTrabajo2(pendienteMuestra, noProcesado, enProceso, procesado, validado, dtpDesde.Value, dtpHasta.Value).DefaultView
                ' MsgBox("E3")
            End If
            'dv.RowFilter = String.Format("codigoSucursal Like '%{0}%'", txtsucursal.Text)
            'dv.RowFilter = "codigoSubArea=" & Integer.Parse(lblCodeSubArea.Text)
            dt = dv.ToTable
            If rbtnNroOrdTrab.Checked = True Then
                dt.DefaultView.Sort = "cod_orden_trabajo DESC"
            ElseIf rbtnCortesia.Checked = True Then
                dt.DefaultView.Sort = "codigoTerminoPago DESC"
            ElseIf rbtnNombrePaciente.Checked = True Then
                dt.DefaultView.Sort = "paciente DESC"
            End If
            dv = dt.DefaultView
            dt = dv.ToTable
            pbProgreso.Value += 20
            'ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = False
            For index As Integer = 0 To dt.Rows.Count - 1
                rowO = dt.Rows(index)

                'anuladas del dia de hoy se muestran de lo contrario no

                If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then 'IF ES DE VALIDACION ANULAR DEL DIA



                    edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))
                    'edad = rowO("edad").ToString

                    row = ds.Tables("HojaTrabajo").Rows.Add

                    row.Item(0) = CStr(rowO("cod_orden_trabajo"))
                    If rowO("estadoFactura") = "0" Then

                        If rowO("codigoTerminoPago") = 4 Then
                            row.Item(1) = CStr(rowO("paciente") & " (Cortesía)")
                        Else
                            row.Item(1) = CStr(rowO("paciente"))
                        End If
                        row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
                    Else
                        'anuladas del dia de hoy se muestran de lo contrario no

                        If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then
                            row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
                            row.Item(colColl.IndexOf("Estado")) = "ANULADA"

                        End If

                    End If

                    row.Item(2) = edad
                    row.Item(3) = CStr(rowO("genero").ToString.First)
                    Dim medico As String = ""
                    medico = IIf(rowO("medico") Is DBNull.Value, "", rowO("medico"))
                    row.Item(4) = medico
                    'row.Item(4) = CStr(rowO("medico"))
                    'If IsDBNull((row("medico"))) = True Then
                    '    'row.Item(4) = CStr(rowO("medico"))
                    'End If

                    'LLENADO DETALLE ORDEN DE TRABAJO
                    objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

                    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
                    For index2 As Integer = 0 To dtDet.Rows.Count - 1
                        rowDet = dtDet.Rows(index2)
                        'marcar los * 

                        If IsDBNull(rowDet("resultado")) = True Then
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
                        ElseIf CStr(rowDet("resultado")) = "0" Then
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
                        Else
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
                        End If
                    Next
                ElseIf rowO("estadoFactura") = "0" Then

                    edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))
                    'edad = rowO("edad").ToString

                    row = ds.Tables("HojaTrabajo").Rows.Add

                    row.Item(0) = CStr(rowO("cod_orden_trabajo"))
                    If rowO("estadoFactura") = "0" Then
                        If rowO("codigoTerminoPago") = 4 Then
                            row.Item(1) = CStr(rowO("paciente") & " (Cortesía)")
                        Else
                            row.Item(1) = CStr(rowO("paciente"))
                        End If
                        row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
                    Else
                        'anuladas del dia de hoy se muestran de lo contrario no

                        If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then
                            row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
                            row.Item(colColl.IndexOf("Estado")) = "ANULADA"

                        End If

                    End If
                    row.Item(2) = edad
                    row.Item(3) = CStr(rowO("genero").ToString.First)

                    Dim medico As String = ""
                    medico = IIf(rowO("medico") Is DBNull.Value, "", "Dr(a). " + rowO("medico"))
                    row.Item(4) = medico
                    'IIf(rowO("medico") Is DBNull.Value, row.Item(4) = "", row.Item(4) = rowO("medico"))
                    ''If IsDBNull((row("medico"))) = False Then
                    '    'row.Item(4) = CStr(rowO("medico"))
                    'End If

                    'LLENADO DETALLE ORDEN DE TRABAJO
                    objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

                    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
                    For index2 As Integer = 0 To dtDet.Rows.Count - 1
                        rowDet = dtDet.Rows(index2)
                        'marcar los * 

                        If IsDBNull(rowDet("resultado")) = True Then
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
                        ElseIf CStr(rowDet("resultado")) = "0" Then
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
                        Else
                            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
                        End If
                    Next
                End If 'IF ES DE VALIDACION ANULAR DEL DIA
            Next

            'dgvHojaTrab.Columns(colColl.IndexOf("Estado")).ReadOnly = True
            'ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = True
            'le asigno la tabla
            pbProgreso.Value += 40
            dgvHojaTrab.DataSource = ds.Tables(0)
            dgvHojaTrab.Columns(0).Frozen = True
            dgvHojaTrab.Columns(1).Frozen = True
            dgvHojaTrab.Columns(2).Frozen = True
            dgvHojaTrab.Columns(3).Frozen = True


            If rbtnNombrePaciente.Checked Then
                dgvHojaTrab.Sort(dgvHojaTrab.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
            ElseIf rbtnNroOrdTrab.Checked Then
                dgvHojaTrab.Sort(dgvHojaTrab.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
            ElseIf rbtnCortesia.Checked Then

            ElseIf rbtnUrgentes.Checked Then

            End If
            pbProgreso.Value += 10

            'Limpiar campos 
            txtOrden.Clear()
            txtPaciente.Clear()
            txtParametro.Clear()
            txtValorActual.Clear()
            'dgvHojaTrab.DefaultCellStyle.Font = New Font(Font.Name = "Tahoma", 11, FontStyle.Regular)
            dgvHojaTrab.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvHojaTrab.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter








            'If cbxPendMuestra.Checked = False And cbxNoProcesado.Checked = False And cbxEnProceso.Checked = False And cbxProcesado.Checked = False And cbxValidado.Checked = False Then
            '    MsgBox("Debe seleccionar un estado de orden de trabajo.", MsgBoxStyle.Information)
            '    Exit Sub
            'End If

            ''If dgvHojaTrab.Rows.Count > 1 Then
            'ds.Tables(0).Rows.Clear()
            '' End If

            'Dim pendienteMuestra, noProcesado, enProceso, procesado, validado As String
            'Dim codeSuc As System.Nullable(Of Integer)

            'If cbxPendMuestra.Checked Then
            '    pendienteMuestra = "Pendiente Muestra"
            'Else
            '    pendienteMuestra = Nothing
            'End If
            'If cbxNoProcesado.Checked Then
            '    noProcesado = "No Procesado"
            'Else
            '    noProcesado = Nothing
            'End If
            'If cbxEnProceso.Checked Then
            '    enProceso = "En Proceso"
            'Else
            '    enProceso = Nothing
            'End If
            'If cbxProcesado.Checked Then
            '    procesado = "Procesado"
            'Else
            '    procesado = Nothing
            'End If
            'If cbxValidado.Checked Then
            '    validado = "Validado"
            'Else
            '    validado = Nothing
            'End If

            'Dim edad As String
            'Dim dt As New DataTable
            'Dim row As DataRow


            'Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns

            ''orden de trabajo
            'Dim objOrdTrab As New ClsOrdenDeTrabajo
            ''Dim dt As New DataTable ' ordenes de trabajo
            'Dim rowO As DataRow ' fila orden de trabajo

            ''detalle orden de trabajo
            'Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
            'Dim dtDet As New DataTable ' detalle orden de trabajo
            'Dim rowDet As DataRow ' fila detalle orden de trabajo
            ''parametros de busqueda
            'objOrdTrab.codigoSubArea_ = Convert.ToInt64(lblCodeSubArea.Text)
            'If Trim(lblCodeSucursal.Text) <> "" Then
            '    codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
            'Else
            '    codeSuc = Nothing
            'End If
            ''objOrdTrab.codigoSucursal_ = Convert.ToInt64(lblCodeSucursal.Text)
            'Dim dv As DataView
            'If Trim(lblCodeSucursal.Text) <> "" Then
            '    codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
            '    dv = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value).DefaultView
            'Else
            '    codeSuc = Nothing
            '    dv = objOrdTrab.ActualizarListadoHojaDeTrabajo2(pendienteMuestra, noProcesado, enProceso, procesado, validado, dtpDesde.Value, dtpHasta.Value).DefaultView
            'End If
            ''dv.RowFilter = String.Format("codigoSucursal Like '%{0}%'", txtsucursal.Text)
            ''dv.RowFilter = "codigoSubArea=" & Integer.Parse(lblCodeSubArea.Text)
            'dt = dv.ToTable

            ''ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = False
            'For index As Integer = 0 To dt.Rows.Count - 1
            '    rowO = dt.Rows(index)
            '    edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))

            '    row = ds.Tables("HojaTrabajo").Rows.Add

            '    row.Item(0) = CStr(rowO("cod_orden_trabajo"))
            '    If rowO("estadoFactura") = "0" Then
            '        row.Item(1) = CStr(rowO("paciente"))
            '        row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
            '    Else
            '        row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
            '        row.Item(colColl.IndexOf("Estado")) = "ANULADA"
            '    End If
            '    row.Item(2) = edad
            '    row.Item(3) = CStr(rowO("genero").ToString.First)

            '    If IsDBNull((row("medico"))) = False Then
            '        row.Item(4) = CStr(rowO("medico"))
            '    End If

            '    'LLENADO DETALLE ORDEN DE TRABAJO
            '    objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

            '    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
            '    For index2 As Integer = 0 To dtDet.Rows.Count - 1
            '        rowDet = dtDet.Rows(index2)
            '        'marcar los * 

            '        If IsDBNull(rowDet("resultado")) = True Then
            '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
            '        ElseIf CStr(rowDet("resultado")) = "0" Then
            '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
            '        Else
            '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
            '        End If
            '    Next
            'Next
            ''dgvHojaTrab.Columns(colColl.IndexOf("Estado")).ReadOnly = True
            ''ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = True
            ''le asigno la tabla
            'dgvHojaTrab.DataSource = ds.Tables(0)
            'dgvHojaTrab.Columns(0).Frozen = True
            'dgvHojaTrab.Columns(1).Frozen = True
            'dgvHojaTrab.Columns(2).Frozen = True
            'dgvHojaTrab.Columns(3).Frozen = True
            'GenerarSecuencia(dgvHojaTrab)


            'If rbtnNombrePaciente.Checked Then
            '    dgvHojaTrab.Sort(dgvHojaTrab.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
            'ElseIf rbtnNroOrdTrab.Checked Then
            '    dgvHojaTrab.Sort(dgvHojaTrab.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
            'ElseIf rbtnCortesia.Checked Then

            'ElseIf rbtnUrgentes.Checked Then

            'End If


            ''Limpiar campos 
            'txtOrden.Clear()
            'txtPaciente.Clear()
            'txtParametro.Clear()
            'txtValorActual.Clear()
            'dgvHojaTrab.DefaultCellStyle.Font = New Font("Tahoma", 11)
            'dgvHojaTrab.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'dgvHojaTrab.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


            For index = 0 To dgvHojaTrab.Columns.Count - 1
                dgvHojaTrab.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            dgvHojaTrab.Columns(0).ReadOnly = True
            dgvHojaTrab.Columns(1).ReadOnly = True
            dgvHojaTrab.Columns(2).ReadOnly = True
            dgvHojaTrab.Columns(3).ReadOnly = True
            dgvHojaTrab.Columns(4).ReadOnly = True

            'pbProgreso.Value += 10
            GenerarSecuencia(dgvHojaTrab)
            pbProgreso.Value = 0
        Catch ex As Exception
            MsgBox("Actualizar listado: " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    'Private Sub btnActualizarVista_Click(sender As Object, e As EventArgs) Handles btnActualizarVista.Click
    '    Try
    '        If dgvHojaTrab.Rows.Count > 1 Then
    '            ds.Tables(0).Rows.Clear()
    '        End If
    '        Dim pendienteMuestra, noProcesado, enProceso, procesado, validado As String
    '        If cbxPendMuestra.Checked Then
    '            pendienteMuestra = "Pendiente Muestra"
    '        Else
    '            pendienteMuestra = Nothing
    '        End If
    '        If cbxNoProcesado.Checked Then
    '            noProcesado = "No Procesado"
    '        Else
    '            noProcesado = Nothing
    '        End If
    '        If cbxEnProceso.Checked Then
    '            enProceso = "En Proceso"
    '        Else
    '            enProceso = Nothing
    '        End If
    '        If cbxProcesado.Checked Then
    '            procesado = "Procesado"
    '        Else
    '            procesado = Nothing
    '        End If
    '        If cbxValidado.Checked Then
    '            validado = "Validado"
    '        Else
    '            validado = Nothing
    '        End If

    '        Dim edad As String
    '        Dim dt As New DataTable
    '        Dim row As DataRow


    '        Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns


    '        'orden de trabajo
    '        Dim objOrdTrab As New ClsOrdenDeTrabajo
    '        'Dim dt As New DataTable ' ordenes de trabajo
    '        Dim rowO As DataRow ' fila orden de trabajo

    '        'detalle orden de trabajo
    '        Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
    '        Dim dtDet As New DataTable ' detalle orden de trabajo
    '        Dim rowDet As DataRow ' fila detalle orden de trabajo

    '        'parametros de busqueda
    '        objOrdTrab.codigoSubArea_ = Convert.ToInt64(lblCodeSubArea.Text)
    '        objOrdTrab.codigoSucursal_ = Convert.ToInt64(lblCodeSucursal.Text)
    '        'Dim dv As DataView = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado).DefaultView
    '        dt = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado)
    '        'ds.Tables(0).DefaultView.RowFilter = "codigoSubArea LIKE '*" & lblCodeSubArea.Text & "*'"
    '        'dv.RowFilter = String.Format("codigoSubArea Like '%{0}%'", lblCodeSubArea.Text)
    '        For index As Integer = 0 To dt.Rows.Count - 1
    '            rowO = dt.Rows(index)
    '            edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))

    '            row = ds.Tables("HojaTrabajo").Rows.Add

    '            row.Item(0) = CStr(rowO("cod_orden_trabajo"))
    '            If rowO("estadoFactura") = "0" Then
    '                row.Item(1) = CStr(rowO("paciente"))
    '                row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
    '            Else
    '                row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
    '                row.Item(colColl.IndexOf("Estado")) = "ANULADA"
    '            End If
    '            row.Item(2) = edad
    '            row.Item(3) = CStr(rowO("genero"))
    '            row.Item(4) = CStr(rowO("medico"))

    '            'LLENADO DETALLE ORDEN DE TRABAJO
    '            objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))
    '            dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
    '            For index2 As Integer = 0 To dtDet.Rows.Count - 1
    '                rowDet = dtDet.Rows(index2)
    '                'marcar los * 

    '                If IsDBNull(rowDet("resultado")) = True Then
    '                    row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                ElseIf CStr(rowDet("resultado")) = "0" Then
    '                    row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                Else
    '                    row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
    '                End If
    '            Next
    '        Next

    '        'le asigno la tabla
    '        dgvHojaTrab.DataSource = ds.Tables(0)

    '        If rbtnNombrePaciente.Checked Then
    '            dgvHojaTrab.Sort(dgvHojaTrab.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
    '        ElseIf rbtnNroOrdTrab.Checked Then
    '            dgvHojaTrab.Sort(dgvHojaTrab.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
    '        ElseIf rbtnCortesia.Checked Then

    '        ElseIf rbtnUrgentes.Checked Then

    '        End If

    '    Catch ex As Exception
    '        MsgBox("Actualizar listado: " & ex.Message, MsgBoxStyle.Critical)
    '    End Try

    'End Sub

    Private Function CalcularEdad(ByVal fecha As Date) As String
        Dim yr As Integer = DateDiff(DateInterval.Year, fecha, Now)
        Dim month As Integer = DateDiff(DateInterval.Month, fecha, Now)
        Dim day As Integer = DateDiff(DateInterval.Day, fecha, Now)
        Dim edad As String = ""

        If (Now.Month < fecha.Month) Then
            yr -= 1
        ElseIf (Now.Month = fecha.Month And Now.Day < fecha.Day) Then
            yr -= 1
        End If

        If (yr = 0 And month = 1 And Now.Day < fecha.Day) Then
            month -= 1
        End If

        If (yr >= 1) Then
            edad = yr & "a"
        ElseIf (yr = 0 And month > 0) Then
            edad = month & "m"
        Else
            edad = day & "d"
        End If

        'retorna la edad 
        Return edad
    End Function
    Private Sub LlenadoDatos()


    End Sub

    Private Sub BuscarObservacionesHojaTrabajo()
        If (Trim(txtParametro.Text) <> "") Then
            Try
                Dim objSede As New ClsOrdenDeTrabajo
                With objSede
                    .cod_orden_trabajo_ = txtOrden.Text
                End With
                Dim dt As New DataTable
                dt = objSede.BuscarObservacionesHojaDeTrabajo(txtParametro.Text)
                Dim row As DataRow = dt.Rows(0)
                txtInstrTecnico.Text = CStr(row("observaciones"))
            Catch ex As Exception
                txtInstrTecnico.Text = ""
            End Try
        End If
    End Sub

    Private Sub btnDetalleResultado_Click(sender As Object, e As EventArgs) Handles btnDetalleResultado.Click
        Try
            celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
            fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)

            '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NUEVA CONSULTA
            If txtParametro.Text <> "" And lblcodDescrip.Text <> txtOrden.Text And dgvHojaTrab.Rows(fila).Cells(celda).Value = "Ver Detalle" Or dgvHojaTrab.Rows(fila).Cells(celda).Value = "Ver detalle" Then
                'MsgBox("entra " + lblcodDescrip.Text)

                Dim objItem As New ClsItemExamenDetalle
                Dim rowItem As DataRow
                Dim dtItem As New DataTable
                With objItem
                    .Nombre_ = txtParametro.Text
                    dtItem = .BuscarItemDetalleNombre(Integer.Parse(txtOrden.Text))
                    rowItem = dtItem(0)

                    HT_DescripcionResultado.rtxtDescripcionResultado.Text = "Resultado " + rowItem("descripcion") + ":"
                End With
                'ElseIf txtParametro.Text <> "" And lblcodDescrip.Text <> txtOrden.Text And dgvHojaTrab.Rows(fila).Cells(celda).Value = "Ver detalle" Then
                '    'MsgBox("entra " + lblcodDescrip.Text)

                '    Dim objItem As New ClsItemExamenDetalle
                '    Dim rowItem As DataRow
                '    Dim dtItem As New DataTable
                '    With objItem
                '        .Nombre_ = txtParametro.Text
                '        dtItem = .BuscarItemDetalleNombre(Integer.Parse(txtOrden.Text))
                '        rowItem = dtItem(0)

                '        HT_DescripcionResultado.rtxtDescripcionResultado.Text = "Resultado " + rowItem("descripcion") + ":"
                '    End With

            End If


            '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NUEVA CONSULTA FIN

            '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CODIGO ANTERIOR
            If lblcodDescrip.Text <> txtOrden.Text Then
                'MsgBox("entra sin haber guardado ")
                Dim campo As String
                Dim miseparador() As String
                Dim simbolo As String
                Dim plantillatexto As New ClsPlantillaResultado
                Dim dtPlantillas As New DataTable
                Dim rowPlantillas As DataRow

                celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
                fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                campo = dgvHojaTrab.Rows(fila).Cells(celda).Value

                HT_DescripcionResultado.txtOrdenTrabajo.Text = dgvHojaTrab.Rows(fila).Cells(0).Value
                HT_DescripcionResultado.txtPaciente.Text = txtPaciente.Text
                HT_DescripcionResultado.txtParametro.Text = txtParametro.Text
                miseparador = Split(campo, " ")

                For i = 0 To UBound(miseparador)

                    simbolo = miseparador(i).ToString
                    'dtPlantillas = plantillatexto.BuscarPlantillaXSimbolo(simbolo)
                    plantillatexto.simbolo_ = simbolo
                    dtPlantillas = plantillatexto.BuscarPlantillaXSimboloYSubarea(Integer.Parse(lblCodeSubArea.Text))

                    If dtPlantillas.Rows.Count <> Nothing Then

                        rowPlantillas = dtPlantillas.Rows(0)
                        'HT_DescripcionResultado.rtxtDescripcionResultado.Text = HT_DescripcionResultado.rtxtDescripcionResultado.Text + System.Environment.NewLine + rowPlantillas("simbolo") + rowPlantillas("descripcion")
                        HT_DescripcionResultado.rtxtDescripcionResultado.Text = HT_DescripcionResultado.rtxtDescripcionResultado.Text + System.Environment.NewLine + rowPlantillas("descripcion")

                    End If

                Next i
                MostrarForm(HT_DescripcionResultado)

            Else

                'MsgBox("entra para actualizar ")
                Dim MisResultados As New ClsDescripcionResultado
                Dim dtResultado As New DataTable
                Dim rowResultado As DataRow

                With MisResultados

                    .Codigo_OTrabajo = Integer.Parse(txtOrden.Text)
                    .parametro_ = txtParametro.Text
                    dtResultado = .buscarDescripcionResultado

                    If dtResultado.Rows.Count <> 0 Then
                        rowResultado = dtResultado.Rows(0)
                        HT_DescripcionResultado.txtCodigo.Text = rowResultado("codDescripcionResultado")
                        HT_DescripcionResultado.txtOrdenTrabajo.Text = rowResultado("cod_orden_trabajo")
                        HT_DescripcionResultado.txtPaciente.Text = txtPaciente.Text
                        HT_DescripcionResultado.txtParametro.Text = txtParametro.Text
                        HT_DescripcionResultado.rtxtDescripcionResultado.Text = rowResultado("descripcionResultado")
                        HT_DescripcionResultado.GuardarToolStripMenuItem.Text = "Modificar"
                    Else
                        HT_DescripcionResultado.txtOrdenTrabajo.Text = txtOrden.Text
                        HT_DescripcionResultado.txtPaciente.Text = txtPaciente.Text
                        HT_DescripcionResultado.txtParametro.Text = txtParametro.Text
                    End If

                End With

                MostrarForm(HT_DescripcionResultado)

            End If
            '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CODIGO ANTERIOR FIN
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    'Private Sub btnDetalleResultado_Click(sender As Object, e As EventArgs) Handles btnDetalleResultado.Click
    '    Try

    '        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NUEVA CONSULTA



    '        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NUEVA CONSULTA FIN

    '        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CODIGO ANTERIOR
    '        If lblcodDescrip.Text <> txtOrden.Text Then

    '            Dim campo As String
    '            Dim miseparador() As String
    '            Dim simbolo As String
    '            Dim plantillatexto As New ClsPlantillaResultado
    '            Dim dtPlantillas As New DataTable
    '            Dim rowPlantillas As DataRow

    '            celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
    '            fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
    '            campo = dgvHojaTrab.Rows(fila).Cells(celda).Value

    '            HT_DescripcionResultado.txtOrdenTrabajo.Text = dgvHojaTrab.Rows(fila).Cells(0).Value
    '            HT_DescripcionResultado.txtPaciente.Text = txtPaciente.Text
    '            HT_DescripcionResultado.txtParametro.Text = txtParametro.Text
    '            miseparador = Split(campo, " ")

    '            For i = 0 To UBound(miseparador)

    '                simbolo = miseparador(i).ToString
    '                'dtPlantillas = plantillatexto.BuscarPlantillaXSimbolo(simbolo)
    '                plantillatexto.simbolo_ = simbolo
    '                dtPlantillas = plantillatexto.BuscarPlantillaXSimboloYSubarea(Integer.Parse(lblCodeSubArea.Text))

    '                If dtPlantillas.Rows.Count <> Nothing Then

    '                    rowPlantillas = dtPlantillas.Rows(0)
    '                    'HT_DescripcionResultado.rtxtDescripcionResultado.Text = HT_DescripcionResultado.rtxtDescripcionResultado.Text + System.Environment.NewLine + rowPlantillas("simbolo") + rowPlantillas("descripcion")
    '                    HT_DescripcionResultado.rtxtDescripcionResultado.Text = HT_DescripcionResultado.rtxtDescripcionResultado.Text + System.Environment.NewLine + rowPlantillas("descripcion")

    '                End If

    '            Next i
    '            MostrarForm(HT_DescripcionResultado)

    '        Else

    '            Dim MisResultados As New ClsDescripcionResultado
    '            Dim dtResultado As New DataTable
    '            Dim rowResultado As DataRow

    '            With MisResultados

    '                .Codigo_OTrabajo = Integer.Parse(txtOrden.Text)
    '                .parametro_ = txtParametro.Text
    '                dtResultado = .buscarDescripcionResultado

    '                If dtResultado.Rows.Count <> 0 Then
    '                    rowResultado = dtResultado.Rows(0)
    '                End If

    '            End With

    '            HT_DescripcionResultado.txtCodigo.Text = rowResultado("codDescripcionResultado")
    '            HT_DescripcionResultado.txtOrdenTrabajo.Text = rowResultado("cod_orden_trabajo")
    '            HT_DescripcionResultado.txtPaciente.Text = txtPaciente.Text
    '            HT_DescripcionResultado.txtParametro.Text = txtParametro.Text
    '            HT_DescripcionResultado.rtxtDescripcionResultado.Text = rowResultado("descripcionResultado")
    '            HT_DescripcionResultado.GuardarToolStripMenuItem.Text = "Modificar"
    '            MostrarForm(HT_DescripcionResultado)

    '        End If
    '        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>CODIGO ANTERIOR FIN
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub btnValoresRef_Click(sender As Object, e As EventArgs) Handles btnValoresRef.Click
        If txtParametro.Text <> "" Then
            'M_ListadoValoresReferencia.lblcodeCateCli.Text = txtParametro.Text
            'M_ListadoValoresReferencia.show()
            M_ListadoValoresRefTxt.lblParamtro.Text = txtParametro.Text
            M_ListadoValoresRefTxt.lblcodigo_orden.Text = txtOrden.Text
            M_ListadoValoresRefTxt.lblgenero.Text = dgvHojaTrab.Rows(fila).Cells(3).Value

            M_ListadoValoresRefTxt.Show()
        End If
    End Sub

    Private Sub ValidarDatosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ValidarDatosToolStripMenuItem.Click
        Try
            Dim n As String = MsgBox("¿Desea validar la orden de trabajo?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
                If dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "Validado" And dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "ANULADA" Then
                    'Valida que todos los parametros esten llenos
                    Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
                    Dim dtDet As New DataTable
                    Dim count As Integer = 0

                    objOrdTrabDet.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle

                    For index = 5 To dgvHojaTrab.Columns.Count - 2
                        If Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) = "*" Then
                            MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 1", MsgBoxStyle.Exclamation, "Validación.")
                            Exit Sub
                        ElseIf Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) <> "" Then
                            count += 1
                        End If
                    Next
                    If count <> dtDet.Rows.Count Then
                        'MsgBox(count)
                        'MsgBox(dtDet.Rows.Count)
                        MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 2", MsgBoxStyle.Exclamation, "Validación.")
                        Exit Sub
                    End If

                    'Valida la orden de trabajo
                    Dim objOrdTrab As New ClsOrdenDeTrabajo
                    With objOrdTrab
                        .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                        .coUsuario_ = Integer.Parse(Form1.lblUserCod.Text)

                        'Procedimiento actualizar orden de trabajo
                        If .ModificarOrdenDeTrabajoEstadoValidado() = 1 Then
                            MsgBox("Validado correctamente.", MsgBoxStyle.Information)
                            'dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Validado"
                            'dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(fila))
                            If rbtnMantener.Checked = True Then
                                dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Validado"
                            Else
                                dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(fila))
                            End If
                        Else
                            MsgBox("Error al momento de validar la orden de trabajo.", MsgBoxStyle.Critical, "Validación.")
                        End If

                    End With
                Else
                    'Muestra un mensaje de validacion
                    MsgBox("La orden de trabajo no puede ser validada.", MsgBoxStyle.Exclamation, "Validación.")
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al validar: " + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub eliminarValidados()
        'ELIMNIAR VALIDADOS DE LA HOJA DE TRABAJO
        Try
            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
            Dim estado As Integer = Integer.Parse(colColl.IndexOf("Estado").ToString)
            For index = 0 To dgvHojaTrab.Rows.Count - 1
                If dgvHojaTrab.Rows(index).Cells(estado).Value = "Validado" Then
                    dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(index))
                End If
            Next

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Try
            If Trim(txtBuscar.Text) <> "" Then

                alternarColoFilasDatagridview(dgvHojaTrab)

                For index = 0 To dgvHojaTrab.Rows.Count - 1
                    If dgvHojaTrab.Rows(index).Cells(1).Value.ToString.Contains(txtBuscar.Text) Then
                        dgvHojaTrab.Rows(index).DefaultCellStyle.BackColor = Color.DeepSkyBlue
                    Else
                        dgvHojaTrab.Rows(index).DefaultCellStyle.BackColor = Color.White
                    End If
                Next

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub ValidarResultadosIngresados()
        Try
            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
            fila = Integer.Parse(dgvHojaTrab.CurrentCell.RowIndex.ToString)

            If dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Procesado" Then
                Exit Sub
            End If

            'Valida que todos los parametros esten llenos
            Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
            Dim dtDet As New DataTable
            Dim count As Integer = 0
            Dim bandera As Integer = 0
            objOrdTrabDet.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
            dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle

            For index = 5 To dgvHojaTrab.Columns.Count - 2
                If Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) = "*" Then
                    bandera = 1
                    'MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 1", MsgBoxStyle.Exclamation, "Validación.")
                    Exit For
                ElseIf Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) <> "" Then
                    count += 1
                End If
            Next
            If count <> dtDet.Rows.Count Then
                'MsgBox(count)
                'MsgBox(dtDet.Rows.Count)
                'MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 2", MsgBoxStyle.Exclamation, "Validación.")
                'Exit Sub
                bandera = 1
            End If
            If bandera = 0 Then
                'cambiar el estado
                dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Procesado"
                Dim objOrd As New ClsOrdenDeTrabajo
                objOrd.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                objOrd.prUsuario_ = Integer.Parse(Form1.lblUserCod.Text)
                If objOrd.ModificarOrdenDeTrabajoEstadoProcesado() = 1 Then
                    'MsgBox("Actualizado correctamente el estado.", MsgBoxStyle.Information, "Validación.")
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtParametro_TextChanged(sender As Object, e As EventArgs) Handles txtParametro.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnValidarResultado.Click
        Try
            Dim n As String = MsgBox("¿Desea validar la orden de trabajo?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
                fila = Integer.Parse(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                If dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "Validado" And dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "ANULADA" Then
                    'Valida que todos los parametros esten llenos
                    Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
                    Dim dtDet As New DataTable
                    Dim count As Integer = 0

                    objOrdTrabDet.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle

                    For index = 5 To dgvHojaTrab.Columns.Count - 2
                        If Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) = "*" Then
                            MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 1", MsgBoxStyle.Exclamation, "Validación.")
                            Exit Sub
                        ElseIf Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) <> "" Then
                            count += 1
                        End If
                    Next
                    If count <> dtDet.Rows.Count Then
                        'MsgBox(count)
                        'MsgBox(dtDet.Rows.Count)
                        MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 2", MsgBoxStyle.Exclamation, "Validación.")
                        Exit Sub
                    End If


                    'Valida la orden de trabajo
                    Dim objOrdTrab As New ClsOrdenDeTrabajo
                    With objOrdTrab
                        .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                        .coUsuario_ = Integer.Parse(Form1.lblUserCod.Text)

                        'Procedimiento actualizar orden de trabajo
                        If .ModificarOrdenDeTrabajoEstadoValidado() = 1 Then
                            MsgBox("Validado correctamente.", MsgBoxStyle.Information)
                            If rbtnMantener.Checked = True Then
                                dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Validado"
                            Else
                                dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(fila))
                            End If
                            'Limpia los campos
                            txtOrden.Clear()
                            txtPaciente.Clear()
                            txtParametro.Clear()
                            txtValorActual.Clear()


                        Else
                            MsgBox("Error al momento de validar la orden de trabajo.", MsgBoxStyle.Critical, "Validación.")
                        End If

                    End With


                Else
                    'Muestra un mensaje de validacion
                    MsgBox("La orden de trabajo no puede ser validada.", MsgBoxStyle.Exclamation, "Validación.")
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al validar: " + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub MantenimientoDePlantillasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MantenimientoDePlantillasToolStripMenuItem.Click
        Try
            MostrarForm(A_PlantillasDeResultado)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub OrdenDeTrabajoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OrdenDeTrabajoToolStripMenuItem.Click
        Try
            'Dim n As String = ""
            'If Integer.Parse(txtOrden.Text) >= 0 Then
            '    n = MsgBox("¿Desea ver la orden de trabajo?", MsgBoxStyle.YesNo, "Validación")
            'End If
            'If n = vbYes Then
            '    E_OrdenTrabajo.cargarOrdenTrabajo(Integer.Parse(txtOrden.Text))
            '    E_OrdenTrabajo.Show()
            '    'Me.Close()
            'End If

            If Integer.Parse(txtOrden.Text) >= 0 Then
                E_OrdenTrabajo.cargarOrdenTrabajo(Integer.Parse(txtOrden.Text))
                MostrarForm(E_OrdenTrabajo)
            End If
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btnValidarUnicoResultado_Click(sender As Object, e As EventArgs) Handles btnValidarUnicoResultado.Click
        Try
            Dim n As String = MsgBox("¿Desea validar los resultados de la orden de trabajo?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
                fila = Integer.Parse(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                If dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "Validado" And dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value <> "ANULADA" Then
                    'Valida que todos los parametros esten llenos
                    Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
                    Dim dtDet As New DataTable
                    Dim count As Integer = 0

                    objOrdTrabDet.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle

                    'Valida la orden de trabajo
                    Dim objOrdTrab As New ClsOrdenDeTrabajo
                    With objOrdTrab
                        .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                        .coUsuario_ = Integer.Parse(Form1.lblUserCod.Text)

                        'Procedimiento actualizar orden de trabajo
                        If .ModificarOrdenDeTrabajoResultadoValidado() = 1 Then
                            MsgBox("Validado los resultados correctamente.", MsgBoxStyle.Information)
                        Else
                            MsgBox("Error al momento de validar la orden de trabajo.", MsgBoxStyle.Critical, "Validación.")
                        End If

                    End With

                    For index = 5 To dgvHojaTrab.Columns.Count - 2
                        If Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) = "*" Then
                            'MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 1", MsgBoxStyle.Exclamation, "Validación.")
                            Exit Sub
                        ElseIf Trim(dgvHojaTrab.Rows(fila).Cells(index).Value.ToString) <> "" Then
                            count += 1
                        End If
                    Next
                    If count <> dtDet.Rows.Count Then
                        'MsgBox(count)
                        'MsgBox(dtDet.Rows.Count)
                        'MsgBox("Debe ingresar todos los resultados para validar la orden de trabajo. 2", MsgBoxStyle.Exclamation, "Validación.")
                        Exit Sub
                    End If

                    'Valida la orden de trabajo
                    Dim objOrdTrab2 As New ClsOrdenDeTrabajo
                    With objOrdTrab2
                        .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                        .coUsuario_ = Integer.Parse(Form1.lblUserCod.Text)

                        'Procedimiento actualizar orden de trabajo
                        If .ModificarOrdenDeTrabajoEstadoValidado() = 1 Then
                            MsgBox("Validado correctamente.", MsgBoxStyle.Information)
                            'dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Validado"
                            'dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(fila))
                            If rbtnMantener.Checked = True Then
                                dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "Validado"
                            Else
                                dgvHojaTrab.Rows.Remove(dgvHojaTrab.Rows(fila))
                            End If
                        Else
                            MsgBox("Error al momento de validar la orden de trabajo.", MsgBoxStyle.Critical, "Validación.")
                        End If

                    End With

                Else
                    'Muestra un mensaje de validacion
                    MsgBox("La orden de trabajo no puede ser validada.", MsgBoxStyle.Exclamation, "Validación.")
                End If
            End If
        Catch ex As Exception
            MsgBox("Error al validar: " + ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ImprimirExcelCtrlPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirExcelCtrlPToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvHojaTrab)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ResultadosPorDefectoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResultadosPorDefectoToolStripMenuItem.Click
        Try
            If Trim(txtOrden.Text) <> "" Then

                Dim objOrd As New ClsOrdenDeTrabajo
                objOrd.cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                Dim dt As New DataTable
                dt = objOrd.BuscarResultadosPorDefectoOrdenTrabajo()
                Dim row As DataRow
                Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
                For index = 0 To dt.Rows.Count - 1
                    row = dt.Rows(index)
                    dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf(CStr(row("nombre")))).Value = CStr(row("resultado"))
                    Dim objOrdDet As New ClsOrdenTrabajoDetalle
                    With objOrdDet
                        .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)
                        .resultado_ = CStr(row("resultado"))
                        .nombreItemDetalle_ = CStr(row("nombre"))
                        .estado_ = "Procesado"
                    End With
                    If objOrdDet.ModificarOrdenTrabajoDetalleTecnico() <> 1 Then
                        'En caso que no exista el detalle de orden de trabajo entonces le asigna un valor nulo o vacio
                        MsgBox("Error al modificar el parametro de examen de la orden de trabajo.", MsgBoxStyle.Critical)
                    End If
                Next
                ValidarResultadosIngresados()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ResultadosParametroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResultadosParametroToolStripMenuItem.Click
        Try

            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
            'MsgBox(cadenaIngresada)
            'ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(txtParametro.Text)) = ds.Tables("HojaTrabajo").Rows(fila).Item(colColl.IndexOf(txtParametro.Text))
            M_ListadoResultadosParametro.fila = dgvHojaTrab.CurrentCell.RowIndex.ToString
            'M_ListadoResultadosParametro.fila = fila
            MostrarForm(M_ListadoResultadosParametro)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub cbxPlantillas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPlantillas.SelectedIndexChanged

        Try

            dgvHojaTrab.Rows(fila).Cells(celda).Value = dgvHojaTrab.Rows(fila).Cells(celda).Value + " " + cbxPlantillas.Text

        Catch ex As Exception

        End Try


    End Sub

    Private Sub ImprimirHojaToolStripMenuItem_Click(sender As Object, e As EventArgs)
        'Dim dt As New DataTable
        'dt = dgvHojaTrab.DataSource
        'M_ImprimirHojaTrabajo.CrystalReportViewer1.ReportSource = dt
        'M_ImprimirFacturaReport.CrystalReportViewer1.Refresh()
        'M_ImprimirHojaTrabajo.dgvHojaTrabajo.DataSource = dgvHojaTrab.DataSource
        'M_ImprimirHojaTrabajo.Show()
    End Sub
    Public Function GetColumnasSize(ByVal dg As DataGridView) As Single() 'Se obtiene el ancho de las columnas del Datagridview
        Dim values As Single() = New Single(dg.ColumnCount) {} 'Se declara un array vacio de tipo "Single" cuyo tamaño sera el numero de columnas del datagridview
        values(0) = CSng(35)
        For i As Integer = 0 To dg.ColumnCount - 1 'Con un ciclo for recorremos las columnas del datagridview
            'If dg.Columns(i + 1).Width <= 40 And i > 4 Then
            '    values(i + 1) = CSng(dg.Columns(i + 1).Width + 5) 'Se obtiene y se convierte el ancho de la columna a tipo "Single" para cargarlo en el array "values"

            'Else
            '    values(i + 1) = CSng(dg.Columns(i).Width) 'Se obtiene y se convierte el ancho de la columna a tipo "Single" para cargarlo en el array "values"

            'End If
            values(i + 1) = CSng(dg.Columns(i).Width) 'Se obtiene y se convierte el ancho de la columna a tipo "Single" para cargarlo en el array "values"

        Next
        Return values 'Y por ultimo nos retorna un array, que contiene el ancho de cada columna del datagridview.
    End Function

    'Creamos un proceso para crear el reporte
    '***************************************************
    Public Sub ExportarDatosPDF2(ByVal document As Document)
        Dim size As Integer = 17
        Dim fuente As String = "Times New Roman"
        'Se crea un objeto PDFTable con el numero de columnas del DataGridView. 
        Dim datatable As New PdfPTable(dgvHojaTrab.ColumnCount + 1)
        'Se asignan algunas propiedades para el diseño del PDF.
        datatable.DefaultCell.Padding = 15.5
        Dim headerwidths As Single() = GetColumnasSize(dgvHojaTrab) ' Aqui se realiza una llamada a la funcion GetColumnasSize y le pasamos como parametro nuestro datagridview
        datatable.SetWidths(headerwidths) 'Pasamos como parametro el array que contiene los ancho de columna, a la propiedad "SetWidths"
        datatable.WidthPercentage = 100
        datatable.DefaultCell.BorderWidth = 2
        datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER
        'Se crea el encabezado en el PDF. 
        Dim encabezado As New Paragraph("HOJA DE TRABAJO SLM", New Font(Font.Name = fuente, 20, Font.Bold))

        'Se crea el texto abajo del encabezado.
        Dim texto As New Phrase("IMPRESIÓN: " + Date.Now, New Font(Font.Name = fuente, 14, Font.Bold))
        Dim texto2 As New Phrase("#", New Font(Font.Name = "Arial", 15, 1))

        datatable.AddCell(texto2)
        'Se capturan los nombres de las columnas del DataGridView.
        For i As Integer = 0 To dgvHojaTrab.ColumnCount - 1
            Dim texto3 As New Phrase(dgvHojaTrab.Columns(i).HeaderText, New Font(Font.Name = fuente, size, 1))
            datatable.AddCell(texto3)
        Next

        datatable.HeaderRows = 1
        datatable.DefaultCell.BorderWidth = 1

        'Se generan las columnas del DataGridView. 
        For i As Integer = 0 To dgvHojaTrab.RowCount - 1 'Recorre la filas del datagridview
            Dim texto6 As New Phrase(i + 1, New Font(Font.Name = fuente, size, 1))
            datatable.AddCell(texto6)
            For j As Integer = 0 To dgvHojaTrab.ColumnCount - 1 'Recorre las columnas del datagridview

                Dim texto4 As New Phrase(dgvHojaTrab(j, i).Value.ToString(), New Font(Font.Name = fuente, 20, 1))
                datatable.AddCell(texto4)
            Next
            datatable.CompleteRow()
        Next
        'Se agrega el PDFTable al documento.
        document.Add(encabezado)
        document.Add(texto)
        document.Add(datatable)
    End Sub
    'Creamos un proceso para crear el reporte
    '***************************************************
    Public Sub ExportarDatosPDF(ByVal document As Document)
        Dim size As Integer = 17
        Dim fuente As String = "Times New Roman"
        'Se crea un objeto PDFTable con el numero de columnas del DataGridView. 
        Dim datatable As New PdfPTable(dgvHojaTrab.ColumnCount + 1)
        'Se asignan algunas propiedades para el diseño del PDF.
        datatable.DefaultCell.Padding = 15.5
        Dim headerwidths As Single() = GetColumnasSize(dgvHojaTrab) ' Aqui se realiza una llamada a la funcion GetColumnasSize y le pasamos como parametro nuestro datagridview
        datatable.SetWidths(headerwidths) 'Pasamos como parametro el array que contiene los ancho de columna, a la propiedad "SetWidths"
        datatable.WidthPercentage = 100
        datatable.DefaultCell.BorderWidth = 2
        datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER
        'Se crea el encabezado en el PDF. 
        Dim encabezado As New Paragraph("HOJA DE TRABAJO SLM", New Font(Font.Name = fuente, 20, Font.Bold))

        'Se crea el texto abajo del encabezado.
        Dim texto As New Phrase("IMPRESIÓN: " + Date.Now, New Font(Font.Name = fuente, 14, Font.Bold))
        Dim texto2 As New Phrase("#", New Font(Font.Name = "Arial", 15, 1))

        datatable.AddCell(texto2)
        'Se capturan los nombres de las columnas del DataGridView.
        For i As Integer = 0 To dgvHojaTrab.ColumnCount - 1
            Dim texto3 As New Phrase(dgvHojaTrab.Columns(i).HeaderText, New Font(Font.Name = fuente, size, 1))
            datatable.AddCell(texto3)
        Next

        datatable.HeaderRows = 1
        datatable.DefaultCell.BorderWidth = 1

        'Se generan las columnas del DataGridView. 
        For i As Integer = 0 To dgvHojaTrab.RowCount - 1 'Recorre la filas del datagridview
            Dim texto6 As New Phrase(i + 1, New Font(Font.Name = fuente, size, 1))
            datatable.AddCell(texto6)
            For j As Integer = 0 To dgvHojaTrab.ColumnCount - 2 'Recorre las columnas del datagridview
                If j < 5 Then
                    Dim texto4 As New Phrase(dgvHojaTrab(j, i).Value.ToString(), New Font(Font.Name = fuente, 20, 1))
                    datatable.AddCell(texto4)
                Else
                    datatable.AddCell("")
                End If
            Next
            Dim texto5 As New Phrase(dgvHojaTrab(dgvHojaTrab.ColumnCount - 1, i).Value.ToString(), New Font(Font.Name = fuente, 20, 1))

            datatable.AddCell(texto5)
            datatable.CompleteRow()
        Next
        'Se agrega el PDFTable al documento.
        document.Add(encabezado)
        document.Add(texto)
        document.Add(datatable)
    End Sub

    'Creamos un proceso para crear el reporte
    '***************************************************
    'Public Sub ExportarDatosPDF2(ByVal document As Document)
    '    'Se crea un objeto PDFTable con el numero de columnas del DataGridView. 
    '    Dim datatable As New PdfPTable(dgvHojaTrab.ColumnCount + 1)
    '    'Se asignan algunas propiedades para el diseño del PDF.
    '    datatable.DefaultCell.Padding = 15.5
    '    Dim headerwidths As Single() = GetColumnasSize(dgvHojaTrab) ' Aqui se realiza una llamada a la funcion GetColumnasSize y le pasamos como parametro nuestro datagridview
    '    datatable.SetWidths(headerwidths) 'Pasamos como parametro el array que contiene los ancho de columna, a la propiedad "SetWidths"
    '    datatable.WidthPercentage = 100
    '    datatable.DefaultCell.BorderWidth = 2
    '    datatable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER
    '    'Se crea el encabezado en el PDF. 
    '    Dim encabezado As New Paragraph("HOJA DE TRABAJO SLM", New Font(Font.Name = "Tahoma", 20, Font.Bold))

    '    'Se crea el texto abajo del encabezado.
    '    Dim texto As New Phrase("IMPRESIÓN: " + Date.Now, New Font(Font.Name = "Tahoma", 14, Font.Bold))

    '    datatable.AddCell("#")
    '    'Se capturan los nombres de las columnas del DataGridView.
    '    For i As Integer = 0 To dgvHojaTrab.ColumnCount - 1
    '        datatable.AddCell(dgvHojaTrab.Columns(i).HeaderText)
    '    Next

    '    datatable.HeaderRows = 1
    '    datatable.DefaultCell.BorderWidth = 1

    '    'Se generan las columnas del DataGridView. 
    '    For i As Integer = 0 To dgvHojaTrab.RowCount - 1 'Recorre la filas del datagridview
    '        datatable.AddCell(i + 1)
    '        For j As Integer = 0 To dgvHojaTrab.ColumnCount - 1 'Recorre las columnas del datagridview

    '            datatable.AddCell(dgvHojaTrab(j, i).Value.ToString())
    '        Next
    '        datatable.CompleteRow()
    '    Next
    '    'Se agrega el PDFTable al documento.
    '    document.Add(encabezado)
    '    document.Add(texto)
    '    document.Add(datatable)
    'End Sub
    Private Sub imprimirHojaTrabajo2()
        Try
            'Intentar generar el documento.
            'Dim doc As New Document(PageSize.A4.Rotate(), 10, 10, 10, 10)
            Dim doc As New Document(PageSize.A1.Rotate(), 5, 5, 10, 10)

            'Path que guarda el reporte en el escritorio de windows (Desktop).
            Dim filename As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\Reporteproductos2.pdf"

            Dim file As New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)
            PdfWriter.GetInstance(doc, file)
            doc.Open()
            ExportarDatosPDF(doc)
            doc.Close()
            Process.Start(filename)


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub imprimirHojaTrabajo()
        Try
            'Using outfile As New StreamWriter("C:\Users\Usuario\Documents\Zoom\textFiles.txt")

            '    'aqui derfines la cabecer  
            '    Dim tabla As DataTable
            '    Dim fila As DataRow
            '    Dim col As DataColumn
            '    For Each tabla In ds.Tables
            '        For Each fila In tabla.Rows
            '            For Each col In tabla.Columns
            '                outfile.Write(String.Format("{0}", fila(col)))
            '            Next
            '            outfile.WriteLine("")
            '        Next
            '    Next

            'End Using

            'Intentar generar el documento.
            'Dim doc As New Document(PageSize.A4.Rotate(), 10, 10, 10, 10)
            Dim doc As New Document(PageSize.A1.Rotate(), 10, 10, 10, 10)

            'Path que guarda el reporte en el escritorio de windows (Desktop).
            Dim filename As String = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\Reporteproductos2.pdf"

            Dim file As New FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)
            PdfWriter.GetInstance(doc, file)
            doc.Open()
            ExportarDatosPDF2(doc)
            doc.Close()
            Process.Start(filename)


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    'dgvHojaTrab.CellStateChanged
    Private Sub Form12_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If (e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down) Then

                'MsgBox("entra")
                If e.KeyCode = Keys.Tab Or e.KeyCode = Keys.Right Then
                    'MsgBox("entra en enter")
                    celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString) + 1
                    fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                ElseIf e.KeyCode = Keys.Left Then
                    celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString) - 1
                    fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                ElseIf e.KeyCode = Keys.Up Then
                    celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
                    fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString) - 1
                ElseIf e.KeyCode = Keys.Down Then
                    celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString)
                    fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString) + 1
                End If
                'celda = Convert.ToInt64(dgvHojaTrab.CurrentCell.ColumnIndex.ToString) + 1
                'fila = Convert.ToInt64(dgvHojaTrab.CurrentCell.RowIndex.ToString)
                If celda > 4 And fila >= 0 Then
                    'Mostrar valores de la fila seleccionada
                    txtOrden.Text = dgvHojaTrab.Rows(fila).Cells(0).Value()
                    txtPaciente.Text = dgvHojaTrab.Rows(fila).Cells(1).Value()
                    txtParametro.Text = dgvHojaTrab.Columns.Item(celda).Name
                    txtValorActual.Text = dgvHojaTrab.Rows(fila).Cells(celda).Value()



                    'buscar valores referencia 
                    If (Trim(txtParametro.Text) <> "") Then
                        Try
                            Dim objCat As New ClsValoresReferencia
                            Dim dt As New DataTable
                            dt = objCat.buscarValorReferenciaHojaTrabajo(txtParametro.Text, Integer.Parse(txtOrden.Text), dgvHojaTrab.Rows(fila).Cells(3).Value().ToString.First)
                            Dim row As DataRow = dt.Rows(0)
                            txtValoresRef.Text = CStr(row("valoresReferencia"))
                        Catch ex As Exception
                            txtValoresRef.Text = ""
                        End Try
                    End If

                    'buscar observaciones hoja de trabajo
                    'BuscarObservacionesHojaTrabajo()
                    'ObservacionesOT()

                    'Buscar Descripcion de Resultado::::::::::::::::::::::::::::::::::::::::::::::

                    'Dim plantillaResult As New ClsDescripcionResultado
                    'Dim dtPlantillasResult As New DataTable
                    'Dim rowPlantillasResult As DataRow

                    'With plantillaResult
                    '    .Codigo_OTrabajo = txtOrden.Text
                    '    dtPlantillasResult = .buscarDescripcionResultado
                    '    rowPlantillasResult = dtPlantillasResult.Rows(0)

                    '    If dtPlantillasResult.Rows.Count <= 0 Then

                    '        lblcodDescrip.Text = "codigoResultadoDescrip"
                    '    Else
                    '        lblcodDescrip.Text = rowPlantillasResult("cod_orden_trabajo")
                    '    End If

                    'End With

                    ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

                    'buscar valores referencia 
                    'If (Trim(txtParametro.Text) <> "") Then
                    '    Try
                    '        Dim objCat As New ClsValoresReferencia
                    '        Dim dt As New DataTable
                    '        dt = objCat.buscarValorReferenciaParametro(txtParametro.Text)
                    '        Dim row As DataRow = dt.Rows(0)
                    '        txtValoresRef.Text = CStr(row("ValoresReferencia"))
                    '    Catch ex As Exception
                    '        txtValoresRef.Text = ""
                    '    End Try
                    'End If


                Else
                    txtOrden.Text = dgvHojaTrab.Rows(fila).Cells(0).Value()
                    txtPaciente.Text = dgvHojaTrab.Rows(fila).Cells(1).Value()
                    txtParametro.Text = ""
                    txtValorActual.Text = ""
                    txtInstrTecnico.Text = ""
                    txtValoresRef.Text = ""
                End If
                ObservacionesOT()
            End If


            'BLOQUEO DE FILA ANULADA
            Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns
            If dgvHojaTrab.Rows(fila).Cells(colColl.IndexOf("Estado")).Value = "ANULADA" Then

                dgvHojaTrab.Rows(fila).ReadOnly = True

            End If


            'cadenaIngresada = dgvHojaTrab.CurrentCell.Value.ToString
            'MsgBox(cadenaIngresada)
        Catch ex As Exception
            'MsgBox("Tab:  " & ex.Message)
        End Try

    End Sub



    Private Sub ImrpimirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImrpimirToolStripMenuItem.Click
        'imprimirHojaTrabajo()
    End Sub

    Private Sub ImprimirSinValoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirSinValoresToolStripMenuItem.Click
        imprimirHojaTrabajo2()
    End Sub

    Private Sub ImprimirConValoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirConValoresToolStripMenuItem.Click
        imprimirHojaTrabajo()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvHojaTrab)
        Catch ex As Exception

        End Try
    End Sub

    Sub ObservacionesOT()

        Try

            If txtOrden.Text <> "" Then

                Dim ot As New ClsOrdenDeTrabajo
                Dim dt As New DataTable
                Dim row As DataRow

                With ot

                    .cod_orden_trabajo_ = Integer.Parse(txtOrden.Text)

                    dt = .BuscarObservacionesHT()
                    row = dt.Rows(0)

                    txtInstrTecnico.Text = "NT:" + IIf(row("NOTATECNICO") Is DBNull.Value, "", row("NOTATECNICO").ToString) + " | | | | TM:" + IIf(row("TOMAMUESTRA") Is DBNull.Value, "", row("TOMAMUESTRA").ToString)

                End With

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub GuardarCambiosEnOTToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarCambiosEnOTToolStripMenuItem.Click
        eliminarValidados()
    End Sub

    Private Sub PrevisualizarResultadoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrevisualizarResultadoToolStripMenuItem.Click
        Try

            E_frmReporteOrdenTrabajoResultados.lblform.Text = "Previa"
            E_frmReporteOrdenTrabajoResultados.Show()
        Catch ex As Exception

        End Try

    End Sub

    'Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

    '    'CARGAR DATA EN SEGUNDO PLANO

    '    Try
    '        ' pbProgreso.Value += 5
    '        If cbxPendMuestra.Checked = False And cbxNoProcesado.Checked = False And cbxEnProceso.Checked = False And cbxProcesado.Checked = False And cbxValidado.Checked = False Then
    '            MsgBox("Debe seleccionar un estado de orden de trabajo.", MsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        'If dgvHojaTrab.Rows.Count > 1 Then
    '        ds.Tables(0).Rows.Clear()
    '        ' End If

    '        Dim pendienteMuestra, noProcesado, enProceso, procesado, validado As String
    '        Dim codeSuc As System.Nullable(Of Integer)

    '        If cbxPendMuestra.Checked Then
    '            pendienteMuestra = "Pendiente Muestra"
    '        Else
    '            pendienteMuestra = Nothing
    '        End If
    '        If cbxNoProcesado.Checked Then
    '            noProcesado = "No Procesado"
    '        Else
    '            noProcesado = Nothing
    '        End If
    '        If cbxEnProceso.Checked Then
    '            enProceso = "En Proceso"
    '        Else
    '            enProceso = Nothing
    '        End If
    '        If cbxProcesado.Checked Then
    '            procesado = "Procesado"
    '        Else
    '            procesado = Nothing
    '        End If
    '        If cbxValidado.Checked Then
    '            validado = "Validado"
    '        Else
    '            validado = Nothing
    '        End If

    '        Dim edad As String
    '        Dim dt As New DataTable
    '        Dim row As DataRow


    '        Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns

    '        'orden de trabajo
    '        Dim objOrdTrab As New ClsOrdenDeTrabajo
    '        'Dim dt As New DataTable ' ordenes de trabajo
    '        Dim rowO As DataRow ' fila orden de trabajo

    '        'detalle orden de trabajo
    '        Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
    '        Dim dtDet As New DataTable ' detalle orden de trabajo
    '        Dim rowDet As DataRow ' fila detalle orden de trabajo
    '        'parametros de busqueda
    '        objOrdTrab.codigoSubArea_ = Convert.ToInt64(lblCodeSubArea.Text)
    '        If Trim(lblCodeSucursal.Text) <> "" Then
    '            codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
    '        Else
    '            codeSuc = Nothing
    '        End If

    '        'If code1 Is Nothing Then
    '        '    code1 = Nothing
    '        'End If

    '        'If code2 Is Nothing Then
    '        '    code2 = Nothing
    '        'End If

    '        'If code3 Is Nothing Then
    '        '    code3 = Nothing
    '        'End If
    '        'pbProgreso.Value += 15
    '        'objOrdTrab.codigoSucursal_ = Convert.ToInt64(lblCodeSucursal.Text)
    '        Dim dv As DataView
    '        If Trim(lblCodeSucursal.Text) <> "" Then
    '            codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
    '            dv = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value).DefaultView
    '            ' MsgBox("E1")
    '        ElseIf code1 IsNot Nothing Then
    '            ' MsgBox("E2")
    '            codeSuc = Nothing
    '            dv = objOrdTrab.ActualizarListadoHojaDeTrabajoSucursales(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value, code1, code2, code3, code4, code5, code6).DefaultView

    '        ElseIf E_EspecificarHojaTrabajo.txtSucursal.Text = "" Then
    '            codeSuc = Nothing
    '            dv = objOrdTrab.ActualizarListadoHojaDeTrabajo2(pendienteMuestra, noProcesado, enProceso, procesado, validado, dtpDesde.Value, dtpHasta.Value).DefaultView
    '            ' MsgBox("E3")
    '        End If
    '        'dv.RowFilter = String.Format("codigoSucursal Like '%{0}%'", txtsucursal.Text)
    '        'dv.RowFilter = "codigoSubArea=" & Integer.Parse(lblCodeSubArea.Text)
    '        dt = dv.ToTable
    '        If rbtnNroOrdTrab.Checked = True Then
    '            dt.DefaultView.Sort = "cod_orden_trabajo DESC"
    '        ElseIf rbtnCortesia.Checked = True Then
    '            dt.DefaultView.Sort = "codigoTerminoPago DESC"
    '        ElseIf rbtnNombrePaciente.Checked = True Then
    '            dt.DefaultView.Sort = "paciente DESC"
    '        End If
    '        dv = dt.DefaultView
    '        dt = dv.ToTable
    '        'pbProgreso.Value += 20
    '        'ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = False
    '        For index As Integer = 0 To dt.Rows.Count - 1
    '            rowO = dt.Rows(index)

    '            'anuladas del dia de hoy se muestran de lo contrario no

    '            If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then 'IF ES DE VALIDACION ANULAR DEL DIA



    '                edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))
    '                'edad = rowO("edad").ToString

    '                row = ds.Tables("HojaTrabajo").Rows.Add

    '                row.Item(0) = CStr(rowO("cod_orden_trabajo"))
    '                If rowO("estadoFactura") = "0" Then

    '                    If rowO("codigoTerminoPago") = 4 Then
    '                        row.Item(1) = CStr(rowO("paciente") & " (Cortesía)")
    '                    Else
    '                        row.Item(1) = CStr(rowO("paciente"))
    '                    End If
    '                    row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
    '                Else
    '                    'anuladas del dia de hoy se muestran de lo contrario no

    '                    If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then
    '                        row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
    '                        row.Item(colColl.IndexOf("Estado")) = "ANULADA"

    '                    End If

    '                End If

    '                row.Item(2) = edad
    '                row.Item(3) = CStr(rowO("genero").ToString.First)
    '                Dim medico As String = ""
    '                medico = IIf(rowO("medico") Is DBNull.Value, "", rowO("medico"))
    '                row.Item(4) = medico
    '                'row.Item(4) = CStr(rowO("medico"))
    '                'If IsDBNull((row("medico"))) = True Then
    '                '    'row.Item(4) = CStr(rowO("medico"))
    '                'End If

    '                'LLENADO DETALLE ORDEN DE TRABAJO
    '                objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

    '                dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
    '                For index2 As Integer = 0 To dtDet.Rows.Count - 1
    '                    rowDet = dtDet.Rows(index2)
    '                    'marcar los * 

    '                    If IsDBNull(rowDet("resultado")) = True Then
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                    ElseIf CStr(rowDet("resultado")) = "0" Then
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                    Else
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
    '                    End If
    '                Next
    '            ElseIf rowO("estadoFactura") = "0" Then

    '                edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))
    '                'edad = rowO("edad").ToString

    '                row = ds.Tables("HojaTrabajo").Rows.Add

    '                row.Item(0) = CStr(rowO("cod_orden_trabajo"))
    '                If rowO("estadoFactura") = "0" Then
    '                    If rowO("codigoTerminoPago") = 4 Then
    '                        row.Item(1) = CStr(rowO("paciente") & " (Cortesía)")
    '                    Else
    '                        row.Item(1) = CStr(rowO("paciente"))
    '                    End If
    '                    row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
    '                Else
    '                    'anuladas del dia de hoy se muestran de lo contrario no

    '                    If CDate(Date.Now).ToString("MM/dd/yyy") = CDate(rowO("fechaFactura")).ToString("MM/dd/yyy") And rowO("estadoFactura") = "1" Then
    '                        row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
    '                        row.Item(colColl.IndexOf("Estado")) = "ANULADA"

    '                    End If

    '                End If
    '                row.Item(2) = edad
    '                row.Item(3) = CStr(rowO("genero").ToString.First)

    '                Dim medico As String = ""
    '                medico = IIf(rowO("medico") Is DBNull.Value, "", "Dr(a). " + rowO("medico"))
    '                row.Item(4) = medico
    '                'IIf(rowO("medico") Is DBNull.Value, row.Item(4) = "", row.Item(4) = rowO("medico"))
    '                ''If IsDBNull((row("medico"))) = False Then
    '                '    'row.Item(4) = CStr(rowO("medico"))
    '                'End If

    '                'LLENADO DETALLE ORDEN DE TRABAJO
    '                objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

    '                dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
    '                For index2 As Integer = 0 To dtDet.Rows.Count - 1
    '                    rowDet = dtDet.Rows(index2)
    '                    'marcar los * 

    '                    If IsDBNull(rowDet("resultado")) = True Then
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                    ElseIf CStr(rowDet("resultado")) = "0" Then
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '                    Else
    '                        row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
    '                    End If
    '                Next
    '            End If 'IF ES DE VALIDACION ANULAR DEL DIA
    '        Next

    '        'dgvHojaTrab.Columns(colColl.IndexOf("Estado")).ReadOnly = True
    '        'ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = True
    '        'le asigno la tabla
    '        'pbProgreso.Value += 40
    '        dgvHojaTrab.DataSource = ds.Tables(0)
    '        dgvHojaTrab.Columns(0).Frozen = True
    '        dgvHojaTrab.Columns(1).Frozen = True
    '        dgvHojaTrab.Columns(2).Frozen = True
    '        dgvHojaTrab.Columns(3).Frozen = True


    '        If rbtnNombrePaciente.Checked Then
    '            dgvHojaTrab.Sort(dgvHojaTrab.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
    '        ElseIf rbtnNroOrdTrab.Checked Then
    '            dgvHojaTrab.Sort(dgvHojaTrab.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
    '        ElseIf rbtnCortesia.Checked Then

    '        ElseIf rbtnUrgentes.Checked Then

    '        End If
    '        'pbProgreso.Value += 10

    '        'Limpiar campos 
    '        txtOrden.Clear()
    '        txtPaciente.Clear()
    '        txtParametro.Clear()
    '        txtValorActual.Clear()
    '        'dgvHojaTrab.DefaultCellStyle.Font = New Font(Font.Name = "Tahoma", 11, FontStyle.Regular)
    '        dgvHojaTrab.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '        dgvHojaTrab.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter








    '        'If cbxPendMuestra.Checked = False And cbxNoProcesado.Checked = False And cbxEnProceso.Checked = False And cbxProcesado.Checked = False And cbxValidado.Checked = False Then
    '        '    MsgBox("Debe seleccionar un estado de orden de trabajo.", MsgBoxStyle.Information)
    '        '    Exit Sub
    '        'End If

    '        ''If dgvHojaTrab.Rows.Count > 1 Then
    '        'ds.Tables(0).Rows.Clear()
    '        '' End If

    '        'Dim pendienteMuestra, noProcesado, enProceso, procesado, validado As String
    '        'Dim codeSuc As System.Nullable(Of Integer)

    '        'If cbxPendMuestra.Checked Then
    '        '    pendienteMuestra = "Pendiente Muestra"
    '        'Else
    '        '    pendienteMuestra = Nothing
    '        'End If
    '        'If cbxNoProcesado.Checked Then
    '        '    noProcesado = "No Procesado"
    '        'Else
    '        '    noProcesado = Nothing
    '        'End If
    '        'If cbxEnProceso.Checked Then
    '        '    enProceso = "En Proceso"
    '        'Else
    '        '    enProceso = Nothing
    '        'End If
    '        'If cbxProcesado.Checked Then
    '        '    procesado = "Procesado"
    '        'Else
    '        '    procesado = Nothing
    '        'End If
    '        'If cbxValidado.Checked Then
    '        '    validado = "Validado"
    '        'Else
    '        '    validado = Nothing
    '        'End If

    '        'Dim edad As String
    '        'Dim dt As New DataTable
    '        'Dim row As DataRow


    '        'Dim colColl As DataColumnCollection = ds.Tables("HojaTrabajo").Columns

    '        ''orden de trabajo
    '        'Dim objOrdTrab As New ClsOrdenDeTrabajo
    '        ''Dim dt As New DataTable ' ordenes de trabajo
    '        'Dim rowO As DataRow ' fila orden de trabajo

    '        ''detalle orden de trabajo
    '        'Dim objOrdTrabDet As New ClsOrdenTrabajoDetalle
    '        'Dim dtDet As New DataTable ' detalle orden de trabajo
    '        'Dim rowDet As DataRow ' fila detalle orden de trabajo
    '        ''parametros de busqueda
    '        'objOrdTrab.codigoSubArea_ = Convert.ToInt64(lblCodeSubArea.Text)
    '        'If Trim(lblCodeSucursal.Text) <> "" Then
    '        '    codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
    '        'Else
    '        '    codeSuc = Nothing
    '        'End If
    '        ''objOrdTrab.codigoSucursal_ = Convert.ToInt64(lblCodeSucursal.Text)
    '        'Dim dv As DataView
    '        'If Trim(lblCodeSucursal.Text) <> "" Then
    '        '    codeSuc = Convert.ToInt64(lblCodeSucursal.Text)
    '        '    dv = objOrdTrab.ActualizarListadoHojaDeTrabajo(pendienteMuestra, noProcesado, enProceso, procesado, validado, codeSuc, dtpDesde.Value, dtpHasta.Value).DefaultView
    '        'Else
    '        '    codeSuc = Nothing
    '        '    dv = objOrdTrab.ActualizarListadoHojaDeTrabajo2(pendienteMuestra, noProcesado, enProceso, procesado, validado, dtpDesde.Value, dtpHasta.Value).DefaultView
    '        'End If
    '        ''dv.RowFilter = String.Format("codigoSucursal Like '%{0}%'", txtsucursal.Text)
    '        ''dv.RowFilter = "codigoSubArea=" & Integer.Parse(lblCodeSubArea.Text)
    '        'dt = dv.ToTable

    '        ''ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = False
    '        'For index As Integer = 0 To dt.Rows.Count - 1
    '        '    rowO = dt.Rows(index)
    '        '    edad = CalcularEdad(Convert.ToDateTime(rowO("fechaNacimiento")))

    '        '    row = ds.Tables("HojaTrabajo").Rows.Add

    '        '    row.Item(0) = CStr(rowO("cod_orden_trabajo"))
    '        '    If rowO("estadoFactura") = "0" Then
    '        '        row.Item(1) = CStr(rowO("paciente"))
    '        '        row.Item(colColl.IndexOf("Estado")) = CStr(rowO("estado"))
    '        '    Else
    '        '        row.Item(1) = CStr(rowO("paciente") & " (ANULADA)")
    '        '        row.Item(colColl.IndexOf("Estado")) = "ANULADA"
    '        '    End If
    '        '    row.Item(2) = edad
    '        '    row.Item(3) = CStr(rowO("genero").ToString.First)

    '        '    If IsDBNull((row("medico"))) = False Then
    '        '        row.Item(4) = CStr(rowO("medico"))
    '        '    End If

    '        '    'LLENADO DETALLE ORDEN DE TRABAJO
    '        '    objOrdTrabDet.cod_orden_trabajo_ = Convert.ToInt64(rowO("cod_orden_trabajo"))

    '        '    dtDet = objOrdTrabDet.BuscarOrdenTrabajoDetalle
    '        '    For index2 As Integer = 0 To dtDet.Rows.Count - 1
    '        '        rowDet = dtDet.Rows(index2)
    '        '        'marcar los * 

    '        '        If IsDBNull(rowDet("resultado")) = True Then
    '        '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '        '        ElseIf CStr(rowDet("resultado")) = "0" Then
    '        '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = "*"
    '        '        Else
    '        '            row.Item(colColl.IndexOf(CStr(rowDet("nombre")))) = CStr(rowDet("resultado"))
    '        '        End If
    '        '    Next
    '        'Next
    '        ''dgvHojaTrab.Columns(colColl.IndexOf("Estado")).ReadOnly = True
    '        ''ds.Tables(0).Columns(colColl.IndexOf("Estado")).ReadOnly = True
    '        ''le asigno la tabla
    '        'dgvHojaTrab.DataSource = ds.Tables(0)
    '        'dgvHojaTrab.Columns(0).Frozen = True
    '        'dgvHojaTrab.Columns(1).Frozen = True
    '        'dgvHojaTrab.Columns(2).Frozen = True
    '        'dgvHojaTrab.Columns(3).Frozen = True
    '        'GenerarSecuencia(dgvHojaTrab)


    '        'If rbtnNombrePaciente.Checked Then
    '        '    dgvHojaTrab.Sort(dgvHojaTrab.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
    '        'ElseIf rbtnNroOrdTrab.Checked Then
    '        '    dgvHojaTrab.Sort(dgvHojaTrab.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
    '        'ElseIf rbtnCortesia.Checked Then

    '        'ElseIf rbtnUrgentes.Checked Then

    '        'End If


    '        ''Limpiar campos 
    '        'txtOrden.Clear()
    '        'txtPaciente.Clear()
    '        'txtParametro.Clear()
    '        'txtValorActual.Clear()
    '        'dgvHojaTrab.DefaultCellStyle.Font = New Font("Tahoma", 11)
    '        'dgvHojaTrab.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '        'dgvHojaTrab.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


    '        For index = 0 To dgvHojaTrab.Columns.Count - 1
    '            dgvHojaTrab.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
    '        Next

    '        dgvHojaTrab.Columns(0).ReadOnly = True
    '        dgvHojaTrab.Columns(1).ReadOnly = True
    '        dgvHojaTrab.Columns(2).ReadOnly = True
    '        dgvHojaTrab.Columns(3).ReadOnly = True
    '        dgvHojaTrab.Columns(4).ReadOnly = True

    '        'pbProgreso.Value += 10
    '        GenerarSecuencia(dgvHojaTrab)
    '        'pbProgreso.Value = 0
    '    Catch ex As Exception
    '        MsgBox("Actualizar listado: " & ex.Message, MsgBoxStyle.Critical)
    '    End Try






    'End Sub

    Private Sub GenerarSecuencia(grid As DataGridView)

        If grid IsNot Nothing Then

            For Each r As DataGridViewRow In grid.Rows

                r.HeaderCell.Value = (r.Index + 1).ToString

            Next

        End If



    End Sub

End Class