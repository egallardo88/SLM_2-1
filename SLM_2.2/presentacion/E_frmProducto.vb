﻿Imports System.IO

Public Class E_frmProducto
    'ESTA VENTANA YA TIENE BITACORA
    Private top As String
    Private Sub E_frmProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet8.Departamento' Puede moverla o quitarla según sea necesario.
        Me.DepartamentoTableAdapter.Fill(Me.Slm_testDataSet8.Departamento)
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet7.Centro_Costo' Puede moverla o quitarla según sea necesario.
        Me.Centro_CostoTableAdapter.Fill(Me.Slm_testDataSet7.Centro_Costo)

        RegistrarVentanas(nombre_usurio, Me.Name)
        'llenar combobox unidad medida
        Dim clsD As New ClsUnidadMedidaAlmacen
        Dim clsCP As New ClsCategoriaroducto
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        Dim ds As New DataTable
        Dim ds2 As New DataTable


        ds.Load(clsD.RecuperarUnidadMedida())
        ds2.Load(clsCP.RecuperarCategoriaProducto())



        cmbUnidadMedida.DataSource = ds
        cmbUnidadMedida.DisplayMember = "nombre_unidad_medida"
        cmbUnidadMedida.ValueMember = "id_unidad_medida"

        'llenar combobox categoria producto
        cmbCategoria.DataSource = ds2
        cmbCategoria.DisplayMember = "nombre_categoria"
        cmbCategoria.ValueMember = "id_categoria_producto"

        txtCantidadMinima.Text = "0"


        ' DataGridView1.DataSource = TableUM
        'campos 
        txtNombre.ReadOnly = True
        txtModelo.ReadOnly = True
        txtMarca.ReadOnly = True

        txtDescripcion.ReadOnly = True
        txtCantidadMinima.ReadOnly = True
        'botones
        Button1.Enabled = False
        alternarColoFilasDatagridview(DataGridView1)
        cargarData()
        ' BackgroundWorker1.RunWorkerAsync()
        'cargarComboCP()

    End Sub
    Private Sub cargarData()
        Try
            'datagridview
            Dim TableUM As New DataTable
            Dim clsP As New ClsProducto
            TableUM.Load(clsP.RecuperarProductoVentanaProducto())
            BindingSource1.DataSource = TableUM

            DataGridView1.DataSource = BindingSource1

            BindingNavigator1.BindingSource = BindingSource1
        Catch ex As Exception

            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub
    Public Sub cargarComboCP()
        Try
            Dim clsC As New ClsCuentasAlmacen
            Dim ds As New DataTable

            ds.Load(clsC.RecuperarCP)


            ComboBox1.DataSource = ds
            ComboBox1.DisplayMember = "nombre"
            ComboBox1.ValueMember = "cuenta"
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try



    End Sub

    Public Sub cargarComboCS()
        Try
            Dim clsC As New ClsCuentasAlmacen
            Dim ds As New DataTable

            ds.Load(clsC.RecuperarCSProductos(ComboBox1.SelectedValue.ToString))


            'ComboBox2.DataSource = ds
            'ComboBox2.DisplayMember = "nombre"
            'ComboBox2.ValueMember = "cuenta"
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try



    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        'botones
        Button1.Enabled = True

        PictureBox1.Image = Nothing
        'campos 
        txtNombre.ReadOnly = False
        txtModelo.ReadOnly = False
        txtMarca.ReadOnly = False
        txtPrecioProducto.ReadOnly = False
        txtDescripcion.ReadOnly = False
        txtCantidadMinima.ReadOnly = False
        'txtCodigoBarra.ReadOnly = False
        txtFabricante.ReadOnly = False
        txtProveedor.ReadOnly = False
        Try
            'ComboBox2.Text = ""
            'seleccionar datos gridview 
            txtCodigo.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtNombre.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            txtPrecioProducto.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            txtMarca.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            txtModelo.Text = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            txtDescripcion.Text = DataGridView1.Rows(e.RowIndex).Cells(5).Value
            txtCantidadMinima.Text = DataGridView1.Rows(e.RowIndex).Cells(6).Value
            cmbUnidadMedida.Text = DataGridView1.Rows(e.RowIndex).Cells(7).Value
            cmbCategoria.Text = DataGridView1.Rows(e.RowIndex).Cells(8).Value

            ComboBox3.SelectedValue = DataGridView1.Rows(e.RowIndex).Cells(9).Value
            TextBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(10).Value.ToString
            ComboBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(11).Value.ToString

            'ComboBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(10).Value
            txtCodigoBarra.Text = DataGridView1.Rows(e.RowIndex).Cells(12).Value
            Dim clsp As New ClsProducto

            Dim datos As Byte() = clsp.RecuperarImagenProducto(DataGridView1.Rows(e.RowIndex).Cells(0).Value)
            Dim ms = New MemoryStream(datos)

            PictureBox1.Image = Image.FromStream(ms)


        Catch ex As Exception

            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Dim clsP As New ClsProducto
        'validar campos vacios
        If Campo_requerido(txtNombre, Label2) = 1 Then
            Exit Sub
        End If
        If validarGuardar("Producto") = 1 Then
            'registar producto
            If txtCodigo.Text = "" Then
                Try
                    With clsP

                        .NombreProducto = txtNombre.Text
                        .MarcaProducto = txtMarca.Text
                        .ModeloProducto = txtModelo.Text
                        .DescripcionProducto = txtDescripcion.Text
                        .ExistenciaProducto = "0"
                        .CantidadMinimaProducto = Integer.Parse(txtCantidadMinima.Text)
                        .UnidadMedida = Integer.Parse(cmbUnidadMedida.SelectedValue)
                        .CategoriaProducto = Integer.Parse(cmbCategoria.SelectedValue)
                        .Precio_base1 = txtPrecioProducto.Text
                        'cuentas

                    End With
                Catch ex As Exception
                    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)

                    MsgBox(mensaje_error_actualizacion)
                    Exit Sub
                End Try
                'actualizar producto
                Try
                    If clsP.RegistrarProducto() = "1" Then
                        'campos 
                        txtNombre.ReadOnly = True
                        txtModelo.ReadOnly = True
                        txtMarca.ReadOnly = True
                        txtPrecioProducto.ReadOnly = True
                        txtDescripcion.ReadOnly = True
                        txtCantidadMinima.ReadOnly = True
                        txtCodigoBarra.ReadOnly = True
                        'botones
                        Button1.Enabled = False
                        MsgBox(mensaje_registro)
                        cargarData()
                        RegistrarAcciones(nombre_usurio, Me.Name, "Registrar producto" + txtNombre.Text)
                    End If
                Catch ex As Exception
                    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                    MsgBox(mensaje_error_registro)
                End Try


            ElseIf txtCodigo.Text <> "" Then
                With clsP
                    .IdProducto = Integer.Parse(txtCodigo.Text)
                    .NombreProducto = txtNombre.Text
                    .MarcaProducto = txtMarca.Text
                    .ModeloProducto = txtModelo.Text
                    .DescripcionProducto = txtDescripcion.Text
                    .ExistenciaProducto = "0"
                    .CantidadMinimaProducto = Integer.Parse(txtCantidadMinima.Text)
                    .UnidadMedida = Integer.Parse(cmbUnidadMedida.SelectedValue)
                    .CategoriaProducto = Integer.Parse(cmbCategoria.SelectedValue)
                    .Precio_base1 = txtPrecioProducto.Text
                    'cuentas


                End With
                If clsP.ActualizarProducto() = "1" Then
                    MsgBox(mensaje_actualizacion)
                    RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar producto id" + txtCodigo.Text)
                    cargarData()
                End If
            End If
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub



    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs)
        'Dim objOrd As New ClsProducto


        'Dim dv As DataView = objOrd.RecuperarProducto2.DefaultView

        'dv.RowFilter = String.Format("CONVERT(nombre_producto+marca+modelo, System.String) LIKE '%{0}%'", txtBuscar.Text)

        'DataGridView1.DataSource = dv


    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        GridAExcel(DataGridView1)
    End Sub
    Function GridAExcel(ByVal miDataGridView As DataGridView) As Boolean
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet
        Try
            exLibro = exApp.Workbooks.Add 'crea el libro de excel 
            exHoja = exLibro.Worksheets.Add() 'cuenta filas y columnas
            Dim NCol As Integer = miDataGridView.ColumnCount
            Dim NRow As Integer = miDataGridView.RowCount
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = miDataGridView.Columns(i - 1).Name.ToString
            Next
            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = miDataGridView.Rows(Fila).Cells(Col).Value
                Next
            Next
            exHoja.Rows.Item(1).Font.Bold = 1 'titulo en negritas
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            'alineacion al centro
            exHoja.Columns.AutoFit() 'autoajuste de la columna
            exHoja.Columns.HorizontalAlignment = 2
            exApp.Application.Visible = True
            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
            Return False
        End Try
        Return True
    End Function




    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Try


            Dim clsP As New ClsProducto
            With clsP
                .IdProducto = txtCodigo.Text
            End With
            If validarGuardar("Producto") = 1 Then
                Try
                    If clsP.BajarProducto = "1" Then
                        MsgBox(mensaje_dar_baja)
                        RegistrarAcciones(nombre_usurio, Me.Name, "Eliminar producto id " + txtCodigo.Text)
                        cargarData()
                    End If
                Catch ex As Exception
                    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                    MsgBox("No ha seleccionado ninguna fila")
                End Try

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'limpiar despues de registar
        'botones
        Button1.Enabled = True
        'campos 
        txtNombre.ReadOnly = False
        txtModelo.ReadOnly = False
        txtMarca.ReadOnly = False

        txtDescripcion.ReadOnly = False
        txtCantidadMinima.ReadOnly = False
        txtPrecioProducto.ReadOnly = False
        txtCodigoBarra.ReadOnly = False
        txtFabricante.ReadOnly = False
        txtProveedor.ReadOnly = False
        'limpiar
        txtCodigo.Text = ""
        txtNombre.Text = ""
        txtModelo.Text = ""
        txtMarca.Text = ""
        ' TextBox1.Text = ""
        TextBox2.Text = ""
        txtCodigoBarra.Text = ""
        txtPrecioProducto.Text = ""
        txtDescripcion.Text = ""
        txtCantidadMinima.Text = ""
        txtFabricante.Clear()
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub ToolStripComboBox1_TextChanged(sender As Object, e As EventArgs)
        'cambiar tabla
        'top = ToolStripComboBox1.Text
        'If top <> "Todos" Then
        '    cargarData("top" + " " + top)
        'Else
        '    cargarData("")

        'End If

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Dim clsP As New ClsProducto
        Dim ms As New System.IO.MemoryStream()
        If TextBox2.Text = "" Then
            MsgBox("Debe agregar la cuenta contable")
        End If
        Try

            PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
        Catch ex As Exception
            MsgBox("La imagen es obligatoria")
            Exit Sub
        End Try

        'validar campos vacios
        If Campo_requerido(txtNombre, Label2) = 1 Then
            Exit Sub
        End If
        If validarGuardar("Guardar Producto") = 1 Then
            'registar producto
            If txtCodigo.Text = "" Then
                Try
                    With clsP

                        .NombreProducto = txtNombre.Text
                        .MarcaProducto = txtMarca.Text
                        .ModeloProducto = txtModelo.Text
                        .DescripcionProducto = txtDescripcion.Text
                        .ExistenciaProducto = "0"
                        .CantidadMinimaProducto = Integer.Parse(txtCantidadMinima.Text)
                        .UnidadMedida = Integer.Parse(cmbUnidadMedida.SelectedValue)
                        .CategoriaProducto = Integer.Parse(cmbCategoria.SelectedValue)
                        .Precio_base1 = txtPrecioProducto.Text
                        'cuentas
                        .Centrocosto1 = ComboBox3.SelectedValue
                        .CuentaCC1 = TextBox2.Text
                        .Grupo1 = ComboBox1.Text

                        .Nombrecuenta1 = ComboBox3.Text
                        .Nombrecuenta21 = TextBox2.Text
                        .CodigoBarra1 = txtCodigoBarra.Text
                        .Fabricante1 = txtFabricante.Text
                        .Proveedor1 = txtProveedor.Text
                        .Imagen1 = ms.GetBuffer()
                    End With
                Catch ex As Exception
                    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                    MsgBox(mensaje_error_actualizacion)

                    Exit Sub
                End Try
                'actualizar producto
                Try
                    If clsP.RegistrarProducto() = "1" Then
                        'campos 
                        txtNombre.ReadOnly = True
                        txtModelo.ReadOnly = True
                        txtMarca.ReadOnly = True
                        txtPrecioProducto.ReadOnly = True
                        txtDescripcion.ReadOnly = True
                        txtCantidadMinima.ReadOnly = True
                        txtFabricante.ReadOnly = True
                        txtProveedor.ReadOnly = True
                        txtCodigoBarra.ReadOnly = True
                        'botones
                        Button1.Enabled = False
                        MsgBox(mensaje_registro)
                        RegistrarAcciones(nombre_usurio, Me.Name, "Registrar")
                        cargarData()
                    ElseIf clsP.RegistrarProducto() = "2" Then
                        MsgBox("Este producto ya se encuentra registrado")
                    End If
                Catch ex As Exception
                    RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                    MsgBox(mensaje_error_registro)
                End Try

                'REGISTRAR UN NUEVO PRODUCTO
            ElseIf txtCodigo.Text <> "" Then
                With clsP
                    .IdProducto = Integer.Parse(txtCodigo.Text)
                    .NombreProducto = txtNombre.Text
                    .MarcaProducto = txtMarca.Text
                    .ModeloProducto = txtModelo.Text
                    .DescripcionProducto = txtDescripcion.Text
                    .ExistenciaProducto = "0"
                    .CantidadMinimaProducto = Integer.Parse(txtCantidadMinima.Text)
                    .UnidadMedida = Integer.Parse(cmbUnidadMedida.SelectedValue)
                    .CategoriaProducto = Integer.Parse(cmbCategoria.SelectedValue)
                    .Precio_base1 = txtPrecioProducto.Text
                    'CUENTAS DE ALMACEN

                    .Centrocosto1 = ComboBox3.SelectedValue
                    .CuentaCC1 = TextBox2.Text
                    .Grupo1 = ComboBox1.Text
                    .Nombrecuenta1 = ComboBox3.Text
                    .Nombrecuenta21 = TextBox2.Text
                    .CodigoBarra1 = txtCodigoBarra.Text
                    .Fabricante1 = txtFabricante.Text
                    .Proveedor1 = txtProveedor.Text
                    .Imagen1 = ms.GetBuffer()
                End With
                If clsP.ActualizarProducto() = "1" Then
                    MsgBox(mensaje_actualizacion)
                    RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar")
                    cargarData()
                End If
            End If
        End If
    End Sub

    Private Sub ToolStripTextBox1_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox1.TextChanged
        Dim objOrd As New ClsProducto


        'Dim dv As DataView = objOrd.RecuperarProducto2.DefaultView

        'dv.RowFilter = String.Format("CONVERT(nombre_producto+marca+modelo, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)

        '.DataSource = dv

        ' BindingSource1.DataSource = objOrd.RecuperarProducto2.DefaultView
        BindingSource1.Filter = String.Format("CONVERT(nombre_producto+marca+modelo+codigoBarra, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
        DataGridView1.DataSource = BindingSource1
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Try
            Dim result As New DialogResult
            OpenFileDialog1.InitialDirectory = “C:\”
            OpenFileDialog1.Filter = “archivos de imagen (*.jpg)|*.png|All files (*.*)|*.*”
            OpenFileDialog1.FilterIndex = 3
            OpenFileDialog1.RestoreDirectory = True
            result = OpenFileDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                Dim hola As New FileInfo(OpenFileDialog1.FileName)
                If hola.Length < 1000000 Then
                    PictureBox1.Image = Image.FromFile(OpenFileDialog1.FileName)
                Else
                    MsgBox("El tamaño de la imagen excede el limte de 1 mb")
                End If



            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        cargarData()
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    'Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
    '    E_frmCategoriaProducto.Show()
    'End Sub

    'Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
    '    E_frmUnidadMedida_almacen.Show()
    'End Sub

    'Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
    '    E_frmCuentaAlmacen.Show()
    'End Sub
End Class