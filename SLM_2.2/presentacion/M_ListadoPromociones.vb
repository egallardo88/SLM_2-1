﻿Public Class M_ListadoPromociones
    Dim objPromo As New ClsPromociones
    Dim objFeriado As New ClsFeriados
    Dim promo As New ClsPromociones
    Private Sub M_ListadoPromociones_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If lblform.Text = "Form" Then

            seleccionarPromociones()
        Else
            CargarData()
        End If


        'Me.dgbtabla.Columns("contador").Visible = False
        'Me.dgbtabla.Columns("img").Visible = False
    End Sub
    Private Sub seleccionarPromociones()
        Dim dv As DataView = objPromo.SeleccionarPromociones.DefaultView
        dgbtabla.DataSource = dv
        lblcantidad.Text = dv.Count
        dgbtabla.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
        alternarColoFilasDatagridview(dgbtabla)
    End Sub


    Private Sub CargarData()

        'LLENAR EL DATA GRID VIEW 
        dgbtabla.DataSource = promo.ListarPromociones

    End Sub



    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub
    Private Sub dgbtabla_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgbtabla.CellMouseDoubleClick
        Try

            Dim n As String = ""
            If lblform.Text = "Form" Then 'LLAMADO DE FORMULARIO

                Dim dt, dtc As New DataTable
                Dim row, rowc As DataRow

                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar la promoción en la factura?", MsgBoxStyle.YesNo)
                End If
                If n = vbYes And M_Factura.validarFactura2(dgbtabla.Rows(e.RowIndex).Cells(0).Value()) = 0 Then
                    'M_Factura.dgblistadoExamenes.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "1", dgbtabla.Rows(e.RowIndex).Cells(5).Value(), dgbtabla.Rows(e.RowIndex).Cells(1).Value(), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", dgbtabla.Rows(e.RowIndex).Cells(5).Value()})
                    'M_Factura.dgblistadoExamenes.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "1", dgbtabla.Rows(e.RowIndex).Cells(5).Value(), dgbtabla.Rows(e.RowIndex).Cells(1).Value(), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", dgbtabla.Rows(e.RowIndex).Cells(5).Value(), "0", "0", dgbtabla.Rows(e.RowIndex).Cells(0).Value()})
                    M_Factura.dgblistadoExamenes.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "1", "0", dgbtabla.Rows(e.RowIndex).Cells(1).Value(), M_Factura.dtpfechaFactura.Value.Date.AddDays(0), "0", "0", "0", "0", dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "0"})
                    ' M_ClienteVentana.dgvtabla.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "1", dgbtabla.Rows(e.RowIndex).Cells(5).Value(), dgbtabla.Rows(e.RowIndex).Cells(1).Value(), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", dgbtabla.Rows(e.RowIndex).Cells(5).Value()})
                    M_ClienteVentana.dgvtabla.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), "1", "0", dgbtabla.Rows(e.RowIndex).Cells(1).Value(), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", "0"})

                    'observaciones
                    M_Factura.dgbObservaciones.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), ""})
                    M_Factura.dgbObservaciones2.Rows.Add(New String() {dgbtabla.Rows(e.RowIndex).Cells(0).Value(), ""})

                    Dim objDetPromo As New ClsDetallePromociones
                    objDetPromo.codigoPromocion_ = Integer.Parse(dgbtabla.Rows(e.RowIndex).Cells(0).Value())
                    dt = objDetPromo.SeleccionarDetallePromocion()

                    For index As Integer = 0 To dt.Rows.Count - 1

                        row = dt.Rows(index)
                        M_Factura.examenesPromo.Add(row("codigoExamen").ToString)

                        Dim dias As Integer = Integer.Parse(row("diasEntrega"))
                        Dim horas As DateTime = New DateTime((row("horaMax")).Ticks)

                        Dim CC As String = RetornarCentroCosto(row("codigoExamen").ToString)
                        'SUSTITUCION DE CENTRO DE COSTO
                        Dim cantDias As Integer = 0
                        'SUSTITUCION CENTRO DE COSTO
                        If CC = "" Then

                            Dim dtFeriados As New DataTable

                            If DateTime.Now.TimeOfDay > horas.TimeOfDay Then

                                dias = dias + 1

                            End If

                            Dim hoy As Integer

                            hoy = Now.DayOfWeek

                            Dim bandFecha As Boolean = True
                            Dim cont As Integer = dias
                            While bandFecha = True
                                If cont > 10 Then
                                    Exit While
                                End If
                                For index3 = 1 To 2
                                    For index2 = dias + hoy To 7
                                        If row("LU") = True And index2 = 1 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MA") = True And index2 = 2 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MI") = True And index2 = 3 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("JU") = True And index2 = 4 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("VI") = True And index2 = 5 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("SA") = True And index2 = 6 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("DO") = True And index2 = 7 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        Else
                                            cont += 1
                                        End If
                                    Next
                                    For index2 = 1 To 7
                                        If row("LU") = True And index2 = 1 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MA") = True And index2 = 2 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MI") = True And index2 = 3 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("JU") = True And index2 = 4 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("VI") = True And index2 = 5 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("SA") = True And index2 = 6 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("DO") = True And index2 = 7 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        Else
                                            cont += 1
                                        End If
                                    Next
                                Next
                            End While
                            ''hoy = dias + hoy

                            'FALTA IF PARA DIAS MARCADOS

                            'Dim days As Long

                            'If row("LU") = True And hoy <> 1 Then

                            '    days = DateDiff(DateInterval.Day, Date.Now, Date.Now.AddDays(7))
                            '    dias = dias + days

                            'ElseIf row("MA") = True And hoy <> 2 Then

                            'End If


                            dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(M_Factura.dtpfechaFactura.Value), Date.Parse(M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias)))
                            'dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(dtpfechaFactura.Value), Date.Parse(dtpfechaFactura.Value.Date.AddDays(dias)))

                            cantDias += dtFeriados.Rows.Count  'CAMBIAR POR CAMPO DE DIAS
                            'cantDias = dtFeriados.Rows.Count + dias  'CAMBIAR POR CAMPO DE DIAS

                            'M_Factura.dgblistadoExamenes.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), Me.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal, CStr(row("codigoSubArea")), 0, CStr(row("codigoItem")), CStr(row("id_centrocosto"))})

                            'dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(dtpfechaFactura.Value), Date.Parse(dtpfechaFactura.Value.Date.AddDays(dias)))
                            'cantDias = dtFeriados.Rows.Count + dias
                            'dgblistadoExamenes.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), Me.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal, CStr(row("codigoSubArea")), 0, CStr(row("codigoItem")), CStr(row("id_centrocosto"))})

                            M_Factura.totalFactura()


                            M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("descuento")), CStr(row("precioPromo")), CStr(row("codigoSubArea")), 0, CStr(row("codigoExamen")), CStr(row("id_centrocosto"))})
                            M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("descuento")), CStr(row("precioPromo"))})

                            'observaciones
                            M_Factura.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), ""})
                            M_Factura.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), ""})

                            'observaciones
                            'M_Factura.dgbObservaciones.Rows.Add(New String() {objExam.codeIntExam_, ""})
                            'M_Factura.dgbObservaciones2.Rows.Add(New String() {objExam.codeIntExam_, ""})

                            'M_ClienteVentana.dgvtabla.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), Me.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal})

                        Else


                            Dim dtFeriados As New DataTable

                            If DateTime.Now.TimeOfDay > horas.TimeOfDay Then

                                dias = dias + 1

                            End If

                            Dim hoy As Integer

                            hoy = Now.DayOfWeek

                            Dim bandFecha As Boolean = True
                            Dim cont As Integer = dias
                            While bandFecha = True
                                If cont > 10 Then
                                    Exit While
                                End If
                                For index3 = 1 To 2
                                    For index2 = dias + hoy To 7
                                        If row("LU") = True And index2 = 1 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MA") = True And index2 = 2 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MI") = True And index2 = 3 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("JU") = True And index2 = 4 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("VI") = True And index2 = 5 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("SA") = True And index2 = 6 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("DO") = True And index2 = 7 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        Else
                                            cont += 1
                                        End If
                                    Next
                                    For index2 = 1 To 7
                                        If row("LU") = True And index2 = 1 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MA") = True And index2 = 2 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("MI") = True And index2 = 3 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("JU") = True And index2 = 4 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("VI") = True And index2 = 5 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("SA") = True And index2 = 6 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        ElseIf row("DO") = True And index2 = 7 Then
                                            bandFecha = False
                                            cantDias = cont
                                            Exit While
                                        Else
                                            cont += 1
                                        End If
                                    Next
                                Next
                            End While
                            ''hoy = dias + hoy

                            'FALTA IF PARA DIAS MARCADOS

                            'Dim days As Long

                            'If row("LU") = True And hoy <> 1 Then

                            '    days = DateDiff(DateInterval.Day, Date.Now, Date.Now.AddDays(7))
                            '    dias = dias + days

                            'ElseIf row("MA") = True And hoy <> 2 Then

                            'End If


                            dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(M_Factura.dtpfechaFactura.Value), Date.Parse(M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias)))
                            'dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(dtpfechaFactura.Value), Date.Parse(dtpfechaFactura.Value.Date.AddDays(dias)))

                            cantDias += dtFeriados.Rows.Count  'CAMBIAR POR CAMPO DE DIAS
                            'cantDias = dtFeriados.Rows.Count + dias  'CAMBIAR POR CAMPO DE DIAS

                            'M_Factura.dgblistadoExamenes.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal, CStr(row("codigoSubArea")), 0, CStr(row("codigoItem")), CC})

                            'dtFeriados = objFeriado.BuscarFeriadosRangoFecha(Date.Parse(dtpfechaFactura.Value), Date.Parse(dtpfechaFactura.Value.Date.AddDays(dias)))
                            'cantDias = dtFeriados.Rows.Count + dias
                            'dgblistadoExamenes.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), Me.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal, CStr(row("codigoSubArea")), 0, CStr(row("codigoItem")), CC})

                            M_Factura.totalFactura()


                            M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("descuento")), CStr(row("precioPromo")), CStr(row("codigoSubArea")), 0, CStr(row("codigoExamen")), CC})
                            M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("descuento")), CStr(row("precioPromo"))})

                            'observaciones
                            M_Factura.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), ""})
                            M_Factura.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), ""})



                            'observaciones
                            'M_Factura.dgbObservaciones.Rows.Add(New String() {objExam.codeIntExam_, ""})
                            'M_Factura.dgbObservaciones2.Rows.Add(New String() {objExam.codeIntExam_, ""})

                            'M_ClienteVentana.dgvtabla.Rows.Add(New String() {objExam.codeIntExam_, "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(cantDias), CStr(row("porcentaje")), subtotal})

                        End If











                        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                        'M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codigoExamen")), "1", "0", CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", "0", CStr(row("grupo"))})
                        'M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), "1", "0", CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", "0", CStr(row("codigoSubArea")), "0", CStr(row("codigoExamen"))})
                        ' M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), "1", "0", CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(7), "0", "0"})
                        'M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(0), CStr(row("descuento")), CStr(row("precioPromo")), CStr(row("codigoSubArea")), 0, CStr(row("codigoExamen")), CStr(row("id_centrocosto"))})
                        'M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), "1", CStr(row("precio")), CStr(row("descripcion")), M_Factura.dtpfechaFactura.Value.Date.AddDays(0), CStr(row("descuento")), CStr(row("precioPromo"))})

                        ''observaciones
                        'M_Factura.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), ""})
                        'M_Factura.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), ""})

                        '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    Next
                    M_Factura.lblPromocion.Text = dgbtabla.Rows(e.RowIndex).Cells(0).Value()
                    M_Factura.totalFactura()
                    Me.Close()
                Else
                    MsgBox("La promoción ya a sido registrada en la factura.", MsgBoxStyle.Critical)
                End If


            Else

                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar la promoción en la factura?", MsgBoxStyle.YesNo)
                End If
                If n = vbYes Then

                    M_DiarioFacturacion.lblcodPromo.Text = dgbtabla.Rows(e.RowIndex).Cells(0).Value()
                    M_DiarioFacturacion.txtPromocion.Text = dgbtabla.Rows(e.RowIndex).Cells(1).Value()
                    Me.Close()
                End If

            End If 'LLAMADO DE FORMULARIO
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class