﻿Public Class M_ListarFacturasEmpresarial
    Private Sub M_ListarFacturasEmpresarial_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MostrarFacturas()
    End Sub


    'Mostrar Informacion 
    Private Sub MostrarFacturas()
        Try
            alternarColoFilasDatagridview(dgvEmpresa)
            alternarColoFilasDatagridview(dgvPaciente)

            'Objeto
            Dim objFactu As New ClsFacturaEmpresarial
            Dim dt As New DataTable

            With objFactu
                dt = .ListarFacturaEmpresarial()
            End With

            'Listado Facturas Empresariales
            dgvEmpresa.DataSource = dt


            With objFactu
                dt = .ListarFacturaPacienteEmpresarial()
            End With

            'Listado Facturas Empresariales
            dgvPaciente.DataSource = dt


            OcultarColumnasEmpresa()
            OcultarColumnasPaciente()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtNumero_TextChanged(sender As Object, e As EventArgs) Handles txtNumero.TextChanged
        Try
            If (txtNumero.Text <> "") Then

                Try
                    Dim objFact As New ClsFacturaEmpresarial
                    objFact.numero_ = Integer.Parse(txtNumero.Text)
                    Dim dv As New DataTable
                    dv = objFact.BuscarFacturaEmpresarial1()
                    dgvEmpresa.DataSource = dv

                    dgvEmpresa.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                Catch ex As Exception
                    MsgBox("No existe la factura.", MsgBoxStyle.Critical, "Validación")
                End Try

                OcultarColumnasEmpresa()

            Else
                MostrarFacturas()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtEmpresa_TextChanged(sender As Object, e As EventArgs) Handles txtEmpresa.TextChanged
        Try
            If (txtEmpresa.Text <> "") Then

                Try
                    Dim objFact As New ClsFacturaEmpresarial

                    Dim dv As New DataTable
                    objFact.empresa_ = txtEmpresa.Text
                    dv = objFact.BuscarFacturaEmpresarialPorNombre1()
                    dgvEmpresa.DataSource = dv

                    dgvEmpresa.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                Catch ex As Exception
                    ' MsgBox("No existe la factura.", MsgBoxStyle.Critical, "Validación")
                End Try

                OcultarColumnasEmpresa()

            Else
                MostrarFacturas()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtFactEmpresa_TextChanged(sender As Object, e As EventArgs) Handles txtFactEmpresa.TextChanged
        Try
            If (txtFactEmpresa.Text <> "") Then

                Try
                    Dim objFact As New ClsFacturaEmpresarial
                    objFact.numero_ = Integer.Parse(txtFactEmpresa.Text)
                    Dim dv As New DataTable
                    dv = objFact.BuscarFacturaPacienteEmpresarialPorEmpresa()
                    dgvPaciente.DataSource = dv

                    dgvPaciente.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                Catch ex As Exception
                    MsgBox("No existe la factura.", MsgBoxStyle.Critical, "Validación")
                End Try

                OcultarColumnasPaciente()

            Else
                MostrarFacturas()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtNumeroPaciente_TextChanged(sender As Object, e As EventArgs) Handles txtNumeroPaciente.TextChanged
        Try
            If (txtNumeroPaciente.Text <> "") Then

                Try
                    Dim objFact As New ClsFacturaEmpresarial
                    objFact.numero_ = Integer.Parse(txtNumeroPaciente.Text)
                    Dim dv As New DataTable
                    dv = objFact.BuscarFacturaPacienteEmpresarial()
                    dgvPaciente.DataSource = dv

                    dgvPaciente.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                Catch ex As Exception
                    MsgBox("No existe la factura.", MsgBoxStyle.Critical, "Validación")
                End Try

                OcultarColumnasPaciente()

            Else
                MostrarFacturas()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtNombrePaciente_TextChanged(sender As Object, e As EventArgs) Handles txtNombrePaciente.TextChanged
        Try
            If (txtNombrePaciente.Text <> "") Then

                Try
                    Dim objFact As New ClsFacturaEmpresarial
                    objFact.empresa_ = txtNombrePaciente.Text
                    Dim dv As New DataTable
                    dv = objFact.BuscarFacturaPacienteEmpresarialPorNombre()
                    dgvPaciente.DataSource = dv

                    dgvPaciente.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                Catch ex As Exception
                    MsgBox("No existe la factura.", MsgBoxStyle.Critical, "Validación")
                End Try

                OcultarColumnasPaciente()

            Else
                MostrarFacturas()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvEmpresa_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvEmpresa.CellDoubleClick

        'Seleccion de Factura Empresarial
        Try
            Dim n As String = ""
            If e.RowIndex >= 0 Then
                n = MsgBox("¿Desea ver la Factura Empresarial?", MsgBoxStyle.YesNo, "Validación")
            End If
            If n = vbYes Then
                M_FacturaEmpresarial.limpiar()
                M_FacturaEmpresarial.banderaTipo = True
                M_FacturaEmpresarial.lbltipo.Text = "empresa"
                M_FacturaEmpresarial.txtnumeroFactura.Text = dgvEmpresa.Rows(e.RowIndex).Cells(0).Value()
                M_FacturaEmpresarial.txtnumeroOficial.Text = dgvEmpresa.Rows(e.RowIndex).Cells(1).Value()
                M_FacturaEmpresarial.dtpfechaFactura.Value = dgvEmpresa.Rows(e.RowIndex).Cells(2).Value()
                M_FacturaEmpresarial.txtcodigoCliente.Text = dgvEmpresa.Rows(e.RowIndex).Cells(28).Value()
                M_FacturaEmpresarial.txtcodigoRecepecionista.Text = dgvEmpresa.Rows(e.RowIndex).Cells(4).Value()

                If Trim(CStr(dgvEmpresa.Rows(e.RowIndex).Cells(5).Value())) <> "0" Then
                    M_FacturaEmpresarial.txtcodigoMedico.Text = dgvEmpresa.Rows(e.RowIndex).Cells(5).Value()
                Else
                    M_FacturaEmpresarial.txtcodigoMedico.Text = ""
                End If
                'M_FacturaEmpresarial.txtcodigoMedico.Text = CStr(row("codigoMedico"))
                M_FacturaEmpresarial.txtcodigoCajero.Text = dgvEmpresa.Rows(e.RowIndex).Cells(6).Value()
                M_FacturaEmpresarial.lblcodeTerminoPago.Text = dgvEmpresa.Rows(e.RowIndex).Cells(7).Value()
                M_FacturaEmpresarial.txtcodigoSede.Text = dgvEmpresa.Rows(e.RowIndex).Cells(8).Value()
                M_FacturaEmpresarial.dtpfechaVto.Value = dgvEmpresa.Rows(e.RowIndex).Cells(9).Value()
                M_FacturaEmpresarial.lblcodeSucursal.Text = dgvEmpresa.Rows(e.RowIndex).Cells(10).Value()

                'M_FacturaEmpresarial.lblcodePriceList.Text = CStr(row("codigoConvenio"))
                'M_FacturaEmpresarial.txtcodigoConvenio.Text = CStr(row("codigoConvenio"))

                M_FacturaEmpresarial.txtnumeroPoliza.Text = dgvEmpresa.Rows(e.RowIndex).Cells(11).Value()
                M_FacturaEmpresarial.txtcodigoTerminal.Text = dgvEmpresa.Rows(e.RowIndex).Cells(12).Value()
                'M_FacturaEmpresarial.lblcodeSucursal.Text = dgvEmpresa.Rows(e.RowIndex).Cells(0).Value()
                M_FacturaEmpresarial.cbxentregarMedico.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(13).Value()
                M_FacturaEmpresarial.cbxentregarPaciente.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(14).Value()
                M_FacturaEmpresarial.cbxenviarCorreo.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(15).Value()
                M_FacturaEmpresarial.txtpagoPaciente.Text = dgvEmpresa.Rows(e.RowIndex).Cells(16).Value()
                M_FacturaEmpresarial.txtEfectivo.Text = dgvEmpresa.Rows(e.RowIndex).Cells(20).Value()
                M_FacturaEmpresarial.txtTarjeta.Text = dgvEmpresa.Rows(e.RowIndex).Cells(21).Value()
                M_FacturaEmpresarial.txtvuelto.Text = dgvEmpresa.Rows(e.RowIndex).Cells(17).Value()
                M_FacturaEmpresarial.txttotal.Text = dgvEmpresa.Rows(e.RowIndex).Cells(18).Value()
                M_FacturaEmpresarial.cbxok.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(19).Value()
                M_FacturaEmpresarial.cbxAnular.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(22).Value()
                M_FacturaEmpresarial.chkSegundoOK.Checked = dgvEmpresa.Rows(e.RowIndex).Cells(29).Value()
                M_FacturaEmpresarial.txtCheque.Text = dgvEmpresa.Rows(e.RowIndex).Cells(25).Value()
                M_FacturaEmpresarial.txtTransferencia.Text = dgvEmpresa.Rows(e.RowIndex).Cells(24).Value()
                M_FacturaEmpresarial.txtDeposito.Text = dgvEmpresa.Rows(e.RowIndex).Cells(23).Value()


                'LLENAR DETALLE
                Dim dt As New DataTable
                Dim row As DataRow
                Dim objDetFact As New ClsDetalleFacturaEmpresarial
                objDetFact.numeroFactura_ = dgvEmpresa.Rows(e.RowIndex).Cells(0).Value()
                dt = objDetFact.BuscarDetalleFacturaEmpresarial()

                For i = 0 To dt.Rows.Count - 1
                    row = dt.Rows(i)

                    'buscar feriados

                    M_FacturaEmpresarial.dgblistadoExamenes.Rows.Add(New String() {row("codInterno"), row("cantidad"), CStr(row("precio")), CStr(row("descripcion")), row("fechaEntrega"), CStr(row("descuento")), CStr(row("subtotal")), CStr(row("codigoSubArea")), row("numero"), CStr(row("codigoExamen")), CStr(row("id_centrocosto")), CStr(row("contador"))})

                    'observaciones
                    M_FacturaEmpresarial.dgbObservaciones.Rows.Add(New String() {row("codInterno"), ""})
                    M_FacturaEmpresarial.dgbObservaciones2.Rows.Add(New String() {row("codInterno"), ""})


                    M_ClienteVentana.dgvtabla.Rows.Add(New String() {row("codInterno"), row("cantidad"), CStr(row("precio")), CStr(row("descripcion")), row("fechaEntrega"), CStr(row("descuento")), CStr(row("subtotal"))})

                Next

                M_FacturaEmpresarial.deshabilitar()
                If (M_FacturaEmpresarial.cbxok.Checked = "0") Then
                    M_FacturaEmpresarial.HabilitarActualizarFactura()
                Else
                    M_FacturaEmpresarial.btnActualizar.Enabled = True
                End If
                'Me.Close()
                'txtnombreB.Text = ""
                'txtnumeroB.Text = ""
                'Me.Hide()

                'HABILITA LA OPCION DE MODIFICAR EL CLIENTE O PACIENTE
                If Trim(M_FacturaEmpresarial.txtnumeroOficial.Text) = "" And M_FacturaEmpresarial.cbxAnular.Checked = False Then
                    M_FacturaEmpresarial.btnbuscarCliente.Enabled = True
                End If

                M_FacturaEmpresarial.habilitar_Factura_Empresarial()
                If dgvEmpresa.Rows(e.RowIndex).Cells(29).Value() = True Then
                    M_FacturaEmpresarial.chkSegundoOK.Enabled = False
                End If
                M_FacturaEmpresarial.Show()
                M_FacturaEmpresarial.BringToFront()
                M_FacturaEmpresarial.WindowState = FormWindowState.Normal
                Me.Close()
                '    Form1.WindowState = FormWindowState.Minimized

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub OcultarColumnasPaciente()
        If dgvPaciente.Columns.Contains("numero") = True Then

            'Ocultar columnas en grid empresa
            With dgvPaciente

                .Columns("numeroOficial").HeaderText = "Nro. Oficial"
                .Columns("fechaFactura").HeaderText = "Fecha de Factura"
                .Columns("codigoCliente").Visible = False
                .Columns("codigoRecepcionista").Visible = False
                .Columns("codigoMedico").Visible = False
                .Columns("codigoCajero").Visible = False
                .Columns("codigoSede").Visible = False
                .Columns("fechaVto").HeaderText = "Fecha Vto."
                .Columns("codigoSucursal").Visible = False
                .Columns("numeroPoliza").Visible = False
                .Columns("codigoTerminal").Visible = False
                .Columns("codigoTerminoPago").Visible = False
                .Columns("entregaMedico").Visible = False
                .Columns("entregaPaciente").Visible = False
                .Columns("enviarEmail").Visible = False
                .Columns("pagoPaciente").Visible = False
                .Columns("vuelto").Visible = False
                .Columns("total").Visible = False
                .Columns("ingresoEfectivo").Visible = False
                .Columns("ingresoTarjeta").Visible = False
                .Columns("estado").Visible = False
                .Columns("deposito").Visible = False
                .Columns("transferencia").Visible = False
                .Columns("agregarCola").Visible = False
                .Columns("cheque").Visible = False
                .Columns("codFactura").Visible = False
                .Columns("codPaciente").Visible = False
                .Columns("numeroPoliza").Visible = False
                .Columns("saldoPendiente").Visible = False

                '.Columns("numeroOficial").HeaderText = "Nro. Oficial"
                '.Columns("fechaFactura").HeaderText = "Fecha de Factura"
                '.Columns("codigoCliente").Visible = False
                '.Columns("codigoRecepcionista").Visible = False
                '.Columns("codigoMedico").Visible = False
                '.Columns("codigoCajero").Visible = False
                '.Columns("codigoSede").Visible = False
                '.Columns("fechaVto").HeaderText = "Fecha Vto."
                '.Columns("codigoSucursal").Visible = False
                '.Columns("numeroPoliza").Visible = False
                '.Columns("codigoTerminal").Visible = False
                '.Columns("codFactura").Visible = False
                '.Columns("codPaciente").Visible = False

            End With

        End If
    End Sub

    Private Sub OcultarColumnasEmpresa()
        If dgvEmpresa.Columns.Contains("numero") = True Then

            'Ocultar columnas en grid empresa
            With dgvEmpresa

                .Columns("numeroOficial").HeaderText = "Nro. Oficial"
                .Columns("fechaFactura").HeaderText = "Fecha de Factura"
                .Columns("codigoCliente").Visible = False
                .Columns("codigoRecepcionista").Visible = False
                .Columns("codigoMedico").Visible = False
                .Columns("codigoCajero").Visible = False
                .Columns("codigoSede").Visible = False
                .Columns("fechaVto").HeaderText = "Fecha Vto."
                .Columns("codigoSucursal").Visible = False
                .Columns("numeroPoliza").Visible = False
                .Columns("codigoTerminal").Visible = False
                .Columns("codigoTerminoPago").Visible = False
                .Columns("entregaMedico").Visible = False
                .Columns("entregaPaciente").Visible = False
                .Columns("enviarEmail").Visible = False
                .Columns("pagoPaciente").Visible = False
                .Columns("vuelto").Visible = False
                .Columns("total").Visible = False
                .Columns("ingresoEfectivo").Visible = False
                .Columns("ingresoTarjeta").Visible = False
                .Columns("estado").Visible = False
                .Columns("deposito").Visible = False
                .Columns("transferencia").Visible = False
                .Columns("agregarCola").Visible = False
                .Columns("cheque").Visible = False
                .Columns("codFactura").Visible = False
                .Columns("codPaciente").Visible = False
                .Columns("numeroPoliza").Visible = False

            End With

        End If
    End Sub

    Private Sub dgvPaciente_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPaciente.CellDoubleClick
        'Seleccion de Factura Empresarial
        Try
            Dim n As String = ""
            If e.RowIndex >= 0 Then
                n = MsgBox("¿Desea ver la Factura del Paciente Empresarial?", MsgBoxStyle.YesNo, "Validación")
            End If

            If n = vbYes Then
                M_FacturaEmpresarial.limpiar()
                'MsgBox("entra")
                M_FacturaEmpresarial.banderaTipo = True
                M_FacturaEmpresarial.lbltipo.Text = "tipo"
                M_FacturaEmpresarial.txtnumeroFactura.Text = dgvPaciente.Rows(e.RowIndex).Cells(0).Value()
                M_FacturaEmpresarial.txtnumeroOficial.Text = dgvPaciente.Rows(e.RowIndex).Cells(1).Value()
                M_FacturaEmpresarial.dtpfechaFactura.Value = dgvPaciente.Rows(e.RowIndex).Cells(2).Value()
                M_FacturaEmpresarial.txtcodigoCliente.Text = dgvPaciente.Rows(e.RowIndex).Cells(27).Value()
                M_FacturaEmpresarial.txtcodigoRecepecionista.Text = dgvPaciente.Rows(e.RowIndex).Cells(4).Value()

                If Trim(CStr(dgvPaciente.Rows(e.RowIndex).Cells(5).Value())) <> "0" Then
                    M_FacturaEmpresarial.txtcodigoMedico.Text = dgvPaciente.Rows(e.RowIndex).Cells(5).Value()
                Else
                    M_FacturaEmpresarial.txtcodigoMedico.Text = ""
                End If
                'M_FacturaEmpresarial.txtcodigoMedico.Text = CStr(row("codigoMedico"))
                M_FacturaEmpresarial.txtcodigoCajero.Text = dgvPaciente.Rows(e.RowIndex).Cells(6).Value()
                M_FacturaEmpresarial.lblcodeTerminoPago.Text = dgvPaciente.Rows(e.RowIndex).Cells(7).Value()
                M_FacturaEmpresarial.txtcodigoSede.Text = dgvPaciente.Rows(e.RowIndex).Cells(8).Value()
                M_FacturaEmpresarial.dtpfechaVto.Value = dgvPaciente.Rows(e.RowIndex).Cells(9).Value()
                M_FacturaEmpresarial.lblcodeSucursal.Text = dgvPaciente.Rows(e.RowIndex).Cells(10).Value()

                'M_FacturaEmpresarial.lblcodePriceList.Text = CStr(row("codigoConvenio"))
                'M_FacturaEmpresarial.txtcodigoConvenio.Text = CStr(row("codigoConvenio"))

                M_FacturaEmpresarial.txtnumeroPoliza.Text = dgvPaciente.Rows(e.RowIndex).Cells(11).Value()
                M_FacturaEmpresarial.txtcodigoTerminal.Text = dgvPaciente.Rows(e.RowIndex).Cells(12).Value()
                'M_FacturaEmpresarial.lblcodeSucursal.Text = dgvEmpresa.Rows(e.RowIndex).Cells(0).Value()
                M_FacturaEmpresarial.cbxentregarMedico.Checked = dgvPaciente.Rows(e.RowIndex).Cells(13).Value()
                M_FacturaEmpresarial.cbxentregarPaciente.Checked = dgvPaciente.Rows(e.RowIndex).Cells(14).Value()
                M_FacturaEmpresarial.cbxenviarCorreo.Checked = dgvPaciente.Rows(e.RowIndex).Cells(15).Value()
                M_FacturaEmpresarial.txtpagoPaciente.Text = dgvPaciente.Rows(e.RowIndex).Cells(16).Value()
                M_FacturaEmpresarial.txtEfectivo.Text = dgvPaciente.Rows(e.RowIndex).Cells(20).Value()
                M_FacturaEmpresarial.txtTarjeta.Text = dgvPaciente.Rows(e.RowIndex).Cells(21).Value()
                M_FacturaEmpresarial.txtvuelto.Text = dgvPaciente.Rows(e.RowIndex).Cells(17).Value()
                M_FacturaEmpresarial.txttotal.Text = dgvPaciente.Rows(e.RowIndex).Cells(18).Value()
                M_FacturaEmpresarial.cbxok.Checked = dgvPaciente.Rows(e.RowIndex).Cells(19).Value()
                M_FacturaEmpresarial.cbxAnular.Checked = dgvPaciente.Rows(e.RowIndex).Cells(22).Value()
                M_FacturaEmpresarial.chkSegundoOK.Checked = dgvPaciente.Rows(e.RowIndex).Cells(29).Value()
                M_FacturaEmpresarial.txtCheque.Text = dgvPaciente.Rows(e.RowIndex).Cells(25).Value()
                M_FacturaEmpresarial.txtTransferencia.Text = dgvPaciente.Rows(e.RowIndex).Cells(24).Value()
                M_FacturaEmpresarial.txtDeposito.Text = dgvPaciente.Rows(e.RowIndex).Cells(23).Value()


                M_FacturaEmpresarial.lblcodEmpresa.Text = dgvPaciente.Rows(e.RowIndex).Cells(28).Value()
                M_FacturaEmpresarial.txtPaciente.Text = dgvPaciente.Rows(e.RowIndex).Cells(3).Value()
                'MsgBox("pasa")


                M_FacturaEmpresarial.chkCola.Checked = dgvPaciente.Rows(e.RowIndex).Cells(30).Value()

                'LLENAR DETALLE
                Dim dt As New DataTable
                Dim row As DataRow
                Dim objDetFact As New ClsDetalleFacturaEmpresarial
                objDetFact.numeroFactura_ = dgvPaciente.Rows(e.RowIndex).Cells(0).Value()
                dt = objDetFact.BuscarDetalleFacturaEmpresarial()

                For i = 0 To dt.Rows.Count - 1
                    row = dt.Rows(i)

                    'buscar feriados

                    M_FacturaEmpresarial.dgblistadoExamenes.Rows.Add(New String() {row("codInterno"), row("cantidad"), CStr(row("precio")), CStr(row("descripcion")), row("fechaEntrega"), CStr(row("descuento")), CStr(row("subtotal")), CStr(row("codigoSubArea")), row("numero"), CStr(row("codigoExamen")), CStr(row("id_centrocosto")), CStr(row("contador"))})

                    'observaciones
                    M_FacturaEmpresarial.dgbObservaciones.Rows.Add(New String() {row("codInterno"), ""})
                    M_FacturaEmpresarial.dgbObservaciones2.Rows.Add(New String() {row("codInterno"), ""})


                    M_ClienteVentana.dgvtabla.Rows.Add(New String() {row("codInterno"), row("cantidad"), CStr(row("precio")), CStr(row("descripcion")), row("fechaEntrega"), CStr(row("descuento")), CStr(row("subtotal"))})

                Next

                M_FacturaEmpresarial.deshabilitar()
                If (M_FacturaEmpresarial.cbxok.Checked = "0") Then
                    M_FacturaEmpresarial.HabilitarActualizarFactura()
                Else
                    M_FacturaEmpresarial.btnActualizar.Enabled = True
                End If
                'Me.Close()
                'txtnombreB.Text = ""
                'txtnumeroB.Text = ""
                'Me.Hide()

                'HABILITA LA OPCION DE MODIFICAR EL CLIENTE O PACIENTE
                If Trim(M_FacturaEmpresarial.txtnumeroOficial.Text) = "" And M_FacturaEmpresarial.cbxAnular.Checked = False Then
                    M_FacturaEmpresarial.btnbuscarCliente.Enabled = True
                End If

                M_FacturaEmpresarial.habilitar_Factura_Tipo()

                M_FacturaEmpresarial.Show()

                M_FacturaEmpresarial.BringToFront()
                M_FacturaEmpresarial.WindowState = FormWindowState.Normal
                Me.Close()
                '    Form1.WindowState = FormWindowState.Minimized

            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub FacturaEmpresarialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturaEmpresarialToolStripMenuItem.Click
        Try
            M_FacturaEmpresarial.limpiar()
            M_FacturaEmpresarial.habilitar_Factura_Empresarial()
            M_FacturaEmpresarial.Show()
            M_FacturaEmpresarial.BringToFront()
            M_FacturaEmpresarial.WindowState = WindowState.Normal

        Catch ex As Exception

        End Try
    End Sub

    Private Sub PacienteEmpresarialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGenerarOrdenTrabajo.Click
        Try
            M_FacturaEmpresarial.limpiar()
            M_FacturaEmpresarial.Habilitar_Cliente_Empresarial()
            M_FacturaEmpresarial.Show()
            M_FacturaEmpresarial.BringToFront()
            M_FacturaEmpresarial.WindowState = WindowState.Normal

        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvPaciente_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPaciente.CellContentClick

    End Sub
End Class