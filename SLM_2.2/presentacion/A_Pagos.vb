﻿Imports System
Imports System.Text
Imports System.Globalization

Public Class frmPagos

    Dim formaPago As New ClsFormaPago
    Dim factuCompra As New ClsFacturaCompra
    Dim pagos As New ClsPago
    Dim detallePago As New ClsDetallePago
    Dim codigoDetallePago As New ArrayList
    Public letras2 As String


    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs)

        'Cerrar Ventana Pagos
        Me.Close()

    End Sub

    Private Sub frmPagos_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        'Presionar ESC para salida
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
            'frmMenuConta.Show()
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnBuscarFormaPago.Click

        'Mostrar formas de pago
        A_ListarFormasPagoPF.lblForm.Text = "Pagos"
        A_ListarFormasPagoPF.Show()
    End Sub

    Private Sub dtDetallePagos_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetallePagos.CellEndEdit
        'Bloqueo de Celdas 
        dtDetallePagos.Columns(1).ReadOnly = True
        dtDetallePagos.Columns(2).ReadOnly = True
        'dtDetallePagos.Columns(3).ReadOnly = False

        Try

            If e.ColumnIndex = 3 Then

                lblTotalSuma.Text = 0.0
                For a = 0 To dtDetallePagos.Rows.Count - 1
                    lblTotalSuma.Text = Convert.ToDouble(dtDetallePagos.Rows(a).Cells(3).Value) + Convert.ToDouble(lblTotalSuma.Text)
                Next

            ElseIf e.ColumnIndex = 0 Then

                Dim dt As New DataTable
                With factuCompra
                    .Cod_Factura = dtDetallePagos.Rows(e.RowIndex).Cells(0).Value
                    dt = .comprobarFactura() 'Comprobar existencia de factura compra

                    If dt.Rows.Count > 0 Then 'Si la factura existe, llenar campos

                        Dim row As DataRow = dt.Rows(0)
                        If row("estado") = "Pagada" Then
                            'Si la factura fue pagada, no agregar al pago
                            dtDetallePagos.Rows.Remove(dtDetallePagos.Rows(e.RowIndex.ToString))
                            MsgBox("La factura ya fue pagada, no se agregara al pago.")

                        Else

                            dtDetallePagos.Rows.Remove(dtDetallePagos.Rows(e.RowIndex.ToString))
                            dtDetallePagos.Rows.Insert(e.RowIndex.ToString, New String() {row("codfactura"), row("nombreProveedor"), row("moneda"), row("pendiente"), "", "", "0"})
                            lblCodigoProveedor.Text = row("codProveedor")
                            'Sumar totales de factura
                            lblTotalSuma.Text = dtDetallePagos.Rows(e.RowIndex).Cells(3).Value


                        End If

                    End If

                End With
            ElseIf e.ColumnIndex = 5 Then
                chkPagado.Checked = True
            End If

        Catch ex As Exception
            MsgBox("La factura de compra no existe o hubo un error. Verifique el código.")
        End Try

    End Sub

    Private Sub dtDetallePagos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetallePagos.CellClick
        'Bloqueo de edicion de celdas
        dtDetallePagos.Columns(1).ReadOnly = True
        dtDetallePagos.Columns(2).ReadOnly = True
        'dtDetallePagos.Columns(3).ReadOnly = True

        If e.ColumnIndex = 7 Then
            Try
                Dim n As String = MsgBox("¿Desea eliminar la factura?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then
                    If dtDetallePagos.Rows(e.RowIndex).Cells(6).Value() <> "0" Then

                        codigoDetallePago.Add(Me.dtDetallePagos.Rows(e.RowIndex).Cells(6).Value())

                    End If

                    dtDetallePagos.Rows.Remove(dtDetallePagos.Rows(e.RowIndex.ToString))

                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try

            suma()

        End If

    End Sub

    Private Sub dtDetallePagos_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dtDetallePagos.EditingControlShowing
        AddHandler e.Control.KeyPress, AddressOf Validar_Numeros
    End Sub

    'Solo permitir numeros en celda codigo y nro de cheque

    Private Sub Validar_Numeros(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim Celda As DataGridViewCell = Me.dtDetallePagos.CurrentCell()

        If Celda.ColumnIndex = 0 Or Celda.ColumnIndex = 5 Then
            If e.KeyChar = "."c Then
                If InStr(Celda.EditedFormattedValue.ToString, ".", CompareMethod.Text) > 0 Then
                    e.Handled = True
                Else
                    e.Handled = False
                End If
            Else
                If Len(Trim(Celda.EditedFormattedValue.ToString)) > 0 Then

                    If Char.IsNumber(e.KeyChar) Or e.KeyChar = Convert.ToChar(8) Then

                        e.Handled = False
                    Else

                        e.Handled = True
                    End If
                Else

                    If e.KeyChar = "0"c Then

                        e.Handled = True
                    Else

                        If Char.IsNumber(e.KeyChar) Or e.KeyChar = Convert.ToChar(8) Then

                            e.Handled = False
                        Else

                            e.Handled = True
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Sub limpiar() 'Limpiar todos los campos

        txtNro.Text = ""
        txtComentario.Text = ""
        txtCtaBanco.Text = ""
        txtFormaP.Text = ""
        dtpFechaP.ResetText()
        dtpFechaT.ResetText()
        dtDetallePagos.Rows.Clear()

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)
        If dtDetallePagos.Rows(0).Cells(5).Value <> "" Then
            chkPagado.Checked = True
        End If

        Try
            Dim n As String = MsgBox("¿Desea guardar el pago?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then 'validacion

                'Ingresar un nuevo pago
                If txtFormaP.Text <> "" Then

                    Dim codigoPago As String
                    With pagos

                        'Variables de Pago
                        .codForma_Pago = Convert.ToInt32(lblCodFormaPago.Text)
                        .Comentari_o = txtComentario.Text
                        .Fecha_transfer = dtpFechaT.Value
                        .Fecha_Pago = dtpFechaP.Value
                        .Referenci_a = txtReferencia.Text
                        .Paga_do = chkPagado.Checked
                        .Suma_Total = lblTotalSuma.Text
                        'Ingresar registro en base de datos
                        'MsgBox("antes")
                        codigoPago = .registrarNuevoPago()
                        'MsgBox("despues")
                    End With
                    ' MsgBox(codigoPago)

                    'Ingresar detalle de pago
                    For a = 0 To dtDetallePagos.Rows.Count - 2
                        With detallePago

                            .Cod_Pago = Convert.ToInt32(codigoPago)
                            .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(a).Cells(0).Value)

                            If dtDetallePagos.Rows(a).Cells(4).Value = "" Then
                                .Forma_Pago = "-"
                            Else
                                .Forma_Pago = dtDetallePagos.Rows(a).Cells(4).Value.ToString

                            End If

                            If dtDetallePagos.Rows(a).Cells(5).Value = "" Then

                                .Nro_Cheque = "-"

                            Else
                                .Nro_Cheque = dtDetallePagos.Rows(a).Cells(5).Value.ToString

                            End If

                            .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(a).Cells(3).Value.ToString)

                            .registrarDetallePago()

                        End With
                    Next

                    ':::::::::ACTUALIZAR PENDIENTE DE FACTURA DE COMPRA::::::::::::::::
                    Dim codfactura As Integer
                    Dim resultadof, pendiente, montoc As Double
                    Dim factura As New ClsFacturaCompra
                    Dim dtP As New DataTable
                    Dim rowf As DataRow
                    'MsgBox("Se crearon variables")

                    codfactura = dtDetallePagos.Rows(0).Cells(0).Value
                    'MsgBox("Se capturo codigo de fact")

                    factura.Cod_Factura = Convert.ToInt32(codfactura)
                    dtP = factura.comprobarFactura
                    rowf = dtP.Rows(0)
                    'MsgBox("Busco la factura")

                    montoc = Convert.ToDouble(lblTotalSuma.Text)
                    pendiente = Convert.ToDouble(rowf("pendiente"))
                    resultadof = pendiente - montoc
                    'MsgBox("Ya resto " + resultadof.ToString)

                    factura.Cod_Factura = Convert.ToInt32(codfactura)
                    factura.Pendiente_ = Convert.ToDouble(resultadof)

                    'cambio de estado
                    If resultadof = 0 Then
                        factura.Estado_ = "Pagada"
                    Else
                        factura.Estado_ = "Pendiente"
                    End If

                    factura.SaldoPendiente()
                    ' MsgBox("Ya actualizo")


                    '.........................................................

                    MsgBox("Se registro un nuevo pago.")

                End If 'ingresar nuevo pago

            End If 'validacion
            limpiar()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub dtDetallePagos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetallePagos.CellDoubleClick

        If e.ColumnIndex = 4 Then

            'Columna de Forma de Pago
            A_ListarFormasPagoPF.lblForm.Text = "DetallePagoFormaPagos"
            lblFila.Text = e.RowIndex
            A_ListarFormasPagoPF.Show()


        ElseIf e.ColumnIndex = 5 Then
            Dim data As String = dtDetallePagos.Rows(e.RowIndex).Cells(5).Value
            'MsgBox("data" = data)

            Try
                If data <> "False" Then
                    'Columna de Cheques

                    lblFila.Text = e.RowIndex
                    A_ListarChequesHabilitados.Show()

                    '
                    'Else

                    '    Try
                    '        Dim nroCheque As Integer
                    '        Dim cheque As New ClsCheques
                    '        Dim dt As New DataTable
                    '        Dim row As DataRow
                    '        nroCheque = Convert.ToInt32(dtDetallePagos.Rows(e.RowIndex).Cells(5).Value.ToString)

                    '        cheque.Cod_Cheque = nroCheque

                    '        dt = cheque.BuscarChequeXCodigo()

                    '        If dt.Rows.Count = 0 Then
                    '            MsgBox("El numero pertenece a un pago por transferencia.")
                    '        Else
                    '            row = dt.Rows(0)

                    '            'llenar campos de formulario con data de cheque
                    '            With A_Cheques

                    '                .txtNro.Text = row("codCheque")
                    '                .txtNroCheq.Text = row("nroCheque")
                    '                .txtMonto.Text = row("monto")
                    '                .dtpFechaReg.Value = row("fechaReg")
                    '                .dtpFechaVto.Value = row("fechaVto")
                    '                .txtMoneda.Text = row("moneda")
                    '                .lblEstado.Text = row("estado")
                    '                .txtcodProvee.Text = row("codBreveProveedor")
                    '                .txtNombreProvee.Text = row("nombreProveedor")
                    '                .txtBanco.Text = row("codBreveBanco")
                    '                '.txtNroCtaBanco.Text = row2("nroCtaBanco")
                    '                .txtnombreBanco.Text = row("nombreBanco")
                    '                .dtpAcredita.Value = row("fechaacreditacion")
                    '                .dtpRechazo.Value = row("fechaRechazo")
                    '                .dtpEmision.Value = row("fechaEmision")
                    '                .dtpCancelado.Value = row("fechaCancelado")
                    '                .txtCtaOrigen.Text = row("ctaOrigen")
                    '                .txtCtaDestino.Text = row("ctaDestino")
                    '                .txtCtaTemporal.Text = row("ctaTemporal")
                    '                .lblForm.Text = "ChequeSeleccionado"
                    '                .Show()

                    '            End With
                    '        End If

                    '    Catch ex As Exception
                    '        MsgBox("aqui perrin" + ex.Message)
                    '    End Try

                End If

            Catch ex As Exception
                RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                MsgBox("aqui es" + ex.Message)
            End Try

        ElseIf e.ColumnIndex = 0 Then

            A_ListarFacCompraPagos.Show()
            lblFila.Text = e.RowIndex

        End If

    End Sub

    Private Sub frmPagos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
        Try
            alternarColoFilasDatagridview(dtDetallePagos)
            If txtNro.Text <> "" Then

                btnModificar.Enabled = True
                btnCrear.Enabled = True
                btnGuardar.Enabled = False
                VerAsientoToolStripMenuItem.Enabled = True

            End If

            'bloquear datos si pago ya fue realizado
            If chkPagado.Checked = True Then

                dtDetallePagos.Columns(0).ReadOnly = True
                dtDetallePagos.Columns(1).ReadOnly = True
                dtDetallePagos.Columns(2).ReadOnly = True
                'dtDetallePagos.Columns(3).ReadOnly = True
                dtDetallePagos.Columns(4).ReadOnly = True

                btnModificar.Enabled = False
                btnCrear.Enabled = False
                btnGuardar.Enabled = False

            End If

            'habilitar boton para eliminar fila
            If dtDetallePagos.Columns.Contains("btnEliminar") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dtDetallePagos.Columns.Add(btn)
                btn.HeaderText = "Eliminar"
                btn.Text = "Eliminar"
                btn.Name = "btnEliminar"
                btn.UseColumnTextForButtonValue = True
            End If

            suma()

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub dtDetallePagos_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dtDetallePagos.RowsAdded

        suma()

    End Sub

    Private Sub txtFormaP_TextChanged(sender As Object, e As EventArgs) Handles txtFormaP.TextChanged
        If txtFormaP.BackColor = Color.Red Then
            txtFormaP.BackColor = Color.White
        End If
    End Sub

    Sub suma()
        Dim Total2 As Double
        Dim Col2 As Integer = 3
        For Each row As DataGridViewRow In dtDetallePagos.Rows
            Total2 += Convert.ToDouble(row.Cells(Col2).Value)
        Next
        lblTotalSuma.Text = Total2
    End Sub

    Public Function validarFacturaPago(ByVal codigo As Integer)
        For index As Integer = 0 To dtDetallePagos.Rows.Count - 2
            If (dtDetallePagos.Rows(index).Cells(0).Value().ToString = codigo) Then
                Return 1
            End If
        Next
        Return 0
    End Function

    Private Sub btnRegresar_Click(sender As Object, e As EventArgs)
        Me.Close()
        A_ListarPagos.Show()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs)

        Try
            Dim n As String = MsgBox("¿Desea modificar el pago?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then 'validacion


                'actualizar datos de pago
                With pagos

                    'Variables de Pago
                    .Cod_Pago = Integer.Parse(txtNro.Text)
                    .codForma_Pago = Convert.ToInt32(lblCodFormaPago.Text)
                    .Comentari_o = txtComentario.Text
                    .Fecha_transfer = dtpFechaT.Value
                    .Fecha_Pago = dtpFechaP.Value
                    .Referenci_a = txtReferencia.Text
                    .Paga_do = chkPagado.Checked
                    .Suma_Total = lblTotalSuma.Text

                    'realizar modificacion y comprobacion de guardado para modificar detalle
                    If .modificarPago = 1 Then


                        'modificar detalle de pago
                        For i As Integer = 0 To dtDetallePagos.Rows.Count - 2
                            If dtDetallePagos.Rows(i).Cells(6).Value() = 0 Then
                                'agrega
                                With detallePago

                                    .Cod_Pago = Convert.ToInt32(txtNro.Text)
                                    .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(0).Value)

                                    If dtDetallePagos.Rows(i).Cells(4).Value = "" Then
                                        .Forma_Pago = "-"
                                    Else
                                        .Forma_Pago = dtDetallePagos.Rows(i).Cells(4).Value.ToString

                                    End If

                                    If dtDetallePagos.Rows(i).Cells(5).Value = "" Then

                                        .Nro_Cheque = "-"

                                    Else
                                        .Nro_Cheque = dtDetallePagos.Rows(i).Cells(5).Value.ToString

                                    End If

                                    .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(i).Cells(3).Value.ToString)

                                End With

                                If detallePago.registrarDetallePago = 0 Then
                                    MsgBox("Error al querer insertar el detalle de factura.", MsgBoxStyle.Critical)
                                End If
                                'MsgBox("fin agrega")
                            Else
                                'actualiza los detalles de asiento

                                With detallePago

                                    With detallePago

                                        .Cod_Detalle = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(6).Value)
                                        '.Cod_Pago = Convert.ToInt32(txtNro.Text)
                                        .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(0).Value)

                                        If dtDetallePagos.Rows(i).Cells(4).Value = "" Then
                                            .Forma_Pago = "-"
                                        Else
                                            .Forma_Pago = dtDetallePagos.Rows(i).Cells(4).Value.ToString

                                        End If

                                        If dtDetallePagos.Rows(i).Cells(5).Value = "" Then

                                            .Nro_Cheque = "-"

                                        Else
                                            .Nro_Cheque = dtDetallePagos.Rows(i).Cells(5).Value.ToString

                                        End If

                                        .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(i).Cells(3).Value.ToString)

                                    End With

                                End With

                                If detallePago.modificarDetallePago() = 0 Then
                                    MsgBox("Error al querer modificar el detalle de factura.", MsgBoxStyle.Critical)
                                End If

                            End If
                        Next

                    End If

                End With

                For index As Integer = 0 To codigoDetallePago.Count - 1
                    detallePago.Cod_Detalle = Convert.ToInt64(codigoDetallePago(index))
                    If detallePago.EliminarDetallePago() <> 1 Then
                        MsgBox("Error al querer modificar el detalle de factura.", MsgBoxStyle.Critical)
                    End If
                Next

                codigoDetallePago.Clear()

                MessageBox.Show("El pago de modificó exitosamente.")
                Me.Close()
                A_ListarPagos.Show()

            End If ' validacion
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox("Error. " + ex.Message)
        End Try

    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)
        Me.Close()

    End Sub

    Private Sub frmPagos_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        'If dtDetallePagos.Rows(0).Cells(5).Value <> "" Then

        '    If MessageBox.Show("El Pago tiene un cheque vinculado, se guardara automaticamente ¿Seguro que desea cerrar el formulario?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
        '        e.Cancel = True
        '    End If

        'End If

    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        limpiar()
        RegistrarAcciones(nombre_usurio, Me.Name, "Creo un nuevo pago")
        dtDetallePagos.DataSource = Nothing
        btnModificar.Enabled = False
        btnCrear.Enabled = False
        btnGuardar.Enabled = True
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar pago " + txtNro.Text)
            Dim n As String = MsgBox("¿Desea modificar el pago?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then 'validacion


                'actualizar datos de pago
                With pagos

                    'Variables de Pago
                    .Cod_Pago = Integer.Parse(txtNro.Text)
                    .codForma_Pago = Convert.ToInt32(lblCodFormaPago.Text)
                    .Comentari_o = txtComentario.Text
                    .Fecha_transfer = dtpFechaT.Value
                    .Fecha_Pago = dtpFechaP.Value
                    .Referenci_a = txtReferencia.Text
                    .Paga_do = chkPagado.Checked
                    .Suma_Total = lblTotalSuma.Text

                    'realizar modificacion y comprobacion de guardado para modificar detalle
                    If .modificarPago = 1 Then


                        'modificar detalle de pago
                        For i As Integer = 0 To dtDetallePagos.Rows.Count - 2
                            If dtDetallePagos.Rows(i).Cells(6).Value() = 0 Then
                                'agrega
                                With detallePago

                                    .Cod_Pago = Convert.ToInt32(txtNro.Text)
                                    .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(0).Value)

                                    If dtDetallePagos.Rows(i).Cells(4).Value = "" Then
                                        .Forma_Pago = "-"
                                    Else
                                        .Forma_Pago = dtDetallePagos.Rows(i).Cells(4).Value.ToString

                                    End If

                                    If dtDetallePagos.Rows(i).Cells(5).Value = "" Then

                                        .Nro_Cheque = "-"

                                    Else
                                        .Nro_Cheque = dtDetallePagos.Rows(i).Cells(5).Value.ToString

                                    End If

                                    .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(i).Cells(3).Value.ToString)

                                End With

                                If detallePago.registrarDetallePago = 0 Then
                                    MsgBox("Error al querer insertar el detalle de factura.", MsgBoxStyle.Critical)
                                End If
                                'MsgBox("fin agrega")
                            Else
                                'actualiza los detalles de asiento

                                With detallePago

                                    With detallePago

                                        .Cod_Detalle = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(6).Value)
                                        '.Cod_Pago = Convert.ToInt32(txtNro.Text)
                                        .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(i).Cells(0).Value)

                                        If dtDetallePagos.Rows(i).Cells(4).Value = "" Then
                                            .Forma_Pago = "-"
                                        Else
                                            .Forma_Pago = dtDetallePagos.Rows(i).Cells(4).Value.ToString

                                        End If

                                        If dtDetallePagos.Rows(i).Cells(5).Value = "" Then

                                            .Nro_Cheque = "-"

                                        Else
                                            .Nro_Cheque = dtDetallePagos.Rows(i).Cells(5).Value.ToString

                                        End If

                                        .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(i).Cells(3).Value.ToString)

                                    End With

                                End With

                                If detallePago.modificarDetallePago() = 0 Then
                                    MsgBox("Error al querer modificar el detalle de factura.", MsgBoxStyle.Critical)
                                End If

                            End If
                        Next

                    End If

                End With

                For index As Integer = 0 To codigoDetallePago.Count - 1
                    detallePago.Cod_Detalle = Convert.ToInt64(codigoDetallePago(index))
                    If detallePago.EliminarDetallePago() <> 1 Then
                        MsgBox("Error al querer modificar el detalle de factura.", MsgBoxStyle.Critical)
                    End If
                Next

                codigoDetallePago.Clear()

                MessageBox.Show("El pago de modificó exitosamente.")
                Me.Close()
                A_ListarPagos.Show()

            End If ' validacion
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox("Error. " + ex.Message)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If dtDetallePagos.Rows(0).Cells(5).Value <> "" Then
            chkPagado.Checked = True
        End If

        RegistrarAcciones(nombre_usurio, Me.Name, "Guardo el pago " + txtNro.Text)

        Try
            Dim n As String = MsgBox("¿Desea guardar el pago?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then 'validacion

                'Ingresar un nuevo pago
                If txtFormaP.Text <> "" Then

                    Dim codigoPago As String
                    With pagos

                        'Variables de Pago
                        .codForma_Pago = Convert.ToInt32(lblCodFormaPago.Text)
                        .Comentari_o = txtComentario.Text
                        .Fecha_transfer = dtpFechaT.Value
                        .Fecha_Pago = dtpFechaP.Value
                        .Referenci_a = txtReferencia.Text
                        .Paga_do = chkPagado.Checked
                        .Suma_Total = lblTotalSuma.Text
                        'Ingresar registro en base de datos
                        'MsgBox("antes")
                        codigoPago = .registrarNuevoPago()
                        'MsgBox("despues")
                    End With
                    ' MsgBox(codigoPago)
                    txtNro.Text = codigoPago
                    'Ingresar detalle de pago
                    For a = 0 To dtDetallePagos.Rows.Count - 2
                        With detallePago

                            .Cod_Pago = Convert.ToInt32(codigoPago)
                            .Cod_Factura = Convert.ToInt32(dtDetallePagos.Rows(a).Cells(0).Value)

                            If dtDetallePagos.Rows(a).Cells(4).Value = "" Then
                                .Forma_Pago = "-"
                            Else
                                .Forma_Pago = dtDetallePagos.Rows(a).Cells(4).Value.ToString

                            End If

                            If dtDetallePagos.Rows(a).Cells(5).Value = "" Then

                                .Nro_Cheque = "-"

                            Else
                                .Nro_Cheque = dtDetallePagos.Rows(a).Cells(5).Value.ToString

                            End If

                            .Monto_ = Convert.ToDouble(dtDetallePagos.Rows(a).Cells(3).Value.ToString)

                            .registrarDetallePago()

                        End With
                    Next

                    ':::::::::ACTUALIZAR PENDIENTE DE FACTURA DE COMPRA::::::::::::::::
                    Dim codfactura As Integer
                    Dim resultadof, pendiente, montoc As Double
                    Dim factura As New ClsFacturaCompra
                    Dim dtP As New DataTable
                    Dim rowf As DataRow
                    'MsgBox("Se crearon variables")

                    codfactura = dtDetallePagos.Rows(0).Cells(0).Value
                    'MsgBox("Se capturo codigo de fact")

                    factura.Cod_Factura = Convert.ToInt32(codfactura)
                    dtP = factura.comprobarFactura
                    rowf = dtP.Rows(0)
                    'MsgBox("Busco la factura")

                    montoc = Convert.ToDouble(lblTotalSuma.Text)
                    pendiente = Convert.ToDouble(rowf("pendiente"))
                    resultadof = pendiente - montoc
                    'MsgBox("Ya resto " + resultadof.ToString)

                    factura.Cod_Factura = Convert.ToInt32(codfactura)
                    factura.Pendiente_ = Convert.ToDouble(resultadof)

                    'cambio de estado
                    If resultadof = 0 Then
                        factura.Estado_ = "Pagada"
                    Else
                        factura.Estado_ = "Pendiente"
                    End If

                    factura.SaldoPendiente()
                    ' MsgBox("Ya actualizo")

                    '.........................................................
                    MsgBox("Se registro un nuevo pago.")
                    GenerarAsientoPago()



                End If 'ingresar nuevo pago

            End If 'validacion
            ' limpiar()

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub GenerarAsientoPago()

        'Variables y objetos de asiento
        Dim asiento As New ClsAsientoContable
        Dim detalleasiento As New ClsDetalleAsiento
        Dim periodocontable As New ClsPeriodoContable
        Dim proveedor As New ClsProveedor
        Dim codigoAsiento As String

        'Capturando periodo contable
        Dim codPeriodo As String

        Dim dtp As New DataTable
        Dim rowp As DataRow
        dtp = periodocontable.periodoContableActivo()
        rowp = dtp.Rows(0)
        codPeriodo = rowp("codPeriodo").ToString

        'Capturar cuenta de proveedor
        Dim dtpro As New DataTable
        Dim rowpro As DataRow

        proveedor.Cod_Proveedor = Integer.Parse(lblCodigoProveedor.Text)
        dtpro = proveedor.cuentaProveedor()
        rowpro = dtpro.Rows(0)

        'Registra encabezado de asiento
        With asiento

            .Cod_Periodo = Convert.ToInt32(codPeriodo)
            .Descrip = txtComentario.Text
            .Fecha_ = dtpFechaP.Value
            .Campo_Llave = txtNro.Text
            .Estado_ = 0
            .Origen_ = "Pagos"
            codigoAsiento = .registrarAsiento

        End With
        '::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::DETALLE DE ASIENTO

        'Proveedor
        With detalleasiento
            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
            .Cuenta_ = Integer.Parse(rowpro("cuenta"))
            .Debe_ = Double.Parse(dtDetallePagos.Rows(0).Cells(3).Value)
            .Haber_ = 0
            .Origen_ = "Pagos"
            .registrarDetalleAsiento()

        End With

        'Forma de Pago
        Dim buscarCodigo As New ClsFormaPago
        Dim dt As DataTable
        Dim row As DataRow
        buscarCodigo.Cod = txtFormaP.Text
        dt = buscarCodigo.infoFormaPago
        row = dt.Rows(0)

        With detalleasiento
            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
            .Cuenta_ = Integer.Parse(row("cuenta"))
            .Debe_ = 0
            .Haber_ = Double.Parse(dtDetallePagos.Rows(0).Cells(3).Value)
            .Origen_ = "Pagos"
            .registrarDetalleAsiento()

        End With

        'Capturar cuenta de proveedor si hay retencion
        Try
            If IIf(dtDetallePagos.Rows(1).Cells(4).Value Is DBNull.Value, "", "1") <> "" Then

                'RECORRER PARA VARIAS RETENCIONES

                For i = 1 To dtDetallePagos.Rows.Count - 1
                    'RETENCION PROVEEDOR
                    With detalleasiento
                        .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                        .Cuenta_ = Integer.Parse(rowpro("cuenta"))
                        .Debe_ = Double.Parse(dtDetallePagos.Rows(i).Cells(3).Value)
                        .Haber_ = 0
                        .Origen_ = "Pagos"
                        .registrarDetalleAsiento()

                    End With

                    'RETENCION FORMA PAGO
                    buscarCodigo.Cod = dtDetallePagos.Rows(i).Cells(4).Value
                    dt = buscarCodigo.infoFormaPago
                    row = dt.Rows(0)

                    With detalleasiento
                        .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                        .Cuenta_ = Integer.Parse(row("cuenta"))
                        .Debe_ = 0
                        .Haber_ = Double.Parse(dtDetallePagos.Rows(i).Cells(3).Value)
                        .Origen_ = "Pagos"
                        .registrarDetalleAsiento()

                    End With
                Next

            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub VerAsientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerAsientoToolStripMenuItem.Click
        Try
            If chkPagado.Checked = True Then

                frmAsientos.lblForm.Text = "Consultar"
                frmAsientos.txtNro.Text = Me.txtNro.Text
                frmAsientos.lblOrigen.Text = "Pagos"
                frmAsientos.Show()
            Else
                MsgBox("No se ha generado ningún haciento contable.", MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ImprimirChequeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirChequeToolStripMenuItem.Click

        If txtNro.Text <> "" Then

            Dim n As String = MsgBox("Se mostrara una vista previa del cheque para verificar información antes de imprimir.", MsgBoxStyle.OkCancel, "Validación")
            If n = vbOK Then

                A_PrintCheque.Show()

            End If

        End If

    End Sub



    'Convertir numeros a letras para impresión de cheque
    Public NotInheritable Class Numalet

#Region "Miembros estáticos"

        Private Const UNI As Integer = 0, DIECI As Integer = 1, DECENA As Integer = 2, CENTENA As Integer = 3
        Private Shared _matriz As String(,) = New String(CENTENA, 9) {
            {Nothing, " UNO", " DOS", " TRES", " CUATRO", " CINCO", " SEIS", " SIETE", " OCHO", " NUEVE"},
            {" DIEZ", " ONCE", " DOCE", " TRECE", " CATORCE", " QUINCE", " DIECISEIS", " DIECISIETE", " DIECIOCHO", " DIECINUEVE"},
            {Nothing, Nothing, Nothing, " TREINTA", " CUARENTA", " CONCUENTA", " SESENTA", " SETENTA", " OCHENTA", " NOVENTA"},
            {Nothing, Nothing, Nothing, Nothing, Nothing, " QUINIENTOS", Nothing, " SETECIENTOS", Nothing, " NOVECIENTOS"}}
        Private Const [sub] As Char = CChar(ChrW(26))
        'Cambiar acá si se quiere otro comportamiento en los métodos de clase
        Public Const SeparadorDecimalSalidaDefault As String = "LEMPIRAS CON "
        Public Const MascaraSalidaDecimalDefault As String = "00' CENTAVOS.****'"
        Public Const DecimalesDefault As Int32 = 2
        Public Const LetraCapitalDefault As Boolean = False
        Public Const ConvertirDecimalesDefault As Boolean = False
        Public Const ApocoparUnoParteEnteraDefault As Boolean = False
        Public Const ApocoparUnoParteDecimalDefault As Boolean = False

#End Region

#Region "Propiedades"

        Private _decimales As Int32 = DecimalesDefault
        Private _cultureInfo As CultureInfo = Globalization.CultureInfo.CurrentCulture
        Private _separadorDecimalSalida As String = SeparadorDecimalSalidaDefault
        Private _posiciones As Int32 = DecimalesDefault
        Private _mascaraSalidaDecimal As String, _mascaraSalidaDecimalInterna As String = MascaraSalidaDecimalDefault
        Private _esMascaraNumerica As Boolean = True
        Private _letraCapital As Boolean = LetraCapitalDefault
        Private _convertirDecimales As Boolean = ConvertirDecimalesDefault
        Private _apocoparUnoParteEntera As Boolean = False
        Private _apocoparUnoParteDecimal As Boolean

        ''' <summary>
        ''' Indica la cantidad de decimales que se pasarán a entero para la conversión
        ''' </summary>
        ''' <remarks>Esta propiedad cambia al cambiar MascaraDecimal por un valor que empieze con '0'</remarks>
        Public Property Decimales() As Int32
            Get
                Return _decimales
            End Get
            Set(ByVal value As Int32)
                If value > 10 Then
                    Throw New ArgumentException(value.ToString() + " excede el número máximo de decimales admitidos, solo se admiten hasta 10.")
                End If
                _decimales = value
            End Set
        End Property

        ''' <summary>
        ''' Objeto CultureInfo utilizado para convertir las cadenas de entrada en números
        ''' </summary>
        Public Property CultureInfo() As CultureInfo
            Get
                Return _cultureInfo
            End Get
            Set(ByVal value As CultureInfo)
                _cultureInfo = value
            End Set
        End Property

        ''' <summary>
        ''' Indica la cadena a intercalar entre la parte entera y la decimal del número
        ''' </summary>
        Public Property SeparadorDecimalSalida() As String
            Get
                Return _separadorDecimalSalida
            End Get
            Set(ByVal value As String)
                _separadorDecimalSalida = value
                'Si el separador decimal es compuesto, infiero que estoy cuantificando algo,
                'por lo que apocopo el "uno" convirtiéndolo en "un"
                If value.Trim().IndexOf(" ") > 0 Then
                    _apocoparUnoParteEntera = True
                Else
                    _apocoparUnoParteEntera = False
                End If
            End Set
        End Property

        ''' <summary>
        ''' Indica el formato que se le dara a la parte decimal del número
        ''' </summary>
        Public Property MascaraSalidaDecimal() As String
            Get
                If Not [String].IsNullOrEmpty(_mascaraSalidaDecimal) Then
                    Return _mascaraSalidaDecimal
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                'determino la cantidad de cifras a redondear a partir de la cantidad de '0' o ''
                'que haya al principio de la cadena, y también si es una máscara numérica
                Dim i As Integer = 0
                While i < value.Length AndAlso (value(i) = "0"c OrElse value(i) = "#")
                    i += 1
                End While
                _posiciones = i
                If i > 0 Then
                    _decimales = i
                    _esMascaraNumerica = True
                Else
                    _esMascaraNumerica = False
                End If
                _mascaraSalidaDecimal = value
                If _esMascaraNumerica Then
                    _mascaraSalidaDecimalInterna = value.Substring(0, _posiciones) + "'" + value.Substring(_posiciones).Replace("''", [sub].ToString()).Replace("'", [String].Empty).Replace([sub].ToString(), "'") + "'"
                Else
                    _mascaraSalidaDecimalInterna = value.Replace("''", [sub].ToString()).Replace("'", [String].Empty).Replace([sub].ToString(), "'")
                End If
            End Set
        End Property

        ''' <summary>
        ''' Indica si la primera letra del resultado debe estár en mayúscula
        ''' </summary>
        Public Property LetraCapital() As Boolean
            Get
                Return _letraCapital
            End Get
            Set(ByVal value As Boolean)
                _letraCapital = value
            End Set
        End Property

        ''' <summary>
        ''' Indica si se deben convertir los decimales a su expresión nominal
        ''' </summary>
        Public Property ConvertirDecimales() As Boolean
            Get
                Return _convertirDecimales
            End Get
            Set(ByVal value As Boolean)
                _convertirDecimales = value
                _apocoparUnoParteDecimal = value
                If value Then
                    ' Si la máscara es la default, la borro
                    If _mascaraSalidaDecimal = MascaraSalidaDecimalDefault Then
                        MascaraSalidaDecimal = ""
                    End If
                ElseIf [String].IsNullOrEmpty(_mascaraSalidaDecimal) Then
                    MascaraSalidaDecimal = MascaraSalidaDecimalDefault
                    'Si no hay máscara dejo la default
                End If
            End Set
        End Property

        ''' <summary>
        ''' Indica si de debe cambiar "uno" por "un" en las unidades.
        ''' </summary>
        Public Property ApocoparUnoParteEntera() As Boolean
            Get
                Return _apocoparUnoParteEntera
            End Get
            Set(ByVal value As Boolean)
                _apocoparUnoParteEntera = value
            End Set
        End Property

        ''' <summary>
        ''' Determina si se debe apococopar el "uno" en la parte decimal
        ''' </summary>
        ''' <remarks>El valor de esta propiedad cambia al setear ConvertirDecimales</remarks>
        Public Property ApocoparUnoParteDecimal() As Boolean
            Get
                Return _apocoparUnoParteDecimal
            End Get
            Set(ByVal value As Boolean)
                _apocoparUnoParteDecimal = value
            End Set
        End Property

#End Region

#Region "Constructores"

        Public Sub New()
            MascaraSalidaDecimal = MascaraSalidaDecimalDefault
            SeparadorDecimalSalida = SeparadorDecimalSalidaDefault
            LetraCapital = LetraCapitalDefault
            ConvertirDecimales = _convertirDecimales
        End Sub

        Public Sub New(ByVal ConvertirDecimales As Boolean, ByVal MascaraSalidaDecimal As String, ByVal SeparadorDecimalSalida As String, ByVal LetraCapital As Boolean)
            If Not [String].IsNullOrEmpty(MascaraSalidaDecimal) Then
                Me.MascaraSalidaDecimal = MascaraSalidaDecimal
            End If
            If Not [String].IsNullOrEmpty(SeparadorDecimalSalida) Then
                _separadorDecimalSalida = SeparadorDecimalSalida
            End If
            _letraCapital = LetraCapital
            _convertirDecimales = ConvertirDecimales
        End Sub

#End Region

#Region "Conversores de instancia"

        Public Function ToCustomCardinal(ByVal Numero As Double) As String
            Return Convertir(Convert.ToDecimal(Numero), _decimales, _separadorDecimalSalida, _mascaraSalidaDecimalInterna, _esMascaraNumerica, _letraCapital,
            _convertirDecimales, _apocoparUnoParteEntera, _apocoparUnoParteDecimal)
        End Function

        Public Function ToCustomCardinal(ByVal Numero As String) As String
            Dim dNumero As Double
            If [Double].TryParse(Numero, NumberStyles.Float, _cultureInfo, dNumero) Then
                Return ToCustomCardinal(dNumero)
            Else
                Throw New ArgumentException("'" + Numero + "' no es un número válido.")
            End If
        End Function

        Public Function ToCustomCardinal(ByVal Numero As Decimal) As String
            Return ToCardinal(Numero)
        End Function

        Public Function ToCustomCardinal(ByVal Numero As Int32) As String
            Return Convertir(Convert.ToDecimal(Numero), 0, _separadorDecimalSalida, _mascaraSalidaDecimalInterna, _esMascaraNumerica, _letraCapital,
            _convertirDecimales, _apocoparUnoParteEntera, False)
        End Function

#End Region

#Region "Conversores estáticos"

        Public Shared Function ToCardinal(ByVal Numero As Int32) As String
            Return Convertir(Convert.ToDecimal(Numero), 0, Nothing, Nothing, True, LetraCapitalDefault,
            ConvertirDecimalesDefault, ApocoparUnoParteEnteraDefault, ApocoparUnoParteDecimalDefault)
        End Function

        Public Shared Function ToCardinal(ByVal Numero As Double) As String
            Return Convertir(Convert.ToDecimal(Numero), DecimalesDefault, SeparadorDecimalSalidaDefault, MascaraSalidaDecimalDefault, True, LetraCapitalDefault,
            ConvertirDecimalesDefault, ApocoparUnoParteEnteraDefault, ApocoparUnoParteDecimalDefault)
        End Function

        Public Shared Function ToCardinal(ByVal Numero As String, ByVal ReferenciaCultural As CultureInfo) As String
            Dim dNumero As Double
            If [Double].TryParse(Numero, NumberStyles.Float, ReferenciaCultural, dNumero) Then
                Return ToCardinal(dNumero)
            Else
                Throw New ArgumentException("'" + Numero + "' no es un número válido.")
            End If
        End Function

        Public Shared Function ToCardinal(ByVal Numero As String) As String
            Return Numalet.ToCardinal(Numero, CultureInfo.CurrentCulture)
        End Function

        Public Shared Function ToCardinal(ByVal Numero As Decimal) As String
            Return ToCardinal(Convert.ToDouble(Numero))
        End Function

#End Region

        Private Shared Function Convertir(ByVal Numero As Decimal, ByVal Decimales As Int32, ByVal SeparadorDecimalSalida As String, ByVal MascaraSalidaDecimal As String, ByVal EsMascaraNumerica As Boolean, ByVal LetraCapital As Boolean,
        ByVal ConvertirDecimales As Boolean, ByVal ApocoparUnoParteEntera As Boolean, ByVal ApocoparUnoParteDecimal As Boolean) As String
            Dim Num As Int64
            Dim terna As Int32, centenaTerna As Int32, decenaTerna As Int32, unidadTerna As Int32, iTerna As Int32
            Dim cadTerna As String
            Dim Resultado As New StringBuilder()

            Num = Math.Floor(Math.Abs(Numero))

            If Num >= 1000000000001 OrElse Num < 0 Then
                Throw New ArgumentException("El número '" + Numero.ToString() + "' excedió los límites del conversor: [0;1.000.000.000.001]")
            End If
            If Num = 0 Then
                Resultado.Append(" CERO")
            Else
                iTerna = 0

                Do Until Num = 0

                    iTerna += 1
                    cadTerna = String.Empty
                    terna = Num Mod 1000

                    centenaTerna = Int(terna / 100)
                    decenaTerna = terna - centenaTerna * 100 'Decena junto con la unidad
                    unidadTerna = (decenaTerna - Math.Floor(decenaTerna / 10) * 10)

                    Select Case decenaTerna
                        Case 1 To 9
                            cadTerna = _matriz(UNI, unidadTerna) + cadTerna
                        Case 10 To 19
                            cadTerna = cadTerna + _matriz(DIECI, unidadTerna)
                        Case 20
                            cadTerna = cadTerna + " VEINTE"
                        Case 21 To 29
                            cadTerna = " VEINTI" + _matriz(UNI, unidadTerna).Substring(1)
                        Case 30 To 99
                            If unidadTerna <> 0 Then
                                cadTerna = _matriz(DECENA, Int(decenaTerna / 10)) + " Y" + _matriz(UNI, unidadTerna) + cadTerna
                            Else
                                cadTerna += _matriz(DECENA, Int(decenaTerna / 10))
                            End If
                    End Select

                    Select Case centenaTerna
                        Case 1
                            If decenaTerna > 0 Then
                                cadTerna = " CIENTO" + cadTerna
                            Else
                                cadTerna = " CIEN" + cadTerna
                            End If
                            Exit Select
                        Case 5, 7, 9
                            cadTerna = _matriz(CENTENA, Int(terna / 100)) + cadTerna
                            Exit Select
                        Case Else
                            If Int(terna / 100) > 1 Then
                                cadTerna = _matriz(UNI, Int(terna / 100)) + "CIENTOS" + cadTerna
                            End If
                            Exit Select
                    End Select
                    'Reemplazo el 'uno' por 'un' si no es en las únidades o si se solicító apocopar
                    If (iTerna > 1 OrElse ApocoparUnoParteEntera) AndAlso decenaTerna = 21 Then
                        cadTerna = cadTerna.Replace("VEINTIUNO", "VEINTIUN")
                    ElseIf (iTerna > 1 OrElse ApocoparUnoParteEntera) AndAlso unidadTerna = 1 AndAlso decenaTerna <> 11 Then
                        cadTerna = cadTerna.Substring(0, cadTerna.Length - 1)
                        'Acentúo 'veintidós', 'veintitrés' y 'veintiséis'
                    ElseIf decenaTerna = 22 Then
                        cadTerna = cadTerna.Replace("VEINTIDOS", "VEINTIDOS")
                    ElseIf decenaTerna = 23 Then
                        cadTerna = cadTerna.Replace("VEINTITRES", "VEINTITRES")
                    ElseIf decenaTerna = 26 Then
                        cadTerna = cadTerna.Replace("VEINTISEIS", "VEINTISEIS")
                    End If

                    'Completo miles y millones
                    Select Case iTerna
                        Case 3
                            If Numero < 2000000 Then
                                cadTerna += " MILLON"
                            Else
                                cadTerna += " MILLONES"
                            End If
                        Case 2, 4
                            If terna > 0 Then cadTerna += " MIL"
                    End Select
                    Resultado.Insert(0, cadTerna)
                    Num = Int(Num / 1000)
                Loop
            End If

            'Se agregan los decimales si corresponde
            If Decimales > 0 Then
                Resultado.Append(" " + SeparadorDecimalSalida + " ")
                Dim EnteroDecimal As Int32 = Int(Math.Round((Numero - Int(Numero)) * Math.Pow(10, Decimales)))
                If ConvertirDecimales Then
                    Dim esMascaraDecimalDefault As Boolean = MascaraSalidaDecimal = MascaraSalidaDecimalDefault
                    Resultado.Append(Convertir(Convert.ToDecimal(EnteroDecimal), 0, Nothing, Nothing, EsMascaraNumerica, False,
                    False, (ApocoparUnoParteDecimal AndAlso Not EsMascaraNumerica), False) + " " + (IIf(EsMascaraNumerica, "", MascaraSalidaDecimal)))
                ElseIf EsMascaraNumerica Then
                    Resultado.Append(EnteroDecimal.ToString(MascaraSalidaDecimal))
                Else
                    Resultado.Append(EnteroDecimal.ToString() + " " + MascaraSalidaDecimal)
                End If
            End If
            'Se pone la primer letra en mayúscula si corresponde y se retorna el resultado
            If LetraCapital Then
                Return Resultado(1).ToString().ToUpper() + Resultado.ToString(2, Resultado.Length - 2)
            Else
                Return Resultado.ToString().Substring(1)
            End If
        End Function

    End Class 'Fin clase Numalet






    Sub Imprimir()
        Try

            Dim codCheque As Integer
            Dim codPago As Integer
            Dim objVistaCheque As New VistaCheque

            letras2 = frmPagos.Numalet.ToCardinal(lblmontocheque.Text)

            codCheque = Integer.Parse(dtDetallePagos.Rows(0).Cells(5).Value)
            codPago = Integer.Parse(txtNro.Text)

            objVistaCheque.SetParameterValue("@codCheque", codCheque)
            objVistaCheque.SetParameterValue("@pago", codPago)
            objVistaCheque.SetParameterValue("numalet", letras2)
            objVistaCheque.SetParameterValue("Cheque", lblNumeroCheque.Text)

            objVistaCheque.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            A_PrintCheque.crvImprimirCheque.ReportSource = objVistaCheque

        Catch ex As Exception
            MsgBox(ex.Message)
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub
End Class