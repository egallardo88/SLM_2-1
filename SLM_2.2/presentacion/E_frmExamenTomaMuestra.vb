﻿Imports System.Data.SqlClient

Public Class E_frmExamenTomaMuestra
    Private Sub E_frmExamenTomaMuestra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet5.tubos' Puede moverla o quitarla según sea necesario.
        Me.TubosTableAdapter.Fill(Me.Slm_testDataSet5.tubos)
        'TODO: esta línea de código carga datos en la tabla 'Slm_testDataSet4.TubosXExamenes' Puede moverla o quitarla según sea necesario.
        ' Me.TubosXExamenesTableAdapter.Fill(Me.Slm_testDataSet4.TubosXExamenes)
        cargarData()

        ColoresForm(Panel1, StatusStrip1)
        'TextBox3.Text = "1"
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs)
        'Dim asas As New slm_testDataSet4TableAdapters.TubosXExamenesTableAdapter

        ' asas.Adapter.SelectCommand.Parameters.AddWithValue("id_examen", "10001")

    End Sub

    Private Sub FillByToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            '    Me.TubosXExamenesTableAdapter.FillBy(Me.Slm_testDataSet4.TubosXExamenes, New System.Nullable(Of Integer)(CType(Id_examenToolStripTextBox.Text, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Function listarExamenes() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter(" E_slmListarExamenesTubos", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Function listarExamenesXTubos() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter(" E_slmListarExamenesXTubos " + TextBox1.Text + "", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Sub cargarData()
        Try


            BindingSource1.DataSource = listarExamenes()

            DataGridView2.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar los bancos. " + ex.Message)
        End Try
    End Sub

    Public Sub cargarData2()
        Try


            BindingSource2.DataSource = listarExamenesXTubos()

            DataGridView1.DataSource = BindingSource2


            BindingNavigator2.BindingSource = BindingSource2



        Catch ex As Exception
            MsgBox("Hubo un error al consultar los bancos. " + ex.Message)
        End Try
    End Sub

    Private Sub DataGridView2_CellClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            TextBox1.Text = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            TextBox2.Text = DataGridView2.Rows(e.RowIndex).Cells(1).Value
            cargarData2()
        Catch ex As Exception

        End Try



    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If registrarTuboXExamen() = "1" Then
                MsgBox(mensaje_registro)
            ElseIf registrarTuboXExamen() = "2" Then
                MsgBox("Ya existe esta relacion")
            End If
        Catch ex As Exception
            MsgBox("No ha selecionado todos los campos")
        End Try
        '  Me.TubosXExamenesTableAdapter.Fill(Me.Slm_testDataSet4.TubosXExamenes)
        cargarData2()
    End Sub
    Public Function registrarTuboXExamen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        'PROCEDIMIENTO ALMACENADO
        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarTubosXExamenes"

        'VARIABLES 
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_tubo"
        sqlpar.Value = ComboBox1.SelectedValue
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_examen"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion
        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub BindingNavigatorDeleteItem_Click(sender As Object, e As EventArgs) Handles BindingNavigatorDeleteItem.Click

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim RptDocument As New etiquetaTubos
        RptDocument.SetParameterValue("@id_factura", TextBox3.Text)
        RptDocument.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        CrystalReportViewer1.ReportSource = RptDocument
    End Sub
End Class