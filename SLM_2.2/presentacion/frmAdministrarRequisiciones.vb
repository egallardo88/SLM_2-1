﻿Imports System.Data.SqlClient

Public Class frmAdministrarRequisiciones
    Private Sub frmAdministrarRequisiciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ColoresForm(Panel1, StatusStrip1)
            Dim clsOCOB As New ClsRequisicion
            Dim dvOC As DataView = clsOCOB.RecuperarRequisicion().DefaultView
            BindingSource2.DataSource = dvOC
            BindingNavigator2.BindingSource = BindingSource2
            DataGridView2.DataSource = BindingSource2
        Catch ex As Exception

        End Try
        alternarColoFilasDatagridview(DataGridView1)
        alternarColoFilasDatagridview(DataGridView2)

        Label12.Text = ""
        Label13.Text = ""

    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            txtCodRequi.Text = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            Dim objP As New ClsRequisicion

            Dim dt As New DataTable
            dt = objP.RecuperarRequisicionCodigo(DataGridView2.Rows(e.RowIndex).Cells(0).Value)
            Dim row As DataRow = dt.Rows(0)

            txtPuesto.Text = CStr(row("puesto_usuario"))
            txtDepartamento.Text = CStr(row("departamento_usuario"))
            descripcion1.Text = CStr(row("descripcion"))
            txtFecha.Text = CStr(row("fecha_solicitud"))
            txtCodUsuario.Text = CStr(row("nombre_usuario"))

        Catch ex As Exception


        End Try
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtFecha.TextChanged

    End Sub

    Public Sub limpiarData()
        txtCodRequi.Text = ""
        txtPuesto.Text = ""
        txtDepartamento.Text = ""
        descripcion1.Text = ""
        txtFecha.Text = ""
        txtCodUsuario.Text = ""
        comentario1.Clear()
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Try
            Dim clsOCOB As New ClsRequisicion
            Dim dvOC As DataView = clsOCOB.RecuperarRequisicionFechasAutorizaciones(DateTimePicker3.Value.Date, DateTimePicker4.Value.Date).DefaultView

            BindingSource1.DataSource = dvOC
            BindingNavigator1.BindingSource = BindingSource1
            DataGridView1.DataSource = BindingSource1
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)

    End Sub
    Function GridAExcel(ByVal miDataGridView As DataGridView) As Boolean
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet
        Try
            exLibro = exApp.Workbooks.Add 'crea el libro de excel 
            exHoja = exLibro.Worksheets.Add() 'cuenta filas y columnas
            Dim NCol As Integer = miDataGridView.ColumnCount
            Dim NRow As Integer = miDataGridView.RowCount
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = miDataGridView.Columns(i - 1).Name.ToString
            Next
            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = miDataGridView.Rows(Fila).Cells(Col).Value
                Next
            Next
            exHoja.Rows.Item(1).Font.Bold = 1 'titulo en negritas
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            'alineacion al centro
            exHoja.Columns.AutoFit() 'autoajuste de la columna
            exHoja.Columns.HorizontalAlignment = 2
            exApp.Application.Visible = True
            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
            Return False
        End Try
        Return True
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            codigo_requisicion = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            RichTextBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            RichTextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(5).Value
        Catch ex As Exception


        End Try
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        GridAExcel(DataGridView1)
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        If comentario1.Text = "" Then
            MsgBox("Debe asignar un comentario para poder aprobar o rechazar")
            Exit Sub
        End If
        Dim clsR As New ClsRequisicion
        If validarGuardar("Esta seguro que desea aprobar la requisicion " + txtCodRequi.Text + "?") = "1" Then



            With clsR
                .Cod_requi1 = txtCodRequi.Text
                .Comentario_rechazo1 = comentario1.Text
                .Usuario_aprobo1 = codigo_usuario
            End With

            If clsR.AprobarRequisicion() = "1" Then
                MsgBox("Usted acaba de aprobar la requisicion " + txtCodRequi.Text)
                Try
                    Dim clsOCOB As New ClsRequisicion
                    Dim dvOC As DataView = clsOCOB.RecuperarRequisicion().DefaultView
                    DataGridView2.DataSource = dvOC
                    limpiarData()
                Catch ex As Exception

                End Try
            End If

        End If

    End Sub

    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        If comentario1.Text = "" Then
            MsgBox("Debe asignar un comentario para poder aprobar o rechazar")
            Exit Sub
        End If
        If validarGuardar("Esta seguro que desea rechazar la requisicion " + txtCodRequi.Text + "?") = "1" Then


            Try
                Dim clsR As New ClsRequisicion
                With clsR
                    .Cod_requi1 = txtCodRequi.Text
                    .Comentario_rechazo1 = comentario1.Text
                    .Usuario_aprobo1 = codigo_usuario
                End With

                If clsR.RechazarRequisicion() = "1" Then
                    MsgBox("Usted acaba de rechazar la requisicion " + txtCodRequi.Text)
                    Try
                        Dim clsOCOB As New ClsRequisicion
                        Dim dvOC As DataView = clsOCOB.RecuperarRequisicion().DefaultView
                        DataGridView2.DataSource = dvOC
                        limpiarData()
                    Catch ex As Exception

                    End Try
                End If
            Catch ex As Exception
                MsgBox("Debe seleccionar una requisicion")
            End Try
        End If

    End Sub

    Private Sub ToolStripButton4_Click(sender As Object, e As EventArgs) Handles ToolStripButton4.Click
        GridAExcel(DataGridView2)
    End Sub

    Private Sub ToolStripTextBox1_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox1.TextChanged
        Try
            Dim clsOCOB As New ClsRequisicion
            Dim dvOC As DataView = clsOCOB.RecuperarRequisicionFechasAutorizaciones(DateTimePicker3.Value.Date, DateTimePicker4.Value.Date).DefaultView

            BindingSource1.DataSource = dvOC
            BindingNavigator1.BindingSource = BindingSource1

            dvOC.RowFilter = String.Format("CONVERT(cod_requisicion+estado, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
            DataGridView1.DataSource = dvOC
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ToolStripTextBox2_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox2.TextChanged
        Try
            Dim clsOCOB As New ClsRequisicion
            Dim dvOC As DataView = clsOCOB.RecuperarRequisicion().DefaultView

            BindingSource2.DataSource = dvOC


            dvOC.RowFilter = String.Format("CONVERT(cod_requisicion, System.String) LIKE '%{0}%'", ToolStripTextBox2.Text)
            DataGridView2.DataSource = BindingSource2
            BindingNavigator2.BindingSource = BindingSource2
        Catch ex As Exception


        End Try
    End Sub
    Public Function GuardarCorreos(ByVal correos As String) As String
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "update notificacionCorreoRequisicion set correos ='" + correos + "'
"
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteScalar
    End Function

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If GuardarCorreos(RichTextBox3.Text) = "" Then
            MsgBox("Se actualizo la lista de correos")
        End If
    End Sub


    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        RichTextBox3.Text = RecuperarCorreos()
    End Sub
    Public Function RecuperarCorreos() As String
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "select * from notificacionCorreoRequisicion"
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteScalar.ToString
    End Function
End Class