﻿Imports System.Data.OleDb
Public Class ImportarPlanilla
    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            importarExcel2(dtExcel)
        Catch ex As Exception

        End Try

    End Sub

    Sub importarExcel2(ByVal tabla As DataGridView)
        Dim myFileDialog As New OpenFileDialog()
        Dim xSheet As String = ""

        With myFileDialog
            .Filter = "Excel Files |*.xlsx"
            .Title = "Open File"
            .ShowDialog()
        End With

        If myFileDialog.FileName.ToString <> "" Then

            Dim ExcelFile As String = myFileDialog.FileName.ToString

            Dim ds As New DataSet
            Dim da As OleDbDataAdapter
            Dim dt As DataTable
            Dim conn As OleDbConnection

            xSheet = InputBox("Ingrese el nombre de la hoja a importar", "Complete")
            conn = New OleDbConnection(
                              "Provider=Microsoft.ACE.OLEDB.12.0;" &
                              "data source=" & ExcelFile & "; " &
                             "Extended Properties='Excel 12.0 Xml;HDR=Yes'")

            Try
                da = New OleDbDataAdapter("SELECT * FROM  [" & xSheet & "$]", conn)

                conn.Open()
                da.Fill(ds, "MyData")
                dt = ds.Tables("MyData")
                tabla.DataSource = ds
                tabla.DataMember = "MyData"

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Información")
            Finally
                conn.Close()
            End Try

            MsgBox("Se ha cargado la importación correctamente. ", MsgBoxStyle.Information, "Importado con exito")

        Else

            MsgBox("No se logro la importación.")

        End If

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

            'A_PlanillaCalculo.dtData.Rows.Clear()
            For fila = 0 To dtExcel.Rows.Count - 3

                A_PlanillaCalculo.dtData.Rows.Add(New String() {dtExcel.Rows(fila).Cells(0).Value, dtExcel.Rows(fila).Cells(1).Value, dtExcel.Rows(fila).Cells(2).Value, dtExcel.Rows(fila).Cells(3).Value, dtExcel.Rows(fila).Cells(4).Value, dtExcel.Rows(fila).Cells(5).Value, dtExcel.Rows(fila).Cells(6).Value, dtExcel.Rows(fila).Cells(7).Value, dtExcel.Rows(fila).Cells(8).Value, dtExcel.Rows(fila).Cells(9).Value, dtExcel.Rows(fila).Cells(10).Value, dtExcel.Rows(fila).Cells(11).Value, dtExcel.Rows(fila).Cells(12).Value, dtExcel.Rows(fila).Cells(13).Value, dtExcel.Rows(fila).Cells(14).Value, dtExcel.Rows(fila).Cells(15).Value, dtExcel.Rows(fila).Cells(16).Value, dtExcel.Rows(fila).Cells(17).Value, dtExcel.Rows(fila).Cells(18).Value, dtExcel.Rows(fila).Cells(19).Value, dtExcel.Rows(fila).Cells(20).Value, dtExcel.Rows(fila).Cells(21).Value, dtExcel.Rows(fila).Cells(22).Value})

            Next

            A_PlanillaCalculo.TotalSalarios()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class