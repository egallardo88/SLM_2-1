﻿Imports System.Drawing.Imaging
Imports System.IO

Public Class A_Promociones

    Dim promo As New ClsPromociones
    Dim detallepromo As New ClsDetallePromociones
    Dim Imagen As String
    Dim img As Image
    Dim datos As Byte()

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)

        If dtDetallePromo.Rows.Count > 0 Then

            If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
                txtDescrip.BackColor = Color.White
                txtPrecio.BackColor = Color.White

                Try

                    With promo

                        .descripcion_ = txtDescrip.Text
                        .fechaInicio_ = dtpFechaI.Value
                        .fechaFinal_ = dtpFechaF.Value
                        .precio_ = Convert.ToDouble(txtPrecio.Text)

                        If .RegistrarPromocion = 1 Then

                            MsgBox("Se registro una nueva promoción.")

                        End If

                    End With

                    'Ingreso detalle de promo
                    Dim detalle As New ClsDetallePromociones

                    Dim dt As DataTable

                    dt = promo.capturarCodPromocion()

                    Dim row As DataRow = dt.Rows(0)
                    txtCod.Text = CStr(row("codigo"))
                    Dim fila As Integer

                    For fila = 0 To dtDetallePromo.Rows.Count

                        With detalle

                            .codigoExamen_ = Convert.ToInt32(dtDetallePromo.Rows(fila).Cells(0).Value)
                            .codigoPromocion_ = Convert.ToInt32(txtCod.Text)
                            If .RegistrarDetallePromocion <> 1 Then
                                MsgBox("Error al registrar el detalle.")
                            End If
                        End With

                    Next
                    'LimpiarForma()
                Catch ex As Exception
                    MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
                End Try

            ElseIf txtDescrip.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtDescrip.BackColor = Color.Red
            ElseIf txtPrecio.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtPrecio.BackColor = Color.Red
            End If

        Else

            MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

        End If

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs)

        If dtDetallePromo.Rows.Count > 1 Then

            If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
                txtDescrip.BackColor = Color.White
                txtPrecio.BackColor = Color.White

                Try

                    With promo

                        .codigo_ = Convert.ToInt32(txtCod.Text)
                        .descripcion_ = txtDescrip.Text
                        .fechaInicio_ = dtpFechaI.Value
                        .fechaFinal_ = dtpFechaF.Value
                        .precio_ = Convert.ToDouble(txtPrecio.Text)
                        '.imagen_ = txtRuta.Text

                        If .ModificarPromocion = 1 Then

                            MsgBox("Se modifico el registro.")

                        End If

                    End With

                    'Ingreso detalle de promo
                    Dim detalle As New ClsDetallePromociones

                    Dim dt As DataTable

                    dt = promo.capturarCodPromocion()

                    Dim row As DataRow = dt.Rows(0)
                    txtCod.Text = CStr(row("codigo"))
                    Dim fila As Integer

                    For fila = 0 To dtDetallePromo.Rows.Count

                        With detalle

                            .codigoExamen_ = Convert.ToInt32(dtDetallePromo.Rows(fila).Cells(0).Value)
                            .codigoPromocion_ = Convert.ToInt32(txtCod.Text)
                            If .RegistrarDetallePromocion <> 1 Then
                                MsgBox("Error al registrar el detalle.")
                            End If
                        End With

                    Next
                    LimpiarForma()
                Catch ex As Exception
                    MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
                End Try

            ElseIf txtDescrip.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtDescrip.BackColor = Color.Red
            ElseIf txtPrecio.Text = "" Then
                MsgBox("Existen campos vacíos.")
                txtPrecio.BackColor = Color.Red
            End If

        Else

            MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

        End If

    End Sub
    Private Sub btnBuscarImage_Click(sender As Object, e As EventArgs) Handles btnBuscarImage.Click
        SubirImagen()
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        pbxPromo.Image = Nothing
    End Sub
    Sub SubirImagen() 'Metodo que selecciona imagen

        Try

            Dim openFileDialog1 As New OpenFileDialog()

            Dim result As New DialogResult
            openFileDialog1.InitialDirectory = “C:\”
            openFileDialog1.Filter = “archivos de imagen (*.jpg)|*.png|All files (*.*)|*.*”
            openFileDialog1.FilterIndex = 3
            openFileDialog1.RestoreDirectory = True
            result = openFileDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                pbxPromo.Image = Image.FromFile(openFileDialog1.FileName)
            End If

            'Me.OpenFileDialog1.ShowDialog()
            'If Me.OpenFileDialog1.FileName <> "" Then

            '    Imagen = OpenFileDialog1.FileName
            '    'txtRuta.Text = Imagen.ToString
            '    Dim largo As Integer = Imagen.Length
            '    Dim imagen2 As String
            '    imagen2 = CStr(Microsoft.VisualBasic.Mid(RTrim(Imagen), largo - 2, largo))
            '    If imagen2 <> "gif" And imagen2 <> "bmp" And imagen2 <> "jpg" And imagen2 <> "jpeg" And imagen2 <> "GIF" And imagen2 <> "BMP" And imagen2 <> "JPG" And imagen2 <> "JPEG" Then
            '        imagen2 = CStr(Microsoft.VisualBasic.Mid(RTrim(Imagen), largo - 3, largo))
            '        If imagen2 <> "jpeg" And imagen2 <> "JPEG" And imagen2 <> "log1" Then
            '            MsgBox("Formato no valido") : Exit Sub
            '            If imagen2 <> "log1" Then Exit Sub
            '        End If
            '        pbxPromo.Load(Imagen)
            '    End If

            '    pbxPromo.Load(Imagen)
            'End If
        Catch ex As Exception
            MsgBox("Error al cargar la imagen." + ex.Message)
        End Try

    End Sub

    Private Sub txtDescrip_TextChanged(sender As Object, e As EventArgs) Handles txtDescrip.TextChanged
        txtDescrip.BackColor = Color.White
    End Sub

    Private Sub txtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged
        txtPrecio.BackColor = Color.White
    End Sub


    Sub LimpiarForma()

        Me.Close()
        Dim frm As New A_Promociones
        frm.Show()

    End Sub

    Public Function validarDetalle(ByVal codigoExamen As String)
        Try
            For index As Integer = 0 To dtDetallePromo.Rows.Count - 1
                If (dtDetallePromo.Rows(index).Cells(0).Value().ToString = codigoExamen) Then
                    Return 1
                End If
            Next
            Return 0
        Catch ex As Exception

        End Try

    End Function

    Private Sub A_Promociones_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        'Presionar ESC para cerrar ventana
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If

    End Sub

    Private Sub A_Promociones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
        Try

            'If txtCod.Text <> "" Then

            '    detallepromo.codigoPromocion_ = Convert.ToInt32(txtCod.Text)
            '    dtDetallePromo.DataSource = detallepromo.VerDetallePromocion()


            '    dtDetallePromo.Columns("Cod").Visible = False
            '    dtDetallePromo.Columns("Descrip").Visible = False
            '    dtDetallePromo.Columns("Descrip").Width = 300

            'Else
            '    dtDetallePromo.Columns("Cod").Visible = True
            '    dtDetallePromo.Columns("Descrip").Visible = True
            'End If

            'botones

            If dtDetallePromo.Columns.Contains("btnEliminar") = False Then
                Dim btn As New DataGridViewButtonColumn()
                dtDetallePromo.Columns.Add(btn)
                btn.HeaderText = "Eliminar"
                btn.Text = "Eliminar"
                btn.Name = "btnEliminar"
                btn.UseColumnTextForButtonValue = True
            End If

            btnCrear.Enabled = True
            'btnModificar.Enabled = False
            'btnGuardar.Enabled = True

        Catch ex As Exception
            MsgBox(ex.Message)
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
        alternarColoFilasDatagridview(dtDetallePromo)
    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs)



    End Sub

    Sub limpiar()

        Try
            txtCod.Text = ""
            dtpFechaF.Value = Date.Now
            dtpFechaI.Value = Date.Now
            txtDescrip.Text = ""
            pbxPromo.Image = Nothing
            txtPrecio.Text = ""
            'txtRuta.Text = ""
            dtDetallePromo.Rows.Clear()

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub btnCancelarRegistro_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        btnModificar.Enabled = False
        RegistrarAcciones(nombre_usurio, Me.Name, "Creo una nueva promocion")
        btnCrear.Enabled = True
        btnGuardar.Enabled = True
        dtDetallePromo.DataSource = Nothing
        'dtDetallePromo.Columns("Cod").Visible = True
        'dtDetallePromo.Columns("Descrip").Visible = True
        limpiar()
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Guardo la promocion " + txtCod.Text)
            Dim ms As New System.IO.MemoryStream()
            If Not pbxPromo.Image Is Nothing Then
                pbxPromo.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
            Dim codigoPromo As Integer
            'VALIDAR SI ES PROMO O PUBLICIDAD
            If chkPromo.Checked = True Then 'ES PUBLICIDAD>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                Dim objPromo As New ClsPromociones

                With promo

                    .descripcion_ = txtDescrip.Text
                    .fechaInicio_ = dtpFechaI.Value
                    .fechaFinal_ = dtpFechaF.Value
                    .precio_ = 0.00
                    .imagen_ = ms.GetBuffer()
                    .publicidad_ = chkPromo.Checked

                    codigoPromo = .RegistrarPromocion

                    'If .RegistrarPromocion = 1 Then
                    If codigoPromo > 0 Then 'INICIO EL CODIGO DE PROMO ES MAYOR A 0

                        ' MsgBox("Se registro una nueva promoción.")
                        txtCod.Text = codigoPromo

                        btnGuardar.Enabled = False
                        btnModificar.Enabled = True
                        MsgBox("Se registro correctamente.", MsgBoxStyle.Information)

                    End If

                End With

            ElseIf dtDetallePromo.Rows.Count > 0 And chkPromo.Checked = False Then ' SI ES PROMOCION>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
                    txtDescrip.BackColor = Color.White
                    txtPrecio.BackColor = Color.White

                    Try

                        With promo

                            .descripcion_ = txtDescrip.Text
                            .fechaInicio_ = dtpFechaI.Value
                            .fechaFinal_ = dtpFechaF.Value
                            .precio_ = Convert.ToDouble(txtPrecio.Text)
                            .imagen_ = ms.GetBuffer()
                            .publicidad_ = chkPromo.Checked

                            codigoPromo = .RegistrarPromocion

                            If codigoPromo > 0 Then 'INICIO EL CODIGO DE PROMO ES MAYOR A 0

                                ' MsgBox("Se registro una nueva promoción.")
                                txtCod.Text = codigoPromo

                                'Ingreso detalle de promo
                                Dim detalle As New ClsDetallePromociones

                                For fila = 0 To dtDetallePromo.Rows.Count - 1

                                    With detalle
                                        .codigoExamen_ = Integer.Parse(dtDetallePromo.Rows(fila).Cells(0).Value)
                                        .codigoPromocion_ = Integer.Parse(txtCod.Text)
                                        .descuento_ = Double.Parse(dtDetallePromo.Rows(fila).Cells(4).Value)
                                        .precioPromo_ = Double.Parse(dtDetallePromo.Rows(fila).Cells(5).Value)
                                        If .RegistrarDetallePromocion <> 1 Then
                                            MsgBox("Error al registrar el detalle.")
                                        End If
                                    End With

                                Next
                                btnGuardar.Enabled = False
                                btnModificar.Enabled = True
                                MsgBox("Se registro la promoción correctamente.", MsgBoxStyle.Information)

                                '  LimpiarForma()
                            End If 'fin EL CODIGO DE PROMO ES MAYOR A 0

                        End With

                    Catch ex As Exception
                        MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
                    End Try

                ElseIf txtDescrip.Text = "" Then
                    MsgBox("Existen campos vacíos.")
                    txtDescrip.BackColor = Color.Red
                ElseIf txtPrecio.Text = "" Then
                    MsgBox("Existen campos vacíos.")
                    txtPrecio.BackColor = Color.Red
                End If

            Else

                MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

            End If 'fin VALIDAR SI ES PROMO O PUBLICIDAD>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            If A_ListarPromociones.ActiveForm.Visible = True Then

                A_ListarPromociones.CargarData()

            End If

        Catch ex As Exception

        End Try



        'If dtDetallePromo.Rows.Count > 0 Then

        '    If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
        '        txtDescrip.BackColor = Color.White
        '        txtPrecio.BackColor = Color.White

        '        Try

        '            With promo

        '                .descripcion_ = txtDescrip.Text
        '                .fechaInicio_ = dtpFechaI.Value
        '                .fechaFinal_ = dtpFechaF.Value
        '                .precio_ = Convert.ToDouble(txtPrecio.Text)
        '                '.imagen_ = txtRuta.Text

        '                If .RegistrarPromocion = 1 Then

        '                    MsgBox("Se registro una nueva promoción.")

        '                End If

        '            End With

        '            'Ingreso detalle de promo
        '            Dim detalle As New ClsDetallePromociones

        '            Dim dt As DataTable

        '            dt = promo.capturarCodPromocion()

        '            Dim row As DataRow = dt.Rows(0)
        '            txtCod.Text = CStr(row("codigo"))
        '            Dim fila As Integer

        '            For fila = 0 To dtDetallePromo.Rows.Count

        '                With detalle

        '                    .codigoExamen_ = Convert.ToInt32(dtDetallePromo.Rows(fila).Cells(0).Value)
        '                    .codigoPromocion_ = Convert.ToInt32(txtCod.Text)
        '                    If .RegistrarDetallePromocion <> 1 Then
        '                        MsgBox("Error al registrar el detalle.")
        '                    End If
        '                End With

        '            Next
        '            LimpiarForma()
        '        Catch ex As Exception
        '            MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
        '        End Try

        '    ElseIf txtDescrip.Text = "" Then
        '        MsgBox("Existen campos vacíos.")
        '        txtDescrip.BackColor = Color.Red
        '    ElseIf txtPrecio.Text = "" Then
        '        MsgBox("Existen campos vacíos.")
        '        txtPrecio.BackColor = Color.Red
        '    End If

        'Else

        '    MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

        'End If
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar promocion " + txtCod.Text)
            Dim ms As New System.IO.MemoryStream()
            If Not pbxPromo.Image Is Nothing Then
                pbxPromo.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
            'VALIDAR SI ES PROMO O PUBLICIDAD
            If chkPromo.Checked = True Then 'ES PUBLICIDAD>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                Dim objPromo As New ClsPromociones

                With promo
                    .codigo_ = Integer.Parse(txtCod.Text)
                    .descripcion_ = txtDescrip.Text
                    .fechaInicio_ = dtpFechaI.Value
                    .fechaFinal_ = dtpFechaF.Value
                    .precio_ = 0.00
                    .imagen_ = ms.GetBuffer()
                    .publicidad_ = chkPromo.Checked

                    If .ModificarPromocion = 1 Then
                        'If codigoPromo > 0 Then 'INICIO EL CODIGO DE PROMO ES MAYOR A 0
                        MsgBox("Actualización realizada correctamente.", MsgBoxStyle.Information)

                    End If

                End With

            ElseIf dtDetallePromo.Rows.Count > 0 And chkPromo.Checked = False Then ' SI ES PROMOCION>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
                    txtDescrip.BackColor = Color.White
                    txtPrecio.BackColor = Color.White

                    Try

                        With promo
                            .codigo_ = Integer.Parse(txtCod.Text)
                            .descripcion_ = txtDescrip.Text
                            .fechaInicio_ = dtpFechaI.Value
                            .fechaFinal_ = dtpFechaF.Value
                            .precio_ = Convert.ToDouble(txtPrecio.Text)
                            .imagen_ = ms.GetBuffer()
                            .publicidad_ = chkPromo.Checked

                            If .ModificarPromocion = 1 Then
                                'If codigoPromo > 0 Then 'INICIO EL CODIGO DE PROMO ES MAYOR A 0
                                'Ingreso detalle de promo
                                Dim detalle As New ClsDetallePromociones
                                detalle.codigoPromocion_ = Integer.Parse(txtCod.Text)
                                If detalle.EliminarDetallePromocion() <> 1 Then
                                    MsgBox("Error al querer borrar el detalle de la promoción.", MsgBoxStyle.Information)
                                End If

                                For fila = 0 To dtDetallePromo.Rows.Count - 1
                                    With detalle
                                        .codigoExamen_ = Integer.Parse(dtDetallePromo.Rows(fila).Cells(0).Value)
                                        .codigoPromocion_ = Integer.Parse(txtCod.Text)
                                        .descuento_ = Double.Parse(dtDetallePromo.Rows(fila).Cells(4).Value)
                                        .precioPromo_ = Double.Parse(dtDetallePromo.Rows(fila).Cells(5).Value)
                                        If .RegistrarDetallePromocion <> 1 Then
                                            MsgBox("Error al registrar el detalle.")
                                        End If
                                    End With
                                Next
                                btnGuardar.Enabled = False
                                MsgBox("Actualización realizada correctamente.", MsgBoxStyle.Information)

                                '  LimpiarForma()
                            End If 'fin EL CODIGO DE PROMO ES MAYOR A 0

                        End With

                    Catch ex As Exception
                        MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
                    End Try

                ElseIf txtDescrip.Text = "" Then
                    MsgBox("Existen campos vacíos.")
                    txtDescrip.BackColor = Color.Red
                ElseIf txtPrecio.Text = "" Then
                    MsgBox("Existen campos vacíos.")
                    txtPrecio.BackColor = Color.Red
                End If

            Else

                MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

            End If 'fin VALIDAR SI ES PROMO O PUBLICIDAD>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        Catch ex As Exception

        End Try


        'If dtDetallePromo.Rows.Count > 1 Then

        '    If txtDescrip.Text <> "" And txtPrecio.Text <> "" Then
        '        txtDescrip.BackColor = Color.White
        '        txtPrecio.BackColor = Color.White

        '        Try

        '            With promo

        '                .codigo_ = Convert.ToInt32(txtCod.Text)
        '                .descripcion_ = txtDescrip.Text
        '                .fechaInicio_ = dtpFechaI.Value
        '                .fechaFinal_ = dtpFechaF.Value
        '                .precio_ = Convert.ToDouble(txtPrecio.Text)
        '                '.imagen_ = txtRuta.Text

        '                If .ModificarPromocion = 1 Then

        '                    MsgBox("Se modifico el registro.")

        '                End If

        '            End With

        '            'Ingreso detalle de promo
        '            Dim detalle As New ClsDetallePromociones

        '            Dim dt As DataTable

        '            dt = promo.capturarCodPromocion()

        '            Dim row As DataRow = dt.Rows(0)
        '            txtCod.Text = CStr(row("codigo"))
        '            Dim fila As Integer

        '            For fila = 0 To dtDetallePromo.Rows.Count - 1

        '                With detalle

        '                    .codigoExamen_ = Convert.ToInt32(dtDetallePromo.Rows(fila).Cells(0).Value)
        '                    .codigoPromocion_ = Convert.ToInt32(txtCod.Text)
        '                    If .RegistrarDetallePromocion <> 1 Then
        '                        MsgBox("Error al registrar el detalle.")
        '                    End If
        '                End With

        '            Next
        '            LimpiarForma()
        '        Catch ex As Exception
        '            MsgBox("Error al guardar o falta imagen de promoción. Detalle: " + ex.Message)
        '        End Try

        '    ElseIf txtDescrip.Text = "" Then
        '        MsgBox("Existen campos vacíos.")
        '        txtDescrip.BackColor = Color.Red
        '    ElseIf txtPrecio.Text = "" Then
        '        MsgBox("Existen campos vacíos.")
        '        txtPrecio.BackColor = Color.Red
        '    End If

        'Else

        '    MsgBox("La promoción esta incompleta. Debe gregar exámenes y llenar todos los campos.")

        'End If
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCancelarRegistro.Click
        Me.Close()
    End Sub

    Private Sub AgregarExamenesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AgregarExamenesToolStripMenuItem.Click
        MostrarForm(A_ListarExamenes)
    End Sub


    Public Function validarExamen(ByVal codigoExamen As Integer)
        For index As Integer = 0 To dtDetallePromo.Rows.Count - 1
            If (dtDetallePromo.Rows(index).Cells(0).Value().ToString = codigoExamen) Then
                Return 1
            End If
        Next
        Return 0
    End Function



    Private Sub dtDetallePromo_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetallePromo.CellEndEdit

        If e.ColumnIndex = 4 Then
            'CALCULO DE PRECIO CON DESCUENTO
            Dim precio As Double
            Dim descuento As Double
            Dim precioDescuento As Double
            Dim precioFinal As Double = 0
            'CAPTURA DE VARIABLES
            precio = Double.Parse(dtDetallePromo.Rows(e.RowIndex).Cells(3).Value)
            descuento = Double.Parse(dtDetallePromo.Rows(e.RowIndex).Cells(4).Value)
            'CALCULO DEL DESCUENTO
            precioDescuento = precio - (precio * (descuento / 100))
            'VISTA EN GRID DEL PRECIO
            dtDetallePromo.Rows(e.RowIndex).Cells(5).Value = Math.Round(precioDescuento, 2)

            'SUMA DE LOS PRECIOS PARA PRECIO FINAL DE PROMO
            For i = 0 To dtDetallePromo.Rows.Count - 1


                precioFinal = precioFinal + Double.Parse(dtDetallePromo.Rows(i).Cells(5).Value)


            Next

            txtPrecio.Text = Math.Round(precioFinal, 2)

        End If



    End Sub

    Public Sub CalcularTotal()
        Try
            Dim precioFinal As Double = 0
            'SUMA DE LOS PRECIOS PARA PRECIO FINAL DE PROMO
            For i = 0 To dtDetallePromo.Rows.Count - 1
                precioFinal = precioFinal + Double.Parse(dtDetallePromo.Rows(i).Cells(5).Value)
            Next

            txtPrecio.Text = Math.Floor(precioFinal)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dtDetallePromo_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtDetallePromo.CellClick
        If e.ColumnIndex = 6 And chkPromo.Checked = False Then
            Try

                Dim n As String = MsgBox("¿Desea eliminar el examen de la promocion?", MsgBoxStyle.YesNo, "Validación")
                If n = vbYes Then

                    If dtDetallePromo.Rows(e.RowIndex).Cells(0).Value() <> "0" Then

                        dtDetallePromo.Rows.Remove(dtDetallePromo.Rows(e.RowIndex.ToString))
                        CalcularTotal()
                    End If

                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            End Try
        End If
    End Sub
End Class