﻿Public Class EstadodeCuentasporPagar
    Private Sub EstadodeCuentasporPagar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            RegistrarVentanas(nombre_usurio, Me.Name)

        Catch ex As Exception

        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarToolStripMenuItem.Click
        Try

            Dim fc As New ClsFacturaCompra

            Dim data As New DataTable
            Dim FInicial, FFinal As Date

            FInicial = dtpDesde.Value
            FFinal = dtpHasta.Value

            data = fc.ReporteCuentasPorPagar(FInicial, FFinal)

            dgvData.DataSource = data

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ImprimirReporteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirReporteToolStripMenuItem.Click
        Try

            Dim objReporte As New CuentasPorPagar

            Dim fechadesde, fechahasta As Date
            ' Dim codCate, codLista As System.Nullable(Of Integer)

            fechadesde = dtpDesde.Value
            fechahasta = dtpHasta.Value


            'If chkTodo.Checked <> True Then
            '    codLista = cbxListaPrecio.SelectedValue
            'End If

            objReporte.SetParameterValue("@fechaI", fechadesde)
            objReporte.SetParameterValue("@fechaF", fechahasta)
            'objReporte.SetParameterValue("@codigoCategoria", codCate)
            'objReporte.SetParameterValue("@codigoConvenio", codLista)

            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            M_ImpresionFacturaEmpresarial.CrystalReportViewer2.ReportSource = objReporte
            M_ImpresionFacturaEmpresarial.Text = "Estado Cuentas por Pagar"

            M_ImpresionFacturaEmpresarial.Show()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvData)
        Catch ex As Exception

        End Try
    End Sub
End Class