﻿Public Class E_frmReporteMuestraPendienteTM
    Private Sub E_frmReporteMuestraPendienteTM_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.DisplayMember = "Text"
        ComboBox1.ValueMember = "Value"
        Dim tb As New DataTable
        tb.Columns.Add("Text", GetType(String))
        tb.Columns.Add("Value", GetType(Integer))
        tb.Rows.Add("LCA", 1)
        tb.Rows.Add("LRD", 2)
        tb.Rows.Add("LCM", 3)
        tb.Rows.Add("LTP", 4)
        tb.Rows.Add("LSA", 5)
        tb.Rows.Add("LFI", 6)
        tb.Rows.Add("VTU", 1007)
        tb.Rows.Add("LMA", 1008)
        tb.Rows.Add("LMN", 1009)
        ComboBox1.DataSource = tb
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim rpt As New E_rptReporteMuestraPendiente
            ' Dim PrinterSettings1 As New Printing.PrinterSettings
            'Dim PageSettings1 As New Printing.PageSettings
            Dim frm As New M_Factura
            rpt.SetParameterValue("@id_sucursal", ComboBox1.SelectedValue)


            rpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

            CrystalReportViewer1.ReportSource = rpt
            'PrinterSettings1.paper = CrystalDecisions.Shared.PaperOrientation.Landscape; 
            'PrinterSettings1.PrinterName = ("Gprinter GP-3120TL").ToString
            'rpt.PrintToPrinter(PrinterSettings1, PageSettings1, False)
            'rpt.VerifyDatabase()
            'rpt.Refresh()

        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub
End Class