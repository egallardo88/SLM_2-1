﻿Public Class frmDeposito

    Dim Deposito As New ClsDeposito
    Dim buscarCodigo As New ClsFormaPago
    Dim asiento As New ClsAsientoContable
    Dim detalleAsiento As New ClsDetalleAsiento
    Dim periodo As New ClsPeriodoContable

    Private Sub CerrarToolStripMenuItem1_Click(sender As Object, e As EventArgs)

        Dim n As String = MsgBox("¿Desea cerrar la ventana?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then
            Me.Close()
        End If

    End Sub


    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)

        Dim n As String = MsgBox("¿Desea guardar el depósito?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then


            If txtBanco.Text <> "" And txtContado.Text <> "" And txtTipoConta.Text <> "" And txtCajero.Text <> "" And txtBanco.Text <> txtTipoConta.Text Then


                'Guardar deposito en la base de datos

                Try

                    Dim ultimo As DataTable
                    Dim nro As DataRow
                    Dim codigoAsiento As String

                    If lblTipoDeposito.Text = "Tarjeta" Then

                        'Campos para tipo de deposito : Tarjeta
                        With Deposito
                            .Fech_a = dtpFecha.Value
                            .CodFP_Banco = Convert.ToInt32(lblCodFPBanco.Text)
                            .conta_do = Convert.ToDouble(txtContado.Text)
                            .CodFP_Contado = Convert.ToInt32(lblCodFPContado.Text)
                            .total_Depositado = Convert.ToDouble(txtTotalDep.Text)
                            .Mone_da = txtMoneda.Text
                            .mon_base = Convert.ToInt32(txtMonBase.Text)
                            .comisi_on = Convert.ToDouble(txtComision.Text)
                            .Comenta_rio = txtComentario.Text
                            .Tipo_Deposito = lblTipoDeposito.Text
                            .cod_Cajero = txtCajero.Text
                            .Estado_ = chkAnular.Checked
                            .registrarDepositos()


                            ultimo = Deposito.listarUltimoDeposito
                            nro = ultimo.Rows(0)
                        End With

                        '::::::::::::::::::::::::::::::::  Partida Contable  ::::::::::::::::::::::::::::::::

                        Try
                            Dim periodoContable As New ClsPeriodoContable
                            Dim codPeriodo As String
                            Dim dtp As New DataTable
                            Dim rowp As DataRow
                            dtp = periodoContable.periodoContableActivo()
                            rowp = dtp.Rows(0)
                            codPeriodo = rowp("codPeriodo").ToString

                            With asiento

                                .Cod_Periodo = Convert.ToInt32(codPeriodo)
                                .Descrip = txtComentario.Text
                                .Fecha_ = dtpFecha.Value
                                .Origen_ = "DepositoBancario"
                                .Campo_Llave = Convert.ToInt32(nro("codDeposito"))
                                codigoAsiento = .registrarAsiento

                                If codigoAsiento > 0 Then

                                    Try

                                        With detalleAsiento

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtTotalDep.Text)
                                            .Haber_ = 0.0
                                            .Origen_ = "DepositoBancario"

                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtBanco.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With

                                        'Segunda Partida
                                        Dim detalleAsiento2 As New ClsDetalleAsiento

                                        With detalleAsiento2

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = 0.0
                                            .Haber_ = Convert.ToDouble(txtContado.Text)

                                            .Origen_ = "DepositoBancario"
                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtTipoConta.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With

                                        'Partida de Comisión
                                        Dim comision As New ClsDetalleAsiento

                                        With comision

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtComision.Text)
                                            .Haber_ = 0.0
                                            .Cuenta_ = 660005

                                            .Origen_ = "DepositoBancario"
                                            .registrarDetalleAsiento()

                                        End With

                                    Catch ex As Exception
                                        MsgBox("Error en detalle de asiento" + ex.Message)
                                    End Try

                                End If

                            End With

                        Catch ex As Exception
                            MsgBox("Aqui hay error: " + ex.Message)
                        End Try 'Partida Contable

                        Limpiar()

                    Else
                        'Campos para tipo de deposito: Deposito Bancario
                        With Deposito
                            .Fech_a = dtpFecha.Value
                            .CodFP_Banco = Convert.ToInt32(lblCodFPBanco.Text)
                            .conta_do = Convert.ToDouble(txtContado.Text)
                            .CodFP_Contado = Convert.ToInt32(lblCodFPContado.Text)
                            .total_Depositado = Convert.ToDouble(txtContado.Text)
                            .Mone_da = txtMoneda.Text
                            .mon_base = Convert.ToInt32(txtMonBase.Text)
                            .comisi_on = 0.0
                            .Comenta_rio = txtComentario.Text
                            .Tipo_Deposito = lblTipoDeposito.Text
                            .cod_Cajero = txtCajero.Text
                            .registrarDepositos()


                            ultimo = Deposito.listarUltimoDeposito
                            nro = ultimo.Rows(0)
                        End With

                        '::::::::::::::::::::::::::::::::  Partida Contable  ::::::::::::::::::::::::::::::::

                        Try
                            Dim periodoContable As New ClsPeriodoContable
                            Dim codPeriodo As String
                            Dim dtp As New DataTable
                            Dim rowp As DataRow
                            dtp = periodoContable.periodoContableActivo()
                            rowp = dtp.Rows(0)
                            codPeriodo = rowp("codPeriodo").ToString

                            With asiento

                                .Cod_Periodo = Convert.ToInt32(codPeriodo)
                                .Descrip = txtComentario.Text
                                .Fecha_ = dtpFecha.Value
                                .Origen_ = "DepositoBancario"
                                .Campo_Llave = Convert.ToInt32(nro("codDeposito"))
                                codigoAsiento = .registrarAsiento()
                                'MsgBox("este es el codigo" + codigoAsiento)

                                If codigoAsiento > 0 Then

                                    Try

                                        With detalleAsiento

                                            'MsgBox("1")
                                            .Cod_Asiento = Convert.ToInt64(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtContado.Text)
                                            .Haber_ = 0.0
                                            .Origen_ = "DepositoBancario"

                                            'MsgBox("2")
                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtBanco.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)
                                            'MsgBox("3")
                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))
                                            'MsgBox("4")

                                            .registrarDetalleAsiento()

                                        End With

                                        'Segunda Partida
                                        Dim detalleAsiento2 As New ClsDetalleAsiento

                                        With detalleAsiento2

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = 0.0
                                            .Haber_ = Convert.ToDouble(txtContado.Text)
                                            .Origen_ = "DepositoBancario"

                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtTipoConta.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With
                                    Catch ex As Exception
                                        MsgBox("Error: " + ex.Message)
                                    End Try 'Partida Contable

                                End If

                            End With

                        Catch ex As Exception
                            MsgBox("Error: " + ex.Message)
                        End Try

                        Limpiar()

                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                MsgBox("Se registro un nuevo deposito.")
                dtDepositos.DataSource = Deposito.listarDepositos

            Else

                If txtBanco.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtBanco.BackColor = Color.Red
                ElseIf txtContado.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtContado.BackColor = Color.Red
                ElseIf txtTipoConta.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtTipoConta.BackColor = Color.Red
                ElseIf txtCajero.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtCajero.BackColor = Color.Red
                ElseIf txtBanco.Text = txtTipoConta.Text Then
                    MsgBox("Las cuentas seleccionadas no pueden ser iguales.")
                    txtBanco.BackColor = Color.Red
                    txtTipoConta.BackColor = Color.Red
                End If

            End If 'campos vacios
        End If 'validacion

    End Sub
    Private Sub btnBuscarBanco_Click(sender As Object, e As EventArgs) Handles btnBuscarBanco.Click
        'Asignar valor a label para diferenciar campo a llenar.
        A_BuscarFormaPago.lblJC.Text = 1
        A_BuscarFormaPago.Show()
    End Sub
    Private Sub btnBuscarTipoConta_Click(sender As Object, e As EventArgs) Handles btnBuscarTipoConta.Click
        'Asignar valor a label para diferenciar campo a llenar.
        A_BuscarFormaPago.lblJC.Text = 2
        A_BuscarFormaPago.Show()
    End Sub
    Private Sub frmDeposito_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RegistrarVentanas(nombre_usurio, Me.Name)
        Try
            alternarColoFilasDatagridview(dtDepositos)
            MenuStrip_slm(MenuStrip1)
            ColoresForm(Panel1, StatusStrip1)
            txtMonBase.Text = "1"
            Dim Deposito As New ClsDeposito
            'Mostrar todos los depositos registrados

            BindingSource1.DataSource = Deposito.listarDepositos
            dtDepositos.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1

            If dtDepositos.Columns.Contains("codDeposito") = True Then

                dtDepositos.Columns("codDeposito").HeaderText = "Cod."
                dtDepositos.Columns("fecha").HeaderText = "Fecha"
                dtDepositos.Columns("codFPBanco").Visible = False
                dtDepositos.Columns("contado").HeaderText = "Contado"
                dtDepositos.Columns("codFPContado").Visible = False
                dtDepositos.Columns("totalDeposito").HeaderText = "Total Deposito"
                dtDepositos.Columns("moneda").Visible = False
                dtDepositos.Columns("monBase").Visible = False
                dtDepositos.Columns("comision").HeaderText = "Comi."
                dtDepositos.Columns("comentario").HeaderText = "Comentario"
                dtDepositos.Columns("tipoDeposito").HeaderText = "Tipo Depos."
                dtDepositos.Columns("codCajero").HeaderText = "Cajero"
                dtDepositos.Columns("estado").HeaderText = "Estado"

            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub btnCrearNuevo_Click(sender As Object, e As EventArgs)



    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub txtContado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtContado.KeyPress
        'Solo acepta numeros.
        'If (Char.IsNumber(e.KeyChar)) Then
        '    e.Handled = False
        'ElseIf (Char.IsControl(e.KeyChar)) Then
        '    e.Handled = False
        'ElseIf (Char.IsPunctuation(e.KeyChar)) Then
        '    e.Handled = False
        'End If

        'NUMEROSCOMA(e, 2, sender)

    End Sub

    Private Sub frmDeposito_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Dim n As String = MsgBox("¿Desea cerrar la ventana?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                Me.Close()
                'frmMenuConta.Show()
            End If
        End If

    End Sub

    Private Sub txtComision_TextChanged(sender As Object, e As EventArgs) Handles txtComision.TextChanged
        Try
            txtTotalDep.Text = Double.Parse(txtContado.Text) - Double.Parse(txtComision.Text)

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub txtComision_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtComision.KeyPress
        'Solo acepta numeros.
        'If (Char.IsNumber(e.KeyChar)) Then
        '    e.Handled = False
        'ElseIf (Char.IsControl(e.KeyChar)) Then
        '    e.Handled = False
        'ElseIf (Char.IsPunctuation(e.KeyChar)) Then
        '    e.Handled = False
        'End If

        'NUMEROSCOMA(e, 2, sender)
    End Sub

    Sub Limpiar()
        txtNro.Text = ""
        dtpFecha.ResetText()
        txtBanco.Text = ""
        txtContado.Text = ""
        'txtTipoConta.Text = ""
        txtTotalDep.Text = ""
        'txtMoneda.Text = ""
        'txtMonBase.Text = ""
        txtComision.Text = ""
        txtCajero.Text = ""
        txtComentario.Text = ""
        chkAnular.Checked = False


        'Color TextBox
        txtBanco.BackColor = Color.White
        txtContado.BackColor = Color.White
        txtTipoConta.BackColor = Color.White
        txtCajero.BackColor = Color.White


    End Sub


    Private Sub txtContado_Leave(sender As Object, e As EventArgs) Handles txtContado.Leave
        'Mostrar el numero con 2 decimales N2 = 1,000.00   y F2 = 1000.00
        Try
            txtContado.Text = Convert.ToDecimal(txtContado.Text).ToString("N2")
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub txtComision_Leave(sender As Object, e As EventArgs) Handles txtComision.Leave
        'Mostrar el numero con 2 decimales N2 = 1,000.00   y F2 = 1000.00
        Try
            txtComision.Text = Convert.ToDecimal(txtComision.Text).ToString("N2")
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub


    Private Sub txtContado_TextChanged(sender As Object, e As EventArgs)
        txtTotalDep.Text = txtContado.Text
    End Sub

    Private Sub txtBanco_TextChanged(sender As Object, e As EventArgs) Handles txtBanco.TextChanged
        If txtBanco.BackColor = Color.Red Then
            txtBanco.BackColor = Color.White

        End If

        If txtTipoConta.BackColor = Color.Red Then
            txtTipoConta.BackColor = Color.White
        End If

    End Sub

    Private Sub txtTipoConta_TextChanged(sender As Object, e As EventArgs) Handles txtTipoConta.TextChanged
        If txtTipoConta.BackColor = Color.Red Then
            txtTipoConta.BackColor = Color.White
        End If

        If txtBanco.BackColor = Color.Red Then
            txtBanco.BackColor = Color.White
        End If
    End Sub

    Private Sub txtContado_TextChanged_1(sender As Object, e As EventArgs) Handles txtContado.TextChanged
        If txtContado.BackColor = Color.Red Then
            txtContado.BackColor = Color.White

        End If
    End Sub


    Private Sub txtCajero_TextChanged(sender As Object, e As EventArgs) Handles txtCajero.TextChanged
        If txtCajero.BackColor = Color.Red Then
            txtCajero.BackColor = Color.White
        End If
    End Sub

    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged

        Try
            'Busqueda por usuario o comentario


            With Deposito

                .Comenta_rio = txtBusqueda.Text
                .cod_Cajero = txtBusqueda.Text
                dtDepositos.DataSource = .buscarDepo

                If dtDepositos.Columns.Contains("codDeposito") = True Then

                    dtDepositos.Columns("codDeposito").HeaderText = "Cod."
                    dtDepositos.Columns("fecha").HeaderText = "Fecha"
                    dtDepositos.Columns("codFPBanco").Visible = False
                    dtDepositos.Columns("contado").HeaderText = "Contado"
                    dtDepositos.Columns("codFPContado").Visible = False
                    dtDepositos.Columns("totalDeposito").HeaderText = "Total Deposito"
                    dtDepositos.Columns("moneda").Visible = False
                    dtDepositos.Columns("monBase").Visible = False
                    dtDepositos.Columns("comision").HeaderText = "Comi."
                    dtDepositos.Columns("comentario").HeaderText = "Comentario"
                    dtDepositos.Columns("tipoDeposito").HeaderText = "Tipo Depos."
                    dtDepositos.Columns("codCajero").HeaderText = "Cajero"
                    dtDepositos.Columns("estado").HeaderText = "Estado"

                End If


            End With

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs)
        Dim n As String = MsgBox("¿Desea cerrar la ventana?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then

            Me.Close()
        End If

    End Sub

    Private Sub dtpFechaBuscar_TextChanged(sender As Object, e As EventArgs) Handles dtpFechaBuscar.TextChanged

        Dim depoFecha As New ClsDeposito

        Try
            Dim dataF As New DataTable
            With depoFecha
                .Fech_a = dtpFechaBuscar.Value
                dtDepositos.DataSource = .buscarDepoFecha()
                If dtDepositos.Columns.Contains("codDeposito") = True Then

                    dtDepositos.Columns("codDeposito").HeaderText = "Cod."
                    dtDepositos.Columns("fecha").HeaderText = "Fecha"
                    dtDepositos.Columns("codFPBanco").Visible = False
                    dtDepositos.Columns("contado").HeaderText = "Contado"
                    dtDepositos.Columns("codFPContado").Visible = False
                    dtDepositos.Columns("totalDeposito").HeaderText = "Total Deposito"
                    dtDepositos.Columns("moneda").Visible = False
                    dtDepositos.Columns("monBase").Visible = False
                    dtDepositos.Columns("comision").HeaderText = "Comi."
                    dtDepositos.Columns("comentario").HeaderText = "Comentario"
                    dtDepositos.Columns("tipoDeposito").HeaderText = "Tipo Depos."
                    dtDepositos.Columns("codCajero").HeaderText = "Cajero"
                    dtDepositos.Columns("estado").HeaderText = "Estado"

                End If
            End With


        Catch ex As Exception
            MsgBox(ex.Message)
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub btnCancelar_Click_1(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Try

            'Mostrar todos los depositos registrados
            dtDepositos.DataSource = Deposito.listarDepositos
            If dtDepositos.Columns.Contains("codDeposito") = True Then

                dtDepositos.Columns("codDeposito").HeaderText = "Cod."
                dtDepositos.Columns("fecha").HeaderText = "Fecha"
                dtDepositos.Columns("codFPBanco").Visible = False
                dtDepositos.Columns("contado").HeaderText = "Contado"
                dtDepositos.Columns("codFPContado").Visible = False
                dtDepositos.Columns("totalDeposito").HeaderText = "Total Deposito"
                dtDepositos.Columns("moneda").Visible = False
                dtDepositos.Columns("monBase").Visible = False
                dtDepositos.Columns("comision").HeaderText = "Comi."
                dtDepositos.Columns("comentario").HeaderText = "Comentario"
                dtDepositos.Columns("tipoDeposito").HeaderText = "Tipo Depos."
                dtDepositos.Columns("codCajero").HeaderText = "Cajero"
                dtDepositos.Columns("estado").HeaderText = "Estado"

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtBuscaCodigo_TextChanged(sender As Object, e As EventArgs) Handles txtBuscaCodigo.TextChanged
        Dim depoCod As New ClsDeposito

        Try
            Dim dataC As New DataTable
            With depoCod
                .Cod = Convert.ToInt32(txtBuscaCodigo.Text)
                dtDepositos.DataSource = .buscarDepoCodigo()

                If dtDepositos.Columns.Contains("codDeposito") = True Then

                    dtDepositos.Columns("codDeposito").HeaderText = "Cod."
                    dtDepositos.Columns("fecha").HeaderText = "Fecha"
                    dtDepositos.Columns("codFPBanco").Visible = False
                    dtDepositos.Columns("contado").HeaderText = "Contado"
                    dtDepositos.Columns("codFPContado").Visible = False
                    dtDepositos.Columns("totalDeposito").HeaderText = "Total Deposito"
                    dtDepositos.Columns("moneda").Visible = False
                    dtDepositos.Columns("monBase").Visible = False
                    dtDepositos.Columns("comision").HeaderText = "Comi."
                    dtDepositos.Columns("comentario").HeaderText = "Comentario"
                    dtDepositos.Columns("tipoDeposito").HeaderText = "Tipo Depos."
                    dtDepositos.Columns("codCajero").HeaderText = "Cajero"
                    dtDepositos.Columns("estado").HeaderText = "Estado"

                End If


            End With


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub dtDepositos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtDepositos.CellDoubleClick


        'Mostrar datos seleccionados del datagrid
        Try

            Dim dt2 As New DataTable
            Dim row2 As DataRow

            btnCrearNuevo.Enabled = True
            btnModificar.Enabled = True
            btnGuardar.Enabled = False

            txtNro.Text = dtDepositos.Rows(e.RowIndex).Cells(0).Value
            dtpFecha.Value = dtDepositos.Rows(e.RowIndex).Cells(1).Value

            lblCodFPBanco.Text = dtDepositos.Rows(e.RowIndex).Cells(2).Value
            buscarCodigo.Codigo_FormaPago = Integer.Parse(lblCodFPBanco.Text)

            dt2 = buscarCodigo.buscarFormaPago()
            row2 = dt2.Rows(0)
            txtBanco.Text = row2("codigo")

            txtContado.Text = dtDepositos.Rows(e.RowIndex).Cells(3).Value

            lblCodFPContado.Text = dtDepositos.Rows(e.RowIndex).Cells(4).Value
            buscarCodigo.Codigo_FormaPago = Integer.Parse(lblCodFPContado.Text)

            dt2 = buscarCodigo.buscarFormaPago()
            row2 = dt2.Rows(0)
            txtTipoConta.Text = row2("codigo")

            txtTotalDep.Text = dtDepositos.Rows(e.RowIndex).Cells(5).Value
            txtMoneda.Text = dtDepositos.Rows(e.RowIndex).Cells(6).Value
            txtMonBase.Text = dtDepositos.Rows(e.RowIndex).Cells(7).Value
            txtComision.Text = dtDepositos.Rows(e.RowIndex).Cells(8).Value
            txtComentario.Text = dtDepositos.Rows(e.RowIndex).Cells(9).Value
            lblTipoDeposito.Text = dtDepositos.Rows(e.RowIndex).Cells(10).Value
            txtCajero.Text = dtDepositos.Rows(e.RowIndex).Cells(11).Value
            chkAnular.Checked = dtDepositos.Rows(e.RowIndex).Cells(12).Value

            If chkAnular.Checked = True Then
                txtNro.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtBanco.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtCajero.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtComentario.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtComision.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtContado.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtMonBase.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtMoneda.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtTipoConta.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                txtTotalDep.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                dtpFecha.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
                lblTipoDeposito.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Strikeout)
            Else
                txtNro.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtBanco.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtCajero.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtComentario.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtComision.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtContado.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtMonBase.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtMoneda.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtTipoConta.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                txtTotalDep.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                dtpFecha.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
                lblTipoDeposito.Font = New Font("Microsoft Sans Serif", 8.25, FontStyle.Regular)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub AsientoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        If txtNro.Text = "" Then

            MsgBox("Debe seleccionar una transacción para ver un asiento contable.")

        Else

            Try
                With asiento

                    .Campo_Llave = Convert.ToInt32(txtNro.Text)

                    Dim dtA As DataTable
                    Dim rows As DataRow

                    dtA = .VerAsiento
                    rows = dtA.Rows(0)

                    'Asignando valores a forma Asiento

                    With frmAsientos

                        .txtNro.Text = rows("campoLlave")
                        .txtTexto.Text = rows("descripcion")
                        .dtpFecha.Value = rows("fecha")
                        .lblCodAsiento.Text = rows("cod_asiento")

                        .Show()
                    End With

                End With
            Catch ex As Exception
                MsgBox("No se registro asiento de la transaccion o ocurrio un error. Detalle: " + ex.Message)
            End Try

        End If
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrearNuevo.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Creo un deposito")
            frmTipoDeposito.Show()
            btnGuardar.Enabled = True
            Me.Close()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Actualizar deposito " + txtNro.Text)
            Dim n As String = MsgBox("¿Desea modificar el depósito?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then

                '::::::::::::::::::::::::::::::::::Enviar datos para modificar depositos

                With Deposito
                    .Cod = Convert.ToInt32(txtNro.Text)
                    .Fech_a = dtpFecha.Value
                    .CodFP_Banco = Convert.ToInt32(lblCodFPBanco.Text)
                    .conta_do = Convert.ToDouble(txtContado.Text)
                    .CodFP_Contado = Convert.ToInt32(lblCodFPContado.Text)
                    .total_Depositado = Convert.ToDouble(txtTotalDep.Text)
                    .Mone_da = txtMoneda.Text
                    .mon_base = Convert.ToInt32(txtMonBase.Text)
                    .comisi_on = Convert.ToDouble(txtComision.Text)
                    .Comenta_rio = txtComentario.Text
                    .Tipo_Deposito = lblTipoDeposito.Text
                    .cod_Cajero = txtCajero.Text
                    .Estado_ = chkAnular.Checked
                    If .modificarDepositos() = 1 Then
                        MsgBox("Se modifico el registro.")
                        Limpiar()
                        BindingSource1.DataSource = Deposito.listarDepositos
                        dtDepositos.DataSource = BindingSource1
                        BindingNavigator1.BindingSource = BindingSource1
                    End If

                End With
                ':::::::::::::::::::::::::::::::::: Fin Enviar datos para modificar depositos

                '::::::::::::::::::::::::::::::::::::Anulacion de asiento
                Dim objasiento As New ClsAsientoContable
                With objasiento
                    .Campo_Llave = Integer.Parse(txtNro.Text)
                    .Origen_ = "DepositoBancario"
                    If chkAnular.Checked = True Then
                        .AnularAsiento(0)
                    Else
                        .AnularAsiento(1)
                    End If
                End With
                '::::::::::::::::::::::::::::::::::::Fin Anulacion de asiento

                '::::::::::::::::::::::::::::::::::::Modificacion de asiento
                '::::::::::::::::::::::::::::::::::::FIN Modificacion de asiento




            End If 'validacion
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim n As String = MsgBox("¿Desea guardar el depósito?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then
            RegistrarAcciones(nombre_usurio, Me.Name, "Guardo el deposito " + txtNro.Text)

            If txtBanco.Text <> "" And txtContado.Text <> "" And txtTipoConta.Text <> "" And txtCajero.Text <> "" And txtBanco.Text <> txtTipoConta.Text Then


                'Guardar deposito en la base de datos

                Try

                    Dim ultimo As DataTable
                    Dim nro As DataRow
                    Dim codigoAsiento As String

                    If lblTipoDeposito.Text = "Tarjeta" Then

                        'Campos para tipo de deposito : Tarjeta
                        With Deposito
                            .Fech_a = dtpFecha.Value
                            .CodFP_Banco = Convert.ToInt32(lblCodFPBanco.Text)
                            .conta_do = Convert.ToDouble(txtContado.Text)
                            .CodFP_Contado = Convert.ToInt32(lblCodFPContado.Text)
                            .total_Depositado = Convert.ToDouble(txtTotalDep.Text)
                            .Mone_da = txtMoneda.Text
                            .mon_base = Double.Parse(txtMonBase.Text)
                            .comisi_on = Convert.ToDouble(txtComision.Text)
                            .Comenta_rio = txtComentario.Text
                            .Tipo_Deposito = lblTipoDeposito.Text
                            .cod_Cajero = txtCajero.Text
                            .Estado_ = chkAnular.Checked
                            .registrarDepositos()


                            ultimo = Deposito.listarUltimoDeposito
                            nro = ultimo.Rows(0)
                        End With

                        '::::::::::::::::::::::::::::::::  Partida Contable  ::::::::::::::::::::::::::::::::

                        Try
                            Dim periodoContable As New ClsPeriodoContable
                            Dim codPeriodo As String
                            Dim dtp As New DataTable
                            Dim rowp As DataRow
                            dtp = periodoContable.periodoContableActivo()
                            rowp = dtp.Rows(0)
                            codPeriodo = rowp("codPeriodo").ToString

                            With asiento

                                .Cod_Periodo = Convert.ToInt32(codPeriodo)
                                .Descrip = txtComentario.Text
                                .Fecha_ = dtpFecha.Value
                                .Origen_ = "DepositoBancario"
                                .Campo_Llave = Convert.ToInt32(nro("codDeposito"))
                                codigoAsiento = .registrarAsiento

                                If codigoAsiento > 0 Then

                                    Try

                                        With detalleAsiento

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtTotalDep.Text)
                                            .Haber_ = 0.0
                                            .Origen_ = "DepositoBancario"

                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtBanco.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With

                                        'Segunda Partida
                                        Dim detalleAsiento2 As New ClsDetalleAsiento

                                        With detalleAsiento2

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = 0.0
                                            .Haber_ = Convert.ToDouble(txtContado.Text)

                                            .Origen_ = "DepositoBancario"
                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtTipoConta.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With

                                        'Partida de Comisión
                                        Dim comision As New ClsDetalleAsiento

                                        With comision

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtComision.Text)
                                            .Haber_ = 0.0
                                            .Cuenta_ = 660005

                                            .Origen_ = "DepositoBancario"
                                            .registrarDetalleAsiento()

                                        End With

                                    Catch ex As Exception
                                        MsgBox("Error en detalle de asiento" + ex.Message)
                                    End Try

                                End If

                            End With

                        Catch ex As Exception
                            MsgBox("Aqui hay error: " + ex.Message)
                        End Try 'Partida Contable

                        'Limpiar()
                        txtNro.Clear()
                        txtContado.Clear()
                        txtCajero.Clear()
                        txtComision.Clear()
                        txtTotalDep.Clear()

                    Else
                        'Campos para tipo de deposito: Deposito Bancario
                        With Deposito
                            .Fech_a = dtpFecha.Value
                            .CodFP_Banco = Convert.ToInt32(lblCodFPBanco.Text)
                            .conta_do = Convert.ToDouble(txtContado.Text)
                            .CodFP_Contado = Convert.ToInt32(lblCodFPContado.Text)
                            .total_Depositado = Convert.ToDouble(txtContado.Text)
                            .Mone_da = txtMoneda.Text
                            .mon_base = Double.Parse(txtMonBase.Text)
                            .comisi_on = 0.0
                            .Comenta_rio = txtComentario.Text
                            .Tipo_Deposito = lblTipoDeposito.Text
                            .cod_Cajero = txtCajero.Text
                            .registrarDepositos()


                            ultimo = Deposito.listarUltimoDeposito
                            nro = ultimo.Rows(0)
                        End With

                        '::::::::::::::::::::::::::::::::  Partida Contable  ::::::::::::::::::::::::::::::::

                        Try
                            Dim periodoContable As New ClsPeriodoContable
                            Dim codPeriodo As String
                            Dim dtp As New DataTable
                            Dim rowp As DataRow
                            dtp = periodoContable.periodoContableActivo()
                            rowp = dtp.Rows(0)
                            codPeriodo = rowp("codPeriodo").ToString

                            With asiento

                                .Cod_Periodo = Convert.ToInt32(codPeriodo)
                                .Descrip = txtComentario.Text
                                .Fecha_ = dtpFecha.Value
                                .Origen_ = "DepositoBancario"
                                .Campo_Llave = Convert.ToInt32(nro("codDeposito"))
                                codigoAsiento = .registrarAsiento()

                                If codigoAsiento > 0 Then

                                    Try

                                        With detalleAsiento

                                            .Cod_Asiento = Convert.ToInt64(codigoAsiento)
                                            .Debe_ = Convert.ToDouble(txtContado.Text)
                                            .Haber_ = 0.0
                                            .Origen_ = "DepositoBancario"

                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtBanco.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)
                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With

                                        'Segunda Partida
                                        Dim detalleAsiento2 As New ClsDetalleAsiento

                                        With detalleAsiento2

                                            .Cod_Asiento = Convert.ToInt32(codigoAsiento)
                                            .Debe_ = 0.0
                                            .Haber_ = Convert.ToDouble(txtContado.Text)
                                            .Origen_ = "DepositoBancario"

                                            Dim dt As DataTable
                                            Dim row As DataRow
                                            buscarCodigo.Cod = txtTipoConta.Text
                                            dt = buscarCodigo.infoFormaPago
                                            row = dt.Rows(0)

                                            .Cuenta_ = Convert.ToInt32(row("cuenta"))

                                            .registrarDetalleAsiento()

                                        End With
                                    Catch ex As Exception
                                        MsgBox("Error: " + ex.Message)
                                    End Try 'Partida Contable

                                End If

                            End With

                        Catch ex As Exception
                            MsgBox("Error: " + ex.Message)
                        End Try

                        'LIMPIAR CAMPOS AL GUARDAR
                        'Limpiar()
                        txtNro.Clear()
                        txtContado.Clear()
                        'txtComentario.Clear()
                        txtCajero.Clear()
                        txtComision.Clear()
                        txtTotalDep.Clear()

                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                MsgBox("Se registro un nuevo deposito.")
                dtDepositos.DataSource = Deposito.listarDepositos

            Else

                If txtBanco.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtBanco.BackColor = Color.Red
                ElseIf txtContado.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtContado.BackColor = Color.Red
                ElseIf txtTipoConta.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtTipoConta.BackColor = Color.Red
                ElseIf txtCajero.Text = "" Then
                    MsgBox("Falta información en el formulario.")
                    txtCajero.BackColor = Color.Red
                ElseIf txtBanco.Text = txtTipoConta.Text Then
                    MsgBox("Las cuentas seleccionadas no pueden ser iguales.")
                    txtBanco.BackColor = Color.Red
                    txtTipoConta.BackColor = Color.Red
                End If

            End If 'campos vacios
        End If 'validacion
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dim n As String = MsgBox("¿Desea cerrar la ventana?", MsgBoxStyle.YesNo, "Validación")
        If n = vbYes Then

            Me.Close()
        End If
    End Sub

    Private Sub CancelarToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub VerAsientoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VerAsientoToolStripMenuItem.Click
        Try

            frmAsientos.lblForm.Text = "Consultar"
            frmAsientos.txtNro.Text = Me.txtNro.Text
            frmAsientos.lblOrigen.Text = "DepositoBancario"
            frmAsientos.Show()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub txtTotalDep_TextChanged(sender As Object, e As EventArgs) Handles txtTotalDep.TextChanged
        Try
            txtTotalDep.Text = Convert.ToDecimal(txtTotalDep.Text).ToString("N2")

        Catch ex As Exception

        End Try
    End Sub
End Class