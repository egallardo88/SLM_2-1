﻿Public Class M_ListadoItemExamenes
    Private Sub M_ListadoItemExamenes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            actualizarListado()
            alternarColoFilasDatagridview(dgvItems)
            dgvItems.Columns("descripcion").HeaderText = "Descripción"
        Catch ex As Exception

        End Try
    End Sub
    Private Sub E_DetalleExamenes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub actualizarListado()
        Try
            Dim objItem As New ClsItemExamen
            Dim dt As New DataTable
            dt = objItem.listarItemExamenFacturables
            dgvItems.DataSource = dt
        Catch ex As Exception

        End Try
    End Sub
    Private Sub dgvDatos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvItems.CellMouseDoubleClick
        Try
            Dim n As String = ""
            If (lblform.Text = "M_Precio") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen seleccionado?", MsgBoxStyle.YesNo, "Validación")
                End If
                If n = vbYes Then
                    M_Precio.txtcodigoItem.Text = dgvItems.Rows(e.RowIndex).Cells(0).Value()
                    Me.Close()
                End If

            ElseIf (lblform.Text = "Informe") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen seleccionado?", MsgBoxStyle.YesNo, "Validación")
                End If
                If n = vbYes Then
                    A_Informes.lblCodExamen.Text = dgvItems.Rows(e.RowIndex).Cells(0).Value()
                    A_Informes.txtCodExamen.Text = dgvItems.Rows(e.RowIndex).Cells(1).Value()
                    A_Informes.txtNombreExamen.Text = dgvItems.Rows(e.RowIndex).Cells(2).Value()
                    Me.Close()
                End If


            ElseIf (lblform.Text = "etiqueta") Then
                If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea utilizar el examen seleccionado?", MsgBoxStyle.YesNo, "Validación")
                End If
                If n = vbYes Then
                    ConfigurarEtiqueta.lblCodItemExa.Text = dgvItems.Rows(e.RowIndex).Cells(0).Value()
                    ConfigurarEtiqueta.txtNombreExamen.Text = dgvItems.Rows(e.RowIndex).Cells(2).Value()
                    Me.Close()
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtnombreB_TextChanged(sender As Object, e As EventArgs) Handles txtnombreB.TextChanged
        Try
            Dim dv As New DataView
            Dim dt As New DataTable

            dt = dgvItems.DataSource

            dv = dt.DefaultView
            dv.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", txtnombreB.Text)

            If dv.Count = "0" Then
                MsgBox("No existe el item facturable.", MsgBoxStyle.Exclamation)
                txtnombreB.Text = ""
                actualizarListado()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class