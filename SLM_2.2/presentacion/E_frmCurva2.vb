﻿Imports System.Data.SqlClient
Imports System.Windows.Forms.DataVisualization.Charting

Public Class E_frmCurva2
    Dim sCommand As SqlCommand
    Dim sAdapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Public respuesta As SqlDataReader
    Dim sDs As DataSet
    Public enunciado As SqlCommand
    Dim sTable As DataTable
    Dim nombre_examen1 As String
    Dim nombre_examen2 As String
    Dim bandera_doble_examen As Integer = 0

    Dim id_ordenconsultas1 As String
    Dim id_ordenconsultas2 As String
    Private Sub CargarData()
        Dim clsc As New ClsConnection
        'Dim connectionString As String = "Data Source=.;Initial Catalog=pubs;Integrated Security=True"
        Dim sql As String = "SELECT * FROM OrdenTrabajoExamenGraficable where id_ordentrabajo ='" + TextBox1.Text + "' order by horas"
        Dim connection As New SqlConnection(clsc.str_con)
        connection.Open()
        sCommand = New SqlCommand(sql, connection)
        sAdapter = New SqlDataAdapter(sCommand)
        sBuilder = New SqlCommandBuilder(sAdapter)
        sDs = New DataSet()
        sAdapter.Fill(sDs, "OrdenTrabajoExamenGraficable$")
        sTable = sDs.Tables("OrdenTrabajoExamenGraficable$")
        connection.Close()
        DataGridView1.DataSource = sDs.Tables("OrdenTrabajoExamenGraficable$")
        DataGridView1.ReadOnly = False
        'save_btn.Enabled = True
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect


        'DataGridView1.Columns(8).Visible = False
        'DataGridView1.Columns(9).Visible = False
        'DataGridView1.Columns(10).Visible = False
    End Sub
    Public Sub cargarDatosPaciente()
        Dim clsC As New ClsConnection
        Try
            enunciado = New SqlCommand("select distinct c.nombreCompleto,c.fechaNacimiento,c.genero,s.nombre from  factura f , OrdenDeTrabajo o,OrdenTrabajoDetalle od, Cliente c,Sucursal s
where  o.cod_orden_trabajo = od.cod_orden_trabajo and f.numero = o.cod_factura and s.codigo = f.codigoSucursal
and c.codigo = f.codigoCliente and f.numero ='" + id_facturatrabajo_grafico + "'

", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()
            While respuesta.Read
                TextBox2.Text = respuesta.Item("nombreCompleto")
                TextBox5.Text = respuesta.Item("genero")
                TextBox6.Text = DateDiff("yyyy", CDate(respuesta.Item("fechaNacimiento")), Date.Now)
                TextBox4.Text = respuesta.Item("nombre")
            End While
            respuesta.Close()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Public Function ValidadorDeResultado(ByVal orden As String) As String
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        Dim valor As String
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select estado from ordendetrabajo
where cod_orden_trabajo ='" + orden + "'", clsC.getConexion)
            valor = enunciado.ExecuteScalar

            Return valor
        Catch ex As Exception

        End Try
    End Function
    Private Sub E_frmCurva2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.Text = id_ordentrabajo_grafico
        ' TextBox9.Text = nombre_usurio
        TextBox10.Text = id_facturatrabajo_grafico
        TextBox7.Text = nombre_examen_paragrafico
        Label15.Text = nombre_examen_paragrafico
        Label17.Text = nombre_examen_paragrafico
        If estado_paragrafico_glucosa = "Procesado" Then
            RadioButton1.Checked = True
        End If

        If estado_paragrafico_glucosa = "Validado" Then
            RadioButton2.Checked = True
        End If
        ' alternarColoFilasDatagridview(DataGridView1)
        ColoresForm(Panel7, StatusStrip1)
        'cargarDataTolerancia()
        'generarGrafica()
        cargarDatosPaciente()
        cargarDatosOrden()
        cargarDatosOrdenValidador()

        If registrarOrdenExamen() = "2" Then

        ElseIf registrarOrdenExamen() = "1" Then


        End If
        Try
            CargarData()
            generarGrafica()
            generarUnaGrafica()

        Catch ex As Exception

        End Try

    End Sub
    Public Sub cargarDatosExamen()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_tecnico = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox8.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub cargarDatosOrden()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_tecnico = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox8.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDatosOrdenValidador()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_validador = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox9.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDataGlucosa()

    End Sub
    Public Sub generarGrafica()

        'VARIABLES DE EXAMENES
        Dim id_ordenExamen1 As String
        Dim id_ordenExamen2 As String
        Dim id_ordenconsultas1 As String
        Dim id_ordenconsultas2 As String
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3



        'RECUPERAR LAS DOS ORDENES DE TRABAJO DE UNA FACTURA
        Dim contador As Integer
        Try
            enunciado = New SqlCommand("select o.cod_factura ,od.cod_orden_trabajo,i.codItemExa,i.descripcion,o.estado,o.pmFecha from   OrdenDeTrabajo o,OrdenTrabajoDetalle od,ItemExamenDetalle id, Item_Examenes i
where od.cod_item_examen_detalle = id.codigo and i.codItemExa = id.codigoItemExamen and o.cod_orden_trabajo = od.cod_orden_trabajo
and o.cod_factura ='" + id_facturatrabajo_grafico + "'
and i.codItemExa  in (select cod_examen from ListadoExamenesGraficables )", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read

                'SI EL ID DE LA ORDEN ES IGUAL AL CODIGO DE ORDEN ENTONCES QUE GUARDE
                'EL PRIMER EXAMEN
                If contador = 0 Then
                    id_ordenExamen1 = respuesta.Item("cod_orden_trabajo")
                    'VALIDAR SI ES EL CODIGO ORIGINAL
                    If id_ordentrabajo_grafico = id_ordenExamen1 Then
                        id_ordenconsultas1 = respuesta.Item("cod_orden_trabajo")
                        nombre_examen1 = respuesta.Item("descripcion")
                    Else
                        id_ordenconsultas2 = respuesta.Item("cod_orden_trabajo")
                        nombre_examen2 = respuesta.Item("descripcion")


                    End If

                ElseIf contador = 1 Then
                    id_ordenExamen2 = respuesta.Item("cod_orden_trabajo")
                    'VALIDAR SI ES EL CODIGO ORIGINAL
                    If id_ordentrabajo_grafico = id_ordenExamen2 Then
                        id_ordenconsultas1 = respuesta.Item("cod_orden_trabajo")
                        nombre_examen1 = respuesta.Item("descripcion")
                    Else
                        id_ordenconsultas2 = respuesta.Item("cod_orden_trabajo")
                        nombre_examen2 = respuesta.Item("descripcion")

                    End If




                End If








                contador = contador + 1

            End While
            respuesta.Close()

            Label10.Text = nombre_examen1
            Label15.Text = nombre_examen1
            TextBox7.Text = nombre_examen1
            Label11.Text = nombre_examen2

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        'SI EL CONTADOS ES IGUAL A 1 
        'Llenar grafico

        If id_ordenExamen1 <> "" And id_ordenExamen2 <> "" Then
            bandera_doble_examen = 1
            Dim tblFields As String = "select distinct a.horas as hora1,a.resultado as res1,b.horas as horas2,b.resultado  as res2 
from  OrdenTrabajoExamenGraficable a,OrdenTrabajoExamenGraficable b 
where a.id_ordentrabajo ='" + id_ordenconsultas2 + "' and b.id_ordentrabajo = '" + id_ordenconsultas1 + "' and
a.horas = b.horas
and a.id_factura = b.id_factura
order by a.horas "
            Dim oData As New SqlDataAdapter(tblFields, cnn3)
            Dim ds As New DataSet
            Dim oCmd As New SqlCommand(tblFields, cnn3)
            cnn3.Open()
            oData.Fill(ds, "tblTagInfo")
            cnn3.Close()
            'Chart1.DataBindTable(ds.Tables("tblTagInfo"), "horas")
            Chart1.DataSource = ds.Tables("tblTagInfo")
            Dim Series1 As Series = Chart1.Series("Series1")


            Chart1.Series(Series1.Name).XValueMember = "hora1"
            Chart1.Series(Series1.Name).YValueMembers = "res1"
            Chart1.DataBind()
            'segundo data source   
            'Dim tblFields2 As String = "select * from  OrdenTrabajoExamenGraficable where id_ordentrabajo ='2561' 
            'order by horas"
            'Dim oData2 As New SqlDataAdapter(tblFields2, cnn3)
            'Dim ds2 As New DataSet
            'Dim oCmd2 As New SqlCommand(tblFields2, cnn3)
            'cnn3.Open()
            'oData.Fill(ds2, "tblTagInfo2")
            'cnn3.Close()

            'Chart1.DataSource = ds2.Tables("tblTagInfo2")
            'Chart1.DataBind()
            Dim Series2 As Series = Chart1.Series("Series2")

            Chart1.Series(Series2.Name).XValueMember = "horas2"
            Chart1.Series(Series2.Name).YValueMembers = "res2"

            Chart1.ChartAreas(0).AxisX.Title = "Horas"
            Chart1.ChartAreas(0).AxisY.Title = "Glicemia / Insulemia"

            Chart1.Series(Series1.Name).LegendText = nombre_examen1
            Chart1.Series(Series2.Name).LegendText = nombre_examen2
            Chart1.Legends.Add("")
            Chart1.Series("Series1").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.Dash
            Chart1.Series("Series1").BorderWidth = 2
            Chart1.Series("Series2").BorderWidth = 2
            Chart1.Series("Series1").Color = Color.OrangeRed

            Chart1.Series("Series1").MarkerStyle = MarkerStyle.Diamond
            Chart1.Series("Series1").MarkerColor = Color.Black
            Chart1.Series("Series1").MarkerBorderColor = Color.Black
            Chart1.Series("Series1").MarkerSize = 7

            Chart1.Series("Series1").IsVisibleInLegend = True
            'Chart1.Series(0).Points(0).AxisLabel.ToString()
        ElseIf id_ordenExamen2 = "" Then


        End If



    End Sub
    Public Function registrarOrdenExamen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCrearHojaDeTrabajoGraficas2"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_factura"
        sqlpar.Value = id_facturatrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) 
        generarUnaGrafica()
    End Sub
    Public Sub generarUnaGrafica()

        Dim id_ordenExamen1 As String
        Dim id_ordenExamen2 As String
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Dim tblFields As String = "select  a.horas as hora1,a.resultado as res1
from  OrdenTrabajoExamenGraficable a
where a.id_ordentrabajo ='" + TextBox1.Text + "'


order by a.horas "
        Dim oData As New SqlDataAdapter(tblFields, cnn3)
        Dim ds As New DataSet
        Dim oCmd As New SqlCommand(tblFields, cnn3)
        cnn3.Open()
        oData.Fill(ds, "tblTagInfo")
        cnn3.Close()
        Try
            'Chart1.DataBindTable(ds.Tables("tblTagInfo"), "horas")
            Chart2.DataSource = ds.Tables("tblTagInfo")
            Dim Series1 As Series = Chart2.Series("Series1")


            Chart2.Series(Series1.Name).XValueMember = "hora1"
            Chart2.Series(Series1.Name).YValueMembers = "res1"
            Chart2.DataBind()
            'segundo data source   
            'Dim tblFields2 As String = "select * from  OrdenTrabajoExamenGraficable where id_ordentrabajo ='2561' 
            'order by horas"
            'Dim oData2 As New SqlDataAdapter(tblFields2, cnn3)
            'Dim ds2 As New DataSet
            'Dim oCmd2 As New SqlCommand(tblFields2, cnn3)
            'cnn3.Open()
            'oData.Fill(ds2, "tblTagInfo2")
            'cnn3.Close()

            'Chart1.DataSource = ds2.Tables("tblTagInfo2")
            'Chart1.DataBind()
            'Dim Series2 As Series = Chart1.Series("Series2")

            'Chart1.Series(Series2.Name).XValueMember = "horas2"
            'Chart1.Series(Series2.Name).YValueMembers = "res2"

            Chart2.ChartAreas(0).AxisX.Title = "Horas"
            Chart2.ChartAreas(0).AxisY.Title = "Glicemia"

            Chart2.Series("Series1").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.Dash
            Chart2.Series("Series1").BorderWidth = 2
            Chart2.Series("Series2").BorderWidth = 2
            Chart2.Series("Series1").Color = Color.Blue

            Chart2.Series("Series1").MarkerStyle = MarkerStyle.Diamond
            Chart2.Series("Series1").MarkerColor = Color.Black
            Chart2.Series("Series1").MarkerBorderColor = Color.Black
            Chart2.Series("Series1").MarkerSize = 7

            Chart2.Series("Series1").IsVisibleInLegend = True
            'Chart1.Series(0).Points(0).AxisLabel.ToString()
        Catch ex As Exception

        End Try

    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        generarGrafica()
        Label10.Visible = True
        Label11.Visible = True
        Label12.Visible = True
        Label13.Visible = True
    End Sub
    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim clsr As New ClsResultados
        ' E_frmInsulinaIndividual.Show()
        clsr.GraficaGlucosaIndividualEnviarCorreo(TextBox1.Text)
    End Sub
    Private Sub BackgroundWorker2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        Dim clsr As New ClsResultados
        ' E_frmInsulinaIndividual.Show()
        clsr.GraficaDobleCurvaEnviarCorreo(TextBox1.Text)
    End Sub
    Private Sub Validar_Click(sender As Object, e As EventArgs) Handles Validar.Click
        If RadioButton1.Checked = True Then
            'IF PARA ACTUALIZAR QUIEN FUE EL QUE PROCESO
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                MsgBox("Se actualizo el estado de la orden")
                TextBox8.Text = nombre_usurio
            End If
        ElseIf RadioButton2.Checked = True Then
            'IF PARA VALIDAR QUIEN FUE EL QUE VALIDO
            If CambiarEstadoOrdendetrabajoValidaro("Validado") = "1" Then
                MsgBox("Se actualizo el estado de la orden")
                TextBox9.Text = nombre_usurio
            End If
        End If
        Try
            generarGrafica()
            generarUnaGrafica()
        Catch ex As Exception

        End Try


        If bandera_doble_examen = 0 Then
            'IMPRIMA EXAMEN DOBLE

            Try
                BackgroundWorker1.RunWorkerAsync()
            Catch ex As Exception

            End Try
        ElseIf bandera_doble_examen = 1 Then
            If ValidadorDeResultado(id_ordenconsultas1) = "Validado" And ValidadorDeResultado(id_ordenconsultas2) = "Validado" Then
                MsgBox("hola")
                'IMPRIMA EXAMEN UNICO
                BackgroundWorker2.RunWorkerAsync()

            End If
        End If

    End Sub

    Public Function CambiarEstadoOrdendetrabajo(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficas"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function CambiarEstadoOrdendetrabajoValidaro(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficasValidador"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            ' RegistrarAcciones(nombre_usurio, Me.Name, "Guardo examen osmosis id orden" + TextBox1.Text)
            sAdapter.Update(sTable)
            DataGridView1.[ReadOnly] = False
            'save_btn.Enabled = True
            ' CargarData()
            'cargarGrafico()
            MsgBox("Datos almacenados")
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then

                TextBox8.Text = nombre_usurio
                RadioButton1.Checked = True
            End If
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

        'VALIDAR SI EL EXAMEN ES DOBLE O 1
        'SI LA BANDERA ES 0 ES UN EXAMEN NO DOBLE

        Try
                generarUnaGrafica()

                Dim imagen_g As New Bitmap(Chart2.Width, Chart2.Height)
                Chart2.DrawToBitmap(imagen_g, Chart2.DisplayRectangle)

                PictureBox1.Image = imagen_g

                'Dim imagen_data As New Bitmap(Chart2.Width, Chart2.Height)
                If RegistrarImagen() = "1" Then

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        'SI LA BANDERA ES 1 ES UN DOBLE EXAMEN

        Try
                generarGrafica()

                Dim imagen_g As New Bitmap(Chart1.Width, Chart1.Height)
                Chart1.DrawToBitmap(imagen_g, Chart1.DisplayRectangle)

                PictureBox1.Image = imagen_g

                'Dim imagen_data As New Bitmap(Chart2.Width, Chart2.Height)
                If RegistrarImagen2() = "1" Then

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        E_frmInsulinaIndividual.Show()
    End Sub

    Private Sub E_frmCurva2_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        E_frmListarOrdenGraficaInsulina.Show()
    End Sub

    Public Function RegistrarImagen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer
        Dim ms As New System.IO.MemoryStream()
        PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp)

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarImagenOrdenTrabajo"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_orden"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "es_doble"
        sqlpar.Value = False
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "imagen"
        sqlpar.Value = ms.GetBuffer()
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function RegistrarImagen2() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer
        Dim ms As New System.IO.MemoryStream()
        PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp)

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarImagenOrdenTrabajo"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_orden"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "es_doble"
        sqlpar.Value = True
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "imagen"
        sqlpar.Value = ms.GetBuffer()
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

    End Sub
End Class