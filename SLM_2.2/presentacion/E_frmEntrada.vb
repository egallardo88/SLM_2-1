﻿Imports System.Data.SqlClient

Public Class E_frmEntrada
    'ESTA VENTANA TIENE BITACORA

    Private codigo_detalleoc As String
    Private fecha_vencimiento As Date
    Private id_oc_modificar As Integer
    Dim txtCantidadSolicita, id_productoSP As Integer
    Private id_detalle_ov_v As Integer

    Public Sub limpiarVentana()

        txtProducto.Clear()
        txtCodProc.Clear()
        txtLote.Clear()

        txtPrecioUnitario.Clear()

        txtCantidad.Clear()

    End Sub

    Private Sub E_frmEntrada_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RegistrarVentanas(nombre_usurio, Me.Name)
        alternarColoFilasDatagridview(DataGridView1)
        alternarColoFilasDatagridview(DataGridView2)
        alternarColoFilasDatagridview(DataGridView3)
        MenuStrip_slm(MenuStrip1)
        ColoresForm(Panel2, StatusStrip1)
        ComboAlmacen()
        ComboAlmacen2()
        cargarDevoluciones()
        Label22.Text = ""
        TextBox2.Text = "Almacén Central"
    End Sub
    Public Sub sumarData()
        Dim Total As Integer = 0

        Try
            For Each row As DataGridViewRow In Me.DataGridView1.Rows
                Total += Integer.Parse(row.Cells(5).Value)

            Next
        Catch ex As Exception

        End Try

        If Total > 0 Then
            Label22.Text = "*Tiene productos pendientes de ser recibidos en esta orden"
        Else
            Label22.Text = "*No tiene productos pendientes en esta orden de compra"
        End If

    End Sub
    Private Sub cargarInventario()
        Dim clsE As New clsEntradaAlmacen
        Dim dvOC As DataView = clsE.ListarEntradaInventario.DefaultView


    End Sub
    Private Sub ComboAlmacen()
        Dim clsD As New ClsAlmacen

        Dim ds As New DataTable


        ds.Load(clsD.RecuperarAlmacenes())

        ' cmbEntrada.DataSource = ds
        '  cmbEntrada.DisplayMember = "nombre_almacen"
        ' cmbEntrada.ValueMember = "id_almacen"
    End Sub
    Private Sub ComboAlmacen2()
        Dim clsD As New ClsAlmacen

        Dim ds As New DataTable


        ds.Load(clsD.RecuperarAlmacenes())

    End Sub

    Private Sub DetalleOC(ByVal cod As String)
        Dim clsDeOC As New clsDetalleOC
        Dim dvOC As DataView = clsDeOC.ListarDetalleOCEntrada(cod).DefaultView
        DataGridView1.DataSource = dvOC
    End Sub


    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        DetalleOC(TextBox1.Text)
        limpiarVentana()
        sumarData()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            codigo_detalleoc = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtProducto.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            txtCodProc.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            txtLote.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value

            txtPrecioUnitario.Text = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            id_detalle_ov_v = codigo_detalleoc
            txtCantidad.Text = DataGridView1.Rows(e.RowIndex).Cells(5).Value
            txtCantidadSolicita = DataGridView1.Rows(e.RowIndex).Cells(6).Value
            If Integer.Parse(txtCantidad.Text) <= 0 Then

                Button1.Enabled = False
            Else
                Button1.Enabled = True
            End If

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If validarGuardar("de ingresar entrada almacen") = "1" Then


            Try

                If txtCantidadSolicita < Integer.Parse(txtCantidad.Text) Then
                    MsgBox("No puede ingresar una cantidad mayor a lo esperado")
                    Exit Sub
                End If
                Dim clsD As New clsDetalleOC
                Dim clsE As New clsEntradaAlmacen
                With clsD
                    .IdDetalleOC = Integer.Parse(codigo_detalleoc)
                    .Cantidad_recibida1 = Integer.Parse(txtCantidad.Text)
                End With

                With clsE
                    .IdProducto = Integer.Parse(txtCodProc.Text)
                    .Id_oc1 = Integer.Parse(TextBox1.Text)
                    .PrecioUnitario = Double.Parse(txtPrecioUnitario.Text)
                    .LoteProducto = txtLote.Text
                    .Descripcion = RichTextBox1.Text
                    .IdAlmacen = "1009"
                    .FechaVencimiento = DateTimePicker1.Value
                    .CantidadProducto = Integer.Parse(txtCantidad.Text)
                    .Id_detalle_oc1 = id_detalle_ov_v
                End With
                If clsD.ActualizarDetalleOCEntrada() = "1" Then

                    If clsE.RegistrarEntradaAlmacen() = "1" Then
                        RegistrarAcciones(nombre_usurio, Me.Name, "Registrar entrada" + txtLote.Text + txtCodProc.Text)
                        MsgBox(mensaje_registro)
                        txtCodProc.Clear()
                        txtPrecioUnitario.Clear()
                        txtProducto.Clear()
                        txtCantidad.Clear()
                        txtLote.Clear()
                        RichTextBox1.Clear()

                    End If

                    DetalleOC(TextBox1.Text)


                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
                RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            End Try
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs)
        cargarInventario()

    End Sub

    Private Sub TextBox5_TextChanged(sender As Object, e As EventArgs)
        cargarInventario()
    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Dim clsOCOB As New clsEntradaAlmacen
            Dim dvOC As DataView = clsOCOB.ListarEntradaInventarioFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date).DefaultView
            DataGridView3.DataSource = dvOC
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub CargarDGOCFecha()
        Try
            Dim clsOCOB As New clsEntradaAlmacen
            Dim dvOC As DataView = clsOCOB.ListarEntradaInventarioFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date).DefaultView
            DataGridView3.DataSource = dvOC
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs)

        'Dim clsOCOB As New clsEntradaAlmacen
        'Dim dv As DataView = clsOCOB.ListarEntradaInventarioFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date).DefaultView

        'dv.RowFilter = String.Format("CONVERT(lote+nombre_producto+id_producto, System.String) LIKE '%{0}%'", TextBox2.Text)
        'DataGridView3.DataSource = dv

    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs)
        Dim clsOCOB As New clsEntradaAlmacen
        Dim dv As DataView = clsOCOB.ListarEntradaInventarioFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date).DefaultView


        DataGridView3.DataSource = dv
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        'modificar entradas 
        Dim clsE As New clsEntradaAlmacen
        'Validar que se desea realizar la entrada
        If validarGuardar("Modificar entrada") = "1" Then

            'si la cantidad esta vacia le lanza este mensaje 
            If TextBox8.Text = "" Then
                MsgBox("Debe agregar una cantidad")
                Exit Sub
            End If
            'cargar la clase 
            Try




                With clsE
                    .Id_entrada1 = TextBox4.Text
                    .CantidadProducto = TextBox8.Text
                End With
                'Actualiza la entrada del almacen
                If clsE.ActualizarEntradaAlmacen() = "1" Then
                    MsgBox(mensaje_actualizacion)

                    'If clsE.registrarDevolucion() = "1" Then
                    '    RegistrarAcciones(nombre_usurio, Me.Name, "Devolucion de producto" + txtCodProc.Text)
                    'End If
                    'carga el datagrid del historial de entradas
                    CargarDGOCFecha()
                    'sale 
                    Exit Sub
                ElseIf clsE.ActualizarEntradaAlmacen() = "4" Then
                    MsgBox("La cantidad es mayor a la orden de compra")
                    Exit Sub
                End If

                If clsE.ActualizarEntradaAlmacen() = "2" Then
                    MsgBox("La cantidad que desea ingresar supera el limite permitido en la orden de compra")
                    Exit Sub
                End If

                If clsE.ActualizarEntradaAlmacen() = "3" Then
                    MsgBox("No se ha realizado ninguna accion, verifique los valores que esta ingresando")
                    Exit Sub
                End If
            Catch ex As Exception
                RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
                MsgBox("Faltan campos por rellenar")
            End Try
        End If
    End Sub

    Private Sub DataGridView3_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView3.CellClick
        Try
            'id_oc_modificar = DataGridView3.Rows(e.RowIndex).Cells(7).Value
            TextBox4.Text = DataGridView3.Rows(e.RowIndex).Cells(0).Value
            TextBox6.Text = DataGridView3.Rows(e.RowIndex).Cells(1).Value
            TextBox8.Text = DataGridView3.Rows(e.RowIndex).Cells(2).Value
            TextBox3.Text = DataGridView3.Rows(e.RowIndex).Cells(6).Value
            ' id_detalle_ov_v = DataGridView3.Rows(e.RowIndex).Cells(8).Value
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try


    End Sub

    Private Sub Label11_Click(sender As Object, e As EventArgs) Handles Label11.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        GridAExcel(DataGridView3)
    End Sub
    Function GridAExcel(ByVal miDataGridView As DataGridView) As Boolean
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet
        Try
            exLibro = exApp.Workbooks.Add 'crea el libro de excel 
            exHoja = exLibro.Worksheets.Add() 'cuenta filas y columnas
            Dim NCol As Integer = miDataGridView.ColumnCount
            Dim NRow As Integer = miDataGridView.RowCount
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = miDataGridView.Columns(i - 1).Name.ToString
            Next
            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = miDataGridView.Rows(Fila).Cells(Col).Value
                Next
            Next
            exHoja.Rows.Item(1).Font.Bold = 1 'titulo en negritas
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            'alineacion al centro
            exHoja.Columns.AutoFit() 'autoajuste de la columna
            exHoja.Columns.HorizontalAlignment = 2
            exApp.Application.Visible = True
            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
            Return False
        End Try
        Return True
    End Function

    Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub ToolStripTextBox1_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox1.TextChanged

        Dim clsOCOB As New clsEntradaAlmacen
        Dim dv As DataView = clsOCOB.ListarEntradaInventarioFecha(DateTimePicker2.Value.Date, DateTimePicker1.Value.Date).DefaultView

        dv.RowFilter = String.Format("CONVERT(lote+nombre_producto+id_producto, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
        DataGridView3.DataSource = dv
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs)
        'Dim clsOCOB As New clsEntradaAlmacen
        'Dim dv As DataView = clsOCOB.ListarDevolucionFecha(DateTimePicker5.Value.Date, DateTimePicker6.Value.Date).DefaultView
        'alternarColoFilasDatagridview(DataGridView2)
        'dv.RowFilter = String.Format("CONVERT(motivo+usuario+observacion, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
        'DataGridView2.DataSource = dv

    End Sub

    Private Sub ToolStripTextBox3_TextChanged(sender As Object, e As EventArgs)
        'Dim clsOCOB As New clsEntradaAlmacen
        'Dim dv As DataView = clsOCOB.ListarDevolucionFecha(DateTimePicker5.Value.Date, DateTimePicker6.Value.Date).DefaultView

        'dv.RowFilter = String.Format("CONVERT(id_oc+nombre_producto, System.String) LIKE '%{0}%'", ToolStripTextBox3.Text)
        'DataGridView2.DataSource = dv
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs)
        'GridAExcel(DataGridView2)
    End Sub
    Public Sub cargarDevoluciones()

        Dim clsOCOB As New clsEntradaAlmacen
        Dim dv As DataView = clsOCOB.ListarDevolucionesAlmacenCentral.DefaultView

        '  dv.RowFilter = String.Format("CONVERT(lote+nombre_producto+id_producto, System.String) LIKE '%{0}%'", ToolStripTextBox1.Text)
        DataGridView2.DataSource = dv
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try

            txtTrasladoid.Text = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            txtAlmacenDevuelve.Text = DataGridView2.Rows(e.RowIndex).Cells(1).Value
            txtProductoDevolucion.Text = DataGridView2.Rows(e.RowIndex).Cells(2).Value
            txtCodDevolucion.Text = DataGridView2.Rows(e.RowIndex).Cells(3).Value
            txtStockDevolucion.Text = DataGridView2.Rows(e.RowIndex).Cells(4).Value
            txtFechaEnvio.Text = DataGridView2.Rows(e.RowIndex).Cells(5).Value
            txtComentarioDevolucion.Text = DataGridView2.Rows(e.RowIndex).Cells(6).Value
            id_productoSP = DataGridView2.Rows(e.RowIndex).Cells(7).Value


        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles Button6.Click
        If validarGuardar("realizar una devolucion al almacen central ") = "1" Then


            If RegistrarDevolucion() = "1" Then
                MsgBox("Registrado exitosamente")
                cargarDevoluciones()
            End If
        End If
    End Sub

    Private Sub AsientosAlmacenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AsientosAlmacenToolStripMenuItem.Click
        E_frmAsientoAlmacen.Show()
    End Sub

    Public Function RegistrarDevolucion() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmDevolucionAlAlmacenCentral"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_producto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = id_productoSP
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_traslado" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = txtTrasladoid.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "stock" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = txtStockDevolucion.Text
        sqlcom.Parameters.Add(sqlpar)



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
End Class