﻿Public Class A_ListadoPlantillas
    Private Sub A_ListadoPlantillas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            'Consulta la subarea
            Dim codSubarea As Integer = Integer.Parse(E_HojaTrabajo.lblCodeSubArea.Text)

            'Traer plantillas de la subarea
            Dim dt As New DataTable
            Dim objPlantilla As New ClsPlantillaResultado
            dt = objPlantilla.BuscarPlantillaXSubarea(codSubarea)

            'Mostrar Data en Grid
            dgvPlantillas.DataSource = dt

            'Ocultar Columnas
            If dgvPlantillas.Columns.Contains("cod_Plantilla") = True Then

                dgvPlantillas.Columns("cod_Plantilla").Visible = False
                dgvPlantillas.Columns("codGrupoExamen").Visible = False
                dgvPlantillas.Columns("simbolo").HeaderText = "Símbolo"
                dgvPlantillas.Columns("descripcion").HeaderText = "Descrip."

            End If
            alternarColoFilasDatagridview(dgvPlantillas)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub
    Private Sub dgvPlantillas_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvPlantillas.CellMouseDoubleClick

        Try

            'Enviar descripcion de plantilla
            Dim n As String = MsgBox("¿Desea agregar la plantilla al resultado?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                HT_DescripcionResultado.rtxtDescripcionResultado.Text = HT_DescripcionResultado.rtxtDescripcionResultado.Text + System.Environment.NewLine + dgvPlantillas.Rows(e.RowIndex).Cells(2).Value

            End If

        Catch ex As Exception

        End Try

    End Sub
End Class