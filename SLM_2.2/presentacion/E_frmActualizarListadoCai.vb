﻿Imports System.Data.SqlClient

Public Class E_frmActualizarListadoCai
    Dim cai, desde, hasta As String

    Dim estado As Integer
    Public Property Cai1 As String
        Get
            Return cai
        End Get
        Set(value As String)
            cai = value
        End Set
    End Property

    Public Property Desde1 As String
        Get
            Return desde
        End Get
        Set(value As String)
            desde = value
        End Set
    End Property

    Public Property Hasta2 As String
        Get
            Return hasta
        End Get
        Set(value As String)
            hasta = value
        End Set
    End Property

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox3.Text = "" Then
            Exit Sub
            MsgBox("Debe ingresar un codigo de cai")
        End If
        Try


            cargarData()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub cargarData()
        Try

            Dim dv As DataView = SeleccionarAreaLaboratorio.DefaultView

            BindingSource1.DataSource = dv.ToTable
            DataGridView1.DataSource = BindingSource1
            BindingNavigator1.BindingSource = BindingSource1

        Catch ex As Exception

        End Try
    End Sub
    Public Function SeleccionarAreaLaboratorio() As DataTable
        Try
            Dim objCon As New ClsConnection
            Dim cn As New SqlConnection
            cn = objCon.getConexion

            Using da As New SqlDataAdapter("E_slmRetornarListradoCAI " + TextBox3.Text + "", cn)
                Dim dt As New DataTable
                da.Fill(dt)
                Return dt
            End Using


        Catch ex As Exception

        End Try
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try

            Hasta2 = TextBox1.Text
            Desde1 = TextBox2.Text
            Cai1 = TextBox4.Text
            If ComboBox1.Text = "Activo" Then
                estado = 0
            ElseIf ComboBox1.Text = "Inactivo" Then
                estado = 1
            End If

            If ActualizarCAI() + "1" Then
                MsgBox(mensaje_actualizacion)
                cargarData()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub E_frmActualizarListadoCai_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub
    Public Function ActualizarCAI() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarDetalleCAI"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigocai" 'nombre del almacen 
        sqlpar.Value = Cai1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "desde" 'nombre del almacen 
        sqlpar.Value = Desde1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "hasta" 'nombre del almacen 
        sqlpar.Value = Hasta2
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado" 'nombre del almacen 
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function


End Class