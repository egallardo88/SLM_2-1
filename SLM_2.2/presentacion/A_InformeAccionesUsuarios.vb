﻿Public Class A_InformeAccionesUsuarios
    Private Sub A_InformeAccionesUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        llenarUsuarios()

    End Sub




    Private Sub llenarUsuarios()

        Try

            Dim objSucursal As New ClsUsuario

            Dim dt As New DataTable
            dt = objSucursal.listarUsuarios
            cbxUsuarios.DataSource = dt
            cbxUsuarios.DisplayMember = "usuario"
            cbxUsuarios.ValueMember = "cod_usuario"

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try

    End Sub

    Private Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click

        Try

            Dim objReporte As New AccionesdeUsuarios

            objReporte.SetParameterValue("@usuario", Integer.Parse(cbxUsuarios.SelectedValue))
            objReporte.SetParameterValue("@accion", Trim(cbxAccion.Text))
            objReporte.SetParameterValue("@desde", dtpDesde.Value)
            objReporte.SetParameterValue("@hasta", dtpHasta.Value)
            objReporte.SetParameterValue("@numero", txtnumero.Text)

            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            crvData.ReportSource = objReporte

        Catch ex As Exception

        End Try

    End Sub

    Private Sub cbxAccion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxAccion.TextChanged

        If cbxAccion.Text = "Ver Factura" Then

            txtnumero.Enabled = True
            cbxUsuarios.Enabled = False
            dtpDesde.Enabled = False
            dtpHasta.Enabled = False

        Else
            txtnumero.Enabled = False

            cbxUsuarios.Enabled = True
            dtpDesde.Enabled = True
            dtpHasta.Enabled = True

        End If


    End Sub
End Class