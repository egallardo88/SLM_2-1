﻿Imports System.Data.SqlClient
Imports System.IO
Public Class A_ListarPromociones

    Dim promo As New ClsPromociones
    Dim objDetProm As New ClsDetallePromociones
    Private Sub A_ListarPromociones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'AGREGARLE COLOR AL DATAGRIDVIEW
        Try
            alternarColoFilasDatagridview(dtPromos)

            CargarData()

            'OCULTAR COLUMNAS
            'dtPromos.Columns("contador").Visible = False
            'dtPromos.Columns("precio").Visible = False
            'dtPromos.Columns("imagen").Visible = False
            'dtPromos.Columns("descripcion").Width = 230

            'CAMBIAR NOMBRE COLUMNAS
            dtPromos.Columns("codigo").HeaderText = "Código"
            dtPromos.Columns("descripcion").HeaderText = "Descripción"
            dtPromos.Columns("fechaInicio").HeaderText = "Fecha Inicial"
            dtPromos.Columns("fechaFinal").HeaderText = "Fecha Final"
            dtPromos.Columns("precio").HeaderText = "Precio"
            dtPromos.Columns("publicidad").HeaderText = "Publicidad"
        Catch ex As Exception

        End Try

    End Sub

    Sub CargarData()
        'LLENAR EL DATA GRID VIEW 
        dtPromos.DataSource = promo.ListarPromociones
    End Sub

    Private Sub dtPromos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtPromos.CellDoubleClick
        'SELECCIONAR DEL DATA GRID VIEW
        Try
            Dim n As String = ""
            Dim dt As DataTable
            Dim rows As DataRow
            If e.RowIndex >= 0 Then
                n = MsgBox("¿Desea ver la promoción?", MsgBoxStyle.YesNo)

                If n = vbYes Then
                    'LIMPIA Y LLENA LA INFORMACION
                    A_Promociones.limpiar()
                    promo.codigo_ = Integer.Parse(dtPromos.Rows(e.RowIndex).Cells(0).Value)
                    A_Promociones.txtCod.Text = dtPromos.Rows(e.RowIndex).Cells(0).Value
                    A_Promociones.txtDescrip.Text = dtPromos.Rows(e.RowIndex).Cells(1).Value
                    A_Promociones.dtpFechaI.Text = dtPromos.Rows(e.RowIndex).Cells(2).Value
                    A_Promociones.dtpFechaF.Text = dtPromos.Rows(e.RowIndex).Cells(3).Value
                    A_Promociones.txtPrecio.Text = dtPromos.Rows(e.RowIndex).Cells(4).Value
                    A_Promociones.chkPromo.Checked = dtPromos.Rows(e.RowIndex).Cells(5).Value

                    'CARGA LA IMAGEN
                    dt = promo.BuscarPromocion()
                    If dt.Rows.Count >= 1 Then
                        rows = dt.Rows(0)
                        'A_Promociones.pbxPromo.Load(rows("imagen"))
                        Dim datos As Byte() = rows("imagen")
                        Dim ms = New MemoryStream(datos)
                        A_Promociones.pbxPromo.Image = Image.FromStream(ms)
                    End If

                    If A_Promociones.chkPromo.Checked = False Then
                        'ES UNA PROMOCION 
                        objDetProm.codigoPromocion_ = Integer.Parse(dtPromos.Rows(e.RowIndex).Cells(0).Value)
                        dt = objDetProm.SeleccionarDetallePromocion()
                        For index As Integer = 0 To dt.Rows.Count - 1
                            'LLENADO DETALLE PROMOCIONES
                            rows = dt.Rows(index)
                            A_Promociones.dtDetallePromo.Rows.Add(New String() {CStr(rows("codigoExamen")), CStr(rows("codInterno")), CStr(rows("descripcion")), CStr(rows("precio")), CStr(rows("descuento")), CStr(rows("precioPromo"))})
                        Next
                    End If

                    'HABILITA LOS BOTONES
                    A_Promociones.btnCrear.Enabled = True
                    A_Promociones.btnModificar.Enabled = True
                    A_Promociones.btnGuardar.Enabled = False
                    MostrarForm(A_Promociones)
                    'Me.Close()
                End If
            End If

            'Dim dt As DataTable
            'Dim rows As DataRow

            'dt = dtPromos.DataSource
            'rows = dt.Rows(e.RowIndex)



            'With A_Promociones

            '    .txtCod.Text = rows("codigo")
            '    .txtDescrip.Text = rows("descripcion")
            '    .txtPrecio.Text = rows("precio")
            '    .dtpFechaI.Value = rows("fechaInicio")
            '    .dtpFechaF.Value = rows("fechaFinal")
            '    '.txtRuta.Text = rows("imagen")
            '    .pbxPromo.Load(rows("imagen"))

            '    Dim objDetProm As New ClsDetallePromociones
            '    objDetProm.codigoPromocion_ = rows("codigo")
            '    dt = objDetProm.SeleccionarDetallePromocion()
            '    For index As Integer = 0 To dt.Rows.Count - 1
            '        rows = dt.Rows(index)
            '        .dtDetallePromo.Rows.Add(New String() {CStr(rows("codigoExamen")), CStr(rows("descripcion"))})
            '    Next

            '    .Show()

            'End With

            'With A_Promociones

            '    .btnCrear.Enabled = True
            '    .btnModificar.Enabled = True
            '    .btnGuardar.Enabled = False

            'End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try


    End Sub

    Private Sub A_ListarPromociones_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        'Presionar ESC para cerrar ventana
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnNuevaPromo.Click
        Try
            'NUEVA PROMOCION
            'Me.Close()
            With A_Promociones
                .btnGuardar.Enabled = True
                .btnCrear.Enabled = True
                .btnModificar.Enabled = False
            End With
            MostrarForm(A_Promociones)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged
        Try
            'BUSQUEDA DESCRIPCION PROMOCION
            If Trim(txtDescripcion.Text) <> "" Then
                Dim dv As DataView = dtPromos.DataSource
                dv.RowFilter = String.Format("descripcion Like '%{0}%'", txtDescripcion.Text)
                If dv.Count = "0" Then
                    'VALIDACION 
                    MsgBox("No existe la promoción.", MsgBoxStyle.Exclamation)
                    txtDescripcion.Text = ""
                    dtPromos.DataSource = promo.ListarPromociones
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class