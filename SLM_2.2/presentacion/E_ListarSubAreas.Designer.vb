﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class E_ListarSubAreas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(E_ListarSubAreas))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblform = New System.Windows.Forms.Label()
        Me.dgbSubAreas = New System.Windows.Forms.DataGridView()
        Me.btnCrear = New System.Windows.Forms.Button()
        Me.txtSubarea = New System.Windows.Forms.TextBox()
        Me.Examen = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgbSubAreas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtSubarea)
        Me.GroupBox1.Controls.Add(Me.Examen)
        Me.GroupBox1.Controls.Add(Me.lblform)
        Me.GroupBox1.Controls.Add(Me.dgbSubAreas)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(459, 233)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SubAreas"
        '
        'lblform
        '
        Me.lblform.AutoSize = True
        Me.lblform.Location = New System.Drawing.Point(195, 0)
        Me.lblform.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblform.Name = "lblform"
        Me.lblform.Size = New System.Drawing.Size(39, 13)
        Me.lblform.TabIndex = 1
        Me.lblform.Text = "Label1"
        Me.lblform.Visible = False
        '
        'dgbSubAreas
        '
        Me.dgbSubAreas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgbSubAreas.BackgroundColor = System.Drawing.Color.White
        Me.dgbSubAreas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgbSubAreas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgbSubAreas.GridColor = System.Drawing.Color.White
        Me.dgbSubAreas.Location = New System.Drawing.Point(6, 49)
        Me.dgbSubAreas.MultiSelect = False
        Me.dgbSubAreas.Name = "dgbSubAreas"
        Me.dgbSubAreas.RowHeadersWidth = 51
        Me.dgbSubAreas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgbSubAreas.Size = New System.Drawing.Size(447, 178)
        Me.dgbSubAreas.TabIndex = 0
        '
        'btnCrear
        '
        Me.btnCrear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCrear.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnCrear.FlatAppearance.BorderSize = 0
        Me.btnCrear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCrear.ForeColor = System.Drawing.Color.White
        Me.btnCrear.Location = New System.Drawing.Point(396, 249)
        Me.btnCrear.Name = "btnCrear"
        Me.btnCrear.Size = New System.Drawing.Size(75, 23)
        Me.btnCrear.TabIndex = 1
        Me.btnCrear.Text = "Crear Nueva"
        Me.btnCrear.UseVisualStyleBackColor = False
        '
        'txtSubarea
        '
        Me.txtSubarea.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtSubarea.Location = New System.Drawing.Point(161, 18)
        Me.txtSubarea.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSubarea.MaxLength = 100
        Me.txtSubarea.Name = "txtSubarea"
        Me.txtSubarea.Size = New System.Drawing.Size(145, 20)
        Me.txtSubarea.TabIndex = 122
        '
        'Examen
        '
        Me.Examen.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Examen.AutoSize = True
        Me.Examen.Location = New System.Drawing.Point(108, 18)
        Me.Examen.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Examen.Name = "Examen"
        Me.Examen.Size = New System.Drawing.Size(47, 13)
        Me.Examen.TabIndex = 121
        Me.Examen.Text = "Subarea"
        '
        'E_ListarSubAreas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(483, 277)
        Me.Controls.Add(Me.btnCrear)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "E_ListarSubAreas"
        Me.Text = "Listado de SubAreas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgbSubAreas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgbSubAreas As DataGridView
    Friend WithEvents btnCrear As Button
    Friend WithEvents lblform As Label
    Friend WithEvents txtSubarea As TextBox
    Friend WithEvents Examen As Label
End Class
