﻿Imports System.Data.SqlClient

Public Class E_frmListadoFragilidadOsmotica
    Private Sub E_frmListadoFragilidadOsmotica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarData()
        MenuStrip_slm(MenuStrip1)
        ColoresForm(Panel1, StatusStrip1)
    End Sub
    Public Function listarExamenesParametro() As DataTable
        Dim estadoP As String
        If RadioButton4.Checked = True Then
            estadoP = "Validado"
        ElseIf RadioButton3.Checked = True Then
            estadoP = "Entregado"
        ElseIf RadioButton2.Checked = True Then
            estadoP = "Procesado"
        ElseIf RadioButton1.Checked = True Then
            estadoP = "Pendiente"
        End If

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select cl.nombreCompleto, o.cod_factura ,od.cod_orden_trabajo,i.codItemExa,i.descripcion,o.estado,o.pmFecha
from   OrdenDeTrabajo o,OrdenTrabajoDetalle od,ItemExamenDetalle id, Item_Examenes i,Cliente cl,Factura f

where od.cod_item_examen_detalle = id.codigo and i.codItemExa = id.codigoItemExamen and o.cod_orden_trabajo = od.cod_orden_trabajo
and cl.codigo= f.codigoCliente
and f.numero = o.cod_factura
and i.descripcion like  '%electroforesis%' 
and o.estado like  '%" + estadoP + "%'
and o.pmFecha between convert(datetime,'" + DateTimePicker1.Value.ToString("dd/MM/yyyy hh:mm:ss") + "',105) and convert(datetime, '" + DateTimePicker2.Value.ToString("dd/MM/yyyy hh:mm:ss") + "',105)", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Function listarExamenesParametros() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select cl.nombreCompleto,o.cod_factura ,od.cod_orden_trabajo,i.codItemExa,i.descripcion,o.estado,o.pmFecha
from   OrdenDeTrabajo o,OrdenTrabajoDetalle od,ItemExamenDetalle id, Item_Examenes i,Cliente cl,Factura f
where od.cod_item_examen_detalle = id.codigo and i.codItemExa = id.codigoItemExamen and o.cod_orden_trabajo = od.cod_orden_trabajo
and cl.codigo= f.codigoCliente
and f.numero = o.cod_factura
and i.codItemExa ='11059' 

and o.estado like  '" + "son ricoja'", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Function listarExamenes() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select cl.nombreCompleto,o.cod_factura ,od.cod_orden_trabajo,i.codItemExa,i.descripcion,o.estado,o.pmFecha
from   OrdenDeTrabajo o,OrdenTrabajoDetalle od,ItemExamenDetalle id, Item_Examenes i,Cliente cl,Factura f
where od.cod_item_examen_detalle = id.codigo and i.codItemExa = id.codigoItemExamen and o.cod_orden_trabajo = od.cod_orden_trabajo
and cl.codigo= f.codigoCliente
and f.numero = o.cod_factura
and i.codItemExa ='11059' 
and o.estado <> 'Entregado'
 and CAST( f.fechaFactura as date) = CAST( GETDATE() as date)", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Sub cargarData()
        Try


            BindingSource1.DataSource = listarExamenes()

            dgbtabla.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar. " + ex.Message)
        End Try
    End Sub

    Private Sub dgbtabla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgbtabla.CellDoubleClick
        Try
            id_facturatrabajo_grafico = dgbtabla.Rows(e.RowIndex).Cells(1).Value
            id_ordentrabajo_grafico = dgbtabla.Rows(e.RowIndex).Cells(2).Value
            nombre_examen_paragrafico = dgbtabla.Rows(e.RowIndex).Cells(4).Value
            estado_paragrafico_osmosis = dgbtabla.Rows(e.RowIndex).Cells(5).Value

            E_frmGraficaEstatica.Show()
            Me.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub E_frmListadoFragilidadOsmotica_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub RealizarBusquedaInteligenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RealizarBusquedaInteligenteToolStripMenuItem.Click
        Try

            BindingSource1.DataSource = listarExamenesParametro()

            dgbtabla.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar. " + ex.Message)
        End Try
    End Sub

    Private Sub txtnumeroB_TextChanged(sender As Object, e As EventArgs) Handles txtnumeroB.TextChanged
        If RadioButton1.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(cod_factura, System.String) LIKE '%{0}%'", txtnumeroB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton4.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(cod_factura, System.String) LIKE '%{0}%'", txtnumeroB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton3.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(cod_factura, System.String) LIKE '%{0}%'", txtnumeroB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton2.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(cod_factura, System.String) LIKE '%{0}%'", txtnumeroB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        Else
            Try
                BindingSource1.DataSource = listarExamenes()
                BindingSource1.Filter = String.Format("CONVERT(cod_factura, System.String) LIKE '%{0}%'", txtnumeroB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub txtnombreB_TextChanged(sender As Object, e As EventArgs) Handles txtnombreB.TextChanged
        Dim estadoP As String
        If RadioButton1.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(nombreCompleto, System.String) LIKE '%{0}%'", txtnombreB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton4.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(nombreCompleto, System.String) LIKE '%{0}%'", txtnombreB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton3.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(nombreCompleto, System.String) LIKE '%{0}%'", txtnombreB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        ElseIf RadioButton2.Checked = True Then
            Try
                BindingSource1.DataSource = listarExamenesParametro()
                BindingSource1.Filter = String.Format("CONVERT(nombreCompleto, System.String) LIKE '%{0}%'", txtnombreB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        Else
            Try
                BindingSource1.DataSource = listarExamenes()
                BindingSource1.Filter = String.Format("CONVERT(nombreCompleto, System.String) LIKE '%{0}%'", txtnombreB.Text)
                dgbtabla.DataSource = BindingSource1
                ' BindingNavigator1.BindingSource = BindingSource1

            Catch ex As Exception

            End Try
        End If
    End Sub
End Class