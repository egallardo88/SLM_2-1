﻿Public Class E_frmBitacoraVentana
    Private Sub E_frmBitacoraVentana_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(DataGridView1)
        ColoresForm(Panel1, StatusStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub
    Public Sub cargarData()
        Try
            Dim clsDeOC As New clsLogs
            Dim dvOC As DataView = clsDeOC.CargarLogsVentanas(DateTimePicker1.Value, DateTimePicker2.Value).DefaultView
            BindingSource1.DataSource = dvOC.ToTable
            DataGridView1.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        cargarData()
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Dim objOrd As New clsLogs


        Dim dv As DataView = objOrd.CargarLogsVentanas(DateTimePicker1.Value, DateTimePicker2.Value).DefaultView

        dv.RowFilter = String.Format("CONVERT(usuario +  + ventana_slm, System.String) LIKE '%{0}%'", TextBox1.Text)

        DataGridView1.DataSource = dv
    End Sub
End Class