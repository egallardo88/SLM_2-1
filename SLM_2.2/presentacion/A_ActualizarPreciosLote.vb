﻿Public Class A_ActualizarPreciosLote
    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvExportar)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ImportarExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImportarExcelToolStripMenuItem.Click
        Try
            ImportarPlanilla.importarExcel2(dgvImportar)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenarListasPrecios()

        Try

            Dim objSucursal As New ClsListaPrecios

            Dim dt As New DataTable
            dt = objSucursal.SeleccionarListaPrecios
            cbxListaPrecio.DataSource = dt
            cbxListaPrecio.DisplayMember = "codigoBreve"
            cbxListaPrecio.ValueMember = "codigo"

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try



    End Sub


    Private Sub A_ActualizarPreciosLote_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            llenarListasPrecios()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbxListaPrecio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxListaPrecio.SelectedIndexChanged
        Try

            Dim precio As New ClsListaPrecios
            Dim dt As New DataTable
            Dim row As DataRow

            precio.codigo_ = Integer.Parse(cbxListaPrecio.SelectedValue)

            dt = precio.BuscarListaPreciosCodigo()
            row = dt.Rows(0)

            TextBox1.Text = row("descripcion")

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try

            'Consultar detalle de lista seleccionada
            Dim detalle As New ClsPrecio
            Dim dt As New DataTable

            detalle.codigoListaPrecios_ = Integer.Parse(cbxListaPrecio.SelectedValue)

            dt = detalle.MostrarPreciosLote()

            dgvExportar.DataSource = dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ActualizarPreciosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ActualizarPreciosToolStripMenuItem.Click
        Try

            Dim precio As New ClsPrecio

            For i = 0 To dgvImportar.Rows.Count - 2

                With precio

                    .codigo_ = Integer.Parse(dgvImportar.Rows(i).Cells(0).Value)
                    .precio_ = Double.Parse(dgvImportar.Rows(i).Cells(3).Value)
                    .ModificarLotePrecio()

                End With

            Next

            MsgBox("Se actualizaron los precios de " + TextBox1.Text)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class