﻿Imports System.Data.SqlClient
Imports System.IO

Public Class E_frmCurvaElectro
    Dim sCommand As SqlCommand
    Dim sAdapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Public respuesta As SqlDataReader
    Dim sDs As DataSet
    Public enunciado As SqlCommand
    Dim sTable As DataTable
    Private Sub CargarData()
        Dim clsc As New ClsConnection
        'Dim connectionString As String = "Data Source=.;Initial Catalog=pubs;Integrated Security=True"
        Dim sql As String = "SELECT * FROM detalleElectro  where id_electro='" + TextBox1.Text + "'"
        Dim connection As New SqlConnection(clsc.str_con)
        connection.Open()
        sCommand = New SqlCommand(sql, connection)
        sAdapter = New SqlDataAdapter(sCommand)
        sBuilder = New SqlCommandBuilder(sAdapter)
        sDs = New DataSet()
        sAdapter.Fill(sDs, "detalleElectro$")
        sTable = sDs.Tables("detalleElectro$")
        connection.Close()
        DataGridView1.DataSource = sDs.Tables("detalleElectro$")
        DataGridView1.ReadOnly = False
        'save_btn.Enabled = True
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect

    End Sub
    Private Sub E_frmCurvaElectro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel7, StatusStrip1)
        ' MenuStrip_slm(MenuStrip1)
        TextBox1.Text = id_ordentrabajo_grafico
        TextBox10.Text = id_facturatrabajo_grafico
        TextBox7.Text = nombre_examen_paragrafico
        TextBox11.Text = fecha_factura_electro
        cargarDatosPaciente()
        cargarDatosOrden()
        cargarDatosOrdenValidador()

        If RegistrarOrden() = "2" Then

        ElseIf RegistrarOrden() = "1" Then


        End If

        TextBox3.Text = estado_paragrafico_electro

        recuperarDatosExamen()
        If estado_paragrafico_electro = "Procesado" Then
            RadioButton1.Checked = True
        End If

        If estado_paragrafico_electro = "Validado" Then
            RadioButton2.Checked = True
        End If

        Try
            CargarData()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDatosOrden()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_tecnico = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox9.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDatosOrdenValidador()
        Dim cnn3 As New SqlConnection
        Dim cmd3 As New SqlCommand
        Dim clsC As New ClsConnection
        cnn3.ConnectionString = clsC.str_con
        cmd3.Connection = cnn3
        Try


            enunciado = New SqlCommand("select o.cod_orden_trabajo,u.usuario
from OrdenDeTrabajo o, Usuario u
where  o.cod_validador = u.cod_usuario 
and o.cod_orden_trabajo='" + id_ordentrabajo_grafico + "'", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()

            While respuesta.Read


                TextBox8.Text = respuesta.Item("usuario")


            End While
            respuesta.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub cargarDatosPaciente()
        Dim clsC As New ClsConnection
        Try
            enunciado = New SqlCommand("select distinct c.nombreCompleto,c.fechaNacimiento,c.genero,s.nombre from  factura f , OrdenDeTrabajo o,OrdenTrabajoDetalle od, Cliente c,Sucursal s
where  o.cod_orden_trabajo = od.cod_orden_trabajo and f.numero = o.cod_factura and s.codigo = f.codigoSucursal
and c.codigo = f.codigoCliente and f.numero ='" + id_facturatrabajo_grafico + "'

", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()
            While respuesta.Read
                TextBox2.Text = respuesta.Item("nombreCompleto")
                TextBox5.Text = respuesta.Item("genero")
                TextBox6.Text = DateDiff("yyyy", CDate(respuesta.Item("fechaNacimiento")), Date.Now)
                TextBox4.Text = respuesta.Item("nombre")
            End While
            respuesta.Close()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim result As New DialogResult
            OpenFileDialog1.InitialDirectory = “C:\”
            OpenFileDialog1.Filter = “archivos de imagen (*.jpg)|*.png|All files (*.*)|*.*”
            OpenFileDialog1.FilterIndex = 3
            OpenFileDialog1.RestoreDirectory = True
            result = OpenFileDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                PictureBox1.Image = Image.FromFile(OpenFileDialog1.FileName)

            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub recuperarDatosExamen()
        Dim clsC As New ClsConnection
        Try




            enunciado = New SqlCommand("select * from ElectroForesis
where cod_ordentrabajo ='" + id_ordentrabajo_grafico + "'

", clsC.getConexion)
            respuesta = enunciado.ExecuteReader()
            While respuesta.Read
                RichTextBox1.Text = respuesta.Item("conclusion")
                RichTextBox2.Text = respuesta.Item("comentario")
                Dim datos As Byte() = respuesta.Item("imagen")
                Dim ms = New MemoryStream(datos)
                PictureBox1.Image = Image.FromStream(ms)

            End While
            respuesta.Close()

        Catch ex As Exception
            'MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Try
            RegistrarProducto()
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                ' MsgBox("Se actualizo el estado de la orden")
                TextBox9.Text = nombre_usurio
            End If
        Catch ex As Exception

        End Try
        Try
            ' RegistrarAcciones(nombre_usurio, Me.Name, "Guardo examen osmosis id orden" + TextBox1.Text)
            sAdapter.Update(sTable)
            DataGridView1.[ReadOnly] = False
            'save_btn.Enabled = True
            ' CargarData()
            'cargarGrafico()
            MsgBox("Datos guardados")
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Public Function RegistrarProducto() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer
        If (PictureBox1.Image Is Nothing) Then
            MessageBox.Show("Debe agregar una imagen")
            Exit Function
        Else

        End If
        Try
            Dim ms As New System.IO.MemoryStream()
            PictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)



            sqlcom = New SqlCommand
            sqlcom.CommandType = CommandType.StoredProcedure
            sqlcom.CommandText = "E_slmCrearElectro"

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "conclusion"
            sqlpar.Value = RichTextBox1.Text
            sqlcom.Parameters.Add(sqlpar)

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "comentario"
            sqlpar.Value = RichTextBox2.Text
            sqlcom.Parameters.Add(sqlpar)

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "cod_ordentrabajo"
            sqlpar.Value = TextBox1.Text
            sqlcom.Parameters.Add(sqlpar)

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "factura"
            sqlpar.Value = TextBox10.Text
            sqlcom.Parameters.Add(sqlpar)


            sqlpar = New SqlParameter
            sqlpar.ParameterName = "cod_validador"
            sqlpar.Value = codigo_usuario
            sqlcom.Parameters.Add(sqlpar)

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "imagen"
            sqlpar.Value = ms.GetBuffer()
            sqlcom.Parameters.Add(sqlpar)

            sqlpar = New SqlParameter
            sqlpar.ParameterName = "salida"
            sqlpar.Value = ""
            sqlcom.Parameters.Add(sqlpar)

            sqlpar.Direction = ParameterDirection.Output

            Dim con As New ClsConnection
            sqlcom.Connection = con.getConexion

            sqlcom.ExecuteNonQuery()

            con.cerrarConexion()

            par_sal = sqlcom.Parameters("salida").Value

            Return par_sal
        Catch ex As Exception
            Exit Function
            MsgBox("Debe agregar una imagen")
        End Try
    End Function


    Public Function RegistrarOrden() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer


        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "PlantillaElectroForesis"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_ordentrabajo"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function CambiarEstadoOrdendetrabajo(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficas"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function CambiarEstadoOrdendetrabajoValidaro(ByVal estado As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCambiarEstadoOrdenesGraficasValidador"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_ordentrabajo"
        sqlpar.Value = id_ordentrabajo_grafico
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = estado
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_empleado"
        sqlpar.Value = codigo_usuario
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub Validar_Click(sender As Object, e As EventArgs) Handles Validar.Click
        If RadioButton1.Checked = True Then
            'IF PARA ACTUALIZAR QUIEN FUE EL QUE PROCESO
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                MsgBox("Se actualizo el estado de la orden")
                TextBox9.Text = nombre_usurio
            End If
        ElseIf RadioButton2.Checked = True Then
            'IF PARA VALIDAR QUIEN FUE EL QUE VALIDO
            If CambiarEstadoOrdendetrabajoValidaro("Validado") = "1" Then
                TextBox8.Text = nombre_usurio
                MsgBox("Se actualizo el estado de la orden")
                Try
                    BackgroundWorker1.RunWorkerAsync()
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub

    Private Sub ImprimirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirToolStripMenuItem.Click

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim clsr As New ClsResultados
        ' E_frmInsulinaIndividual.Show()
        clsr.GraficaElectroEnviarCorreo(TextBox10.Text)
    End Sub

    Private Sub GuardarHojaDeTrabajoToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ImprimirToolStripMenuItem1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub E_frmCurvaElectro_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        E_frmListadoElectro.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            RegistrarProducto()
            If CambiarEstadoOrdendetrabajo("Procesado") = "1" Then
                ' MsgBox("Se actualizo el estado de la orden")
                TextBox9.Text = nombre_usurio
            End If
        Catch ex As Exception

        End Try
        Try
            ' RegistrarAcciones(nombre_usurio, Me.Name, "Guardo examen osmosis id orden" + TextBox1.Text)
            sAdapter.Update(sTable)
            DataGridView1.[ReadOnly] = False
            'save_btn.Enabled = True
            ' CargarData()
            'cargarGrafico()
            MsgBox("Datos guardados")
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        E_frmReporteElectro.Show()
    End Sub
End Class