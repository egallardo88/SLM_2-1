﻿Public Class A_IngresosDetallados
    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()

    End Sub

    Private Sub llenarSucursales()

        Try

            Dim objSucursal As New ClsSucursal

            Dim dt As New DataTable
            dt = objSucursal.SeleccionarSucursal
            cbxSucursal.DataSource = dt
            cbxSucursal.DisplayMember = "nombre"
            cbxSucursal.ValueMember = "codigo"

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try



    End Sub

    Private Sub A_IngresosDetallados_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            RegistrarVentanas(nombre_usurio, Me.Name)
            llenarSucursales()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarToolStripMenuItem.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Consulto los informes")
            Dim fechadesde, fechaHasta As Date
            Dim codigo As String

            fechadesde = dtpDesde.Value
            fechaHasta = dtpHasta.Value

            Dim dt As New DataTable

            If chkTodas.Checked = True Then

                dt = Ingresos_Detallados(fechadesde, fechaHasta, codigo)
                dgvData.DataSource = dt

            Else

                codigo = cbxSucursal.SelectedValue
                dt = Ingresos_Detallados(fechadesde, fechaHasta, codigo)
                dgvData.DataSource = dt


            End If

            alternarColoFilasDatagridview(dgvData)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvData)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkTodas_CheckedChanged(sender As Object, e As EventArgs) Handles chkTodas.CheckedChanged
        Try

            If chkTodas.Checked = True Then
                cbxSucursal.Enabled = False
            Else
                cbxSucursal.Enabled = True
            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub UsoNumeracionLegalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsoNumeracionLegalToolStripMenuItem.Click
        MostrarForm(A_ConsultaNumeroLegal)
    End Sub

    Private Sub dgvData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvData.CellContentClick

    End Sub

    Private Sub dgvData_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvData.CellDoubleClick
        Try
            'MOSTRAR DATOS DE LA FACTURA
            Dim objFact As New ClsFactura
            Dim n As String = ""
            'If lblForm.Text = "M_DiarioFacturacion" Then
            If e.RowIndex >= 0 Then
                    n = MsgBox("¿Desea ver la factura?", MsgBoxStyle.YesNo, "Validación")
                End If
                If n = vbYes Then
                    M_Factura.limpiar()
                'Dim objFact As New ClsFactura
                objFact.numero_ = dgvData.Rows(e.RowIndex).Cells(0).Value()

                Dim dt As New DataTable
                    dt = objFact.BuscarFacturaNumero()
                    Dim row As DataRow = dt.Rows(0)

                    M_Factura.txtnumeroFactura.Text = CStr(row("numero"))
                    M_Factura.txtnumeroOficial.Text = CStr(row("numeroOficial"))
                    M_Factura.dtpfechaFactura.Value = CStr(row("fechaFactura"))
                    M_Factura.txtcodigoCliente.Text = CStr(row("codigoCliente"))
                    M_Factura.txtcodigoRecepecionista.Text = CStr(row("codigoRecepcionista"))
                    M_Factura.txtcodigoMedico.Text = CStr(row("codigoMedico"))
                    M_Factura.txtcodigoCajero.Text = CStr(row("codigoCajero"))
                    M_Factura.lblcodeTerminoPago.Text = CStr(row("codigoTerminoPago"))
                    M_Factura.txtcodigoSede.Text = CStr(row("codigoSede"))
                    M_Factura.dtpfechaVto.Value = CStr(row("fechaVto"))
                    M_Factura.lblcodeSucursal.Text = CStr(row("codigoSucursal"))

                    'M_Factura.lblcodePriceList.Text = CStr(row("codigoConvenio"))
                    'M_Factura.txtcodigoConvenio.Text = CStr(row("codigoConvenio"))

                    M_Factura.txtnumeroPoliza.Text = CStr(row("numeroPoliza"))
                    M_Factura.txtcodigoTerminal.Text = CStr(row("codigoTerminal"))
                    M_Factura.lblcodeSucursal.Text = CStr(row("codigoSucursal"))
                    M_Factura.cbxentregarMedico.Checked = CStr(row("entregaMedico"))
                    M_Factura.cbxentregarPaciente.Checked = CStr(row("entregaPaciente"))
                    M_Factura.cbxenviarCorreo.Checked = CStr(row("enviarEmail"))
                    M_Factura.txtpagoPaciente.Text = CStr(row("pagoPaciente"))
                    M_Factura.txtEfectivo.Text = CStr(row("ingresoEfectivo"))
                    M_Factura.txtTarjeta.Text = CStr(row("ingresoTarjeta"))
                    M_Factura.txtvuelto.Text = CStr(row("vuelto"))
                    M_Factura.txttotal.Text = CStr(row("total"))
                    M_Factura.cbxok.Checked = CStr(row("ok"))
                    M_Factura.cbxAnular.Checked = CStr(row("estado"))

                    Dim objDetFact As New ClsDetalleFactura
                objDetFact.numeroFactura_ = dgvData.Rows(e.RowIndex).Cells(0).Value()
                dt = objDetFact.BuscarDetalleFacturaIngresada()
                    For index As Integer = 0 To dt.Rows.Count - 1
                        row = dt.Rows(index)
                        M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("subtotal")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal")), CStr(row("subArea")), CStr(row("numero")), CStr(row("codigoExamen")), CStr(row("id_centrocosto"))})
                        M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("subtotal")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal"))})

                        'OBSERVACIONES
                        M_Factura.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("observaciones"))})
                        M_Factura.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("observaciones2"))})
                    Next

                    M_Factura.deshabilitar()
                    If (M_Factura.cbxok.Checked = "0") Then
                        M_Factura.HabilitarActualizarFactura()
                    Else
                        M_Factura.btnActualizar.Enabled = True
                    End If
                'Me.Close()
                'txtnombreB.Text = ""
                'txtnumeroB.Text = ""
                'Me.Hide()
                M_Factura.banderaTipo = True
                M_Factura.GenerarAsientoToolStripMenuItem.Visible = True
                MostrarForm(M_Factura)
                End If
            ' End If
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ImprimirReporteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirReporteToolStripMenuItem.Click
        Try

            Dim objReporte As New CrystalReport2

            objReporte.SetParameterValue("@fechaInicial", dtpDesde.Value)
            objReporte.SetParameterValue("@fechaFinal", dtpHasta.Value)
            objReporte.SetParameterValue("@codigoSucursal", Integer.Parse(cbxSucursal.SelectedValue))

            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            M_ImpresionFacturaEmpresarial.CrystalReportViewer2.ReportSource = objReporte
            M_ImpresionFacturaEmpresarial.Show()


        Catch ex As Exception

        End Try
    End Sub
End Class