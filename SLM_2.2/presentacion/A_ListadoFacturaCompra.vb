﻿Public Class A_ListadoFacturaCompra

    Dim FacCompra As New ClsFacturaCompra
    Public Sub A_ListadoFacturaCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            ColoresForm(Panel2, StatusStrip1)

            CargarData()
            MenuStrip_slm(MenuStrip1)

        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargarData()

        dtFacturasCompra.DataSource = FacCompra.listarFacturaCompra
        alternarColoFilasDatagridview(dtFacturasCompra)

        If dtFacturasCompra.Columns.Contains("codFactura") = True Then

            dtFacturasCompra.Columns("codProveedor").Visible = False
            dtFacturasCompra.Columns("moneda").Visible = False
            dtFacturasCompra.Columns("codTerminoPago").Visible = False
            dtFacturasCompra.Columns("fechaTransaccion").Visible = False
            dtFacturasCompra.Columns("cai").Visible = False
            dtFacturasCompra.Columns("ok").Visible = False
            dtFacturasCompra.Columns("anular").Visible = False

            dtFacturasCompra.Columns("fechaFactura").HeaderText = "Fecha de Factura"
            dtFacturasCompra.Columns("fechaVencimiento").HeaderText = "Fecha de Venci."
            dtFacturasCompra.Columns("descripcion").HeaderText = "Descripción"
            dtFacturasCompra.Columns("codFactura").HeaderText = "Cod. Factura"

        End If
    End Sub


    Private Sub dtFacturasCompra_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtFacturasCompra.CellDoubleClick

        Try

            Dim dt As New DataTable

            dt = dtFacturasCompra.DataSource
            Dim row As DataRow = dt.Rows(e.RowIndex)

            A_FacturaCompras.txtCodFactura.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(0).Value
            A_FacturaCompras.txtCodProveedor.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(1).Value
            'A_FacturaCompras.txtNombreProveedor.Text = row("codFactura")
            A_FacturaCompras.txtDescripcion.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(8).Value
            A_FacturaCompras.txtTotal.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(2).Value
            A_FacturaCompras.txtMoneda.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(3).Value
            A_FacturaCompras.dtpFechaFactura.Value = dtFacturasCompra.Rows(e.RowIndex).Cells(4).Value
            A_FacturaCompras.lblCodTerminoPago.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(10).Value

            A_FacturaCompras.dtpTransaccion.Value = dtFacturasCompra.Rows(e.RowIndex).Cells(5).Value
            A_FacturaCompras.dtpVencimiento.Value = dtFacturasCompra.Rows(e.RowIndex).Cells(6).Value

            A_FacturaCompras.txtNroFactura.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(7).Value
            A_FacturaCompras.lblTotal.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(2).Value
            A_FacturaCompras.lblEstado.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(9).Value
            A_FacturaCompras.lblSaldoPend.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(11).Value

            A_FacturaCompras.txtCAI.Text = dtFacturasCompra.Rows(e.RowIndex).Cells(13).Value

            A_FacturaCompras.chkOk.Checked = dtFacturasCompra.Rows(e.RowIndex).Cells(14).Value
            A_FacturaCompras.chkAnular.Checked = dtFacturasCompra.Rows(e.RowIndex).Cells(15).Value



            'Mostrar detalle de factura
            Dim DetalleFac As New ClsDetalleFacturaCompra
            Dim dtFac As New DataTable

            'consulta centro de costo
            Dim ccosto As New ClsCentroCostos
            Dim dtcc As New DataTable
            Dim rowcc As DataRow

            Dim sucursal As New ClsSucursal
            Dim dtsu As New DataTable
            Dim rowsu As DataRow

            DetalleFac.Cod_Factura = row("codFactura")
            'MsgBox("codigo " + row("codFactura").ToString)
            dtFac = DetalleFac.listarDetallesFacturaCompra()

            For index As Integer = 0 To dtFac.Rows.Count - 1
                row = dtFac.Rows(index)

                'If row("area") <> "" Then

                '    ccosto.ID = Integer.Parse(row("area"))
                '    dtcc = ccosto.BuscarCentroCostoCodigo
                '    rowcc = dtcc.Rows(0)
                'End If

                'If row("sede") <> "" Then
                '    sucursal.codigo_ = Integer.Parse(row("sede"))
                '    dtsu = sucursal.BuscarSucursalNumero
                '    rowsu = dtsu.Rows(0)
                'End If

                ' A_FacturaCompras.dtDetalleFactura.Rows.Add(New String() {(row("codDetalle")), (row("cuenta")), (row("objeto")), row("codBreve").ToString, row("codigoSucursal").ToString, CStr(row("descripcion")), CStr(row("monto")), CStr(row("id_centrocosto")), CStr(row("codigo"))})
                A_FacturaCompras.dtDetalleFactura.Rows.Add(New String() {(row("codDetalle")), (row("cuenta")), (row("objeto")), IIf(row("codBreve") Is DBNull.Value, "", row("codBreve").ToString), IIf(row("codigoSucursal") Is DBNull.Value, "", row("codigoSucursal").ToString), CStr(row("descripcion")), CStr(row("monto")), IIf(row("id_centrocosto") Is DBNull.Value, "", row("id_centrocosto")),
                IIf(row("codigo") Is DBNull.Value, "", row("codigo"))})

            Next

            Me.Hide()
            A_FacturaCompras.btnGuardar.Enabled = False
            A_FacturaCompras.btnModificar.Enabled = True
            A_FacturaCompras.btnCrear.Enabled = True
            A_FacturaCompras.Show()


        Catch ex As Exception

            MessageBox.Show(ex.Message)

        End Try
        alternarColoFilasDatagridview(dtFacturasCompra)
    End Sub

    Private Sub txtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        'Busqueda de facturas por nombre de proveedor

        Try

            FacCompra.Nombre_Proveedor = txtBusqueda.Text
            dtFacturasCompra.DataSource = FacCompra.buscarFacturaProveedor()

        Catch ex As Exception

        End Try


    End Sub

    Private Sub A_ListadoFacturaCompra_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub txtBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBusqueda.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim estado, estado2, estado3 As String
            Dim fechaDesde, fechaHasta As Date
            Dim factu As New ClsFacturaCompra

            fechaDesde = dtpDesde.Value
            fechaHasta = dtpHasta.Value

            If chkIngresada.Checked = True Then
                estado = "Ingresada"
            Else
                estado = ""
            End If

            If chkPendientes.Checked = True Then
                estado2 = "Pendiente"
            Else
                estado2 = ""
            End If

            If chkPagadas.Checked = True Then
                estado3 = "Pagada"
            Else
                estado3 = ""
            End If

            dtFacturasCompra.DataSource = factu.ReporteFacturasCompraEstado(estado, estado2, estado3, fechaDesde, fechaHasta)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs)
        Me.Close()

    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            E_frmInventario.GridAExcel(dtFacturasCompra)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NuevaFacturaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCrearNueva.Click
        Me.Hide()
        MostrarForm(A_FacturaCompras)
        A_FacturaCompras.btnModificar.Enabled = False

    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub CuentasPorPagarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CuentasPorPagarToolStripMenuItem.Click
        Try

            'cambio
            MostrarForm(EstadodeCuentasporPagar)

            'cambios 2
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class