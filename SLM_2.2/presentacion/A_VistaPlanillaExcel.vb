﻿Public Class A_VistaPlanillaExcel
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try


            E_frmInventario.GridAExcel(dtPlanilla)
            Me.Close()

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub A_VistaPlanillaExcel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(dtPlanilla)
        ColoresForm(Panel1, StatusStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub
End Class