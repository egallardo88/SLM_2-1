﻿Public Class A_PlanillaCalculo

    Public dtAdelanto As New DataTable

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs)
        Me.Close()

    End Sub
    Private Sub ExcelBanco()

        Try

            Dim empleado As New ClsEmpleados

            For a = 0 To dtData.Rows.Count - 2

                Dim dt As New DataTable
                Dim row As DataRow
                Dim nombre As String = dtData.Rows(a).Cells(0).Value
                Dim salario As String = dtData.Rows(a).Cells(21).Value

                empleado.codigo_ = Convert.ToInt32(dtData.Rows(a).Cells(22).Value)
                dt = empleado.BuscarDatosEmpleadoPorCodigo2
                row = dt.Rows(0)

                A_VistaPlanillaExcel.dtPlanilla.Rows.Add(New String() {row("nIdentidad"), row("cuentaBancaria"), nombre, salario})

            Next

            A_VistaPlanillaExcel.Show()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub TotalSalarios()
        Dim totalSalario As Double
        dtData.Rows.Add("TOTALES:", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0")

        For j = 0 To dtData.Rows.Count - 2

            totalSalario = Convert.ToDouble(dtData.Rows(j).Cells(1).Value) + totalSalario

        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(1).Value = Math.Round(totalSalario, 2)
        dtData.Rows(dtData.Rows.Count - 1).Cells(9).Value = Math.Round(totalSalario, 2)
        dtData.Rows(dtData.Rows.Count - 1).Cells(21).Value = Math.Round(totalSalario, 2)

        Dim style As New DataGridViewCellStyle
        style.Font = New Font("Arial", 9, FontStyle.Bold)
        dtData.Rows(dtData.Rows.Count - 1).DefaultCellStyle = style
        dtData.Rows(dtData.Rows.Count - 1).ReadOnly = True

    End Sub

    Private Sub CargarEmpleados()
        Try

            'Cargar todos los empleados con estado activo (el estado depende de los registros en talento humano)
            Dim objEmpleados As New ClsEmpleados
            Dim dt As New DataTable
            Dim rows As DataRow

            dt = objEmpleados.EmpleadosActivos(0)

            For index As Integer = 0 To dt.Rows.Count - 1
                'Llenado beneficiarios
                rows = dt.Rows(index)
                dtData.Rows.Add(New String() {CStr(rows("nombreCompleto")), CStr(rows("salario")), "0", "0", "0", "0", "0", "0", "0", CStr(rows("salario")), "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", CStr(rows("salario")), CStr(rows("codigo"))})
            Next
            TotalSalarios()
            CargarAdelantosToolStripMenuItem.Visible = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dtData_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dtData.CellEndEdit

        Try
            Dim sueldoNeto, resultado, dias As Double
            Dim dev, ihss, retISR, embargos, prestamocof, adelanto, prestamorap, retOptica, retPrestamo, ImpVecinal, rapVolu, retRap As Double

            ihss = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(10).Value)
            retISR = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(11).Value)
            embargos = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(12).Value)
            prestamocof = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(13).Value)
            adelanto = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(14).Value)
            prestamorap = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(15).Value)
            retOptica = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(16).Value)
            retPrestamo = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(17).Value)
            ImpVecinal = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(18).Value)
            rapVolu = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(19).Value)
            retRap = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(20).Value)
            sueldoNeto = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(1).Value)
            dev = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(21).Value)

            If e.ColumnIndex = 7 Then ' DIAS FALTADOS

                If chkDiasFaltados.Checked = False Then

                    dias = Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(7).Value)
                    resultado = (sueldoNeto / 30) * dias
                    dtData.Rows(e.RowIndex).Cells(7).Value = Math.Round(resultado, 2)

                End If

                sumaDiasFaltados()
            ElseIf e.ColumnIndex = 2 Then 'Adicional

                sumaAdicional()

            ElseIf e.ColumnIndex = 3 Then 'TIEMPO EXTRA

                Dim dte As New DataTable
                Dim rowe As DataRow
                Dim valor As Double
                Dim horaExtra() As String
                Dim hora, min, total, horariotrabajo As Integer
                Dim empleado As New ClsEmpleados
                empleado.codigo_ = Convert.ToInt32(dtData.Rows(e.RowIndex).Cells(22).Value)
                dte = empleado.HorasTrabajo
                rowe = dte.Rows(0)
                horariotrabajo = Convert.ToInt32(rowe("Horas"))

                'Conversion de horas a minutos
                horaExtra = Split(dtData.Rows(e.RowIndex).Cells(3).Value, ":")
                For i = 0 To UBound(horaExtra)
                    'horas a minutos
                    If i = 0 Then
                        hora = horaExtra(0)
                        hora = hora * 60
                    End If
                    'minutos
                    If i = 1 Then
                        min = horaExtra(1)
                    End If
                Next

                'total de minutos
                total = hora + min
                'Formula
                valor = ((((sueldoNeto / 30) / horariotrabajo) / 60) * 1.25) * total

                dtData.Rows(e.RowIndex).Cells(3).Value = Math.Round(valor, 2)

                sumaHExtra()
            ElseIf e.ColumnIndex = 4 Then 'TIEMPO EXTRA DOBLE
                Dim dte As New DataTable
                Dim rowe As DataRow
                Dim valor As Double
                Dim horaExtra() As String
                Dim hora, min, total, horariotrabajo As Integer
                Dim empleado As New ClsEmpleados
                empleado.codigo_ = Convert.ToInt32(dtData.Rows(e.RowIndex).Cells(22).Value)
                dte = empleado.HorasTrabajo
                rowe = dte.Rows(0)
                horariotrabajo = Convert.ToInt32(rowe("Horas"))

                'Conversion de horas a minutos
                horaExtra = Split(dtData.Rows(e.RowIndex).Cells(4).Value, ":")
                For i = 0 To UBound(horaExtra)
                    'horas a minutos
                    If i = 0 Then
                        hora = horaExtra(0)
                        hora = hora * 60
                    End If
                    'minutos
                    If i = 1 Then
                        min = horaExtra(1)
                    End If
                Next

                'total de minutos
                total = hora + min
                'Formula
                MsgBox(total)
                MsgBox(sueldoNeto)
                MsgBox(horariotrabajo)
                valor = ((((sueldoNeto / 30) / horariotrabajo) / 60) * 2) * total
                MsgBox(valor)
                dtData.Rows(e.RowIndex).Cells(4).Value = Math.Round(valor, 2)
                SumaTExtraDoble()


            ElseIf e.ColumnIndex = 5 Then 'TIEMPO EXTRA MIXTO
                Dim dte As New DataTable
                Dim rowe As DataRow
                Dim valor As Double
                Dim horaExtra() As String
                Dim hora, min, total, horariotrabajo As Integer
                Dim empleado As New ClsEmpleados
                empleado.codigo_ = Convert.ToInt32(dtData.Rows(e.RowIndex).Cells(22).Value)
                dte = empleado.HorasTrabajo
                rowe = dte.Rows(0)
                horariotrabajo = Convert.ToInt32(rowe("Horas"))

                'Conversion de horas a minutos
                horaExtra = Split(dtData.Rows(e.RowIndex).Cells(5).Value, ":")
                For i = 0 To UBound(horaExtra)
                    'horas a minutos
                    If i = 0 Then
                        hora = horaExtra(0)
                        hora = hora * 60
                    End If
                    'minutos
                    If i = 1 Then
                        min = horaExtra(1)
                    End If
                Next

                'total de minutos
                total = hora + min
                'Formula
                valor = ((((sueldoNeto / 30) / horariotrabajo) / 60) * 1.5) * total

                dtData.Rows(e.RowIndex).Cells(5).Value = Math.Round(valor, 2)
                SumaTExtraMixto()

            ElseIf e.ColumnIndex = 6 Then 'TIEMPO EXTRA NOCTURNO
                Dim dte As New DataTable
                Dim rowe As DataRow
                Dim valor As Double
                Dim horaExtra() As String
                Dim hora, min, total, horariotrabajo As Integer
                Dim empleado As New ClsEmpleados
                empleado.codigo_ = Convert.ToInt32(dtData.Rows(e.RowIndex).Cells(22).Value)
                dte = empleado.HorasTrabajo
                rowe = dte.Rows(0)
                horariotrabajo = Convert.ToInt32(rowe("Horas"))

                'Conversion de horas a minutos
                horaExtra = Split(dtData.Rows(e.RowIndex).Cells(6).Value, ":")
                For i = 0 To UBound(horaExtra)
                    'horas a minutos
                    If i = 0 Then
                        hora = horaExtra(0)
                        hora = hora * 60
                    End If
                    'minutos
                    If i = 1 Then
                        min = horaExtra(1)
                    End If
                Next

                'total de minutos
                total = hora + min
                'Formula
                valor = ((((sueldoNeto / 30) / horariotrabajo) / 60) * 1.75) * total

                dtData.Rows(e.RowIndex).Cells(6).Value = Math.Round(valor, 2)
                SumaTExtraNocturna()

            ElseIf e.ColumnIndex = 11 Then 'Retencion ISR

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap - retISR
                ActualizarTotales(e.RowIndex)
                SumaRetISR()

            ElseIf e.ColumnIndex = 12 Then 'embargos

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaEmbargos()

            ElseIf e.ColumnIndex = 13 Then 'prestamo cofinter

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaCofinter()
            ElseIf e.ColumnIndex = 14 Then 'adelantos

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaAdelantos()
            ElseIf e.ColumnIndex = 15 Then 'prestamo rap

                'dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaPrestamoRap()
            ElseIf e.ColumnIndex = 16 Then 'Retencion optica

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaRetOptica()
            ElseIf e.ColumnIndex = 17 Then 'Retencion prestamo

                'dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaRetPrestamo()
            ElseIf e.ColumnIndex = 18 Then 'Imp. Veci.

                'dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaImpVecinal()
            ElseIf e.ColumnIndex = 19 Then 'RapVol

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaRapVoluntario()
            ElseIf e.ColumnIndex = 20 Then 'Retencion RAP

                ' dtData.Rows(e.RowIndex).Cells(21).Value = dev - ihss - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap
                ActualizarTotales(e.RowIndex)
                SumaRetRAP()

            End If

            Dim textra, txtramixto, textradoble, textranocturno, salarioExtra, dfalta, devengado, Adicional As Double

            'SUMA SALARIO EXTRAORDINARIO
            textra = dtData.Rows(e.RowIndex).Cells(3).Value
            textradoble = dtData.Rows(e.RowIndex).Cells(4).Value
            txtramixto = dtData.Rows(e.RowIndex).Cells(5).Value
            textranocturno = dtData.Rows(e.RowIndex).Cells(6).Value

            dtData.Rows(e.RowIndex).Cells(8).Value = textra + textradoble + txtramixto + textranocturno

            'ACTUALIZAR SUELDO DEVENGADO

            salarioExtra = dtData.Rows(e.RowIndex).Cells(8).Value
            dfalta = dtData.Rows(e.RowIndex).Cells(7).Value
            Adicional = dtData.Rows(e.RowIndex).Cells(2).Value

            dtData.Rows(e.RowIndex).Cells(9).Value = (Convert.ToDouble(dtData.Rows(e.RowIndex).Cells(1).Value) + salarioExtra + Adicional) - dfalta

            devengado = dtData.Rows(e.RowIndex).Cells(9).Value
            ActualizarSumaDevengado()

            'dtData.Rows(e.RowIndex).Cells(17).Value = devengado - retISR - embargos - prestamocof - adelanto - prestamorap - retOptica - retPrestamo - ImpVecinal - rapVolu - retRap


            ActualizarTotales(e.RowIndex)

        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
        End Try

    End Sub

    Private Sub txtIHSS_TextChanged(sender As Object, e As EventArgs) Handles txtIHSS.TextChanged

        Try
            Dim devengado As Double
            For a = 0 To dtData.Rows.Count - 1

                devengado = dtData.Rows(a).Cells(9).Value
                dtData.Rows(a).Cells(10).Value = txtIHSS.Text
                dtData.Rows(a).Cells(21).Value = Math.Round(devengado - Convert.ToDouble(txtIHSS.Text), 2)
                ActualizarTotales(a)
            Next

            Dim totalihss As Double

            For j = 0 To dtData.Rows.Count - 2

                totalihss = Convert.ToDouble(dtData.Rows(j).Cells(10).Value) + totalihss

            Next

            dtData.Rows(dtData.Rows.Count - 1).Cells(10).Value = Math.Round(totalihss, 2)

        Catch ex As Exception

        End Try

    End Sub



    Private Sub dtData_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dtData.CellValueChanged
        Try
            If e.ColumnIndex = 21 Then
                ActualizarSumaNeto()
            End If

        Catch ex As Exception

        End Try

    End Sub


    Sub sumaAdicional()

        Dim Total As Double
        Dim Col As Integer = 2
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub
    Sub sumaHExtra()

        Dim Total As Double
        Dim Col As Integer = 3
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(3).Value = Math.Round(Total, 2)

    End Sub



    Sub sumaDiasFaltados()
        Dim Total As Double
        Dim Col As Integer = 7
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(7).Value = Math.Round(Total, 2)

    End Sub

    Sub ActualizarSumaDevengado()

        Dim Total As Double
        Dim Col As Integer = 9
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(9).Value = Math.Round(Total, 2)

    End Sub



    Sub ActualizarSumaNeto()

        Dim Total As Double
        Dim Col As Integer = 21
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(21).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaRetISR()

        Dim Total As Double
        Dim Col As Integer = 11
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaEmbargos()

        Dim Total As Double
        Dim Col As Integer = 12
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaCofinter()

        Dim Total As Double
        Dim Col As Integer = 13
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaAdelantos()

        Dim Total As Double
        Dim Col As Integer = 14
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaPrestamoRap()

        Dim Total As Double
        Dim Col As Integer = 15
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub


    Sub SumaRetOptica()

        Dim Total As Double
        Dim Col As Integer = 16
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaRetPrestamo()

        Dim Total As Double
        Dim Col As Integer = 17
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaImpVecinal()

        Dim Total As Double
        Dim Col As Integer = 18
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaRapVoluntario()

        Dim Total As Double
        Dim Col As Integer = 19
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Sub SumaRetRAP()

        Dim Total As Double
        Dim Col As Integer = 20
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub



    'SUMAR T EXTRA DOBLE
    Sub SumaTExtraDoble()

        Dim Total As Double
        Dim Col As Integer = 4
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    'SUMAR TIEMPO EXTRA MIXTO
    Sub SumaTExtraMixto()

        Dim Total As Double
        Dim Col As Integer = 5
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub


    'SUMA TIEMPO EXTRA NOCTURNA
    Sub SumaTExtraNocturna()

        Dim Total As Double
        Dim Col As Integer = 6
        For j = 0 To dtData.Rows.Count - 2
            Total += Convert.ToDouble(dtData.Rows(j).Cells(Col).Value)
        Next

        dtData.Rows(dtData.Rows.Count - 1).Cells(Col).Value = Math.Round(Total, 2)

    End Sub

    Private Sub btnExportarExcel_Click(sender As Object, e As EventArgs) Handles btnExportarExcel.Click
        Try
            E_frmInventario.GridAExcel(dtData)
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub A_PlanillaCalculo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(dtData)
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub

    Private Sub CargarEmpleadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargarEmpleadosToolStripMenuItem.Click
        CargarEmpleados()
        CargarEmpleadosToolStripMenuItem.Enabled = False
    End Sub

    Private Sub GenerarVistaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerarVistaToolStripMenuItem.Click
        ExcelBanco()
    End Sub


    Public Sub ActualizarTotales(fila As Integer)

        Dim sumatotal, restatotal, Totaldevengado, sueldoNeto As Double

        Dim sueldoNeto2, resultado, dias As Double
        Dim dev, ihss, retISR, embargos, prestamocof, adelanto, prestamorap, retOptica, retPrestamo, ImpVecinal, rapVolu, retRap As Double


        ' For i = 0 To dtData.Rows.Count

        'TOTAL DEVENGADO
        sumatotal = (Convert.ToDouble(dtData.Rows(fila).Cells(1).Value) + Convert.ToDouble(dtData.Rows(fila).Cells(2).Value) + Convert.ToDouble(dtData.Rows(fila).Cells(8).Value)) - Convert.ToDouble(dtData.Rows(fila).Cells(7).Value)
        dtData.Rows(fila).Cells(9).Value = Math.Round(sumatotal, 2)

        'RETENCIONES

        ihss = Convert.ToDouble(dtData.Rows(fila).Cells(10).Value)
        retISR = Convert.ToDouble(dtData.Rows(fila).Cells(11).Value)
        embargos = Convert.ToDouble(dtData.Rows(fila).Cells(12).Value)
        prestamocof = Convert.ToDouble(dtData.Rows(fila).Cells(13).Value)
        adelanto = Convert.ToDouble(dtData.Rows(fila).Cells(14).Value)
        prestamorap = Convert.ToDouble(dtData.Rows(fila).Cells(15).Value)
        retOptica = Convert.ToDouble(dtData.Rows(fila).Cells(16).Value)
        retPrestamo = Convert.ToDouble(dtData.Rows(fila).Cells(17).Value)
        ImpVecinal = Convert.ToDouble(dtData.Rows(fila).Cells(18).Value)
        rapVolu = Convert.ToDouble(dtData.Rows(fila).Cells(19).Value)
        retRap = Convert.ToDouble(dtData.Rows(fila).Cells(20).Value)
        sueldoNeto2 = Convert.ToDouble(dtData.Rows(fila).Cells(21).Value)
        dev = Convert.ToDouble(dtData.Rows(fila).Cells(21).Value)

        restatotal = ihss + retISR + embargos + prestamocof + adelanto + prestamorap + retOptica + retPrestamo + ImpVecinal + rapVolu + retRap

        dtData.Rows(fila).Cells(21).Value = sumatotal - restatotal


        ' Next


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            'Calculo de retencion rap ( retRAP = (sueldobase-10282.87)*1.5%)

            For fila = 0 To dtData.Rows.Count - 1

                Dim techo As Double = Double.Parse(txtTechoRap.Text)
                Dim sueldobase As Double = Convert.ToDouble(dtData.Rows(fila).Cells(1).Value)

                dtData.Rows(fila).Cells(20).Value = (sueldobase - techo) * 0.015
                ActualizarTotales(fila)
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Sub CargarAdelantosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargarAdelantosToolStripMenuItem.Click
        Try

            A_BuscarPlanilla.lblform.Text = "A_PlanillaCalculo"
            MostrarForm(A_BuscarPlanilla)



        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            MostrarForm(ImportarPlanilla)
        Catch ex As Exception

        End Try
    End Sub
End Class