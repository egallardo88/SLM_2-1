﻿
Imports System.Data.SqlClient
Imports System.IO
Imports SpeechLib
Imports vb = Microsoft.VisualBasic
Public Class A_MuestraPantallaCliente
    Dim ARCHIVOS As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
    Dim CONTADOR As Integer
    Dim DURACION As Integer
    Dim codigo_sede As Integer
    Dim codigo_sede_str As String


    Public Sub crearVentanaSede()
        Dim estado As Boolean = True
        Try

            Do While estado



                Me.Text = "Control de sucursal"


                codigo_sede_str = InputBox("Introduzca el codigo de sucursal", "Sucursal Ingresada")

                If codigo_sede_str = "LCA" Then
                    estado = False
                    codigo_sede = 1
                    Exit Sub
                ElseIf codigo_sede_str = "LRD" Then
                    estado = False
                    codigo_sede = 2
                    Exit Sub
                ElseIf codigo_sede_str = "LCM" Then
                    estado = False
                    codigo_sede = 3
                    Exit Sub
                ElseIf codigo_sede_str = "LTP" Then
                    estado = False
                    codigo_sede = 4
                    Exit Sub
                ElseIf codigo_sede_str = "LSA" Then
                    codigo_sede = 5
                    estado = False

                    Exit Sub
                ElseIf codigo_sede_str = "LFI" Then
                    estado = False
                    codigo_sede = 6
                    Exit Sub
                ElseIf codigo_sede_str = "VTU" Then
                    estado = False
                    codigo_sede = 1007
                    Exit Sub
                ElseIf codigo_sede_str = "LMA" Then
                    estado = False
                    codigo_sede = 1008
                    Exit Sub
                ElseIf codigo_sede_str = "LMN" Then
                    estado = False
                    codigo_sede = 1009
                    Exit Sub
                ElseIf codigo_sede_str = "EXIT" Then
                    estado = False

                    Me.Close()
                Else
                    MsgBox("Este codigo no existe")
                    estado = True
                End If

            Loop
        Catch ex As Exception

        End Try
    End Sub
    Public Function limpiarPantallaDeLLamado(ByVal id As String) As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("
delete  from ColaDeLlamado
where id_factura in
(select numero from Factura where codigoSucursal = '" + id + "'
)", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Private Sub A_MuestraPantallaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)

        crearVentanaSede()
        limpiarPantallaDeLLamado(codigo_sede)
        Try

            AxWindowsMediaPlayer1.stretchToFit = True 'ADAPTA LA IMAGEN EL ARCHIVO AL TAMAÑO DE LA PANTALLA
            AxWindowsMediaPlayer1.uiMode = "NONE" 'PARA QUE NO SE PUEDAN HACER CAMBIOS EN LA BARRA DE TIEMPO

            '   alternarColoFilasDatagridview(dgvPacientesEspera)

            Timer1.Enabled = True
            TimerCola.Enabled = True

            ARCHIVOS = My.Computer.FileSystem.GetFiles("c:\videos")

            Dim NOMBRE As String = Nothing
            For I = 0 To ARCHIVOS.Count - 1
                NOMBRE = ARCHIVOS(I).Remove(0, ARCHIVOS(I).LastIndexOf("\") + 1)
                cbxVideos.Items.Add(NOMBRE)
            Next
            Reproducir2()
        Catch ex As Exception
            MsgBox("No existen vídeos en la carpeta PROMOCIONALES en la raíz del programa. No se mostrarán vídeos.")
        End Try

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        lblHora.Text = DateTime.Now.ToString("hh:mm:ss")
        lblFecha.Text = DateTime.Now.ToLongDateString

    End Sub

    Private Sub cbxVideos_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            AxWindowsMediaPlayer1.URL = ARCHIVOS(cbxVideos.SelectedIndex) 'AL SELECCIONAR UN ITEM EN EL COMBOBOX EMPIEZA A REPRODUCIRSE
            CONTADOR = cbxVideos.SelectedIndex 'INICIALIZA EL CONTADOR DE ARCHIVOS CON EL INDICE SELECCIONADO 
            Timer2.Interval = 3000 'PARA DAR TIEMPO A CARGAR EL ARCHIVO EN EL REPRODUCTOR Y PODER DETERMINAR SU DURACION
            Timer2.Start()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Try
            'DURACION = WMP.currentMedia.duration - 1 'DETERMINA LA DURACION DEL ARCHIVO
            Timer2.Stop()
            Timer3.Interval = 1000 'EMPIEZA LA CUENTA ATRAS PARA SABER CUANDO ACABA DE REPRODUCIRSE EL ARCHIVO
            Timer3.Start()

        Catch ex As Exception

        End Try
    End Sub
    Public Sub listarColaPacientes()

        Dim objFact As New ClsColaPacientes
        Dim dv As DataView = objFact.ListarColaPacientes(Integer.Parse(codigo_sede)).DefaultView
        dgvPacientesEspera.DataSource = dv
        dgvPacientesEspera.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
    End Sub
    Private Sub TimerCola_Tick(sender As Object, e As EventArgs) Handles TimerCola.Tick
        Try
            BackgroundWorker1.RunWorkerAsync()
        Catch ex As Exception

        End Try

        Try
            TimerCola.Interval = 6000
            TimerCola.Start()
            listarColaPacientes()

            'OCULTAR COLUMNAS
            'Me.dgvPacientesEspera.Columns("codigo").Visible = False
            'Me.dgvPacientesEspera.Columns("prioridad").Visible = False
            'Me.dgvPacientesEspera.Columns("estadoEnCola").Visible = False
            'Me.dgvPacientesEspera.Columns("nombre").Visible = False
            'Me.dgvPacientesEspera.Columns("id_ordenamiento").Visible = False
            'Me.dgvPacientesEspera.Columns("estacion").Visible = False
            ''CAMBIAS NOMBRE COLUMNAS
            '' dgvPacientesEspera.Columns("numeroFactura").HeaderText = "Número Factura"

            'dgvPacientesEspera.Columns("nombreCompleto").Visible = False

            'dgvPacientesEspera.Columns("nombreCompleto").HeaderText = "Paciente o Cliente"
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        Try
            DURACION -= 1 ' CUENTA ATRAS PARA SABER CUANDO ACABA DE REPRODUCIRSE EL ARCHIVO
            If DURACION <= 0 Then 'HA TERMINADO DE REPRODUCIRSE EN AUTOMATICO
                CONTADOR += 1 ' PASA AL SIGUIENTE ARCHIVO
                If CONTADOR > ARCHIVOS.Count - 1 Then ' SI HA LLEGADO AL FIN DEL ULTIMO ARCHIVO VUELVE A EMPEZAR 
                    CONTADOR = 0
                End If
                Timer3.Stop() 'DETIENE LA CUENTA ATRAS
                REPRODUCIR() 'REPRODUCE EL SIGUIENTE ARCHIVO

            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub REPRODUCIR()
        Try
            cbxVideos.SelectedIndex = CONTADOR 'ACTUALIZA LA SELECCION EN EL COMBOBOX
            AxWindowsMediaPlayer1.URL = ARCHIVOS(cbxVideos.SelectedIndex) 'REPRODUCE EL ARCHIVO SELECCIONADO
            Timer2.Interval = 3000 'PARA DAR TIEMPO A CARGAR EL ARCHIVO EN EL REPRODUCTOR Y PODER DETERMINAR SU DURACION
            Timer2.Start()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Reproducir2()
        Dim estado As Boolean = True


        For Each item As Object In Directory.GetFiles("C:\videos")

            Dim nueva As WMPLib.IWMPMedia = AxWindowsMediaPlayer1.newMedia(item)

            AxWindowsMediaPlayer1.currentPlaylist.appendItem(nueva)

        Next
        'Try

        'COMENTARIO 3

        '    Do While estado
        AxWindowsMediaPlayer1.settings.autoStart = True
        AxWindowsMediaPlayer1.settings.setMode("loop", True)
        AxWindowsMediaPlayer1.Ctlcontrols.play()
        'estado = True
        'Loop
        'Catch ex As Exception
        'MsgBox(ex.ToString)
        'End Try
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs)



    End Sub

    Public Sub llamarClientes()

        Dim dt, dtFact, dtDetalle, dtPreguntas As New DataTable
        Dim row, rowFact, rowDetalle, rowPreguntas As DataRow
        Dim paciente As New ClsColaPacientes
        Dim codigodeCola As String


        Dim compVoice As New SpeechLib.SpVoice
        'With paciente
        dt = paciente.SeleccionarClientesParaLLamar()
        '    row = dt.Rows(0)
        '    'txtNumeroFactura.Text = row("numeroFactura")
        'End With
        AxWindowsMediaPlayer1.Ctlcontrols.pause()
        Dim palabra, palabra1, palabra2, palabra3 As String
        Dim i As Integer = 0


        For Each MiDataRow As DataRow In dt.Rows
            If MiDataRow("estado").ToString <> "True" Then
                palabra = MiDataRow("numeroFactura").ToString
                Try
                    Dim rpt As New etiqueta6


                    rpt.SetParameterValue("@id_factura", palabra)
                    rpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")



                    rpt.PrintOptions.PrinterName = MiDataRow("impresora").ToString
                    rpt.PrintToPrinter(1, True, 0, 0)

                Catch ex As Exception
                End Try

                E_frmPantallaTurno.Hide()
                Threading.Thread.Sleep(3000)

                palabra1 = vb.Left(palabra, 3)
                palabra2 = vb.Mid(palabra, 4, 2)
                palabra3 = vb.Right(palabra, 2)
                compVoice.Speak("..........................")
                compVoice.Speak("Siguiente Turno")
                Threading.Thread.Sleep(10)

                compVoice.Speak(palabra1)
                'Threading.Thread.Sleep(10)
                compVoice.Speak(palabra2)
                'Threading.Thread.Sleep(10)
                compVoice.Speak(palabra3)
                Threading.Thread.Sleep(10)

                compVoice.Speak("en estación '" + MiDataRow("estacion").ToString + "'")




                paciente.ActualizarClientesParaLLamar(MiDataRow("numeroFactura").ToString)

            End If
        Next

        AxWindowsMediaPlayer1.Ctlcontrols.play()

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        llamarClientes()
    End Sub

    Private Sub BackgroundWorker2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        E_frmPantallaTurno.Show()
        Threading.Thread.Sleep(2000)
        E_frmPantallaTurno.Close()
    End Sub
End Class