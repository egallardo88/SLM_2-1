﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class M_VerPaciente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M_VerPaciente))
        Me.gbxinfoCliente = New System.Windows.Forms.GroupBox()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.lblcodeTerminoPago = New System.Windows.Forms.Label()
        Me.lblcodeCategoria = New System.Windows.Forms.Label()
        Me.txtnombre2 = New System.Windows.Forms.TextBox()
        Me.txtapellido1 = New System.Windows.Forms.TextBox()
        Me.txtnombreCategoria = New System.Windows.Forms.TextBox()
        Me.txtnombreClasificacion = New System.Windows.Forms.TextBox()
        Me.txtnombreConvenio = New System.Windows.Forms.TextBox()
        Me.txtnombreTerminos = New System.Windows.Forms.TextBox()
        Me.txtnombreAseguradora = New System.Windows.Forms.TextBox()
        Me.txtcodigoClasificacion = New System.Windows.Forms.TextBox()
        Me.txtcodigoTermino = New System.Windows.Forms.TextBox()
        Me.txtconvenio = New System.Windows.Forms.TextBox()
        Me.txtaseguradora = New System.Windows.Forms.TextBox()
        Me.dtpfechaNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.gbxgenero = New System.Windows.Forms.GroupBox()
        Me.rbtnmasculino = New System.Windows.Forms.RadioButton()
        Me.rbtnfemenino = New System.Windows.Forms.RadioButton()
        Me.lblclasificacion = New System.Windows.Forms.Label()
        Me.lblconvenio = New System.Windows.Forms.Label()
        Me.lblfechaNacimiento = New System.Windows.Forms.Label()
        Me.lblterminosPago = New System.Windows.Forms.Label()
        Me.lblaseguradora = New System.Windows.Forms.Label()
        Me.txtcodigoCategoria = New System.Windows.Forms.TextBox()
        Me.lblcategoria = New System.Windows.Forms.Label()
        Me.txtcodigo = New System.Windows.Forms.TextBox()
        Me.rtxtdireccion = New System.Windows.Forms.RichTextBox()
        Me.txtcorreo = New System.Windows.Forms.TextBox()
        Me.lbldireccion = New System.Windows.Forms.Label()
        Me.lblcodigo = New System.Windows.Forms.Label()
        Me.txttelefonoCasa = New System.Windows.Forms.TextBox()
        Me.txtscanId = New System.Windows.Forms.TextBox()
        Me.txtnombreCompleto = New System.Windows.Forms.TextBox()
        Me.mtxtidentidad = New System.Windows.Forms.MaskedTextBox()
        Me.lbltelefonoCasa = New System.Windows.Forms.Label()
        Me.lblcorreo = New System.Windows.Forms.Label()
        Me.lblscanId = New System.Windows.Forms.Label()
        Me.txtcorreo2 = New System.Windows.Forms.TextBox()
        Me.lblcorreo2 = New System.Windows.Forms.Label()
        Me.txtcelular = New System.Windows.Forms.TextBox()
        Me.lblnombre = New System.Windows.Forms.Label()
        Me.lblcelular = New System.Windows.Forms.Label()
        Me.lblidentidadCliente = New System.Windows.Forms.Label()
        Me.txttelefonoTrabajo = New System.Windows.Forms.TextBox()
        Me.lbltelefonoTrabajo = New System.Windows.Forms.Label()
        Me.txtrtn = New System.Windows.Forms.TextBox()
        Me.lblrtn = New System.Windows.Forms.Label()
        Me.txtapellido2 = New System.Windows.Forms.TextBox()
        Me.lblapellido2 = New System.Windows.Forms.Label()
        Me.lblnombre2 = New System.Windows.Forms.Label()
        Me.lblapellido1 = New System.Windows.Forms.Label()
        Me.txtnombre1 = New System.Windows.Forms.TextBox()
        Me.lblnombre1 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.gbxinfoCliente.SuspendLayout()
        Me.gbxgenero.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxinfoCliente
        '
        Me.gbxinfoCliente.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbxinfoCliente.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.gbxinfoCliente.Controls.Add(Me.LinkLabel1)
        Me.gbxinfoCliente.Controls.Add(Me.txtEdad)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodeTerminoPago)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodeCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombre2)
        Me.gbxinfoCliente.Controls.Add(Me.txtapellido1)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreClasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreConvenio)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreTerminos)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreAseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoClasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoTermino)
        Me.gbxinfoCliente.Controls.Add(Me.txtconvenio)
        Me.gbxinfoCliente.Controls.Add(Me.txtaseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.dtpfechaNacimiento)
        Me.gbxinfoCliente.Controls.Add(Me.gbxgenero)
        Me.gbxinfoCliente.Controls.Add(Me.lblclasificacion)
        Me.gbxinfoCliente.Controls.Add(Me.lblconvenio)
        Me.gbxinfoCliente.Controls.Add(Me.lblfechaNacimiento)
        Me.gbxinfoCliente.Controls.Add(Me.lblterminosPago)
        Me.gbxinfoCliente.Controls.Add(Me.lblaseguradora)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigoCategoria)
        Me.gbxinfoCliente.Controls.Add(Me.lblcategoria)
        Me.gbxinfoCliente.Controls.Add(Me.txtcodigo)
        Me.gbxinfoCliente.Controls.Add(Me.rtxtdireccion)
        Me.gbxinfoCliente.Controls.Add(Me.txtcorreo)
        Me.gbxinfoCliente.Controls.Add(Me.lbldireccion)
        Me.gbxinfoCliente.Controls.Add(Me.lblcodigo)
        Me.gbxinfoCliente.Controls.Add(Me.txttelefonoCasa)
        Me.gbxinfoCliente.Controls.Add(Me.txtscanId)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombreCompleto)
        Me.gbxinfoCliente.Controls.Add(Me.mtxtidentidad)
        Me.gbxinfoCliente.Controls.Add(Me.lbltelefonoCasa)
        Me.gbxinfoCliente.Controls.Add(Me.lblcorreo)
        Me.gbxinfoCliente.Controls.Add(Me.lblscanId)
        Me.gbxinfoCliente.Controls.Add(Me.txtcorreo2)
        Me.gbxinfoCliente.Controls.Add(Me.lblcorreo2)
        Me.gbxinfoCliente.Controls.Add(Me.txtcelular)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre)
        Me.gbxinfoCliente.Controls.Add(Me.lblcelular)
        Me.gbxinfoCliente.Controls.Add(Me.lblidentidadCliente)
        Me.gbxinfoCliente.Controls.Add(Me.txttelefonoTrabajo)
        Me.gbxinfoCliente.Controls.Add(Me.lbltelefonoTrabajo)
        Me.gbxinfoCliente.Controls.Add(Me.txtrtn)
        Me.gbxinfoCliente.Controls.Add(Me.lblrtn)
        Me.gbxinfoCliente.Controls.Add(Me.txtapellido2)
        Me.gbxinfoCliente.Controls.Add(Me.lblapellido2)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre2)
        Me.gbxinfoCliente.Controls.Add(Me.lblapellido1)
        Me.gbxinfoCliente.Controls.Add(Me.txtnombre1)
        Me.gbxinfoCliente.Controls.Add(Me.lblnombre1)
        Me.gbxinfoCliente.Location = New System.Drawing.Point(11, 11)
        Me.gbxinfoCliente.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxinfoCliente.Name = "gbxinfoCliente"
        Me.gbxinfoCliente.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxinfoCliente.Size = New System.Drawing.Size(723, 348)
        Me.gbxinfoCliente.TabIndex = 55
        Me.gbxinfoCliente.TabStop = False
        Me.gbxinfoCliente.Text = "Información de Cliente/Paciente"
        Me.gbxinfoCliente.Visible = False
        '
        'txtEdad
        '
        Me.txtEdad.Location = New System.Drawing.Point(651, 75)
        Me.txtEdad.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(56, 20)
        Me.txtEdad.TabIndex = 121
        '
        'lblcodeTerminoPago
        '
        Me.lblcodeTerminoPago.AutoSize = True
        Me.lblcodeTerminoPago.Location = New System.Drawing.Point(4, 245)
        Me.lblcodeTerminoPago.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeTerminoPago.Name = "lblcodeTerminoPago"
        Me.lblcodeTerminoPago.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeTerminoPago.TabIndex = 120
        Me.lblcodeTerminoPago.Visible = False
        '
        'lblcodeCategoria
        '
        Me.lblcodeCategoria.AutoSize = True
        Me.lblcodeCategoria.Location = New System.Drawing.Point(264, 27)
        Me.lblcodeCategoria.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblcodeCategoria.Name = "lblcodeCategoria"
        Me.lblcodeCategoria.Size = New System.Drawing.Size(0, 13)
        Me.lblcodeCategoria.TabIndex = 119
        Me.lblcodeCategoria.Visible = False
        '
        'txtnombre2
        '
        Me.txtnombre2.Location = New System.Drawing.Point(328, 75)
        Me.txtnombre2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombre2.MaxLength = 20
        Me.txtnombre2.Name = "txtnombre2"
        Me.txtnombre2.ReadOnly = True
        Me.txtnombre2.Size = New System.Drawing.Size(135, 20)
        Me.txtnombre2.TabIndex = 118
        '
        'txtapellido1
        '
        Me.txtapellido1.Location = New System.Drawing.Point(116, 100)
        Me.txtapellido1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtapellido1.MaxLength = 20
        Me.txtapellido1.Name = "txtapellido1"
        Me.txtapellido1.ReadOnly = True
        Me.txtapellido1.Size = New System.Drawing.Size(135, 20)
        Me.txtapellido1.TabIndex = 117
        '
        'txtnombreCategoria
        '
        Me.txtnombreCategoria.Location = New System.Drawing.Point(471, 24)
        Me.txtnombreCategoria.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreCategoria.Name = "txtnombreCategoria"
        Me.txtnombreCategoria.ReadOnly = True
        Me.txtnombreCategoria.Size = New System.Drawing.Size(236, 20)
        Me.txtnombreCategoria.TabIndex = 114
        '
        'txtnombreClasificacion
        '
        Me.txtnombreClasificacion.Location = New System.Drawing.Point(261, 310)
        Me.txtnombreClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreClasificacion.Name = "txtnombreClasificacion"
        Me.txtnombreClasificacion.ReadOnly = True
        Me.txtnombreClasificacion.Size = New System.Drawing.Size(201, 20)
        Me.txtnombreClasificacion.TabIndex = 113
        '
        'txtnombreConvenio
        '
        Me.txtnombreConvenio.Location = New System.Drawing.Point(261, 285)
        Me.txtnombreConvenio.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreConvenio.Name = "txtnombreConvenio"
        Me.txtnombreConvenio.ReadOnly = True
        Me.txtnombreConvenio.Size = New System.Drawing.Size(201, 20)
        Me.txtnombreConvenio.TabIndex = 112
        '
        'txtnombreTerminos
        '
        Me.txtnombreTerminos.Location = New System.Drawing.Point(261, 260)
        Me.txtnombreTerminos.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreTerminos.Name = "txtnombreTerminos"
        Me.txtnombreTerminos.ReadOnly = True
        Me.txtnombreTerminos.Size = New System.Drawing.Size(202, 20)
        Me.txtnombreTerminos.TabIndex = 111
        '
        'txtnombreAseguradora
        '
        Me.txtnombreAseguradora.Location = New System.Drawing.Point(261, 235)
        Me.txtnombreAseguradora.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreAseguradora.Name = "txtnombreAseguradora"
        Me.txtnombreAseguradora.ReadOnly = True
        Me.txtnombreAseguradora.Size = New System.Drawing.Size(201, 20)
        Me.txtnombreAseguradora.TabIndex = 110
        '
        'txtcodigoClasificacion
        '
        Me.txtcodigoClasificacion.Location = New System.Drawing.Point(116, 310)
        Me.txtcodigoClasificacion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoClasificacion.Name = "txtcodigoClasificacion"
        Me.txtcodigoClasificacion.ReadOnly = True
        Me.txtcodigoClasificacion.Size = New System.Drawing.Size(138, 20)
        Me.txtcodigoClasificacion.TabIndex = 107
        Me.txtcodigoClasificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtcodigoTermino
        '
        Me.txtcodigoTermino.Location = New System.Drawing.Point(116, 260)
        Me.txtcodigoTermino.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoTermino.Name = "txtcodigoTermino"
        Me.txtcodigoTermino.ReadOnly = True
        Me.txtcodigoTermino.Size = New System.Drawing.Size(138, 20)
        Me.txtcodigoTermino.TabIndex = 105
        Me.txtcodigoTermino.Text = "CO"
        Me.txtcodigoTermino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtconvenio
        '
        Me.txtconvenio.Location = New System.Drawing.Point(116, 285)
        Me.txtconvenio.Margin = New System.Windows.Forms.Padding(2)
        Me.txtconvenio.Name = "txtconvenio"
        Me.txtconvenio.ReadOnly = True
        Me.txtconvenio.Size = New System.Drawing.Size(138, 20)
        Me.txtconvenio.TabIndex = 103
        Me.txtconvenio.Text = "x"
        Me.txtconvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtaseguradora
        '
        Me.txtaseguradora.Location = New System.Drawing.Point(116, 235)
        Me.txtaseguradora.Margin = New System.Windows.Forms.Padding(2)
        Me.txtaseguradora.Name = "txtaseguradora"
        Me.txtaseguradora.ReadOnly = True
        Me.txtaseguradora.Size = New System.Drawing.Size(138, 20)
        Me.txtaseguradora.TabIndex = 101
        Me.txtaseguradora.Text = "x"
        Me.txtaseguradora.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpfechaNacimiento
        '
        Me.dtpfechaNacimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpfechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpfechaNacimiento.Location = New System.Drawing.Point(562, 75)
        Me.dtpfechaNacimiento.Margin = New System.Windows.Forms.Padding(2)
        Me.dtpfechaNacimiento.Name = "dtpfechaNacimiento"
        Me.dtpfechaNacimiento.Size = New System.Drawing.Size(86, 19)
        Me.dtpfechaNacimiento.TabIndex = 99
        '
        'gbxgenero
        '
        Me.gbxgenero.BackColor = System.Drawing.Color.White
        Me.gbxgenero.Controls.Add(Me.rbtnmasculino)
        Me.gbxgenero.Controls.Add(Me.rbtnfemenino)
        Me.gbxgenero.Location = New System.Drawing.Point(562, 100)
        Me.gbxgenero.Margin = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Name = "gbxgenero"
        Me.gbxgenero.Padding = New System.Windows.Forms.Padding(2)
        Me.gbxgenero.Size = New System.Drawing.Size(145, 60)
        Me.gbxgenero.TabIndex = 98
        Me.gbxgenero.TabStop = False
        Me.gbxgenero.Text = "Género"
        '
        'rbtnmasculino
        '
        Me.rbtnmasculino.AutoSize = True
        Me.rbtnmasculino.Location = New System.Drawing.Point(31, 17)
        Me.rbtnmasculino.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnmasculino.Name = "rbtnmasculino"
        Me.rbtnmasculino.Size = New System.Drawing.Size(73, 17)
        Me.rbtnmasculino.TabIndex = 8
        Me.rbtnmasculino.TabStop = True
        Me.rbtnmasculino.Text = "Masculino"
        Me.rbtnmasculino.UseVisualStyleBackColor = True
        '
        'rbtnfemenino
        '
        Me.rbtnfemenino.AutoSize = True
        Me.rbtnfemenino.Location = New System.Drawing.Point(31, 38)
        Me.rbtnfemenino.Margin = New System.Windows.Forms.Padding(2)
        Me.rbtnfemenino.Name = "rbtnfemenino"
        Me.rbtnfemenino.Size = New System.Drawing.Size(71, 17)
        Me.rbtnfemenino.TabIndex = 9
        Me.rbtnfemenino.TabStop = True
        Me.rbtnfemenino.Text = "Femenino"
        Me.rbtnfemenino.UseVisualStyleBackColor = True
        '
        'lblclasificacion
        '
        Me.lblclasificacion.AutoSize = True
        Me.lblclasificacion.Location = New System.Drawing.Point(48, 314)
        Me.lblclasificacion.Name = "lblclasificacion"
        Me.lblclasificacion.Size = New System.Drawing.Size(66, 13)
        Me.lblclasificacion.TabIndex = 96
        Me.lblclasificacion.Text = "Clasificación"
        '
        'lblconvenio
        '
        Me.lblconvenio.AutoSize = True
        Me.lblconvenio.Location = New System.Drawing.Point(60, 286)
        Me.lblconvenio.Name = "lblconvenio"
        Me.lblconvenio.Size = New System.Drawing.Size(52, 13)
        Me.lblconvenio.TabIndex = 94
        Me.lblconvenio.Text = "Convenio"
        '
        'lblfechaNacimiento
        '
        Me.lblfechaNacimiento.AutoSize = True
        Me.lblfechaNacimiento.Location = New System.Drawing.Point(466, 76)
        Me.lblfechaNacimiento.Name = "lblfechaNacimiento"
        Me.lblfechaNacimiento.Size = New System.Drawing.Size(93, 13)
        Me.lblfechaNacimiento.TabIndex = 93
        Me.lblfechaNacimiento.Text = "Fecha Nacimiento"
        '
        'lblterminosPago
        '
        Me.lblterminosPago.AutoSize = True
        Me.lblterminosPago.Location = New System.Drawing.Point(17, 262)
        Me.lblterminosPago.Name = "lblterminosPago"
        Me.lblterminosPago.Size = New System.Drawing.Size(93, 13)
        Me.lblterminosPago.TabIndex = 91
        Me.lblterminosPago.Text = "Términos de Pago"
        '
        'lblaseguradora
        '
        Me.lblaseguradora.AutoSize = True
        Me.lblaseguradora.Location = New System.Drawing.Point(40, 237)
        Me.lblaseguradora.Name = "lblaseguradora"
        Me.lblaseguradora.Size = New System.Drawing.Size(67, 13)
        Me.lblaseguradora.TabIndex = 89
        Me.lblaseguradora.Text = "Aseguradora"
        '
        'txtcodigoCategoria
        '
        Me.txtcodigoCategoria.Location = New System.Drawing.Point(326, 24)
        Me.txtcodigoCategoria.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigoCategoria.MaxLength = 20
        Me.txtcodigoCategoria.Name = "txtcodigoCategoria"
        Me.txtcodigoCategoria.ReadOnly = True
        Me.txtcodigoCategoria.Size = New System.Drawing.Size(136, 20)
        Me.txtcodigoCategoria.TabIndex = 88
        Me.txtcodigoCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcategoria
        '
        Me.lblcategoria.AutoSize = True
        Me.lblcategoria.Location = New System.Drawing.Point(269, 24)
        Me.lblcategoria.Name = "lblcategoria"
        Me.lblcategoria.Size = New System.Drawing.Size(54, 13)
        Me.lblcategoria.TabIndex = 87
        Me.lblcategoria.Text = "Categoría"
        '
        'txtcodigo
        '
        Me.txtcodigo.Location = New System.Drawing.Point(116, 24)
        Me.txtcodigo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcodigo.Name = "txtcodigo"
        Me.txtcodigo.ReadOnly = True
        Me.txtcodigo.Size = New System.Drawing.Size(135, 20)
        Me.txtcodigo.TabIndex = 86
        Me.txtcodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'rtxtdireccion
        '
        Me.rtxtdireccion.Location = New System.Drawing.Point(116, 150)
        Me.rtxtdireccion.Margin = New System.Windows.Forms.Padding(2)
        Me.rtxtdireccion.MaxLength = 200
        Me.rtxtdireccion.Name = "rtxtdireccion"
        Me.rtxtdireccion.ReadOnly = True
        Me.rtxtdireccion.Size = New System.Drawing.Size(347, 78)
        Me.rtxtdireccion.TabIndex = 61
        Me.rtxtdireccion.Text = ""
        '
        'txtcorreo
        '
        Me.txtcorreo.Location = New System.Drawing.Point(562, 267)
        Me.txtcorreo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcorreo.MaxLength = 100
        Me.txtcorreo.Name = "txtcorreo"
        Me.txtcorreo.ReadOnly = True
        Me.txtcorreo.Size = New System.Drawing.Size(146, 20)
        Me.txtcorreo.TabIndex = 65
        Me.txtcorreo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbldireccion
        '
        Me.lbldireccion.AutoSize = True
        Me.lbldireccion.Location = New System.Drawing.Point(2, 153)
        Me.lbldireccion.Name = "lbldireccion"
        Me.lbldireccion.Size = New System.Drawing.Size(111, 13)
        Me.lbldireccion.TabIndex = 60
        Me.lbldireccion.Text = "Dirección Facturación"
        '
        'lblcodigo
        '
        Me.lblcodigo.AutoSize = True
        Me.lblcodigo.Location = New System.Drawing.Point(71, 24)
        Me.lblcodigo.Name = "lblcodigo"
        Me.lblcodigo.Size = New System.Drawing.Size(40, 13)
        Me.lblcodigo.TabIndex = 85
        Me.lblcodigo.Text = "Código"
        '
        'txttelefonoCasa
        '
        Me.txttelefonoCasa.Location = New System.Drawing.Point(562, 191)
        Me.txttelefonoCasa.Margin = New System.Windows.Forms.Padding(2)
        Me.txttelefonoCasa.MaxLength = 20
        Me.txttelefonoCasa.Name = "txttelefonoCasa"
        Me.txttelefonoCasa.ReadOnly = True
        Me.txttelefonoCasa.Size = New System.Drawing.Size(146, 20)
        Me.txttelefonoCasa.TabIndex = 64
        Me.txttelefonoCasa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtscanId
        '
        Me.txtscanId.Location = New System.Drawing.Point(116, 50)
        Me.txtscanId.Margin = New System.Windows.Forms.Padding(2)
        Me.txtscanId.MaxLength = 50
        Me.txtscanId.Name = "txtscanId"
        Me.txtscanId.ReadOnly = True
        Me.txtscanId.Size = New System.Drawing.Size(135, 20)
        Me.txtscanId.TabIndex = 84
        Me.txtscanId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtnombreCompleto
        '
        Me.txtnombreCompleto.Location = New System.Drawing.Point(116, 125)
        Me.txtnombreCompleto.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombreCompleto.MaxLength = 80
        Me.txtnombreCompleto.Name = "txtnombreCompleto"
        Me.txtnombreCompleto.ReadOnly = True
        Me.txtnombreCompleto.Size = New System.Drawing.Size(347, 20)
        Me.txtnombreCompleto.TabIndex = 62
        Me.txtnombreCompleto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'mtxtidentidad
        '
        Me.mtxtidentidad.BackColor = System.Drawing.Color.White
        Me.mtxtidentidad.Location = New System.Drawing.Point(328, 50)
        Me.mtxtidentidad.Margin = New System.Windows.Forms.Padding(2)
        Me.mtxtidentidad.Mask = "0000-0000-00000"
        Me.mtxtidentidad.Name = "mtxtidentidad"
        Me.mtxtidentidad.ReadOnly = True
        Me.mtxtidentidad.Size = New System.Drawing.Size(135, 20)
        Me.mtxtidentidad.TabIndex = 63
        Me.mtxtidentidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbltelefonoCasa
        '
        Me.lbltelefonoCasa.AutoSize = True
        Me.lbltelefonoCasa.Location = New System.Drawing.Point(482, 191)
        Me.lbltelefonoCasa.Name = "lbltelefonoCasa"
        Me.lbltelefonoCasa.Size = New System.Drawing.Size(76, 13)
        Me.lbltelefonoCasa.TabIndex = 59
        Me.lbltelefonoCasa.Text = "Teléfono Casa"
        '
        'lblcorreo
        '
        Me.lblcorreo.AutoSize = True
        Me.lblcorreo.Location = New System.Drawing.Point(512, 269)
        Me.lblcorreo.Name = "lblcorreo"
        Me.lblcorreo.Size = New System.Drawing.Size(38, 13)
        Me.lblcorreo.TabIndex = 58
        Me.lblcorreo.Text = "Correo"
        '
        'lblscanId
        '
        Me.lblscanId.AutoSize = True
        Me.lblscanId.Location = New System.Drawing.Point(64, 54)
        Me.lblscanId.Name = "lblscanId"
        Me.lblscanId.Size = New System.Drawing.Size(50, 13)
        Me.lblscanId.TabIndex = 83
        Me.lblscanId.Text = "SCAN ID"
        '
        'txtcorreo2
        '
        Me.txtcorreo2.Location = New System.Drawing.Point(562, 292)
        Me.txtcorreo2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcorreo2.MaxLength = 100
        Me.txtcorreo2.Name = "txtcorreo2"
        Me.txtcorreo2.ReadOnly = True
        Me.txtcorreo2.Size = New System.Drawing.Size(146, 20)
        Me.txtcorreo2.TabIndex = 82
        Me.txtcorreo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcorreo2
        '
        Me.lblcorreo2.AutoSize = True
        Me.lblcorreo2.Location = New System.Drawing.Point(503, 295)
        Me.lblcorreo2.Name = "lblcorreo2"
        Me.lblcorreo2.Size = New System.Drawing.Size(47, 13)
        Me.lblcorreo2.TabIndex = 81
        Me.lblcorreo2.Text = "Correo 2"
        '
        'txtcelular
        '
        Me.txtcelular.Location = New System.Drawing.Point(562, 242)
        Me.txtcelular.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcelular.MaxLength = 20
        Me.txtcelular.Name = "txtcelular"
        Me.txtcelular.ReadOnly = True
        Me.txtcelular.Size = New System.Drawing.Size(146, 20)
        Me.txtcelular.TabIndex = 80
        Me.txtcelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblnombre
        '
        Me.lblnombre.AutoSize = True
        Me.lblnombre.Location = New System.Drawing.Point(20, 128)
        Me.lblnombre.Name = "lblnombre"
        Me.lblnombre.Size = New System.Drawing.Size(91, 13)
        Me.lblnombre.TabIndex = 56
        Me.lblnombre.Text = "Nombre Completo"
        '
        'lblcelular
        '
        Me.lblcelular.AutoSize = True
        Me.lblcelular.Location = New System.Drawing.Point(516, 242)
        Me.lblcelular.Name = "lblcelular"
        Me.lblcelular.Size = New System.Drawing.Size(39, 13)
        Me.lblcelular.TabIndex = 79
        Me.lblcelular.Text = "Celular"
        '
        'lblidentidadCliente
        '
        Me.lblidentidadCliente.AutoSize = True
        Me.lblidentidadCliente.Location = New System.Drawing.Point(269, 52)
        Me.lblidentidadCliente.Name = "lblidentidadCliente"
        Me.lblidentidadCliente.Size = New System.Drawing.Size(51, 13)
        Me.lblidentidadCliente.TabIndex = 57
        Me.lblidentidadCliente.Text = "Identidad"
        '
        'txttelefonoTrabajo
        '
        Me.txttelefonoTrabajo.Location = New System.Drawing.Point(562, 217)
        Me.txttelefonoTrabajo.Margin = New System.Windows.Forms.Padding(2)
        Me.txttelefonoTrabajo.MaxLength = 20
        Me.txttelefonoTrabajo.Name = "txttelefonoTrabajo"
        Me.txttelefonoTrabajo.ReadOnly = True
        Me.txttelefonoTrabajo.Size = New System.Drawing.Size(146, 20)
        Me.txttelefonoTrabajo.TabIndex = 78
        Me.txttelefonoTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbltelefonoTrabajo
        '
        Me.lbltelefonoTrabajo.AutoSize = True
        Me.lbltelefonoTrabajo.Location = New System.Drawing.Point(469, 219)
        Me.lbltelefonoTrabajo.Name = "lbltelefonoTrabajo"
        Me.lbltelefonoTrabajo.Size = New System.Drawing.Size(88, 13)
        Me.lbltelefonoTrabajo.TabIndex = 77
        Me.lbltelefonoTrabajo.Text = "Teléfono Trabajo"
        '
        'txtrtn
        '
        Me.txtrtn.Location = New System.Drawing.Point(562, 50)
        Me.txtrtn.Margin = New System.Windows.Forms.Padding(2)
        Me.txtrtn.MaxLength = 20
        Me.txtrtn.Name = "txtrtn"
        Me.txtrtn.ReadOnly = True
        Me.txtrtn.Size = New System.Drawing.Size(146, 20)
        Me.txtrtn.TabIndex = 76
        Me.txtrtn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblrtn
        '
        Me.lblrtn.AutoSize = True
        Me.lblrtn.Location = New System.Drawing.Point(527, 50)
        Me.lblrtn.Name = "lblrtn"
        Me.lblrtn.Size = New System.Drawing.Size(30, 13)
        Me.lblrtn.TabIndex = 75
        Me.lblrtn.Text = "RTN"
        '
        'txtapellido2
        '
        Me.txtapellido2.Location = New System.Drawing.Point(328, 100)
        Me.txtapellido2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtapellido2.MaxLength = 20
        Me.txtapellido2.Name = "txtapellido2"
        Me.txtapellido2.ReadOnly = True
        Me.txtapellido2.Size = New System.Drawing.Size(135, 20)
        Me.txtapellido2.TabIndex = 74
        '
        'lblapellido2
        '
        Me.lblapellido2.AutoSize = True
        Me.lblapellido2.Location = New System.Drawing.Point(257, 102)
        Me.lblapellido2.Name = "lblapellido2"
        Me.lblapellido2.Size = New System.Drawing.Size(65, 13)
        Me.lblapellido2.TabIndex = 73
        Me.lblapellido2.Text = "2do Apellido"
        '
        'lblnombre2
        '
        Me.lblnombre2.AutoSize = True
        Me.lblnombre2.Location = New System.Drawing.Point(259, 77)
        Me.lblnombre2.Name = "lblnombre2"
        Me.lblnombre2.Size = New System.Drawing.Size(65, 13)
        Me.lblnombre2.TabIndex = 71
        Me.lblnombre2.Text = "2do Nombre"
        '
        'lblapellido1
        '
        Me.lblapellido1.AutoSize = True
        Me.lblapellido1.Location = New System.Drawing.Point(46, 102)
        Me.lblapellido1.Name = "lblapellido1"
        Me.lblapellido1.Size = New System.Drawing.Size(62, 13)
        Me.lblapellido1.TabIndex = 69
        Me.lblapellido1.Text = "1er Apellido"
        '
        'txtnombre1
        '
        Me.txtnombre1.Location = New System.Drawing.Point(116, 75)
        Me.txtnombre1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnombre1.MaxLength = 20
        Me.txtnombre1.Name = "txtnombre1"
        Me.txtnombre1.ReadOnly = True
        Me.txtnombre1.Size = New System.Drawing.Size(135, 20)
        Me.txtnombre1.TabIndex = 68
        '
        'lblnombre1
        '
        Me.lblnombre1.AutoSize = True
        Me.lblnombre1.Location = New System.Drawing.Point(48, 77)
        Me.lblnombre1.Name = "lblnombre1"
        Me.lblnombre1.Size = New System.Drawing.Size(62, 13)
        Me.lblnombre1.TabIndex = 67
        Me.lblnombre1.Text = "1er Nombre"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 366)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(744, 22)
        Me.StatusStrip1.TabIndex = 56
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(613, 164)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(91, 13)
        Me.LinkLabel1.TabIndex = 122
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Actualizar Género"
        '
        'M_VerPaciente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(744, 388)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxinfoCliente)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "M_VerPaciente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Información del Paciente"
        Me.gbxinfoCliente.ResumeLayout(False)
        Me.gbxinfoCliente.PerformLayout()
        Me.gbxgenero.ResumeLayout(False)
        Me.gbxgenero.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gbxinfoCliente As GroupBox
    Friend WithEvents lblcodeTerminoPago As Label
    Friend WithEvents lblcodeCategoria As Label
    Friend WithEvents txtnombre2 As TextBox
    Friend WithEvents txtapellido1 As TextBox
    Friend WithEvents txtnombreCategoria As TextBox
    Friend WithEvents txtnombreClasificacion As TextBox
    Friend WithEvents txtnombreConvenio As TextBox
    Friend WithEvents txtnombreTerminos As TextBox
    Friend WithEvents txtnombreAseguradora As TextBox
    Friend WithEvents txtcodigoClasificacion As TextBox
    Friend WithEvents txtcodigoTermino As TextBox
    Friend WithEvents txtconvenio As TextBox
    Friend WithEvents txtaseguradora As TextBox
    Friend WithEvents dtpfechaNacimiento As DateTimePicker
    Friend WithEvents gbxgenero As GroupBox
    Friend WithEvents rbtnmasculino As RadioButton
    Friend WithEvents rbtnfemenino As RadioButton
    Friend WithEvents lblclasificacion As Label
    Friend WithEvents lblconvenio As Label
    Friend WithEvents lblfechaNacimiento As Label
    Friend WithEvents lblterminosPago As Label
    Friend WithEvents lblaseguradora As Label
    Friend WithEvents txtcodigoCategoria As TextBox
    Friend WithEvents lblcategoria As Label
    Friend WithEvents txtcodigo As TextBox
    Friend WithEvents rtxtdireccion As RichTextBox
    Friend WithEvents txtcorreo As TextBox
    Friend WithEvents lbldireccion As Label
    Friend WithEvents lblcodigo As Label
    Friend WithEvents txttelefonoCasa As TextBox
    Friend WithEvents txtscanId As TextBox
    Friend WithEvents txtnombreCompleto As TextBox
    Friend WithEvents mtxtidentidad As MaskedTextBox
    Friend WithEvents lbltelefonoCasa As Label
    Friend WithEvents lblcorreo As Label
    Friend WithEvents lblscanId As Label
    Friend WithEvents txtcorreo2 As TextBox
    Friend WithEvents lblcorreo2 As Label
    Friend WithEvents txtcelular As TextBox
    Friend WithEvents lblnombre As Label
    Friend WithEvents lblcelular As Label
    Friend WithEvents lblidentidadCliente As Label
    Friend WithEvents txttelefonoTrabajo As TextBox
    Friend WithEvents lbltelefonoTrabajo As Label
    Friend WithEvents txtrtn As TextBox
    Friend WithEvents lblrtn As Label
    Friend WithEvents txtapellido2 As TextBox
    Friend WithEvents lblapellido2 As Label
    Friend WithEvents lblnombre2 As Label
    Friend WithEvents lblapellido1 As Label
    Friend WithEvents txtnombre1 As TextBox
    Friend WithEvents lblnombre1 As Label
    Friend WithEvents txtEdad As TextBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents LinkLabel1 As LinkLabel
End Class
