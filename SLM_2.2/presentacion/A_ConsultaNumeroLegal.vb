﻿Public Class A_ConsultaNumeroLegal
    Private Sub A_ConsultaNumeroLegal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            RegistrarVentanas(nombre_usurio, Me.Name)
            llenarMaquinasLocales()
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try
    End Sub

    Private Sub llenarMaquinasLocales()

        Try

            Dim objSucursal As New ClsMaquinasLocales

            Dim dt As New DataTable
            dt = objSucursal.SeleccionarMaquinasLocales
            cbxMaquinaLocal.DataSource = dt
            cbxMaquinaLocal.DisplayMember = "codigoMaquinasLocales"
            cbxMaquinaLocal.ValueMember = "codigo"

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try

    End Sub

    Private Sub ConsultarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarToolStripMenuItem.Click
        Try
            RegistrarAcciones(nombre_usurio, Me.Name, "Consulto el numero legal " + txtCAI.Text)
            Dim consulta As New ClsMaquinasLocales
            Dim dt As New DataTable
            Dim row As DataRow

            'ULTIMO USADO
            consulta.codigo_ = cbxMaquinaLocal.SelectedValue
            dt = consulta.ConsultaNumeroLegal(1)

            If dt.Rows.Count = 1 Then
                row = dt.Rows(0)
                txtCAI.Text = row("CAI")
                txtUltimo.Text = row("numeroOficial")
            Else
                MsgBox("No se obtuvo el ultimo numero utilizado o no tiene CAI asignado.")
            End If


            'Numero siguiente por usar
            dt = consulta.ConsultaNumeroLegal(0)

            If dt.Rows.Count = 1 Then
                row = dt.Rows(0)
                txtSiguiente.Text = row("numeroOficial")
            Else
                MsgBox("No se obtuvo el numero siguiente a utilizar.")
            End If


        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class