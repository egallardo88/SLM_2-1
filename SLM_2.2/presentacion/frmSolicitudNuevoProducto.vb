﻿Imports System.Net.Mail

Public Class frmSolicitudNuevoProducto
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If validarGuardar("Enviar Requisicion") = "1" Then

            If txtDescripcion.Text = "" Then
                MsgBox("La descripcion no puede quedar en vacia")
                Exit Sub
            End If
            Dim clsR As New ClsRequisicion
            Dim tipo_compra As String = ""
            If RadioButton1.Checked = True Then
                tipo_compra = "Urgente"
            End If
            If RadioButton2.Checked = True Then
                tipo_compra = "Normal"
            End If
            With clsR
                .Descripcion1 = txtDescripcion.Text
                .Cod_usuario1 = codigo_usuario
                .Tipo_compra1 = tipo_compra
            End With

            If clsR.RegistrarRequisicion() = "1" Then
                MsgBox("Se ha enviado la solicitud de requisicion")
                enviarMailNotificacionRequisicion(txtDescripcion.Text)


                Button2.Enabled = False
                    txtDescripcion.Text = ""
                    RadioButton2.Checked = True
                End If

            End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Try
            Dim clsOCOB As New ClsRequisicion
            Dim dvOC As DataView = clsOCOB.RecuperarRequisicionFechas(DateTimePicker3.Value.Date, DateTimePicker4.Value.Date).DefaultView
            DataGridView1.DataSource = dvOC
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        txtDescripcion.Text = ""
        Button2.Enabled = True
    End Sub

    Private Sub frmSolicitudNuevoProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(DataGridView1)
        Label1.Text = "*Instrucciones, en la parte superior haga click en nuevo, a continuación agregue una descripción del producto nuevo que será solicitado,"
        Label15.Text = "ingrese su nombre y el área que esta realizando esta solicitud."
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        txtCodUsuario.Text = nombre_usurio
        Label13.Text = ""
        Label12.Text = ""
        Label14.Text = ""
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            codigo_requisicion = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            Dim objP As New ClsRequisicion

            Dim dt As New DataTable
            dt = objP.RecuperarRequisicionCodigo(DataGridView1.Rows(e.RowIndex).Cells(0).Value)
            Dim row As DataRow = dt.Rows(0)
            Label14.Text = CStr(row("estado"))
            Label12.Text = CStr(row("usuario_aprobo"))
            RichTextBox1.Text = CStr(row("comentario_rechazo"))
            RichTextBox2.Text = CStr(row("descripcion"))
            Label13.Text = CStr(row("fecha_autorizacion"))

        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)

        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)

    End Sub
    Function GridAExcel(ByVal miDataGridView As DataGridView) As Boolean
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet
        Try
            exLibro = exApp.Workbooks.Add 'crea el libro de excel 
            exHoja = exLibro.Worksheets.Add() 'cuenta filas y columnas
            Dim NCol As Integer = miDataGridView.ColumnCount
            Dim NRow As Integer = miDataGridView.RowCount
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = miDataGridView.Columns(i - 1).Name.ToString
            Next
            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = miDataGridView.Rows(Fila).Cells(Col).Value
                Next
            Next
            exHoja.Rows.Item(1).Font.Bold = 1 'titulo en negritas
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            'alineacion al centro
            exHoja.Columns.AutoFit() 'autoajuste de la columna
            exHoja.Columns.HorizontalAlignment = 2
            exApp.Application.Visible = True
            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing
        Catch ex As Exception
            RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
            Return False
        End Try
        Return True
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs)
        reporteRequisicion.Show()
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        txtDescripcion.Text = ""
        Button2.Enabled = True
        txtDescripcion.ReadOnly = False

    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        GridAExcel(DataGridView1)
    End Sub
    Public Sub enviarMailNotificacionRequisicion(ByVal producto As String)

        'In the shadows of the moon
        'enviarMailResultado("sinergia@laboratoriosmedicos.hn", "Lmsinergia2020", "587", True, "mail.laboratoriosmedicos.hn", "erickgallardo89@yahoo.com", "Resultados")
        Dim correoSalida As String = "sinergia@laboratoriosmedicos.hn"
        Dim pass As String = "Lmsinergia2020"
        Dim puerto As String = "587"
        Dim sslOK As Boolean = True
        Dim host As String = "mail.laboratoriosmedicos.hn"
        Dim texto As String = "Resultados "

        Try
            Dim objP As New clsCorreoResultado

            Dim dt As New DataTable
            dt = objP.BuscarCorreo()
            Dim row As DataRow = dt.Rows(0)
            correoSalida = CStr(row("correo"))
            host = CStr(row("host"))
            pass = CStr(row("pass"))
            puerto = CStr(row("puerto"))
            sslOK = CBool(row("ssl"))


        Catch ex As Exception
            'RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)



        End Try

        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(correoSalida, pass)
            Smtp_Server.Port = puerto
            Smtp_Server.EnableSsl = sslOK
            Smtp_Server.Host = host

            e_mail = New MailMessage()
            'txtfrom.text
            e_mail.From = New MailAddress(correoSalida)
            'txtto.text
            e_mail.To.Add(RecuperarCorreos)
            e_mail.Subject = "SLM - Notificación de solicitud de requisición  "

            '  Dim archivos As String = Path.Combine(Application.StartupPath, "Resultados\resultado" + id_orden.ToString + ".pdf")

            ' Dim archivoAdjunto As New System.Net.Mail.Attachment(archivos)

            ' e_mail.Attachments.Add(archivoAdjunto)
            e_mail.IsBodyHtml = True
            'txtMessage.text
            Dim body As String
            body = "<p>Usted tiene una nueva solicitud de requisición  </p>

<p>" + producto + "</p>
<p>Este mensaje fue generado en SLM el " + Date.Now.ToString + ".</p>"
            e_mail.Body = body
            Smtp_Server.Send(e_mail)

            'omitir mensaje
            ' MsgBox("Mail Enviado")

        Catch ex As Exception
            ' RegistrarExcepciones(nombre_usurio, Me.Name, ex.ToString)

            MsgBox("No se envío el correo. " + ex.Message)
        End Try

    End Sub

    Private Sub ToolStripTextBox1_TextChanged(sender As Object, e As EventArgs) Handles ToolStripTextBox1.TextChanged

    End Sub
End Class