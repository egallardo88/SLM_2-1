﻿Imports System.Data.SqlClient

Public Class E_frmImpresoras
    Private Sub E_frmImpresoras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        alternarColoFilasDatagridview(DataGridView1)
        ColoresForm(Panel1, StatusStrip1)
        MenuStrip_slm(MenuStrip1)
        Dim clsP As New clsImpresora
        cargarData()
        ComboAlmacen()
        Me.Refresh()
    End Sub
    Private Sub ComboAlmacen()


        Dim ds As New DataTable
        Dim clsi As New clsImpresora



        ComboBox1.DataSource = clsi.RecuperarSucursal().DefaultView
        ComboBox1.DisplayMember = "nombre"
        ComboBox1.ValueMember = "codigo"

    End Sub
    Private Sub cargarData()
        Dim clsp As New clsImpresora
        Try
            'datagridview

            DataGridView1.DataSource = clsp.RecuperarImpresoras().DefaultView

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs)
        Try
            btnGuardar.Enabled = True
            TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtAlias.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            txtNombre.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        btnGuardar.Enabled = True
        txtAlias.Text = ""
        txtNombre.Text = ""
        TextBox1.Clear()
    End Sub

    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If TextBox1.Text = "" Then
            Dim cls As New clsImpresora
            With cls
                .Aliasprint1 = txtAlias.Text
                .Nombre1 = txtNombre.Text
                .Sucursal1 = ComboBox1.SelectedValue
            End With

            If cls.RegistrarImpresora() = "1" Then
                MsgBox(mensaje_registro)
                cargarData()
                txtAlias.Clear()
                txtNombre.Clear()
                TextBox1.Clear()
            End If
        Else
            MsgBox("Debe crear un nuevo registro")
        End If
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim clsp As New clsImpresora
        With clsp
            .Id1 = TextBox1.Text
        End With
        If clsp.EliminarImpresora() = "1" Then
            MsgBox(mensaje_dar_baja)
        End If
        cargarData()
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged

        Dim clsp As New clsImpresora
        Try






            Dim dv As DataView = clsp.RecuperarImpresoras().DefaultView

            dv.RowFilter = String.Format("CONVERT(alias+nombre+sucursal, System.String) LIKE '%{0}%'", TextBox2.Text)

            DataGridView1.DataSource = dv
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_CellClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        'CARGAR DATOS DE LAS IMPRESORAS
        Try
            TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            txtAlias.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            txtNombre.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            ComboBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ModificarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ModificarToolStripMenuItem.Click
        If TextBox1.Text <> "" Then
            Dim cls As New clsImpresora
            With cls
                .Id1 = TextBox1.Text
                .Aliasprint1 = txtAlias.Text
                .Nombre1 = txtNombre.Text
                .Sucursal1 = ComboBox1.SelectedValue
            End With

            If cls.ModificarImpresora = "1" Then
                MsgBox(mensaje_actualizacion)
                cargarData()
                txtAlias.Clear()
                txtNombre.Clear()
                TextBox1.Clear()
            End If
        Else
            MsgBox("Debe crear un nuevo registro")
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text <> "" Then
            ModificarToolStripMenuItem.Enabled = True
            btnGuardar.Enabled = False
        Else
            ModificarToolStripMenuItem.Enabled = False
            btnGuardar.Enabled = True
        End If
    End Sub
End Class