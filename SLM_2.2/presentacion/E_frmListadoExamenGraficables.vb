﻿Imports System.Data.SqlClient

Public Class E_frmListadoExamenGraficables
    Private Sub E_frmListadoExamenGraficables_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarData()
        cargarData2()
        ColoresForm(Panel1, StatusStrip1)
    End Sub
    Public Function listarExamenes() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter(" E_slmListarExamenesTubos", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function

    Public Function listarExamenes2() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter(" select * from ListadoExamenesGraficables  ", cn)
            Dim dt As New DataTable
            da.Fill(dt)

            Return dt
            objCon.cerrarConexion()
            da.Dispose()
        End Using
        objCon.cerrarConexion()
    End Function
    Public Sub cargarData()
        Try


            BindingSource1.DataSource = listarExamenes()

            DataGridView1.DataSource = BindingSource1


            BindingNavigator1.BindingSource = BindingSource1



        Catch ex As Exception
            MsgBox("Hubo un error al consultar. " + ex.Message)
        End Try
    End Sub

    Public Sub cargarData2()
        Try


            BindingSource2.DataSource = listarExamenes2()

            DataGridView2.DataSource = BindingSource2


            BindingNavigator2.BindingSource = BindingSource2



        Catch ex As Exception
            MsgBox("Hubo un error al consultar. " + ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Try
            'codigo examen
            TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            TextBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            'nombre examen

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles TextBox4.TextChanged
        Try

            'BindingSource1.DataSource = listarExamenes()

            'DataGridView1.DataSource = BindingSource1


            'BindingNavigator1.BindingSource = BindingSource1


            Dim dvOC As DataView = listarExamenes().DefaultView


            dvOC.RowFilter = String.Format("CONVERT(descripcion, System.String) LIKE '%{0}%'", TextBox4.Text)
            DataGridView1.DataSource = dvOC
            BindingSource1.DataSource = dvOC
            BindingNavigator1.BindingSource = BindingSource1
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If RegistrarExamen() = "1" Then
                MsgBox(mensaje_registro)
                cargarData2()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Function EliminarParametro(ByVal id As String) As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmEliminarExamenListadoGrafica"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id"
        sqlpar.Value = id
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function RegistrarExamen() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCrearExamenListadoGrafica"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cod_examen"
        sqlpar.Value = TextBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre_examen"
        sqlpar.Value = TextBox2.Text
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "categoria"
        sqlpar.Value = ComboBox1.Text
        sqlcom.Parameters.Add(sqlpar)

        'sqlpar = New SqlParameter
        'sqlpar.ParameterName = "categoria"
        'sqlpar.Value = ComboBox1.Text
        'sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Private Sub DataGridView2_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick
        If EliminarParametro(DataGridView2.Rows(e.RowIndex).Cells(1).Value) Then
            MsgBox(mensaje_dar_baja)
            cargarData2()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        E_frmListarOrdenesGraficas.Show()
    End Sub
End Class