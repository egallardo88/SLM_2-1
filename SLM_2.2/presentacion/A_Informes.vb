﻿
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.CrystalReports.Engine

Public Class A_Informes
    Dim seleccion As Integer
    Private Sub btnBuscarSubArea_Click(sender As Object, e As EventArgs) Handles btnBuscarSubArea.Click
        Try
            E_ListarSubAreas.lblform.Text = "informe"
            E_ListarSubAreas.Show()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSubArea_TextChanged(sender As Object, e As EventArgs) Handles txtSubArea.TextChanged

        Try
            If txtSubArea.Text = "" Then

                txtNombreSubArea.Text = ""
                lblCodSubArea.Text = ""
            Else
                Dim subarea As New ClsSubArea
                Dim dt As New DataTable
                Dim row As DataRow

                subarea.Cod_SubArea = txtSubArea.Text

                dt = subarea.BuscarSubAreaCodigoBreve
                row = dt.Rows(0)

                lblCodSubArea.Text = row("codSubArea")
                'txtSubArea.Text = row("SubArea")
                txtNombreSubArea.Text = row("nombre")

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnBuscarExamen_Click(sender As Object, e As EventArgs) Handles btnBuscarExamen.Click
        Try
            'A_ListadoExamenes.Show()
            MostrarForm(M_ListadoItemExamenes)
            M_ListadoItemExamenes.lblform.Text = "Informe"
        Catch ex As Exception

        End Try
    End Sub

    'Función para imprimir informe
    Sub ImprimirInformePeriodo()
        If seleccion = 0 Then

            Try
                Dim codExamen As Integer
                'Dim fecha As DateTime
                Dim fechadesde, fechahasta As DateTime
                Dim objInformeDiarioExamen As New InformeOrdenesDeTrabajoExamen

                ' objInformeDiarioExamen.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
                codExamen = Convert.ToInt32(lblCodExamen.Text)
                fechadesde = dtpDesde.Value
                fechahasta = dtpHasta.Value

                objInformeDiarioExamen.SetParameterValue("@codExamen", codExamen)
                'objInformeDiarioExamen.SetParameterValue("@fecha", fecha)
                objInformeDiarioExamen.SetParameterValue("@fechadesde", fechadesde)
                objInformeDiarioExamen.SetParameterValue("@fechahasta", fechahasta)

                'objInformeDiarioExamen.DataSourceConnections.Item(0).SetConnection("SRV-SQL-LM", "slm_test", False)
                'objInformeDiarioExamen.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")

                objInformeDiarioExamen.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

                A_PrintInforme.crvInformeOrdenesTrabajo.ReportSource = objInformeDiarioExamen

            Catch ex As Exception
                MsgBox("Error Informe: " + ex.Message)
            End Try

        ElseIf seleccion = 1 Then

            Try
                Dim codSubArea As Integer
                Dim fechadesde, fechahasta As DateTime
                Dim objInformeDiario As New InformeOrdenesdeTrabajoPeriodo

                codSubArea = Convert.ToInt32(lblCodSubArea.Text)
                fechadesde = dtpDesde.Value
                fechahasta = dtpHasta.Value

                objInformeDiario.SetParameterValue("@codigoSubArea", codSubArea)
                objInformeDiario.SetParameterValue("@desde", fechadesde)
                objInformeDiario.SetParameterValue("@hasta", fechahasta)

                objInformeDiario.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
                A_PrintInforme.crvInformeOrdenesTrabajo.ReportSource = objInformeDiario

            Catch ex As Exception
                MsgBox("Error Informe: " + ex.Message)
            End Try

        ElseIf seleccion = 2 Then

            Try
                Dim codigoArea As Integer
                Dim fechadesde, fechahasta As DateTime
                Dim objInformeDiario As New InformeOrdenesDeTrabajoArea

                codigoArea = Convert.ToInt32(lblCodArea.Text)
                fechadesde = dtpDesde.Value
                fechahasta = dtpHasta.Value

                objInformeDiario.SetParameterValue("@Area", codigoArea)
                objInformeDiario.SetParameterValue("@desde", fechadesde)
                objInformeDiario.SetParameterValue("@hasta", fechahasta)

                objInformeDiario.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
                A_PrintInforme.crvInformeOrdenesTrabajo.ReportSource = objInformeDiario
            Catch ex As Exception

            End Try

        End If



    End Sub

    Private Sub btnEjecutar_Click(sender As Object, e As EventArgs) Handles btnEjecutar.Click


        Try

            Dim n As String = MsgBox("¿Desea ejecutar la consulta?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                If chkExamenes.Checked = True Then 'FILTRO POR EXAMEN Y FECHA

                    seleccion = 0

                    If lblCodExamen.Text = "CodExamen" Then
                        MsgBox("Se debe completar la información para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If
                ElseIf chkPeriodoTiempo.Checked = True Then 'FILTRO POR SUBAREA Y PERIODO DE TIEMPO
                    seleccion = 1
                    If lblCodSubArea.Text = "CodSubArea" Then
                        MsgBox("Se debe completar la información para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If

                ElseIf chkArea.Checked = True Then

                    seleccion = 2

                    If txtArea.Text = "CodArea" Then

                        MsgBox("Se debe seleccionar un area para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If



                End If


            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub chkExamenes_CheckedChanged(sender As Object, e As EventArgs) Handles chkExamenes.CheckedChanged
        If chkExamenes.Checked = True Then

            'opciones de examenes
            btnBuscarExamen.Enabled = True
            txtCodExamen.Enabled = True
            'dtpFecha.Enabled = True
            'dtpDesde.Enabled = True
            'dtpHasta.Enabled = True

            chkPeriodoTiempo.Checked = False
            chkArea.Checked = False

            Limpiar()

        Else
            btnBuscarExamen.Enabled = False
            txtCodExamen.Enabled = False
            'dtpDesde.Enabled = False
            'dtpHasta.Enabled = False
            Limpiar()
        End If
    End Sub

    Private Sub chkPeriodoTiempo_CheckedChanged(sender As Object, e As EventArgs) Handles chkPeriodoTiempo.CheckedChanged
        If chkPeriodoTiempo.Checked = True Then

            btnBuscarSubArea.Enabled = True
            txtSubArea.Enabled = True
            'dtpDesde.Enabled = True
            'dtpHasta.Enabled = True

            chkExamenes.Checked = False
            chkArea.Checked = False

            Limpiar()

        Else
            btnBuscarSubArea.Enabled = False
            txtSubArea.Enabled = False
            'dtpDesde.Enabled = False
            'dtpHasta.Enabled = False
            Limpiar()
        End If
    End Sub


    Sub Limpiar()

        'AREA
        txtArea.Clear()
        txtNombreArea.Clear()
        'SUBAREA
        txtSubArea.Clear()
        txtNombreSubArea.Clear()
        'EXAMEN
        txtCodExamen.Clear()
        txtNombreExamen.Clear()

        dtpFecha.Value = DateTime.Now
        dtpDesde.Value = DateTime.Now
        dtpHasta.Value = DateTime.Now

    End Sub

    Private Sub A_Informes_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If (e.KeyCode = Keys.Escape) Then
            Me.Close()
        End If
    End Sub

    Private Sub A_Informes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ColoresForm(Panel1, StatusStrip1)
        RegistrarVentanas(nombre_usurio, Me.Name)
    End Sub

    Private Sub txtArea_TextChanged(sender As Object, e As EventArgs) Handles txtArea.TextChanged
        Try
            If txtArea.Text = "" Then

                txtNombreArea.Text = ""
                lblCodArea.Text = ""
            Else
                Dim area As New ClsGrupoExamen
                Dim dt As New DataTable
                Dim row As DataRow

                area.codigoGrupoExamen_ = txtArea.Text

                dt = area.BuscarGrupoExamenCodigoBreve
                row = dt.Rows(0)

                lblCodArea.Text = row("codigo")
                'txtSubArea.Text = row("SubArea")
                txtNombreArea.Text = row("nombre")

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkArea_CheckedChanged(sender As Object, e As EventArgs) Handles chkArea.CheckedChanged
        Try
            If chkArea.Checked = True Then

                btnBuscarArea.Enabled = True
                txtArea.Enabled = True
                'dtpDesde.Enabled = True
                'dtpHasta.Enabled = True


                chkPeriodoTiempo.Checked = False
                chkExamenes.Checked = False

                Limpiar()

            Else
                btnBuscarArea.Enabled = False
                txtArea.Enabled = False
                'dtpDesde.Enabled = False
                'dtpHasta.Enabled = False
                Limpiar()
            End If




        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnBuscarArea_Click(sender As Object, e As EventArgs) Handles btnBuscarArea.Click
        Try
            MostrarForm(E_GrupoExamen)
            E_GrupoExamen.lblform.Text = "Informe"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub EjecutarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EjecutarToolStripMenuItem.Click
        Try

            Dim n As String = MsgBox("¿Desea ejecutar la consulta?", MsgBoxStyle.YesNo, "Validación")
            If n = vbYes Then
                If chkExamenes.Checked = True Then 'FILTRO POR EXAMEN Y FECHA

                    seleccion = 0

                    If lblCodExamen.Text = "CodExamen" Then
                        MsgBox("Se debe completar la información para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If
                ElseIf chkPeriodoTiempo.Checked = True Then 'FILTRO POR SUBAREA Y PERIODO DE TIEMPO
                    seleccion = 1
                    If lblCodSubArea.Text = "CodSubArea" Then
                        MsgBox("Se debe completar la información para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If

                ElseIf chkArea.Checked = True Then

                    seleccion = 2

                    If txtArea.Text = "CodArea" Then

                        MsgBox("Se debe seleccionar un area para generar el informe.")
                    Else
                        A_PrintInforme.lblform.Text = "Informe"
                        A_PrintInforme.Show()
                    End If



                End If


            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class