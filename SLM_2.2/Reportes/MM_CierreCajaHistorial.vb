﻿Public Class MM_CierreCajaHistorial
    Private Sub MM_CierreCajaHistorial_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            alternarColoFilasDatagridview(dgvData)
            llenarUsuarios()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenarMaquinasLocales()

        Try

            Dim objSucursal As New ClsMaquinasLocales

            Dim dt As New DataTable
            dt = objSucursal.BuscarMaquinasLocalesCajero(Integer.Parse(cbxCajero.SelectedValue), dtpFechaDesde.Value)
            cbxMaquinaLocal.DataSource = dt
            cbxMaquinaLocal.DisplayMember = "Sucursal"
            cbxMaquinaLocal.ValueMember = "codigo"

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try

    End Sub

    Private Sub llenarUsuarios()

        Try

            Dim objSucursal As New ClsUsuario

            Dim dt As New DataTable
            dt = objSucursal.listarCajeros
            cbxCajero.DataSource = dt
            cbxCajero.DisplayMember = "usuario"
            cbxCajero.ValueMember = "codigoCajero"

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "Validacion")
        End Try

    End Sub
    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If (e.KeyCode = Keys.Escape) Then

                Me.Close()

            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub ConsultarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarToolStripMenuItem.Click
        Try

            Dim cierre As New ClsArqueos
            Dim fecha As Date = dtpFechaDesde.Value
            cierre.codigoCajero_ = cbxCajero.SelectedValue

            dgvData.DataSource = cierre.CierreCajaHistorial2(fecha, Integer.Parse(cbxMaquinaLocal.SelectedValue))

            SumarCheque()
            SumarDeposito()
            SumarEfectivo()
            SumarSaldoPendiente()
            SumarTarjeta()
            SumarTotales()
            SumarTransferencia()
            SumaCaja()
            SumarSubtotal()

            txtEfectivo.Text = Convert.ToDecimal(txtEfectivo.Text).ToString("N2")
            txtTransferencia.Text = Convert.ToDecimal(txtTransferencia.Text).ToString("N2")
            txtTarjeta.Text = Convert.ToDecimal(txtTarjeta.Text).ToString("N2")
            txtCheque.Text = Convert.ToDecimal(txtCheque.Text).ToString("N2")
            txtDeposito.Text = Convert.ToDecimal(txtDeposito.Text).ToString("N2")
            txtSubtotal.Text = Convert.ToDecimal(txtSubtotal.Text).ToString("N2")
            txtTotal.Text = Convert.ToDecimal(txtTotal.Text).ToString("N2")
            txtSaldo.Text = Convert.ToDecimal(txtSaldo.Text).ToString("N2")
            txtCajaTotal.Text = Convert.ToDecimal(txtCajaTotal.Text).ToString("N2")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub dgbtabla_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvData.CellMouseDoubleClick
        Try
            'MOSTRAR DATOS DE LA FACTURA
            Dim objFact As New ClsFactura
            Dim n As String = ""
            'If lblForm.Text = "M_DiarioFacturacion" Then
            If e.RowIndex >= 0 Then
                n = MsgBox("¿Desea ver la factura?", MsgBoxStyle.YesNo, "Validación")
            End If
            If n = vbYes Then
                M_Factura.limpiar()
                'Dim objFact As New ClsFactura
                objFact.numero_ = dgvData.Rows(e.RowIndex).Cells(0).Value()

                Dim dt As New DataTable
                dt = objFact.BuscarFacturaNumero()
                Dim row As DataRow = dt.Rows(0)

                M_Factura.txtnumeroFactura.Text = CStr(row("numero"))
                M_Factura.txtnumeroOficial.Text = CStr(row("numeroOficial"))
                M_Factura.dtpfechaFactura.Value = CStr(row("fechaFactura"))
                M_Factura.txtcodigoCliente.Text = CStr(row("codigoCliente"))
                M_Factura.txtcodigoRecepecionista.Text = CStr(row("codigoRecepcionista"))
                M_Factura.txtcodigoMedico.Text = CStr(row("codigoMedico"))
                M_Factura.txtcodigoCajero.Text = CStr(row("codigoCajero"))
                M_Factura.lblcodeTerminoPago.Text = CStr(row("codigoTerminoPago"))
                M_Factura.txtcodigoSede.Text = CStr(row("codigoSede"))
                M_Factura.dtpfechaVto.Value = CStr(row("fechaVto"))
                M_Factura.lblcodeSucursal.Text = CStr(row("codigoSucursal"))

                'M_Factura.lblcodePriceList.Text = CStr(row("codigoConvenio"))
                'M_Factura.txtcodigoConvenio.Text = CStr(row("codigoConvenio"))

                M_Factura.txtnumeroPoliza.Text = CStr(row("numeroPoliza"))
                M_Factura.txtcodigoTerminal.Text = CStr(row("codigoTerminal"))
                M_Factura.lblcodeSucursal.Text = CStr(row("codigoSucursal"))
                M_Factura.cbxentregarMedico.Checked = CStr(row("entregaMedico"))
                M_Factura.cbxentregarPaciente.Checked = CStr(row("entregaPaciente"))
                M_Factura.cbxenviarCorreo.Checked = CStr(row("enviarEmail"))
                M_Factura.txtpagoPaciente.Text = CStr(row("pagoPaciente"))
                M_Factura.txtEfectivo.Text = CStr(row("ingresoEfectivo"))
                M_Factura.txtTarjeta.Text = CStr(row("ingresoTarjeta"))
                M_Factura.txtvuelto.Text = CStr(row("vuelto"))
                M_Factura.txttotal.Text = CStr(row("total"))
                M_Factura.cbxok.Checked = CStr(row("ok"))
                M_Factura.cbxAnular.Checked = CStr(row("estado"))

                Dim objDetFact As New ClsDetalleFactura
                objDetFact.numeroFactura_ = dgvData.Rows(e.RowIndex).Cells(0).Value()
                dt = objDetFact.BuscarDetalleFacturaIngresada()
                For index As Integer = 0 To dt.Rows.Count - 1
                    row = dt.Rows(index)
                    M_Factura.dgblistadoExamenes.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("subtotal")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal")), CStr(row("subArea")), CStr(row("numero")), CStr(row("codigoExamen")), CStr(row("id_centrocosto"))})
                    M_ClienteVentana.dgvtabla.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("cantidad")), CStr(row("subtotal")), CStr(row("descripcion")), CStr(row("fechaEntrega")), CStr(row("descuento")), CStr(row("subtotal"))})

                    'OBSERVACIONES
                    M_Factura.dgbObservaciones.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("observaciones"))})
                    M_Factura.dgbObservaciones2.Rows.Add(New String() {CStr(row("codInterno")), CStr(row("observaciones2"))})
                Next

                M_Factura.deshabilitar()
                If (M_Factura.cbxok.Checked = "0") Then
                    M_Factura.HabilitarActualizarFactura()
                Else
                    M_Factura.btnActualizar.Enabled = True
                End If
                'Me.Close()
                'txtnombreB.Text = ""
                'txtnumeroB.Text = ""
                'Me.Hide()
                M_Factura.cambiarFormatoMoneda()
                M_Factura.banderaTipo = True
                MostrarForm(M_Factura)
            End If
            ' End If
        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Sub SumarEfectivo()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(2).Value)

        Next

        txtEfectivo.Text = totals

    End Sub

    Sub SumarDeposito()


        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(4).Value)

        Next

        txtDeposito.Text = totals


    End Sub

    Sub SumarTarjeta()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(3).Value)

        Next

        txtTarjeta.Text = totals

    End Sub

    Sub SumarTransferencia()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(5).Value)

        Next

        txtTransferencia.Text = totals

    End Sub

    Sub SumarCheque()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(6).Value)

        Next

        txtCheque.Text = totals

    End Sub

    Sub SumarTotales()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(8).Value)

        Next

        txtTotal.Text = totals

    End Sub

    Sub SumarSaldoPendiente()

        Dim totals As Double = 0

        For i = 0 To dgvData.Rows.Count - 1

            totals += Double.Parse(dgvData.Rows(i).Cells(9).Value)

        Next

        txtSaldo.Text = totals

    End Sub

    Sub SumaCaja()

        txtCajaTotal.Text = Double.Parse(txtEfectivo.Text) + Double.Parse(txtTarjeta.Text) + Double.Parse(txtDeposito.Text) + Double.Parse(txtTransferencia.Text) + Double.Parse(txtCheque.Text)

    End Sub

    Sub SumarSubtotal()
        txtSubtotal.Text = Double.Parse(txtTotal.Text) - Double.Parse(txtSaldo.Text)
    End Sub

    Private Sub ExportarExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarExcelToolStripMenuItem.Click
        Try
            E_frmInventario.GridAExcel(dgvData)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ImprimirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirToolStripMenuItem.Click
        Try
            'Dim objReporte As New M_HistorialCierreCaja

            'objReporte.SetParameterValue("@codigoCajero", Convert.ToInt64(cbxCajero.SelectedValue))
            'objReporte.SetParameterValue("@fecha", Convert.ToDateTime(dtpFechaDesde.Value))
            'objReporte.SetParameterValue("Cajero", cbxCajero.SelectedText.ToString)
            'objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            'M_ImprimirCotizacionForm.CrystalReportViewer1.ReportSource = objReporte



            Dim objReporte As New CierreCajaM
            objReporte.SetParameterValue("@codigoCajero", Convert.ToInt64(cbxCajero.SelectedValue))
            objReporte.SetParameterValue("@codigoTerminal", Integer.Parse(cbxMaquinaLocal.SelectedValue))
            objReporte.SetParameterValue("@fecha", Convert.ToDateTime(dtpFechaDesde.Value))
            objReporte.SetParameterValue("Cajero", cbxCajero.SelectedText.ToString)
            objReporte.DataSourceConnections.Item(0).SetLogon("sa", "Lbm2019")
            M_ImprimirCotizacionForm.CrystalReportViewer1.ReportSource = objReporte
            M_ImprimirCotizacionForm.Show()

            MostrarForm(M_ImprimirCotizacionForm)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cbxCajero_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCajero.SelectedValueChanged
        llenarMaquinasLocales()
    End Sub
End Class

