﻿Public Class E_frmReporteOrdenTrabajoResultados
    Private Sub E_frmReporteOrdenTrabajoResultados_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim codOrden As Integer


        Try

            codOrden = Integer.Parse(E_HojaTrabajo.txtOrden.Text)

            If lblform.Text = "Previa" Then


                Dim report As New ResultadoHojaTrabajo

                report.SetParameterValue("@id_orden", codOrden)
                report.SetParameterValue("@cod_orden_trabajo", codOrden)
                report.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

                CrystalReportViewer1.ReportSource = report
            Else


                Dim RptDocument As New E_ReporteResultadoIndividual

                Dim var As New E_frmGraficaEstatica
                RptDocument.SetParameterValue("@id_orden", id_factura_erick_ordentrabajo)
                RptDocument.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

                CrystalReportViewer1.ReportSource = RptDocument

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class