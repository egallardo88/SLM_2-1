﻿
Imports System.Data.SqlClient
Public Class ClsFacturaEmpresarial
    Dim numero, codigoClienteEmpresarial, codigoMedico, codigoSede, codigoRecepcionista, codigoCajero, codigoTerminoPago, codFactura, codPaciente As Integer
    Dim numeroOficial, numeroPoliza, nombreCliente, empresa As String
    Dim codigoSucursal, codigoTerminal As Integer
    Dim fechaFactura, fechaVto As Date
    Dim ok, enviarEmail, entregaPaciente, entregaMedico, estado, segundok, agregarCola As Boolean
    Dim pagoPaciente, vuelto, total, ingresoEfectivo, ingresoTarjeta, saldoPendiente As Double

    Dim cheque, transferencia, deposito As Double
    'Constructor
    Public Sub New()

    End Sub
    '::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: METODOS SET Y GET
    Public Property numero_ As Integer
        Get
            Return numero
        End Get
        Set(value As Integer)
            numero = value
        End Set
    End Property
    Public Property codigoClienteEmpresarial_ As Integer
        Get
            Return codigoClienteEmpresarial
        End Get
        Set(value As Integer)
            codigoClienteEmpresarial = value
        End Set
    End Property
    Public Property codigoMedico_ As Integer
        Get
            Return codigoMedico
        End Get
        Set(value As Integer)
            codigoMedico = value
        End Set
    End Property
    Public Property nombreCliente_ As String
        Get
            Return nombreCliente
        End Get
        Set(value As String)
            nombreCliente = value
        End Set
    End Property
    Public Property saldoPendiente_ As Double
        Get
            Return saldoPendiente
        End Get
        Set(value As Double)
            saldoPendiente = value
        End Set
    End Property
    Public Property codigoTerminoPago1 As Integer
        Get
            Return codigoTerminoPago
        End Get
        Set(value As Integer)
            codigoTerminoPago = value
        End Set
    End Property
    Public Property numeroOficial_ As String
        Get
            Return numeroOficial
        End Get
        Set(value As String)
            numeroOficial = value
        End Set
    End Property
    Public Property codigoRecepcionista_ As Integer
        Get
            Return codigoRecepcionista
        End Get
        Set(value As Integer)
            codigoRecepcionista = value
        End Set
    End Property
    Public Property codigoCajero_ As Integer
        Get
            Return codigoCajero
        End Get
        Set(value As Integer)
            codigoCajero = value
        End Set
    End Property
    Public Property codigoSede_ As Integer
        Get
            Return codigoSede
        End Get
        Set(value As Integer)
            codigoSede = value
        End Set
    End Property
    Public Property codigoSucursal_ As Integer
        Get
            Return codigoSucursal
        End Get
        Set(value As Integer)
            codigoSucursal = value
        End Set
    End Property

    Public Property numeroPoliza_ As String
        Get
            Return numeroPoliza
        End Get
        Set(value As String)
            numeroPoliza = value
        End Set
    End Property
    Public Property codigoTerminal_ As Integer
        Get
            Return codigoTerminal
        End Get
        Set(value As Integer)
            codigoTerminal = value
        End Set
    End Property
    Public Property fechaFactura_ As Date
        Get
            Return fechaFactura
        End Get
        Set(value As Date)
            fechaFactura = value
        End Set
    End Property
    Public Property fechaVto_ As Date
        Get
            Return fechaVto
        End Get
        Set(value As Date)
            fechaVto = value
        End Set
    End Property
    Public Property ok_ As Boolean
        Get
            Return ok
        End Get
        Set(value As Boolean)
            ok = value
        End Set
    End Property
    Public Property segundok_ As Boolean
        Get
            Return segundok
        End Get
        Set(value As Boolean)
            segundok = value
        End Set
    End Property
    Public Property estado_ As Boolean
        Get
            Return estado
        End Get
        Set(value As Boolean)
            estado = value
        End Set
    End Property
    Public Property enviarEmail_ As Boolean
        Get
            Return enviarEmail
        End Get
        Set(value As Boolean)
            enviarEmail = value
        End Set
    End Property
    Public Property entregaPaciente_ As Boolean
        Get
            Return entregaPaciente
        End Get
        Set(value As Boolean)
            entregaPaciente = value
        End Set
    End Property
    Public Property entregaMedico_ As Boolean
        Get
            Return entregaMedico
        End Get
        Set(value As Boolean)
            entregaMedico = value
        End Set
    End Property

    Public Property ingresoEfectivo_ As Double
        Get
            Return ingresoEfectivo
        End Get
        Set(value As Double)
            ingresoEfectivo = value
        End Set
    End Property
    Public Property ingresoTarjeta_ As Double
        Get
            Return ingresoTarjeta
        End Get
        Set(value As Double)
            ingresoTarjeta = value
        End Set
    End Property
    Public Property pagoPaciente_ As Double
        Get
            Return pagoPaciente
        End Get
        Set(value As Double)
            pagoPaciente = value
        End Set
    End Property

    Public Property total_ As Double
        Get
            Return total
        End Get
        Set(value As Double)
            total = value
        End Set
    End Property
    Public Property vuelto_ As Double
        Get
            Return vuelto
        End Get
        Set(value As Double)
            vuelto = value
        End Set
    End Property
    Public Property cheque_ As Double
        Get
            Return cheque
        End Get
        Set(value As Double)
            cheque = value
        End Set
    End Property
    Public Property deposito_ As Double
        Get
            Return deposito
        End Get
        Set(value As Double)
            deposito = value
        End Set
    End Property
    Public Property transferencia_ As Double
        Get
            Return transferencia
        End Get
        Set(value As Double)
            transferencia = value
        End Set
    End Property

    Public Property codFactura_ As Integer
        Get
            Return codFactura
        End Get
        Set(value As Integer)
            codFactura = value
        End Set
    End Property

    Public Property codPaciente_ As Integer
        Get
            Return codPaciente
        End Get
        Set(value As Integer)
            codPaciente = value
        End Set
    End Property

    Public Property empresa_ As String
        Get
            Return empresa
        End Get
        Set(value As String)
            empresa = value
        End Set
    End Property

    Public Property agregarCola_ As Boolean
        Get
            Return agregarCola
        End Get
        Set(value As Boolean)
            agregarCola = value
        End Set
    End Property

    '::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: FUNCIONES DE MANTENIMIENTO

    'REGISTRAR FACTURA EMPRESARIAL
    Public Function RegistrarNuevaFacturaEmpresarial() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "MANTE_Factura_Empresarial2"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroOficial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroOficial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCliente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoClienteEmpresarial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoRecepcionista" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoRecepcionista_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminoPago" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminoPago1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSede" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSede_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fechaVto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = fechaVto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSucursal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSucursal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroPoliza" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroPoliza_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "enviarEmail" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = enviarEmail_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "pagoPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = pagoPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "vuelto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = vuelto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoEfectivo" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoEfectivo_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoTarjeta" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoTarjeta_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = estado_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "deposito" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = deposito_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "transferencia" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = transferencia_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cheque" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = cheque_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "saldoPendiente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = saldoPendiente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "segundok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = segundok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "accion"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "agregarCola"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function


    'REGISTRAR FACTURA PACIENTE EMPRESARIAL
    Public Function RegistrarNuevaFacturaEmpresarialPaciente() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "MANTE_Factura_Empresarial"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroOficial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroOficial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCliente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoClienteEmpresarial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoRecepcionista" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoRecepcionista_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminoPago" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminoPago1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSede" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSede_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fechaVto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = fechaVto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSucursal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSucursal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroPoliza" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroPoliza_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "enviarEmail" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = enviarEmail_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "pagoPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = pagoPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "vuelto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = vuelto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoEfectivo" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoEfectivo_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoTarjeta" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoTarjeta_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = estado_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "deposito" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = deposito_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "transferencia" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = transferencia_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cheque" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = cheque_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "saldoPendiente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = saldoPendiente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codFactura" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codFactura_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "agregarCola"
        sqlpar.Value = agregarCola_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "accion"
        sqlpar.Value = 2
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function


    Public Function ActualizarFacturaEmpresarial() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "M_slmModificarFacturaEmpresarial"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroOficial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroOficial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "enviarEmail" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = enviarEmail_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "pagoPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = pagoPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "vuelto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = vuelto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoEfectivo" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoEfectivo_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoTarjeta" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoTarjeta_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = estado_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "deposito" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = deposito_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "transferencia" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = transferencia_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cheque" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = cheque_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "saldoPendiente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = saldoPendiente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "segundok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = segundok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "agregarCola" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = agregarCola_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    'MODIFICAR
    Public Function ModificarFacturaEmpresarial() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "MANTE_Factura_Empresarial"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroOficial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroOficial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCliente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoClienteEmpresarial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoRecepcionista" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoRecepcionista_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminoPago" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminoPago1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSede" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSede_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fechaVto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = fechaVto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSucursal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSucursal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numeroPoliza" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numeroPoliza_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaMedico" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaMedico_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "entregaPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = entregaPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "enviarEmail" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = enviarEmail_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "pagoPaciente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = pagoPaciente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "vuelto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = vuelto_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoEfectivo" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoEfectivo_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "ingresoTarjeta" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = ingresoTarjeta_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = estado_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "deposito" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = deposito_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "transferencia" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = transferencia_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cheque" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = cheque_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "saldoPendiente" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = saldoPendiente_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "segundok" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = segundok_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "agregarCola" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = agregarCola_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "accion"
        sqlpar.Value = 1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
    Public Function BuscarFacturaEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "M_slmBuscarFacturaEmpresarial"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function



    'IMPRESION ENCABEZADO Factura EMPRESARIAL
    Public Function BuscarEncabezadoFacturaEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "M_slmBuscarEncabezadoFacturaEmpresarial"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'BUSCAR DATOS DE FACTURA EMPRESARIAL
    Public Function BuscarDatosFacturaEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "AA_slmBuscarFacturaEmpresarial2"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'LISTAR FACTURAS EMPRESARIALES
    Public Function ListarFacturaEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 3
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function
    'LISTAR FACTURAS DE PACIENTE EMPRESARIAL
    Public Function ListarFacturaPacienteEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 4
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'BUSCAR FACTURA EMPRESARIAL
    Public Function BuscarFacturaEmpresarial1() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 5
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'BUSCAR FACTURA EMPRESARIAL POR NOMBRE DE EMPRESA
    Public Function BuscarFacturaEmpresarialPorNombre1() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 8
            cmd.Parameters.Add("@empresa", SqlDbType.VarChar).Value = empresa_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function
    'BUSCAR FACTURA PACIENTE EMPRESARIAL
    Public Function BuscarFacturaPacienteEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 6
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'BUSCAR FACTURA PACIENTE EMPRESARIAL POR FACTURA DE EMPRESA
    Public Function BuscarFacturaPacienteEmpresarialPorEmpresa() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 7
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    'BUSCAR FACTURA PACIENTE EMPRESARIAL POR NOMBRE
    Public Function BuscarFacturaPacienteEmpresarialPorNombre() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "MANTE_Factura_Empresarial2"
            cmd.Parameters.Add("@accion", SqlDbType.Int).Value = 9
            cmd.Parameters.Add("@empresa", SqlDbType.VarChar).Value = empresa_
            cmd.Parameters.Add("@salida", SqlDbType.Int).Value = 0
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function


End Class
