﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Threading
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms

Public Class ClsResultados
    Private id_orden As String

    Public Property Id_orden1 As String
        Get
            Return id_orden
        End Get
        Set(value As String)
            id_orden = value
        End Set
    End Property

    Public Function RecuperarListadoFacturas(ByVal id_factura) As SqlDataReader
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "exec M_slmReporteResultadoFactura " + id_factura + ""
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteReader
    End Function

    Public Function RecuperarListadoFacturasPorID(ByVal identidad) As SqlDataReader
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "exec E_slmReporteResultadoID " + "'" + identidad + "'" + ""
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteReader
    End Function

    Public Function RecuperarToleranciaInsulina(ByVal id_orden As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select i.codigo,h.hora, ot.cod_orden_trabajo,ie.descripcion,i.nombre,
ot.resultado as resultado  from OrdenTrabajoDetalle ot, ItemExamenDetalle i,Item_Examenes ie,HorasParametros h
where ot.cod_item_examen_detalle = i.codigo and ie.codItemExa = i.codigoItemExamen
and i.codigo = h.id_parametro
and ot.cod_orden_trabajo ='" + id_orden + "' 
order by h.hora", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    'Public Function RecuperarExamenGraficable(ByVal id_orden As String) As String
    '    Dim sqlcom As SqlCommand
    '    sqlcom = New SqlCommand
    '    sqlcom.CommandText = "exec E_slmExamenGraficoExiste " + id_orden + ""
    '    sqlcom.Connection = New ClsConnection().getConexion
    '    Return sqlcom.ExecuteScalar
    'End Function

    Public Function RecuperarExamenGraficable() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmExamenGraficoExiste"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_examen" 'nombre campo en el procedimiento almacenado 
        sqlpar.Value = Id_orden1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function ListarResultadosEnviados(ByVal cod As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select distinct o.cod_orden_trabajo ,c.correo1,c.correo2,f.numero,c.nombreCompleto,f.fechaFactura 
from    OrdenDeTrabajo o, factura f,Cliente c, Item_Examenes ie,ItemExamenDetalle id,OrdenTrabajoDetalle otd
where f.numero = o.cod_factura

and c.codigo = f.codigoCliente
and otd.cod_orden_trabajo = o.cod_orden_trabajo
and ie.codItemExa = id.codigoItemExamen
and otd.cod_item_examen_detalle = id.codigo
and f.numero = '" + cod + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Sub enviarMailResultadoLocal(correoNoti As String, id_orden As String)

        'In the shadows of the moon
        'enviarMailResultado("sinergia@laboratoriosmedicos.hn", "Lmsinergia2020", "587", True, "mail.laboratoriosmedicos.hn", "erickgallardo89@yahoo.com", "Resultados")
        Dim correoSalida As String = "resultados@laboratoriosmedicos.hn"
        Dim pass As String = "Lbresultados76"
        Dim puerto As String = "587"
        Dim sslOK As Boolean = True
        Dim host As String = "mail.laboratoriosmedicos.hn"
        Dim texto As String = "Resultados "

        Try
            Dim objP As New clsCorreoResultado

            Dim dt As New DataTable
            dt = objP.BuscarCorreo()
            Dim row As DataRow = dt.Rows(0)
            correoSalida = CStr(row("correo"))
            host = CStr(row("host"))
            pass = CStr(row("pass"))
            puerto = CStr(row("puerto"))
            sslOK = CBool(row("ssl"))


        Catch ex As Exception

            MsgBox(ex)

        End Try

        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(correoSalida, pass)
            Smtp_Server.Port = puerto
            Smtp_Server.EnableSsl = sslOK
            Smtp_Server.Host = host

            e_mail = New MailMessage()
            'txtfrom.text
            e_mail.From = New MailAddress(correoSalida)
            'txtto.text
            e_mail.To.Add(correoNoti)
            e_mail.Subject = "Resultados:"

            Dim archivos As String = "C:\Resultados\resultado" + id_orden.ToString + ".pdf"

            Dim archivoAdjunto As New System.Net.Mail.Attachment(archivos)

            e_mail.Attachments.Add(archivoAdjunto)
            e_mail.IsBodyHtml = True
            'txtMessage.text
            Dim body As String
            body = "<p>Estimado cliente</p>

<p>Adjunto encontrará el resultado de sus análisis.</p>
<p>Laboratorios Médicos, 45 años agradeciendo su confianza.</p>
<p>Si tiene consultas, favor comunicarse con nuestro Contact Center al 2239-1950 o al correo info@laboratoriosmedicos.hn</p>"

            e_mail.Body = body
            Smtp_Server.Send(e_mail)
            e_mail.Attachments.Clear()

            'omitir mensaje
            ' MsgBox("Mail Enviado")

        Catch ex As Exception
            MsgBox("No se envío el correo. " + ex.Message)
        End Try
        Exit Sub
    End Sub
    Public Sub EnviarCorreo(ByVal cadena As String)

        ''LLAMAR CRYSTAL
        'Dim RptDocument As E_rptInsulinaIndividual
        'RptDocument = New E_rptInsulinaIndividual

        'Try
        '    'If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
        '    '    My.Computer.FileSystem.CreateDirectory(
        '    '  "C:\Resultados")
        '    'End If
        '    'Try
        '    '    ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        '    'Catch ex As Exception
        '    '    MsgBox(ex)
        '    'End Try
        '    RptDocument.SetParameterValue("@id_orden", id_facturatrabajo_grafico)
        '    RptDocument.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        '    'Dim diskOpts As New DiskFileDestinationOptions
        '    RptDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, "C:\Resultados\resultado" + cadena + ".pdf")
        '    RptDocument.Export()

        '    'RptDocument.Close()
        '    Thread.Sleep(3000)


        '  MsgBox("Resultados enviados al correo " + clsc.RecuperarCorreo(TextBox3.Text).ToString)
        'Catch ex As Exception
        '    MsgBox(ex)
        'End Try


        'cryRpt.Load("Resultados\E_rptInsulina.rpt")
        'CrystalReportViewer1.ReportSource = cryRpt
        ' CrystalReportViewer1.Refresh()

    End Sub

    Public Sub GraficaOsmosisEnviarCorreo(ByVal cadena As String)

        If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
            My.Computer.FileSystem.CreateDirectory("C:\Resultados")
        End If
        Try
            ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        Catch ex As Exception
            MsgBox(ex)
        End Try

        Dim cryRpt As New E_rptOsmosis
        cryRpt.SetParameterValue("@id_orden", cadena)
        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            'Dim CrFormatTypeOptions As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + cadena + ".pdf"
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        enviarMailResultadoLocal("erickgallardo89@yahoo.com , resultados3@laboratoriosmedicos.hn", cadena)
    End Sub

    Public Sub GraficaElectroEnviarCorreo(ByVal cadena As String)
        If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
            My.Computer.FileSystem.CreateDirectory(
          "C:\Resultados")
        End If
        Try
            ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        Catch ex As Exception
            MsgBox(ex)
        End Try
        Dim cryRpt As New E_ResultadoElectro
        cryRpt.SetParameterValue("@id_orden", id_facturatrabajo_grafico)
        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            'Dim CrFormatTypeOptions As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + cadena + ".pdf"
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        enviarMailResultadoLocal("erickgallardo89@yahoo.com , resultados3@laboratoriosmedicos.hn", cadena)
    End Sub


    Public Sub GraficaInsulinaIndividualEnviarCorreo(ByVal cadena As String)
        If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
            My.Computer.FileSystem.CreateDirectory(
          "C:\Resultados")
        End If
        Try
            ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        Catch ex As Exception
            MsgBox(ex)
        End Try
        Dim cryRpt As New E_rptInsulinaIndividual
        cryRpt.SetParameterValue("@id_orden", id_facturatrabajo_grafico)
        cryRpt.SetParameterValue("@es_curva", 0)
        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            'Dim CrFormatTypeOptions As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + cadena + ".pdf"
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        enviarMailResultadoLocal("erickgallardo89@yahoo.com , resultados3@laboratoriosmedicos.hn", cadena)
    End Sub

    Public Sub GraficaDobleCurvaEnviarCorreo(ByVal cadena As String)
        If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
            My.Computer.FileSystem.CreateDirectory(
          "C:\Resultados")
        End If
        Try
            ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        Catch ex As Exception
            MsgBox(ex)
        End Try
        Dim cryRpt As New E_rptDosCurvasFinal
        cryRpt.SetParameterValue("@id_orden", id_facturatrabajo_grafico)
        cryRpt.SetParameterValue("@es_curva", 1)
        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")
        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            'Dim CrFormatTypeOptions As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + cadena + ".pdf"
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        enviarMailResultadoLocal("erickgallardo89@yahoo.com , resultados3@laboratoriosmedicos.hn", cadena)
    End Sub


    Public Sub GraficaGlucosaIndividualEnviarCorreo(ByVal cadena As String)
        If My.Computer.FileSystem.FileExists("C:\Resultados") = False Then
            My.Computer.FileSystem.CreateDirectory(
          "C:\Resultados")
        End If
        Try
            ' My.Computer.FileSystem.DeleteFile("C:\Resultados\resultado" + cadena + ".pdf")

        Catch ex As Exception
            MsgBox(ex)
        End Try
        Dim cryRpt As New E_rptGlucosaIndividual
        cryRpt.SetParameterValue("@id_orden", id_facturatrabajo_grafico)
        cryRpt.SetParameterValue("@es_curva", 0)
        cryRpt.SetDatabaseLogon("sa", "Lbm2019", "10.172.3.10", "slm_test")

        Try
            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            'Dim CrFormatTypeOptions As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfFormatOptions
            CrDiskFileDestinationOptions.DiskFileName = "C:\Resultados\resultado" + cadena + ".pdf"
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                '.ExportFormatType = ExportFormatType.Excel
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With

            cryRpt.Export()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        enviarMailResultadoLocal("erickgallardo89@yahoo.com , resultados3@laboratoriosmedicos.hn", cadena)
    End Sub
End Class
