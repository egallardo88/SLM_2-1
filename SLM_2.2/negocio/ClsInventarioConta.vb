﻿Imports System.Data.SqlClient

Public Class ClsInventarioConta
    Dim nombre, marca, modelo, descripcion, departamento, estado, serie As String
    Dim ubicacion, categoria, id As Integer
    Dim fecha_compra, fecha_registro As Date
    Dim valor As Double


    Public Property Nombre1 As String
        Get
            Return nombre
        End Get
        Set(value As String)
            nombre = value
        End Set
    End Property

    Public Property Marca1 As String
        Get
            Return marca
        End Get
        Set(value As String)
            marca = value
        End Set
    End Property

    Public Property Modelo1 As String
        Get
            Return modelo
        End Get
        Set(value As String)
            modelo = value
        End Set
    End Property

    Public Property Descripcion1 As String
        Get
            Return descripcion
        End Get
        Set(value As String)
            descripcion = value
        End Set
    End Property

    Public Property Departamento1 As String
        Get
            Return departamento
        End Get
        Set(value As String)
            departamento = value
        End Set
    End Property

    Public Property Estado1 As String
        Get
            Return estado
        End Get
        Set(value As String)
            estado = value
        End Set
    End Property

    Public Property Fecha_compra1 As Date
        Get
            Return fecha_compra
        End Get
        Set(value As Date)
            fecha_compra = value
        End Set
    End Property

    Public Property Fecha_registro1 As Date
        Get
            Return fecha_registro
        End Get
        Set(value As Date)
            fecha_registro = value
        End Set
    End Property

    Public Property Ubicacion1 As Integer
        Get
            Return ubicacion
        End Get
        Set(value As Integer)
            ubicacion = value
        End Set
    End Property

    Public Property Categoria1 As Integer
        Get
            Return categoria
        End Get
        Set(value As Integer)
            categoria = value
        End Set
    End Property

    Public Property Valor1 As Double
        Get
            Return valor
        End Get
        Set(value As Double)
            valor = value
        End Set
    End Property

    Public Property Serie1 As String
        Get
            Return serie
        End Get
        Set(value As String)
            serie = value
        End Set
    End Property

    Public Property Id1 As Integer
        Get
            Return id
        End Get
        Set(value As Integer)
            id = value
        End Set
    End Property

    Public Function Registrar() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmRegistrarInventarioItem"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre"
        sqlpar.Value = Nombre1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "marca"
        sqlpar.Value = Marca1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "modelo"
        sqlpar.Value = Modelo1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "serie"
        sqlpar.Value = Serie1
        sqlcom.Parameters.Add(sqlpar)



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "descripcion"
        sqlpar.Value = Descripcion1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_departamento"
        sqlpar.Value = Departamento1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fecha_compra"
        sqlpar.Value = Fecha_compra1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fecha_registro"
        sqlpar.Value = Fecha_registro1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "categoria"
        sqlpar.Value = Categoria1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_sucursal"
        sqlpar.Value = Ubicacion1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = Estado1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "valor"
        sqlpar.Value = Valor1

        sqlcom.Parameters.Add(sqlpar)
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
    Public Function Actualizar() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarInventarioItem"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id"
        sqlpar.Value = Id1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre"
        sqlpar.Value = Nombre1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "marca"
        sqlpar.Value = Marca1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "modelo"
        sqlpar.Value = Modelo1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "serie"
        sqlpar.Value = Serie1
        sqlcom.Parameters.Add(sqlpar)



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "descripcion"
        sqlpar.Value = Descripcion1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_departamento"
        sqlpar.Value = Departamento1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "fecha_compra"
        sqlpar.Value = Fecha_compra1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "categoria"
        sqlpar.Value = Categoria1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_sucursal"
        sqlpar.Value = Ubicacion1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "estado"
        sqlpar.Value = Estado1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "valor"
        sqlpar.Value = Valor1

        sqlcom.Parameters.Add(sqlpar)
        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function Eliminar() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmEliminarInventarioItem"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id"
        sqlpar.Value = Id1
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
    Public Function RecuperarRegistros() As SqlDataReader
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "select i.id, i.nombre,i.valor,i.modelo,i.marca,i.serie,i.descripcion,d.nombre as departamento,i.fecha_compra,i.fecha_registro,c.nombre as categoria,s.nombre as sucursal,i.estado from Sucursal s, inventarioconta i, Departamento d, categoriaInventario c
where s.codigo = i.id_sucursal and i.id_departamento = d.codigo and i.categoria = c.id"
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteReader
    End Function
    Public Function RecuperarBusqueda() As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select i.id, i.nombre,i.valor,i.modelo,i.marca,i.serie,i.descripcion,d.nombre as departamento,i.fecha_compra,i.fecha_registro,c.nombre as categoria,s.nombre as sucursal,i.estado from Sucursal s, inventarioconta i, Departamento d, categoriaInventario c
where s.codigo = i.id_sucursal and i.id_departamento = d.codigo and i.categoria = c.id ", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            objCon.cerrarConexion()
            Return dt
        End Using
    End Function

End Class
