﻿Imports System.Data.SqlClient

Public Class clsInventario
    Dim id_producto, id_almacen, cantidad_ideal As Integer
    Dim nombre_producto, nombre_almacen As String

    Public Property Id_producto1 As Integer
        Get
            Return id_producto
        End Get
        Set(value As Integer)
            id_producto = value
        End Set
    End Property

    Public Property Id_almacen1 As Integer
        Get
            Return id_almacen
        End Get
        Set(value As Integer)
            id_almacen = value
        End Set
    End Property

    Public Property Cantidad_ideal1 As Integer
        Get
            Return cantidad_ideal
        End Get
        Set(value As Integer)
            cantidad_ideal = value
        End Set
    End Property

    Public Property Nombre_producto1 As String
        Get
            Return nombre_producto
        End Get
        Set(value As String)
            nombre_producto = value
        End Set
    End Property

    Public Property Nombre_almacen1 As String
        Get
            Return nombre_almacen
        End Get
        Set(value As String)
            nombre_almacen = value
        End Set
    End Property

    Public Function BIAlmacen2(ByVal almacen As String, ByVal cod As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("
select top " + cod + " COUNT(e.existencia) as cantidad ,p.nombre_producto  from EntradaAlmacen e, ProductoAlmacen p
where e.id_producto =p.id_producto and e.id_almacen='" + almacen + "'
group by p.nombre_producto 
", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Public Function BIAlmacen() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select COUNT(e.cantidad) as cantidad , a.nombre_almacen from  Almacen a, EntradaAlmacen e
where a.id_almacen =e.id_almacen
group by a.nombre_almacen", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function DatosGrafica(ByVal id_orden As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select * from datosgrafica where id_orden ='" + id_orden + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Public Function BIAlmacenCostoInventario() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("
select count(s.stock) as cantidad, a.nombre_almacen,FORMAT(sum(s.stock * s.costo_promedio) , 'N', 'en-us')as costo_promedio
from stockProducto s, Almacen a
where s.id_almacen =a.id_almacen 
group by a.nombre_almacen
order by costo_promedio desc", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Public Function ListarInventarioAlmacen(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.codigoBarra,p.nombre_producto,s.stock,s.costo_promedio from stockProducto s, ProductoAlmacen p
where s.id_producto  = p.id_producto and s.id_almacen ='" + codigo + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function ListarInventarioAlmacenProductoVencido(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.nombre_producto,e.lote,e.existencia,e.precio_unitario,e.fecha_vencimiento from EntradaAlmacen e, ProductoAlmacen p
where e.fecha_vencimiento <= GETDATE()  and e.id_producto = p.id_producto  and e.id_almacen='" + codigo + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function ListarInventarioAlmacenSinExistencia(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.codigoBarra,p.nombre_producto,s.stock,s.costo_promedio from stockProducto s, ProductoAlmacen p
where s.id_producto  = p.id_producto
and s.stock<=0
and s.id_almacen='" + codigo + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function



    Public Function TotalInventarioAlmacenProductoVencido() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.nombre_producto,e.lote,e.existencia,e.precio_unitario,e.fecha_vencimiento from EntradaAlmacen e, ProductoAlmacen p
where e.fecha_vencimiento <= GETDATE()  and e.id_producto = p.id_producto  ", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function TotalInventarioAlmacenSinExistencia() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.codigoBarra,p.nombre_producto,s.stock,s.costo_promedio from stockProducto s, ProductoAlmacen p
where s.id_producto  = p.id_producto and s.stock  <=0 ", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function TotalInventarioAlmacen() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.codigoBarra,p.nombre_producto,s.stock,s.costo_promedio from stockProducto s, ProductoAlmacen p
where s.id_producto  = p.id_producto and s.stock  >0
", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function MovimientoInventarioAlmacen(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select p.codigoBarra,p.nombre_producto,s.stock,s.costo_promedio from stockProducto s, ProductoAlmacen p
where s.id_producto  = p.id_producto and s.id_almacen='" + codigo + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
    Public Function MovimientoInventarioAlmacen2(ByVal codigo As String) As SqlDataReader
        Dim sqlcom As SqlCommand
        sqlcom = New SqlCommand
        sqlcom.CommandText = "select e.id_entrada, e.id_producto, p.nombre_producto,e.lote,e.existencia from EntradaAlmacen e, ProductoAlmacen p
where existencia >0 and e.id_producto = p.id_producto and e.id_almacen='" + codigo + "'"
        sqlcom.Connection = New ClsConnection().getConexion
        Return sqlcom.ExecuteReader
    End Function

    Public Function RegistrarInventarioDetalleIdeal() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmInsertarInvIdeal"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_producto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_producto1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre_producto" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Nombre_producto1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "cantidad_ideal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Cantidad_ideal1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_almacen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_almacen1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "nombre_almacen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Nombre_almacen1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    'eliminar producto ideal
    Public Function EliminarProductoIdeal() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmEliminarInvIdeal"



        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_producto1
        sqlcom.Parameters.Add(sqlpar)




        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
    Public Function ListarInventarioAlmacenSalidasSLM(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("
select p.id_producto , p.codigoBarra,s.nombre_producto,s.stock,s.id_almacen from stockProducto s, ProductoAlmacen p
where s.id_producto = p.id_producto and s.id_almacen  ='" + codigo + "' ORDER BY S.STOCK desc", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    Public Function ListarInventarioOrdenAutomatica(ByVal codigo As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("
select codigoBarra,nombre_producto, cantidad_enviar,nombre_almacen from OrdenInternaAutomatica where estado <> 'Recibido' and idinterno = '" + codigo + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function
End Class
