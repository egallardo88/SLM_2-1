﻿Imports System.Data.SqlClient

Public Class ClsGrafica
    Dim id_orden As Integer

    Public Property Id_orden1 As Integer
        Get
            Return id_orden
        End Get
        Set(value As Integer)
            id_orden = value
        End Set
    End Property

    Public Function DatosGrafica(ByVal id_orden As String) As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using da As New SqlDataAdapter("select * from datosgrafica where id_orden ='" + id_orden + "'", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            Return dt
        End Using
    End Function

    'registrar nuevo examenes osmosis
    Public Function RegistrarProducto() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmCrearDatosGraficaOsmosis"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_orden"
        sqlpar.Value = Id_orden1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
End Class
