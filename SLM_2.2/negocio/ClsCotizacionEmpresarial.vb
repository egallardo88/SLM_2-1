﻿
Imports System.Data.SqlClient
Public Class ClsCotizacionEmpresarial
    Dim numero, codigoClienteEmpresarial, codigoRecepcionista, codigoTerminoPago, codigoSucursal, codigoCajero As Integer
    Dim nombreCliente As String
    Dim fecha As Date
    Dim total As Double
    'Constructor
    Public Sub New()

    End Sub
    'VARIABLES
    Public Property numero_ As Integer
        Get
            Return numero
        End Get
        Set(value As Integer)
            numero = value
        End Set
    End Property
    Public Property codigoClienteEmpresarial_ As Integer
        Get
            Return codigoClienteEmpresarial
        End Get
        Set(value As Integer)
            codigoClienteEmpresarial = value
        End Set
    End Property
    Public Property codigoTerminoPago_ As Integer
        Get
            Return codigoTerminoPago
        End Get
        Set(value As Integer)
            codigoTerminoPago = value
        End Set
    End Property
    Public Property nombreCliente_ As String
        Get
            Return nombreCliente
        End Get
        Set(value As String)
            nombreCliente = value
        End Set
    End Property
    Public Property codigoCajero_ As Integer
        Get
            Return codigoCajero
        End Get
        Set(value As Integer)
            codigoCajero = value
        End Set
    End Property
    Public Property codigoRecepcionista_ As Integer
        Get
            Return codigoRecepcionista
        End Get
        Set(value As Integer)
            codigoRecepcionista = value
        End Set
    End Property
    Public Property codigoSucursal_ As Integer
        Get
            Return codigoSucursal
        End Get
        Set(value As Integer)
            codigoSucursal = value
        End Set
    End Property
    Public Property total_ As Double
        Get
            Return total
        End Get
        Set(value As Double)
            total = value
        End Set
    End Property
    Public Property fecha_ As Date
        Get
            Return fecha
        End Get
        Set(value As Date)
            fecha = value
        End Set
    End Property
    '::::::::::::::::::::::::::::::::::::::::::::::::::FUNCIONES DE MANTENIMIENTO
    'REGISTRAR
    Public Function RegistrarNuevaCotizacionEmpresarial() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "MANTE_Cotizacion_Empresarial"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoClienteEmpresarial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoClienteEmpresarial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminoPago" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminoPago_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoRecepcionista" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoRecepcionista_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSucursal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSucursal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "accion" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = 0
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    'MODIFICAR
    Public Function ModificarCotizacionEmpresarial() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "MANTE_Cotizacion_Empresarial"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "numero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = numero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoClienteEmpresarial" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoClienteEmpresarial_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoTerminoPago" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoTerminoPago_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoRecepcionista" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoRecepcionista_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoSucursal" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoSucursal_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "total" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = total_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "codigoCajero" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = codigoCajero_
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "accion" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = 1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function


    'IMPRESION ENCABEZADO COTIZACION EMPRESARIAL
    Public Function BuscarEncabezadoCotizacionEmpresarial() As DataTable

        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "M_slmBuscarEncabezadoCotizacionEmpresarial"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using

    End Function

    Public Function SeleccionarCotizacionRangoFecha(ByVal inicio As Date, ByVal fin As Date) As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion
        Using da As New SqlDataAdapter(" SELECT cotEmp.numero, CLI.nombreEmpresa, cotEmp.codigoClienteEmpresarial, cotEmp.fecha, cotEmp.codigoTerminoPago,
	        cotEmp.codigoRecepcionista, cotEmp.codigoSucursal, cotEmp.total, cotEmp.codigoCajero 
	            FROM Cotizacion_Empresarial cotEmp INNER JOIN Cliente_Empresarial cli ON cotEmp.codigoClienteEmpresarial = cli.codigo
		            WHERE cotEmp.fecha BETWEEN '" + inicio.ToString("yyyyMMdd") + "' and '" + fin.ToString("yyyyMMdd") + "'
			            ORDER BY  cotEmp.numero DESC", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            objCon.cerrarConexion()
            Return dt
        End Using
    End Function


    Public Function SeleccionarCotizacionEmpresarial() As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion
        Using da As New SqlDataAdapter("SELECT cotEmp.numero, CLI.nombreEmpresa, cotEmp.codigoClienteEmpresarial, cotEmp.fecha, cotEmp.codigoTerminoPago,
	        cotEmp.codigoRecepcionista, cotEmp.codigoSucursal, cotEmp.total, cotEmp.codigoCajero 
	            FROM Cotizacion_Empresarial cotEmp INNER JOIN Cliente_Empresarial cli ON cotEmp.codigoClienteEmpresarial = cli.codigo
		            WHERE cotEmp.fecha = CAST(GETDATE() AS DATE)
			            ORDER BY  cotEmp.numero DESC", cn)
            Dim dt As New DataTable
            da.Fill(dt)
            objCon.cerrarConexion()
            Return dt
        End Using
    End Function

    Public Function BuscarCotizacionEmpresarialNumero() As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "M_slmBuscarCotizacionEmpNumero"
            cmd.Parameters.Add("@numero", SqlDbType.Int).Value = numero_
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using
    End Function

    Public Function BuscarCotizacionEmpresarialCliente(ByVal nombre As String) As DataTable
        Dim objCon As New ClsConnection
        Dim cn As New SqlConnection
        cn = objCon.getConexion

        Using cmd As New SqlCommand
            cmd.Connection = cn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "M_slmBuscarCotizacionEmpCliente"
            cmd.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre
            Using da As New SqlDataAdapter
                da.SelectCommand = cmd
                Using dt As New DataTable
                    da.Fill(dt)
                    objCon.cerrarConexion()
                    Return dt
                End Using
            End Using
        End Using
    End Function

End Class
