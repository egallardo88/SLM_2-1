﻿Imports System.Data.SqlClient

Public Class clsCentroCostoCuadrar
    Dim examen As String
    Dim id_examen, id_cc As Integer

    Public Sub New()

    End Sub

    Public Property Examen1 As String
        Get
            Return examen
        End Get
        Set(value As String)
            examen = value
        End Set
    End Property

    Public Property Id_examen1 As Integer
        Get
            Return id_examen
        End Get
        Set(value As Integer)
            id_examen = value
        End Set
    End Property

    Public Property Id_cc1 As Integer
        Get
            Return id_cc
        End Get
        Set(value As Integer)
            id_cc = value
        End Set
    End Property

    Public Function RegistrarNuevoCentroCosto() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmRegistrarExamenesCuadrarCC"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "examen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Examen1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_examen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_examen1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_cc" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_cc1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function ActualizarNuevoCentroCosto() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmActualizarExamenesCuadrarCC"

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "examen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Examen1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_examen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_examen1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_cc" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_cc1
        sqlcom.Parameters.Add(sqlpar)

        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function

    Public Function EliminarNuevoCentroCosto() As String
        Dim sqlcom As SqlCommand
        Dim sqlpar As SqlParameter
        Dim par_sal As Integer

        sqlcom = New SqlCommand
        sqlcom.CommandType = CommandType.StoredProcedure
        sqlcom.CommandText = "E_slmEliminarExamenesCuadrarCC"


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "id_examen" 'nombre campo en el procedimiento almacenado @
        sqlpar.Value = Id_examen1
        sqlcom.Parameters.Add(sqlpar)


        sqlpar = New SqlParameter
        sqlpar.ParameterName = "salida"
        sqlpar.Value = ""
        sqlcom.Parameters.Add(sqlpar)

        sqlpar.Direction = ParameterDirection.Output

        Dim con As New ClsConnection
        sqlcom.Connection = con.getConexion

        sqlcom.ExecuteNonQuery()

        con.cerrarConexion()

        par_sal = sqlcom.Parameters("salida").Value

        Return par_sal

    End Function
End Class
